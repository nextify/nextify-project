# -*- coding: utf-8 -*-
from django.db import models


class IbptWS(models.Model):
    code = models.CharField(max_length=10)
    uf = models.CharField(max_length=2)
    ex = models.IntegerField(null=False)
    nacional = models.DecimalField(max_digits=15, decimal_places=6)
    importado = models.DecimalField(max_digits=15, decimal_places=6)
    estadual = models.DecimalField(max_digits=15, decimal_places=6)
    municipal = models.DecimalField(max_digits=15, decimal_places=6)

