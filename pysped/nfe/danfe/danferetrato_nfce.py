# -*- coding: utf-8 -*-
#
# PySPED - Python libraries to deal with Brazil's SPED Project
#
# Copyright (C) 2010-2012
# Copyright (C) Aristides Caldeira <aristides.caldeira at tauga.com.br>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as
# published by the Free Software Foundation, either version 2.1 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# PySPED - Bibliotecas Python para o
#          SPED - Sistema Público de Escrituração Digital
#
# Copyright (C) 2010-2012
# Copyright (C) Aristides Caldeira <aristides.caldeira arroba tauga.com.br>
#
# Este programa é um software livre: você pode redistribuir e/ou modificar
# este programa sob os termos da licença GNU Library General Public License,
# publicada pela Free Software Foundation, em sua versão 2.1 ou, de acordo
# com sua opção, qualquer versão posterior.
#
# Este programa é distribuido na esperança de que venha a ser útil,
# porém SEM QUAISQUER GARANTIAS, nem mesmo a garantia implícita de
# COMERCIABILIDADE ou ADEQUAÇÃO A UMA FINALIDADE ESPECÍFICA. Veja a
# GNU Library General Public License para mais detalhes.
#
# Você deve ter recebido uma cópia da GNU Library General Public License
# juntamente com este programa. Caso esse não seja o caso, acesse:
# <http://www.gnu.org/licenses/>
#

from __future__ import division, print_function, unicode_literals


from reportlab.lib.units import cm
from reportlab.lib.colors import HexColor

from geraldo import Report, SubReport
from geraldo import SystemField, Line, Rect, Image
from geraldo.barcodes import BarCode

from pysped.relato_sped.base import (BandaDANFE,
                                     Campo,
                                     DADO_CAMPO_NEGRITO,
                                     DADO_CAMPO_NUMERICO_NEGRITO,
                                     DADO_CHAVE,
                                     DADO_COMPLEMENTAR,
                                     DADO_PRODUTO,
                                     DADO_PRODUTO_CENTRALIZADO,
                                     DADO_VARIAVEL_CANCELAMENTO,
                                     DADO_VARIAVEL,
                                     DESCRITIVO_CAMPO,
                                     DESCRITIVO_CAMPO_CANCELAMENTO,
                                     DESCRITIVO_CAMPO_NEGRITO,
                                     DESCRITIVO_DANFE,
                                     DESCRITIVO_DANFE_NFCE,
                                     DESCRITIVO_DANFE_NFCE_CENTRO,
                                     DESCRITIVO_DANFE_NFCE_ESQUERDA,
                                     DESCRITIVO_DANFE_ES,
                                     DESCRITIVO_DANFE_GERAL,
                                     DESCRITIVO_NUMERO,
                                     DESCRITIVO_PRODUTO,
                                     EMIT_DADOS,
                                     EMIT_NOME,
                                     FONTES_ADICIONAIS,
                                     LabelMargemEsquerda,
                                     OBS_CANCELAMENTO,
                                     OBS_DENEGACAO,
                                     OBS_HOMOLOGACAO,
                                     OBS_CONTINGENCIA,
                                     MARGEM_DIREITA,
                                     MARGEM_ESQUERDA,
                                     MARGEM_INFERIOR,
                                     MARGEM_SUPERIOR,
                                     RETRATO,
                                     Texto)
from pysped.nfe.manual_401 import Vol_200, Pag_310


class DanfeNfceRetrato(Report):
    def __init__(self, *args, **kargs):
        super(DanfeNfceRetrato, self).__init__(*args, **kargs)
        self.title = 'DANFE - Documento Auxiliar da Nota Fiscal Eletrônica'
        self.print_if_empty = True
        self.additional_fonts = FONTES_ADICIONAIS

        self.page_size = RETRATO
        self.margin_top = MARGEM_SUPERIOR
        self.margin_bottom = MARGEM_INFERIOR
        self.margin_left = MARGEM_ESQUERDA
        self.margin_right = MARGEM_DIREITA

        # Bandas e observações
        # self.canhoto          = CanhotoRetrato()
        self.remetente        = RemetenteRetrato()
        self.destinatario     = DestinatarioRetrato()
        # self.local_retirada   = LocalRetiradaRetrato()
        # self.local_entrega    = LocalEntregaRetrato()
        # self.fatura_a_vista   = FaturaAVistaRetrato()
        # self.fatura_a_prazo   = FaturaAPrazoRetrato()
        # self.duplicatas       = DuplicatasRetrato()
        # self.calculo_imposto  = CalculoImpostoRetrato()
        # self.transporte       = TransporteRetrato()
        self.cab_produto      = CabProdutoRetrato()
        self.det_produto      = DetProdutoRetrato()
        # self.iss              = ISSRetrato()
        # self.dados_adicionais = DadosAdicionaisRetrato()
        self.rodape_final     = RodapeNFCeRetrato()
        # self.totais = TributosRetrato()

        #
        # Guarda a definição do cabeçalho e rodapé da 1ª página
        #
        self.cabecalho_primeira_pagina = None
        self.cabecalho_primeira_pagina_filhos = None
        self.remetente_filhos = None
        self.rodape_primeira_pagina = None

    def on_new_page(self, page, page_number, generator):
        if page_number == 1:
            if self.cabecalho_primeira_pagina is None:
                self.cabecalho_primeira_pagina = self.band_page_header
                self.cabecalho_primeira_pagina_filhos = list(self.band_page_header.child_bands)
                self.remetente_filhos = list(self.remetente.child_bands)
                self.rodape_primeira_pagina = self.band_page_footer
            #
            # else:
            #     self.band_page_header = self.cabecalho_primeira_pagina
            #     #self.band_page_header.child_bands = []
            #     self.band_page_header.child_bands = self.cabecalho_primeira_pagina_filhos
            #     #self.remetente.child_bands = []
            #     self.remetente.child_bands = self.remetente_filhos
            #     self.band_page_footer = self.rodape_primeira_pagina

        # else:
        #     self.band_page_footer = self.rodape_final
        #
        #     self.band_page_header = self.remetente
        #     self.band_page_header.child_bands = [self.cab_produto]

    def format_date(self, data, formato):
        return data.strftime(formato.encode('utf-8')).decode('utf-8')

    class ObsImpressao(SystemField):
        expression = u'DANFE gerado em %(now:%d/%m/%Y, %H:%M:%S)s'

        def __init__(self):
            self.name = 'obs_impressao'
            self.top = 0*cm
            self.left = 0.0*cm
            self.width = 19.4*cm
            self.height = 0.3*cm
            self.style = DADO_PRODUTO
            self.borders = {'bottom': 0.1}



class RemetenteRetrato(BandaDANFE):
    def __init__(self):
        super(RemetenteRetrato, self).__init__()
        self.elements = []

        # Quadro do emitente
        self.inclui_texto(nome='quadro_emitente', titulo='', texto='', top=0*cm, left=0*cm, width=19.4*cm, height=2.7*cm)

        #
        # Área central - Dados do DANFE
        #
        lbl, txt = self.inclui_texto(nome='danfe', titulo='', texto=u'DANFE NFC-e - Documento Auxiliar de Nota Fiscal de Consumidor Eletrônica<br /><br />Não permite aproveitamento de crédito de ICMS', top=2.7*cm, left=0*cm, width=19.4*cm, height=1.5*cm)
        txt.style = DESCRITIVO_DANFE_NFCE

        # txt = self.inclui_texto_sem_borda(nome='danfe_ext', texto=u'DOCUMENTO AUXILIAR DA NOTA FISCAL ELETRÔNICA', top=0.6*cm, left=8*cm, width=3.4*cm, height=4*cm)
        # txt.style = DESCRITIVO_DANFE_GERAL

        # txt = self.inclui_texto_sem_borda(nome='danfe_ext', texto=u'versão', top=1.1*cm, left=8.8*cm, width=1.4*cm, height=0.6*cm)
        # txt.style = DESCRITIVO_DANFE_GERAL

        # fld = self.inclui_campo_sem_borda(nome='danfe_entrada_saida', conteudo=u'NFe.infNFe.versao.valor', top=1.1*cm, left=9.8*cm, width=0.8*cm, height=0.6*cm)
        # fld.style = DESCRITIVO_DANFE_GERAL

        # txt = self.inclui_texto_sem_borda(nome='danfe_entrada', texto=u'0 - ENTRADA', top=1.5*cm, left=8.3*cm, width=3.4*cm, height=4*cm)
        # txt.style = DESCRITIVO_DANFE_ES
        #
        # txt = self.inclui_texto_sem_borda(nome='danfe_saida', texto=u'1 - SAÍDA', top=1.9*cm, left=8.3*cm, width=3.4*cm, height=4*cm)
        # txt.style = DESCRITIVO_DANFE_ES

        # fld = self.inclui_campo_sem_borda(nome='danfe_entrada_saida', conteudo=u'NFe.infNFe.ide.tpNF.valor', top=1.65*cm, left=10.4*cm, width=0.6*cm, height=0.6*cm)
        # fld.style = DESCRITIVO_NUMERO
        # fld.borders = {'top': 0.1, 'right': 0.1, 'bottom': 0.1, 'left': 0.1}
        # fld.padding_bottom = 0.2*cm
        #
        # fld = self.inclui_campo_sem_borda(nome='danfe_numero', conteudo=u'NFe.numero_formatado', top=2.4*cm, left=8*cm, width=3.4*cm, height=0.5*cm)
        # fld.style = DESCRITIVO_NUMERO
        #
        # fld = self.inclui_campo_sem_borda(nome='danfe_serie', conteudo=u'NFe.serie_formatada', top=2.85*cm, left=8*cm, width=3.4*cm, height=0.5*cm)
        # fld.style = DESCRITIVO_NUMERO
        #
        # fld = SystemField(name='fld_danfe_folha', expression=u'FOLHA %(page_number)02d/%(page_count)02d', top=3.3*cm, left=8*cm, width=3.4*cm, height=0.5*cm)
        # fld.padding_top = 0.1*cm
        # fld.style = DESCRITIVO_NUMERO
        # self.elements.append(fld)

        #
        # No caso dos códigos de barra, altura (height) e largura (width) se referem às barras, não à imagem
        #
        self.elements.append(Line(top=0*cm, bottom=21*cm, left=0*cm, right=0*cm, stroke_width=0.1))
        self.elements.append(Line(top=0*cm, bottom=21*cm, left=19.4*cm, right=19.4*cm, stroke_width=0.1))
        # self.elements.append(Line(top=0 * cm, bottom=1.625*cm, left=19.4*cm, right=19.4 * cm, stroke_width=0.1))
        # self.elements.append(BarCode(type=u'Code128', attribute_name=u'NFe.chave_para_codigo_barras', top=((1.625-0.8)/2.0)*cm, left=11.3*cm, width=0.025*cm, height=0.8*cm))

        # lbl, fld = self.inclui_campo(nome='remetente_chave', titulo=u'CHAVE DE ACESSO', conteudo=u'NFe.chave_formatada', top=1.625*cm, left=11.4*cm, width=8*cm, margem_direita=False)
        # fld.style = DADO_CHAVE

        # self.inclui_campo(nome='remetente_natureza', titulo=u'NATUREZA DA OPERAÇÃO', conteudo=u'NFe.infNFe.ide.natOp.valor', top=4*cm, left=0*cm, width=11.4*cm, margem_direita=True)

        # self.inclui_campo(nome='remetente_ie', titulo=u'INSCRIÇÃO ESTADUAL', conteudo=u'NFe.infNFe.emit.IE.valor', top=4.70*cm, left=0*cm, width=6.4*cm, margem_direita=True)
        # self.inclui_campo(nome='remetente_iest', titulo=u'INSCRIÇÃO ESTADUAL DO SUBSTITUTO TRIBUTÁRIO', conteudo=u'NFe.infNFe.emit.IEST.valor', top=4.70*cm, left=6.4*cm, width=6.6*cm, margem_direita=True)
        # self.inclui_campo(nome='remetente_cnpj', titulo=u'CNPJ', conteudo=u'NFe.cnpj_emitente_formatado', top=4.70*cm, left=13*cm, width=6.4*cm, margem_direita=True)

        self.height = 4.2*cm

    def campo_variavel_conferencia(self):
        lbl, txt = self.inclui_texto(nome='remetente_var1', titulo='', texto=u'<font color="red"><b>Impresso para simples conferência<br />Informações ainda não transmitidas a nenhuma SEFAZ autorizadora, nem ao SCAN<br />Sem valor fiscal</b></font>', top=2.375*cm, left=11.4*cm, width=8*cm, height=1.625*cm)
        txt.padding_top = 0*cm
        txt.style = DADO_VARIAVEL

        lbl, lbl = self.inclui_campo(nome='remetente_var2', titulo=u'PROTOCOLO DE AUTORIZAÇÃO DE USO', conteudo=u'protNFe.protocolo_formatado', top=4*cm, left=11.4*cm, width=8*cm)
        lbl.style = DADO_VARIAVEL

    def campo_variavel_normal(self):
        lbl, txt = self.inclui_texto(nome='remetente_var1', titulo='', texto=u'DANFE NFC-e - Documento Auxiliar de Nota Fiscal de Consumidor Eletrônica<br />Não permite aproveitamento de crédito de ICMS', top=3.5*cm, left=0*cm, width=19.4*cm, height=1.625*cm)
        txt.padding_top = 0.2*cm
        txt.style = DADO_VARIAVEL

        #fld = self.inclui_campo_sem_borda(nome='remetente_var1', conteudo=u'NFe.consulta_autenticidade', top=2.375*cm, left=11.4*cm, width=8*cm, height=1.625*cm)
        #fld.padding_top = 0.2*cm
        #fld.style = DADO_VARIAVEL

        lbl, lbl = self.inclui_campo(nome='remetente_var2', titulo=u'PROTOCOLO DE AUTORIZAÇÃO DE USO', conteudo=u'protNFe.protocolo_formatado', top=4*cm, left=11.4*cm, width=8*cm)
        lbl.style = DADO_VARIAVEL

    def campo_variavel_denegacao(self):
        self.elements.append(Line(top=2.375 * cm, bottom=1.675 * cm, left=19.4 * cm, right=19.4 * cm, stroke_width=0.1))
        lbl, txt = self.inclui_texto(nome='remetente_var1', titulo='', texto=u'A circulação da mercadoria foi <font color="red"><b>PROIBIDA</b></font> pela SEFAZ<br />autorizadora, devido a irregularidades fiscais.', top=2.375*cm, left=11.4*cm, width=8*cm, height=1.625*cm)
        txt.padding_top = 0.2*cm
        txt.style = DADO_VARIAVEL

        lbl, lbl = self.inclui_campo(nome='remetente_var2', titulo=u'PROTOCOLO DE DENEGAÇÃO DE USO', conteudo=u'protNFe.protocolo_formatado', top=4*cm, left=11.4*cm, width=8*cm)
        lbl.style = DADO_VARIAVEL

    def obs_cancelamento(self):
        txt = Texto()
        txt.name   = 'txt_obs_cancelamento'
        txt.text   = u'cancelada'
        #txt.top    = -0.1*cm
        txt.top    = 3.5*cm
        txt.left   = 4.7*cm
        txt.width  = 10*cm
        txt.height = 1.5*cm
        txt.padding_top = 0.1*cm
        txt.style  = OBS_CANCELAMENTO
        self.elements.insert(0, txt)

        lbl = LabelMargemEsquerda()
        lbl.borders = None
        lbl.name = 'lbl_prot_cancelamento'
        lbl.text = u'PROTOCOLO<br />DE CANCELAMENTO'
        lbl.top = 5.35*cm
        lbl.left = 6.15*cm
        lbl.width = 1.75*cm
        lbl.style = DESCRITIVO_CAMPO_CANCELAMENTO
        self.elements.insert(2, lbl)

        fld = Campo()
        fld.name = 'fld_prot_cancelamento'
        fld.attribute_name = u'retCancNFe.protocolo_formatado'
        fld.top  = 5.15*cm
        fld.left = 7.5*cm
        fld.width = 6.3*cm
        fld.padding_top = 0.25*cm
        fld.style = DADO_VARIAVEL_CANCELAMENTO
        self.elements.insert(3, fld)

    def obs_cancelamento_evento(self):
        txt = Texto()
        txt.name   = 'txt_obs_cancelamento'
        txt.text   = u'cancelada'
        #txt.top    = -0.1*cm
        txt.top    = 3.5*cm
        txt.left   = 4.7*cm
        txt.width  = 10*cm
        txt.height = 1.5*cm
        txt.padding_top = 0.1*cm
        txt.style  = OBS_CANCELAMENTO
        self.elements.insert(0, txt)

        lbl = LabelMargemEsquerda()
        lbl.borders = None
        lbl.name = 'lbl_prot_cancelamento'
        lbl.text = u'PROTOCOLO<br />DE CANCELAMENTO'
        lbl.top = 5.35*cm
        lbl.left = 5.2*cm
        lbl.width = 1.75*cm
        lbl.style = DESCRITIVO_CAMPO_CANCELAMENTO
        self.elements.insert(2, lbl)

        fld = Campo()
        fld.name = 'fld_prot_cancelamento'
        fld.attribute_name = u'procEventoCancNFe.retEvento.protocolo_formatado'
        fld.top  = 5.15*cm
        fld.left = 6.85*cm
        fld.width = 6.3*cm
        fld.padding_top = 0.25*cm
        fld.style = DADO_VARIAVEL_CANCELAMENTO
        self.elements.insert(3, fld)

    def obs_cancelamento_com_motivo(self):
        txt = Texto()
        txt.name   = 'txt_obs_cancelamento'
        txt.text   = u'cancelada'
        txt.top    = 3.5*cm
        txt.left   = 4.7*cm
        txt.width  = 10*cm
        txt.height = 1.5*cm
        txt.padding_top = 0.1*cm
        txt.style  = OBS_DENEGACAO
        self.elements.insert(0, txt)

        fld = Campo()
        fld.name = 'fld_motivo_cancelamento'
        fld.attribute_name = u'procCancNFe.cancNFe.infCanc.xJust'
        fld.top  = 5.15*cm
        fld.left = 4.7*cm
        fld.width = 10*cm
        fld.padding_top = 0.25*cm
        fld.style = DADO_VARIAVEL_CANCELAMENTO
        self.elements.insert(1, fld)

        lbl = LabelMargemEsquerda()
        lbl.borders = None
        lbl.name = 'lbl_prot_cancelamento'
        lbl.text = u'PROTOCOLO<br />DE CANCELAMENTO'
        lbl.top = 5.72*cm
        lbl.left = 6.15*cm
        lbl.width = 1.75*cm
        lbl.style = DESCRITIVO_CAMPO_CANCELAMENTO
        self.elements.insert(2, lbl)

        fld = Campo()
        fld.name = 'fld_prot_cancelamento'
        fld.attribute_name = u'retCancNFe.protocolo_formatado'
        fld.top  = 5.52*cm
        fld.left = 7.5*cm
        fld.width = 6.3*cm
        fld.padding_top = 0.25*cm
        fld.style = DADO_VARIAVEL_CANCELAMENTO
        self.elements.insert(3, fld)

    def obs_cancelamento_com_motivo_evento(self):
        txt = Texto()
        txt.name   = 'txt_obs_cancelamento'
        txt.text   = u'cancelada'
        txt.top    = 3.5*cm
        txt.left   = 4.7*cm
        txt.width  = 10*cm
        txt.height = 1.5*cm
        txt.padding_top = 0.1*cm
        txt.style  = OBS_DENEGACAO
        self.elements.insert(0, txt)

        fld = Campo()
        fld.name = 'fld_motivo_cancelamento'
        fld.attribute_name = u'procEventoCancNFe.evento.infEvento.detEvento.xJust'
        fld.top  = 5.15*cm
        fld.left = 4.7*cm
        fld.width = 10*cm
        fld.padding_top = 0.25*cm
        fld.style = DADO_VARIAVEL_CANCELAMENTO
        self.elements.insert(1, fld)

        lbl = LabelMargemEsquerda()
        lbl.borders = None
        lbl.name = 'lbl_prot_cancelamento'
        lbl.text = u'PROTOCOLO<br />DE CANCELAMENTO'
        lbl.top = 5.72*cm
        lbl.left = 5.2*cm
        lbl.width = 1.75*cm
        lbl.style = DESCRITIVO_CAMPO_CANCELAMENTO
        self.elements.insert(2, lbl)

        fld = Campo()
        fld.name = 'fld_prot_cancelamento'
        fld.attribute_name = u'procEventoCancNFe.retEvento.protocolo_formatado'
        fld.top  = 5.52*cm
        fld.left = 6.85*cm
        fld.width = 7.5*cm
        fld.padding_top = 0.25*cm
        fld.style = DADO_VARIAVEL_CANCELAMENTO
        self.elements.insert(3, fld)

    def obs_denegacao(self):
        txt = Texto()
        txt.name   = 'txt_obs_denegacao'
        txt.text   = u'denegada'
        #txt.top    = -0.1*cm
        txt.top    = 3.5*cm
        txt.left   = 4.7*cm
        txt.width  = 10*cm
        txt.height = 1.5*cm
        txt.padding_top = 0.1*cm
        txt.style  = OBS_DENEGACAO
        self.elements.insert(0, txt)

        fld = Campo()
        fld.name = 'fld_motivo_denegacao'
        fld.attribute_name = u'protNFe.infProt.xMotivo'
        fld.top  = 5.15*cm
        fld.left = 4.7*cm
        fld.width = 10*cm
        fld.padding_top = 0.25*cm
        fld.style = DADO_VARIAVEL_CANCELAMENTO
        self.elements.insert(1, fld)

        lbl = LabelMargemEsquerda()
        lbl.borders = None
        lbl.name = 'lbl_prot_denegacao'
        lbl.text = u'PROTOCOLO<br />DE DENEGAÇÃO'
        lbl.top = 5.72*cm
        lbl.left = 6.15*cm
        lbl.width = 1.75*cm
        lbl.style = DESCRITIVO_CAMPO_CANCELAMENTO
        self.elements.insert(2, lbl)

        fld = Campo()
        fld.name = 'fld_prot_denegacao'
        fld.attribute_name = u'protNFe.protocolo_formatado'
        fld.top  = 5.52*cm
        fld.left = 7.5*cm
        fld.width = 6.3*cm
        fld.padding_top = 0.25*cm
        fld.style = DADO_VARIAVEL_CANCELAMENTO
        self.elements.insert(3, fld)

    def obs_contingencia_normal_scan(self):
        lbl = Texto()
        lbl.name  = 'txt_obs_contingencia'
        lbl.text  = u'DANFE em contingência<br /><br />impresso em decorrência de problemas técnicos'
        lbl.top   = 6.6*cm
        lbl.left  = 0*cm
        lbl.width = 19.4*cm
        lbl.padding_top = 0.1*cm
        lbl.style = OBS_CONTINGENCIA
        self.elements.insert(0, lbl)

    def obs_contingencia_dpec(self):
        lbl = Texto()
        lbl.name  = 'txt_obs_contingencia'
        lbl.text  = u'DANFE em contingência<br /><br />DPEC regularmente recebida pela Receita Federal do Brasil'
        lbl.top   = 6.6*cm
        lbl.left  = 0*cm
        lbl.width = 19.4*cm
        lbl.padding_top = 0.1*cm
        lbl.style = OBS_CONTINGENCIA
        self.elements.insert(0, lbl)

    def obs_sem_valor_fiscal(self):
        lbl = Texto()
        lbl.name  = 'txt_obs_homologacao'
        lbl.text  = u'sem valor fiscal'
        lbl.top   = 9*cm
        lbl.left  = 0*cm
        lbl.width = 19.4*cm
        lbl.padding_top = 0.1*cm
        lbl.style = OBS_HOMOLOGACAO
        self.elements.append(lbl)

    def monta_quadro_emitente(self, dados_emitente=[]):
        for de in dados_emitente:
            self.elements.append(de)

    def dados_emitente_sem_logo(self, arquivo_imagem):
        elements = []

        img = Image()
        img.top = 0.1 * cm
        img.left = 0.1 * cm
        #
        # Tamanhos equilaventes, em centímetros, a 2,5 x 3,8, em 128 dpi
        # estranhamente, colocar os tamanhos em centímetros encolhe a imagem
        #
        img.width = 191
        img.height = 116
        img.filename = arquivo_imagem
        elements.append(img)

        fld = Campo()
        fld.nome = 'fld_rem_nome'
        fld.attribute_name = u'NFe.infNFe.emit.xNome.valor'
        fld.top = 0.2*cm
        fld.left = 4*cm
        fld.width = 9*cm
        fld.height = 0.5*cm
        fld.style = DADO_CAMPO_NEGRITO
        elements.append(fld)

        fld = Campo()
        fld.nome  = 'fld_rem_cnpj_1'
        fld.attribute_name = 'NFe.cnpj_emitente_linha'
        fld.top = 0.8*cm
        fld.left = 4*cm
        fld.width = 9*cm
        fld.height = 0.5*cm
        fld.style = DADO_CAMPO_NEGRITO
        elements.append(fld)

        fld = Campo()
        fld.nome = 'fld_rem_ie_1'
        fld.attribute_name = 'NFe.ie_emitente_linha'
        fld.top = 0.8 * cm
        fld.left = 12.4 * cm
        fld.width = 9 * cm
        fld.height = 0.5 * cm
        fld.style = DADO_CAMPO_NEGRITO
        elements.append(fld)

        fld = Campo()
        fld.nome = 'fld_rem_endereco_2'
        fld.attribute_name = u'NFe.endereco_emitente_linha_nfce'
        fld.top = 1.4 * cm
        fld.left = 4*cm
        fld.width = 9 * cm
        fld.height = 0.5 * cm
        fld.style = DADO_CAMPO_NEGRITO
        elements.append(fld)

        return elements

class DestinatarioRetrato(BandaDANFE):
    def __init__(self):
        super(DestinatarioRetrato, self).__init__()
        self.elements = []
        self.inclui_descritivo(nome='remetente', titulo=u'DESTINATÁRIO/REMETENTE', top=0*cm, left=0*cm, width=19.4*cm)

        # 1ª linha
        lbl, fld = self.inclui_campo(nome='remetente_nome', titulo=u'NOME/RAZÃO SOCIAL', conteudo=u'NFe.infNFe.dest.xNome.valor', top=0.42*cm, left=0*cm, width=13.95*cm)
        lbl, fld = self.inclui_campo(nome='remetente_cnpj', titulo=u'CNPJ/CPF', conteudo=u'NFe.cnpj_destinatario_formatado', top=0.42*cm, left=13.95*cm, width=3.26*cm)
        fld.style = DADO_CAMPO_NEGRITO
        lbl, fld = self.inclui_campo(nome='remetente_data_emissao', titulo=u'DATA DA EMISSÃO', conteudo=u'NFe.infNFe.ide.dEmi.formato_danfe', top=0.42*cm, left=17.2*cm, width=2.2*cm)
        fld.style = DADO_CAMPO_NEGRITO

        # 2ª linha
        lbl, fld = self.inclui_campo(nome='remetente_nome', titulo=u'ENDEREÇO', conteudo=u'NFe.endereco_destinatario_formatado', top=1.12*cm, left=0*cm, width=10.9*cm)
        lbl, fld = self.inclui_campo(nome='remetente_bairro', titulo=u'BAIRRO/DISTRITO', conteudo=u'NFe.infNFe.dest.enderDest.xBairro.valor', top=1.12*cm, left=10.9*cm, width=4.5*cm)
        lbl, fld = self.inclui_campo(nome='remetente_cep', titulo=u'CEP', conteudo=u'NFe.cep_destinatario_formatado', top=1.12*cm, left=15.4*cm, width=1.8*cm)
        lbl, fld = self.inclui_campo(nome='remetente_data_entradasaida', titulo=u'DATA DA ENTRADA/SAÍDA', conteudo=u'NFe.infNFe.ide.dSaiEnt.formato_danfe', top=1.12*cm, left=17.2*cm, width=2.2*cm)
        fld.style = DADO_CAMPO_NEGRITO

        ## 3ª linha
        lbl, fld = self.inclui_campo(nome='remetente_municipio', titulo=u'MUNICÍPIO', conteudo=u'NFe.infNFe.dest.enderDest.xMun.valor', top=1.82*cm, left=0*cm, width=9.9*cm)
        lbl, fld = self.inclui_campo(nome='remetente_fone', titulo=u'FONE', conteudo=u'NFe.fone_destinatario_formatado', top=1.82*cm, left=9.9*cm, width=3.3*cm)
        lbl, fld = self.inclui_campo(nome='remetente_uf', titulo=u'UF', conteudo='NFe.infNFe.dest.enderDest.UF.valor', top=1.82*cm, left=13.2*cm, width=0.75*cm)
        lbl, fld = self.inclui_campo(nome='remetente_ie', titulo=u'INSCRIÇÃO ESTADUAL', conteudo=u'NFe.infNFe.dest.IE.valor', top=1.82*cm, left=13.95*cm, width=3.25*cm)
        lbl, fld = self.inclui_campo(nome='remetente_hora_entradasaida', titulo=u'HORA DA ENTRADA/SAÍDA', conteudo=u'NFe.infNFe.ide.hSaiEnt.formato_danfe', top=1.82*cm, left=17.2*cm, width=2.2*cm)
        fld.style = DADO_CAMPO_NEGRITO

        self.height = 2.52*cm


class CabProdutoRetrato(BandaDANFE):
    def __init__(self):
        super(CabProdutoRetrato, self).__init__()
        self.elements = []

        lbl = self.inclui_descritivo_produto(nome='', titulo='CÓDIGO', top=0*cm, left=0*cm, width=2.3*cm)
        lbl.padding_top = 0.15*cm
        lbl = self.inclui_descritivo_produto(nome='', titulo='DESCRIÇÃO', top=0*cm, left=2.3*cm, width=10.6*cm)
        lbl.padding_top = 0.15*cm
        lbl = self.inclui_descritivo_produto(nome='', titulo='QUANTIDADE', top=0*cm, left=12.9*cm, width=1.5*cm)
        lbl.padding_top = 0.15*cm
        lbl = self.inclui_descritivo_produto(nome='', titulo='UNIDADE', top=0*cm, left=14.4*cm, width=1.3*cm)
        lbl.padding_top = 0.15*cm
        lbl = self.inclui_descritivo_produto(nome='', titulo='VALOR UNITÁRIO', top=0*cm, left=15.7*cm, width=2.2*cm)
        lbl.padding_top = 0.15*cm
        lbl = self.inclui_descritivo_produto(nome='', titulo='VALOR TOTAL', top=0*cm, left=17.9*cm, width=1.5*cm)
        lbl.padding_top = 0.15*cm
        self.height = 0.52*cm


class DetProdutoRetrato(BandaDANFE):
    def __init__(self):
        super(DetProdutoRetrato, self).__init__()
        self.elements = []

        self.inclui_campo_produto(nome=u'prod_codigo', conteudo=u'prod.cProd.valor', top=0*cm, left=0*cm, width=2.3*cm)
        self.inclui_campo_produto(nome=u'prod_descricaco', conteudo=u'descricao_produto_formatada', top=0*cm, left=2.3*cm, width=10.6*cm)
        self.inclui_campo_numerico_produto(nome='prod_quantidade', conteudo=u'prod.qCom.formato_danfe', top=0*cm, left=12.90*cm, width=1.5*cm)
        self.inclui_campo_centralizado_produto(nome=u'prod_unidade', conteudo=u'prod.uCom.valor', top=0 * cm, left=14.4 * cm, width=1.3 * cm)
        self.inclui_campo_numerico_produto(nome='vr_unitario', conteudo=u'prod.vUnCom.formato_danfe', top=0*cm, left=15.7*cm, width=2.2*cm)
        self.inclui_campo_numerico_produto(nome='', conteudo='prod.vProd.formato_danfe', top=0*cm, left=17.9*cm, width=1.5*cm)

        #self.height = 0.28*cm
        self.auto_expand_height = True


class RodapeNFCeRetrato(BandaDANFE):
    def __init__(self, homologacao=False, contingencia=False, consumidor=False, endereco=False, message=''):
        super(RodapeNFCeRetrato, self).__init__()
        self.elements = []
        top = 0

        #Totalizadores
        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=u'QTD. TOTAL DE ITENS', top=top * cm,
                                          left=0 * cm, width=12 * cm, height=0.4 * cm)
        top += 0.4

        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=u'VALOR TOTAL R$', top=top * cm,
                                          left=0 * cm, width=12 * cm, height=0.4 * cm)
        fld = self.inclui_campo_sem_borda(nome='clc_vnf',conteudo=u'NFe.infNFe.total.ICMSTot.vProd.formato_danfe',
                                          top=top* cm, left=15.4 * cm, width=4 * cm)
        fld.style = DESCRITIVO_DANFE_NFCE_ESQUERDA
        top += 0.4

        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=u'DESCONTO R$', top=top * cm,
                                          left=0 * cm, width=12 * cm, height=0.4 * cm)
        fld = self.inclui_campo_sem_borda(nome='clc_vdesc', conteudo=u'NFe.infNFe.total.ICMSTot.vDesc.formato_danfe',
                                          top=top * cm, left=15.4 * cm, width=4 * cm)

        fld.style = DESCRITIVO_DANFE_NFCE_ESQUERDA
        top += 0.4


        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=u'VALOR A PAGAR R$', top=top * cm,
                                          left=0 * cm, width=12 * cm, height=0.4 * cm)
        fld = self.inclui_campo_sem_borda(nome='clc_vdesc', conteudo=u'NFe.infNFe.total.ICMSTot.vNF.formato_danfe',
                                          top=top * cm, left=15.4 * cm, width=4 * cm)

        fld.style = DESCRITIVO_DANFE_NFCE_ESQUERDA
        top += 0.4

        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=u'FORMA DE PAGAMENTO', top=top * cm,
                                          left=0 * cm, width=12 * cm, height=0.4 * cm)
        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=u'Valor Pago', top=top * cm,
                                          left=17 * cm, width=2.4 * cm, height=0.4 * cm)
        txt.style = DESCRITIVO_DANFE_NFCE_ESQUERDA
        top += 0.4

        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=u'Outros', top=top * cm,
                                          left=0 * cm, width=12 * cm, height=0.4 * cm)
        fld = self.inclui_campo_sem_borda(nome='clc_vnf', conteudo=u'NFe.infNFe.total.ICMSTot.vNF.formato_danfe',
                                          top=top * cm, left=15.4 * cm, width=4 * cm)
        fld.style = DESCRITIVO_DANFE_NFCE_ESQUERDA

        top += 0.6
        self.elements.append(Line(top=top*cm, bottom=top* cm, left=0* cm, right=19.4 * cm, stroke_width=0.1))
        top += 0.1

        # Quadro do rodapé
        self.inclui_texto(nome='quadro_rodape', titulo='', texto='', top=0*cm, left=0*cm, width=19.4*cm, height=12.0*cm)
        if contingencia:
            txt = self.inclui_texto_sem_borda(nome='danfe_ext', texto=u'EMITIDO EM CONTINGÊNCIA', top=top*cm, left=0*cm, width=19.4*cm, height=0.4*cm)
        else:
            txt = self.inclui_texto_sem_borda(nome='danfe_ext', texto=u'EMISSÃO NORMAL', top=top * cm, left=0 * cm, width=19.4 * cm, height=0.4 * cm)
        txt.style = DESCRITIVO_DANFE_NFCE_CENTRO
        top += 0.4

        if homologacao:
            txt = self.inclui_texto_sem_borda(nome='danfe_ext', texto=u'DOCUMENTO EMITIDO EM HOMOLOGAÇÃO - SEM VALOR FISCAL', top=top * cm, left=0 * cm, width=19.4 * cm, height=0.4 * cm)
            txt.style = DESCRITIVO_DANFE_NFCE_CENTRO
            top += 0.4

        fld = self.inclui_campo_sem_borda(nome='danfe_ext', conteudo=u'NFe.nfce_linha_numero_serie_emissao', top=top * cm, left=0 * cm, width=19.4 * cm, height=0.4 * cm)
        fld.style = DESCRITIVO_DANFE_NFCE_CENTRO
        top += 0.4

        # URL Consulta
        txt = self.inclui_campo_sem_borda(nome='danfe_ext',
                                          conteudo=u'NFe.gera_url_consulta',
                                          top=top * cm,
                                          left=0 * cm, width=19.4 * cm, height=0.4 * cm)
        txt.style = DESCRITIVO_DANFE_NFCE_CENTRO
        top += 0.4

        # Chave de Acesso
        fld = self.inclui_campo_sem_borda(nome='danfe_ext', conteudo=u'NFe.chave_formatada',
                                          top=top * cm, left=0 * cm, width=19.4 * cm, height=0.4 * cm)

        fld.style = DESCRITIVO_DANFE_NFCE_CENTRO
        top += 0.4

        # Protocolo
        fld = self.inclui_campo_sem_borda(nome='danfe_ext', conteudo=u'protNFe.nfce_linha_protocolo_formatado',
                                          top=top*cm, left=0 * cm, width=19.4 * cm, height=0.4 * cm)
        fld.style = DESCRITIVO_DANFE_NFCE_CENTRO
        top += 0.4

        # CONSUMIDOR
        if consumidor:
            fld = self.inclui_campo_sem_borda(nome='danfe_ext', conteudo=u'NFe.consumidor_linha_nfce',
                                              top=top * cm, left=0 * cm, width=19.4 * cm, height=0.4 * cm)
            fld.style = DESCRITIVO_DANFE_NFCE_CENTRO
        else:
            txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                              texto=u'CONSUMIDOR NÃO IDENTIFICADO',
                                              top=top * cm,
                                              left=0 * cm, width=19.4 * cm, height=0.4 * cm)
            txt.style = DESCRITIVO_DANFE_NFCE_CENTRO
        top += 0.4

        # ENDERECO CONSUMIDOR
        if endereco:
            fld = self.inclui_campo_sem_borda(nome='danfe_ext', conteudo=u'NFe.endereco_consumidor_linha_nfce',
                                              top=top * cm, left=0 * cm, width=19.4 * cm, height=0.4 * cm)
            fld.style = DESCRITIVO_DANFE_NFCE_CENTRO
            top += 0.4

        # QR CODE
        qrcode_size = 4.5
        self.elements.append(
            BarCode(type=u'QR', attribute_name=u'NFe.gera_url_xml', top=top*cm,
                    left=((19.4/2)-(qrcode_size/2))*cm, width=qrcode_size*cm, height=qrcode_size*cm))
        top += 5.5 #qrcode_size


        # URL Consulta
        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=message,
                                          top=top * cm,
                                          left=0 * cm, width=19.4 * cm, height=0.4 * cm)
        txt.style = DESCRITIVO_DANFE_NFCE_CENTRO
        top += 0.4

        self.height = 12*cm

