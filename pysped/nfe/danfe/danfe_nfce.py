# -*- coding: utf-8 -*-
#
# PySPED - Python libraries to deal with Brazil's SPED Project
#
# Copyright (C) 2010-2012
# Copyright (C) Aristides Caldeira <aristides.caldeira at tauga.com.br>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as
# published by the Free Software Foundation, either version 2.1 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# PySPED - Bibliotecas Python para o
#          SPED - Sistema Público de Escrituração Digital
#
# Copyright (C) 2010-2012
# Copyright (C) Aristides Caldeira <aristides.caldeira arroba tauga.com.br>
#
# Este programa é um software livre: você pode redistribuir e/ou modificar
# este programa sob os termos da licença GNU Library General Public License,
# publicada pela Free Software Foundation, em sua versão 2.1 ou, de acordo
# com sua opção, qualquer versão posterior.
#
# Este programa é distribuido na esperança de que venha a ser útil,
# porém SEM QUAISQUER GARANTIAS, nem mesmo a garantia implícita de
# COMERCIABILIDADE ou ADEQUAÇÃO A UMA FINALIDADE ESPECÍFICA. Veja a
# GNU Library General Public License para mais detalhes.
#
# Você deve ter recebido uma cópia da GNU Library General Public License
# juntamente com este programa. Caso esse não seja o caso, acesse:
# <http://www.gnu.org/licenses/>
#

from django.conf import settings
from StringIO import StringIO
from geraldo.generators import PDFGenerator
from pysped.nfe.danfe.danferetrato_nfce import DanfeNfceRetrato
from pysped.nfe.leiaute import ProtNFe_310, RetCancNFe_200, ProcCancNFe_200
from pysped.nfe.leiaute import ProcEventoCancNFe_100
from danferetrato_nfce import RodapeNFCeRetrato


class DANFE_NFCE(object):
    def __init__(self):
        self.imprime_canhoto        = True
        self.imprime_duplicatas = True

        self.caminho           = ''
        self.salvar_arquivo    = True

        self.NFe         = None
        self.nfe_model   = None
        self.protNFe     = None
        self.procCancNFe = None
        self.retCancNFe  = None
        self.procEventoCancNFe = None
        self.danfe       = None
        self.conteudo_pdf = None

        self.obs_impressao    = 'DANFE gerado em %(now:%d/%m/%Y, %H:%M:%S)s'
        self.nome_sistema     = ''
        self.site             = ''
        self.logo             = settings.NFE_LOGO
        self.leiaute_logo_vertical = False
        self.dados_emitente   = []

    def gerar_danfe(self):
        if self.NFe is None:
            raise ValueError('Não é possível gerar um DANFE sem a informação de uma NF-e')

        if self.protNFe is None:
            self.protNFe = ProtNFe_310()

        if self.retCancNFe is None:
            self.retCancNFe = RetCancNFe_200()

        if self.procCancNFe is None:
            self.procCancNFe = ProcCancNFe_200()

        if self.procEventoCancNFe is None:
            self.procEventoCancNFe = ProcEventoCancNFe_100()

        #
        # Prepara o queryset para impressão
        #
        self.NFe.monta_chave()
        self.NFe.monta_dados_contingencia_fsda()
        self.NFe.site = self.site

        for detalhe in self.NFe.infNFe.det:
            detalhe.NFe = self.NFe
            detalhe.protNFe = self.protNFe
            detalhe.retCancNFe = self.retCancNFe
            detalhe.procCancNFe = self.procCancNFe
            detalhe.procEventoCancNFe = self.procEventoCancNFe

        #
        # Prepara as bandas de impressão para cada formato
        #
        if self.NFe.infNFe.ide.tpImp.valor == 2:
            raise ValueError('DANFE em formato paisagem ainda não implementado')
        else:
            self.danfe = DanfeNfceRetrato()
            self.danfe.queryset = self.NFe.infNFe.det

        self.danfe.band_page_header = self.danfe.remetente
        self.danfe.band_page_header.child_bands = []

        if self.NFe.infNFe.ide.tpAmb.valor == 2:
            self.danfe.remetente.obs_sem_valor_fiscal()

        self.danfe.band_page_header.child_bands.append(self.danfe.cab_produto)
        homologacao = False
        contingencia = False
        consumidor = False
        endereco = False
        message = ''
        if self.nfe_model is not None:
            message = self.nfe_model.nfe_ibpt_message
        if self.NFe.infNFe.ide.tpAmb.valor == 2:
            homologacao = True
        if self.NFe.infNFe.dest.CPF.valor or self.NFe.infNFe.dest.CNPJ.valor:
            consumidor = True
        if self.NFe.infNFe.dest.enderDest.xLgr.valor and len(self.NFe.infNFe.dest.enderDest.xLgr.valor):
            endereco = True

        self.danfe.band_page_footer = RodapeNFCeRetrato(contingencia=contingencia,
                                                        homologacao=homologacao,
                                                        consumidor=consumidor,
                                                        endereco=endereco,
                                                        message=message)


        self.danfe.band_detail = self.danfe.det_produto
        self.danfe.remetente.monta_quadro_emitente(self.danfe.remetente.dados_emitente_sem_logo(self.logo))

        danfe_pdf = StringIO()
        self.danfe.generate_by(PDFGenerator, filename=danfe_pdf)
        self.conteudo_pdf = danfe_pdf.getvalue()
        danfe_pdf.close()

        if self.salvar_arquivo:
            nome_arq = self.caminho + self.NFe.chave + '.pdf'
            self.danfe.generate_by(PDFGenerator, filename=nome_arq)
