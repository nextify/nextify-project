# -*- coding: utf-8 -*-
#
# PySPED - Python libraries to deal with Brazil's SPED Project
#
# Copyright (C) 2010-2012
# Copyright (C) Aristides Caldeira <aristides.caldeira at tauga.com.br>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# PySPED - Bibliotecas Python para o
#          SPED - Sistema Público de Escrituração Digital
#
# Copyright (C) 2010-2012
# Copyright (C) Aristides Caldeira <aristides.caldeira arroba tauga.com.br>
#
# Este programa é um software livre: você pode redistribuir e/ou modificar
# este programa sob os termos da licença GNU Affero General Public License,
# publicada pela Free Software Foundation, em sua versão 3 ou, de acordo
# com sua opção, qualquer versão posterior.
#
# Este programa é distribuido na esperança de que venha a ser útil,
# porém SEM QUAISQUER GARANTIAS, nem mesmo a garantia implícita de
# COMERCIABILIDADE ou ADEQUAÇÃO A UMA FINALIDADE ESPECÍFICA. Veja a
# GNU Affero General Public License para mais detalhes.
#
# Você deve ter recebido uma cópia da GNU Affero General Public License
# juntamente com este programa. Caso esse não seja o caso, acesse:
# <http://www.gnu.org/licenses/>
#

from __future__ import division, print_function, unicode_literals
from django.conf import settings

from pysped.xml_sped import *
from pysped.nfe.leiaute import ESQUEMA_ATUAL_VERSAO_3 as ESQUEMA_ATUAL
from pysped.nfe.leiaute import nfe_400
from pysped.nfe.leiaute.nfe_400 import Det, AutXML, Pag
from lxml import etree
import os
import hashlib

DIRNAME = os.path.dirname(__file__)


class InfNFe(nfe_400.InfNFe):
    def __init__(self):
        super(InfNFe, self).__init__()
        self.ide.mod.valor = '65'  #  NFC-e
        self.ide.tpImp.valor = '4'  #  DANFE NFC-e em papel
        self.ide.indPres.valor = '1'  #  Operação presencial
        self.ide.indFinal.valor = '1'  #  Consumidor final
        self.transp.modFrete.valor = 9  #  Sem frete
        self.dest.modelo = '65'

    def get_xml(self):
        xml = XMLNFe.get_xml(self)
        xml += '<infNFe versao="' + unicode(self.versao.valor) + '" Id="' + self.Id.valor + '">'
        xml += self.ide.xml
        xml += self.emit.xml
        xml += self.avulsa.xml
        xml += self.dest.xml
        xml += self.retirada.xml
        xml += self.entrega.xml

        for a in self.autXML:
            xml += a.xml

        for d in self.det:
            d.imposto.ICMS.regime_tributario = self.emit.CRT.valor
            xml += d.xml

        xml += self.total.xml
        xml += self.transp.xml
        xml += self.cobr.xml

        for p in self.pag:
            xml += p.xml

        xml += self.infAdic.xml
        xml += self.exporta.xml
        xml += self.compra.xml
        xml += self.cana.xml
        xml += '</infNFe>'
        return xml

    def set_xml(self, arquivo):
        if self._le_xml(arquivo):
            self.versao.xml = arquivo
            self.Id.xml = arquivo
            self.ide.xml = arquivo
            self.emit.xml = arquivo
            self.avulsa.xml = arquivo
            self.dest.xml = arquivo

            self.dest.modelo = self.ide.mod.valor

            self.retirada.xml = arquivo
            self.entrega.xml = arquivo

            #
            # Técnica para leitura de tags múltiplas
            # As classes dessas tags, e suas filhas, devem ser
            # "reenraizadas" (propriedade raiz) para poderem ser
            # lidas corretamente
            #
            self.autXML = self.le_grupo('//NFe/infNFe/autXML', AutXML)
            self.det = self.le_grupo('//NFe/infNFe/det', Det)

            self.total.xml = arquivo
            self.transp.xml = arquivo
            self.cobr.xml = arquivo

            self.pag = self.le_grupo('//NFe/infNFe/pag', Pag)

            self.infAdic.xml = arquivo
            self.exporta.xml = arquivo
            self.compra.xml = arquivo
            self.cana.xml = arquivo

    xml = property(get_xml, set_xml)


class InfNFeSupl(XMLNFe):
    def __init__(self):
        super(InfNFeSupl, self).__init__()
        self.qrCode = TagCaracter(nome='qrCode', codigo='ZX02', tamanho=[100, 600], raiz='//NFe/infNFeSupl')
        self.urlChave = TagCaracter(nome='urlChave', codigo='ZX03', tamanho=[21, 85], raiz='//NFe/infNFeSupl')

    def get_xml(self):
        xml = XMLNFe.get_xml(self)
        xml += '<infNFeSupl>'
        xml += self.qrCode.xml
        xml += self.urlChave.xml
        xml += '</infNFeSupl>'
        return xml

    def set_xml(self, arquivo):

        if self._le_xml(arquivo):
            self.qrCode.xml = arquivo
            self.urlChave.xml = arquivo

    xml = property(get_xml, set_xml)


class NFCe(nfe_400.NFe):
    def __init__(self):
        super(NFCe, self).__init__()
        self.infNFe = InfNFe()
        self.infNFeSupl = InfNFeSupl()
        #self.Signature = Signature()
        self.caminho_esquema = os.path.join(DIRNAME, 'schema/', ESQUEMA_ATUAL + '/')
        self.arquivo_esquema = 'nfe_v3.10.xsd'

    def get_xml(self):
        xml = XMLNFe.get_xml(self)
        xml += ABERTURA
        xml += '<NFe xmlns="http://www.portalfiscal.inf.br/nfe">'
        xml += self.infNFe.xml
        xml += self.infNFeSupl.xml
        #
        # Define a URI a ser assinada
        #
        #self.Signature.URI = '#' + self.infNFe.Id.valor

        #xml += self.Signature.xml

        xml += '</NFe>'
        return xml

    def set_xml(self, arquivo):
        if self._le_xml(arquivo):
            self.infNFe.xml = arquivo
            self.infNFeSupl.xml = arquivo
            self.Signature.xml = self._le_noh('//NFe/sig:Signature')

    xml = property(get_xml, set_xml)

    def gera_url_xml(self):
        # url = 'http://www.fazenda.pr.gov.br/nfce/qrcode?'
        # param = 'chNFe=' + self.infNFe.Id.valor.replace('NFe', '')
        # param += '&nVersao=100'
        # param += '&tpAmb=' + str(settings.NFE_AMBIENTE)
        # if self.infNFe.dest.CPF.valor and len(self.infNFe.dest.CPF.valor):
        #     param += '&cDest=' + self.infNFe.dest.CPF.valor
        # elif self.infNFe.dest.CNPJ.valor and len(self.infNFe.dest.CNPJ.valor):
        #     param += '&cDest=' + self.infNFe.dest.CNPJ.valor
        # param += '&dhEmi=' + self.infNFe.ide.dhEmi.formato_xml().encode('hex')
        # param += '&vNF=' + str(self.infNFe.total.ICMSTot.vNF.valor)
        # param += '&vICMS=0.00'
        # param += '&digVal=' + str(self.Signature.DigestValue.__str__()).encode('hex')
        # param += '&cIdToken=' + settings.CSC_ID
        # strhash = '&cHashQRCode=' + hashlib.sha1(param + settings.CSC).hexdigest()
        #url += '&CSC=0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789'

        # self.infNFeSupl.qrCode.cdata = url + param + strhash

        return self.infNFeSupl.qrCode.valor

    def gera_url_consulta(self):
        return 'Consulte pela Chave de Acesso em ' + self.infNFeSupl.urlChave.valor
