# -*- coding: utf-8 -*-
NFE_VERSAO_APP = 'PANAREA_V_0.0.1'
NFE_VERSAO_LAYOUT = '3.10'
NFE_AMBIENTE = 2  # 1 - Producao, 2 - Homologacao

#  HOMOLOGACAO
NFE_HOMOLOG_RAZAO_SOCIAL = 'NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL'
NFCE_HOMOLOG_ITEM = 'NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL'
NFE_HOMOLOG_CNPJ = '99999999000191'

# caminhos
CERT_PATH = '/home/carlos/certificados/'
NFE_FILE_PATH = '/home/carlos/nfe/'
NFCE_FILE_PATH = '/home/carlos/nfce/'
NFE_LOGO = '/home/carlos/nfce/caa9257c-1a5a-4a81-bf2e-afe22dd67bb3.png'

# CERTIFICADO
CERT_FILE = '12036865_11052017.pfx'
CERT_PASSWORD = '123'

# email
SENDGRID_API_KEY = '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'
MAIL_FROM = 'contato@ddmsuprimentos.com.br'
MAIL_SUBJECT = 'NFe DDM Suprimentos'
MAIL_MESSAGE = u'Prezados Senhores,<br><br> Em observância à legislação corrente, segue em anexo a nossa nota fiscal eletrônica.<br>'
MAIL_CCO = 'carlosmeira@outlook.com'
MAIL_TO_HOMOLOG = 'meira.bcc@gmail.com'
MAIL_MAX_RETRIES = 3

# NFCe
CSC_ID = '000001'
CSC = 'OFCMJMXVHKNVDPUOEGQNCESBJUZF31NH2A4N'

# LEGAL INFORMATION
LEGAL_TEXT = u'Empresa optante pelo simples nacional. Nao gera credito de ICMS e IPI, conforme lei complementar 123/2006'