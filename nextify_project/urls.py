"""nextify_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))net
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic.base import TemplateView

urlpatterns = [
    url(r'^location/', include('nextify.externals.tables.location.urls', namespace='location')),
    url(r'^ncm/', include('nextify.externals.tables.ncm.urls', namespace='ncm')),
    url(r'^cfop/', include('nextify.externals.tables.cfop.urls', namespace='cfop')),
    url(r'^customer/', include('nextify.apps.person.customer.urls', namespace='customer')),
    url(r'^supplier/', include('nextify.apps.person.supplier.urls', namespace='supplier')),
    url(r'^shippingcompany/', include('nextify.apps.person.shippingcompany.urls', namespace='shippingcompany')),
    url(r'^address/', include('nextify.apps.address.urls', namespace='address')),
#    url(r'^operation/', include('nextify.apps.operation.urls', namespace='operation')),
    url(r'^product/', include('nextify.apps.catalogue.product.urls', namespace='product')),
    url(r'^pricing/', include('nextify.apps.pricing.urls', namespace='pricing')),
    url(r'^unit/', include('nextify.apps.unit.urls', namespace='unit')),
    url(r'^itemcategory/', include('nextify.apps.catalogue.itemcategory.urls', namespace='itemcategory')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^operation/', include('nextify.apps.operation.urls', namespace='operation')),
    url(r'^payment/', include('nextify.apps.payment.urls', namespace='payment')),
    url(r'^nfe/', include('nextify.apps.document.nfe.urls', namespace='nfe')),
    url(r'^nfce/', include('nextify.apps.document.nfce.urls', namespace='nfce')),
    url(r'^invoice/', include('nextify.apps.document.invoice.urls', namespace='invoice')),
    url(r'^api/', include('nextify.apps.document.urls', namespace='api')),
    url(r'^', login_required(TemplateView.as_view(template_name='blank.html'))),
    # Document


]

urlpatterns += staticfiles_urlpatterns()
