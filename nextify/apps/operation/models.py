from .abstract_models import AbstractOperation, AbstractOperationCategory, AbstractOperationNFe, AbstractOperationCfop


class Operation(AbstractOperation):
    pass


class OperationCategory(AbstractOperationCategory):
    pass


class OperationNFe(AbstractOperationNFe):
    pass


class OperationCfop(AbstractOperationCfop):
    pass

# class OperationTax(AbstractOperationTax):
#     pass