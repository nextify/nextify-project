from django.contrib import admin
from .models import *


class OperationAdmin(admin.ModelAdmin):
    pass


admin.site.register(Operation, OperationAdmin)
admin.site.register(OperationCategory, OperationAdmin)

