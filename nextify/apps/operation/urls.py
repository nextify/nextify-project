from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^create/$', login_required(views.OperationCreate.as_view()), name='create'),
    url(r'^(?P<pk>\d+)/update/$', login_required(views.OperationUpdate.as_view()), name='update'),
    url(r'^$', login_required(views.OperationList.as_view()), name='list'),
    url(r'^lookup/$', views.OperationLookupView.as_view(), name='lookup'),
    url(r'^json/$', login_required(views.OperationListJson.as_view()), name='list-json'),
]
