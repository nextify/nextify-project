# -*- coding: utf-8 -*-
from django.db import models
from django.apps import apps
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from nextify.core.enum.operation import OperationType, OperationIssuer
from nextify.core.enum.document import DocumentModel
from nextify.core.enum.nfe import NFeFinality
from nextify.core.enum.default import YesNo

from nextify.apps.person.managers import CompanyManager


class AbstractOperation(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, editable=False, default=settings.COMPANY_ID)
    code = models.CharField(u'Código', max_length=10, blank=False)
    description = models.CharField(u'Descrição', max_length=60)
    operation_type = models.IntegerField('Tipo da Operação', choices=OperationType.choices())
    operation_category = models.ForeignKey('operation.OperationCategory', verbose_name=u'Categoria da Operação',
                                           null=False)
    bank_account = models.ForeignKey('banking.BankAccount', null=True)
    issuer_type = models.IntegerField(u'Tipo de Emissão', choices=OperationIssuer.choices())
    model = models.IntegerField(u'Modelo do Documento', choices=DocumentModel.choices())

    on_company = CompanyManager()
    objects = models.Manager()

    def __unicode__(self):
        return '%s | %s' % (self.code, self.description)

    # @property
    # def series(self):
    #     model = apps.get_model('parameters', 'DocumentIssueParameters')
    #     # param = None
    #     try:
    #         param = model.on_entity.get(company=self.company, model=self.model)
    #     except ObjectDoesNotExist:
    #         return None
    #     else:
    #         return param.series
    #
    class Meta:
        abstract = True

    @models.permalink
    def get_absolute_url(self):
        return ('operation:update', (),
                {'pk': self.pk})

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(AbstractOperation, self).save(force_insert, force_update, using,
                                            update_fields)
        self.create_children()

    def create_children(self):
        if self.model == DocumentModel.NFE or self.model == DocumentModel.NFCE:
            model = apps.get_model('operation', 'OperationNFe')
            o, created = model._default_manager.get_or_create(operation=self)

            if created:
                o.save()

        # model = apps.get_model('operation', 'OperationTax')
        # o, created = model._default_manager.get_or_create(operation=self)

    #     if created:
    #         o.calc_method_iss = TaxCalcMethod.NO_CALC
    #         o.calc_method_pis = TaxCalcMethod.NO_CALC
    #         o.calc_method_cofins = TaxCalcMethod.NO_CALC
    #         o.calc_method_ipi = TaxCalcMethod.NO_CALC
    #         o.calc_method_icms = TaxCalcMethod.NO_CALC
    #         o.save()

    # @property
    # def calc_method_iss(self):
    #     if self.taxes is not None:
    #         return self.taxes.calc_method_iss
    #     return None
    #
    # @property
    # def iss(self):
    #     if self.taxes is not None:
    #         return self.taxes.iss
    #     return None


class AbstractOperationCategory(models.Model):
    code = models.CharField(u'Código', max_length=20, blank=False)
    description = models.CharField(u'Descrição', max_length=60)

    customer_as_issuer = models.BooleanField(u'Cliente Emissor?',
                                             choices=YesNo.choices(), default=YesNo.NO)
    customer_as_receiver = models.BooleanField(u'Cliente Destinatário?',
                                               choices=YesNo.choices(), default=YesNo.NO)
    supplier_as_issuer = models.BooleanField(u'Fornecedor Emissor?',
                                             choices=YesNo.choices(), default=YesNo.NO)
    supplier_as_receiver = models.BooleanField(u'Fornecedor Destinatário?',
                                               choices=YesNo.choices(), default=YesNo.NO)
    company_as_issuer = models.BooleanField(u'Empresa Emissora?',
                                            choices=YesNo.choices(), default=YesNo.NO)
    company_as_receiver = models.BooleanField(u'Empresa Destinatária?',
                                              choices=YesNo.choices(), default=YesNo.NO)

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s - %s' % (self.code, self.description)


class AbstractOperationNFe(models.Model):
    operation = models.OneToOneField('operation.Operation', related_name='nfe')
    nfe_finality = models.SmallIntegerField(_('Nfe Finality'), null=False, choices=NFeFinality.choices(),
                                            default=NFeFinality.REGULAR)

    class Meta:
        abstract = True

#
# class AbstractOperationTax(models.Model):
#     operation = models.OneToOneField('operation.Operation', null=False, related_name='taxes')
#     calc_method_icms = models.IntegerField(_("Calc. Method ICMS"), choices=TaxCalcMethod.choices(),
#                                            default=TaxCalcMethod.NO_CALC)
#     #operation_iss = models.ForeignKey('taxes.Iss', null=True, blank=True)
#     #pis = models.ForeignKey('taxes.Pis', null=True)
#     #cofins = models.ForeignKey('taxes.Cofins', null=True)
#     #ipi = models.ForeignKey('taxes.Ipi', null=True)
#     icms = models.ForeignKey('taxes.Tax', null=True, blank=True)
#
#     class Meta:
#         abstract = True


class AbstractOperationCfop(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)
    operation = models.ForeignKey('operation.Operation', null=False)
    cfop = models.ForeignKey('cfop.Cfop', null=False)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True