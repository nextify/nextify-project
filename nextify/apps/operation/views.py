# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse
from django.db.models import Q

from nextify.core.views.generic import ObjectLookupView
from nextify.apps.operation.models import Operation
from nextify.core.views.datatables import BaseDatatableView
from . import forms


class OperationCreate(SuccessMessageMixin, CreateView):
    model = Operation
    form_class = forms.OperationForm
    success_message = u'Operação cadastrada com sucesso.'

    def get_success_url(self):
        if 'save' in self.request.POST:
            return reverse('operation:update', kwargs={'pk': self.object.pk, })
        if 'save-add' in self.request.POST:
            return reverse('operation:create')


class OperationUpdate(SuccessMessageMixin, UpdateView):
    model = Operation
    form_class = forms.OperationForm
    success_message = u'Operação atualizada com sucesso.'

    def get_success_url(self):
        if 'save' in self.request.POST:
            return reverse('operation:update', kwargs={'pk': self.object.pk, })
        if 'save-add' in self.request.POST:
            return reverse('operation:create')

    def get_context_data(self, **kwargs):
        context = super(OperationUpdate, self).get_context_data(**kwargs)
        if self.request.POST:
            context['form'] = forms.OperationForm(self.request.POST, instance=self.object)
            context['cfopformset'] = forms.OperationCfopFormSet(self.request.POST, instance=self.object)
            context['cfopformset_helper'] = forms.OperationCfopInlineHelper()

        else:
            context['form'] = forms.OperationForm(instance=self.object)
            context['cfopformset'] = forms.OperationCfopFormSet(instance=self.object)
            context['cfopformset_helper'] = forms.OperationCfopInlineHelper()
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        cfopformset = forms.OperationCfopFormSet(self.request.POST, instance=self.object)
        if form.is_valid() and cfopformset.is_valid():
            return self.form_valid(form, cfopformset)
        else:
            return self.form_invalid(form, cfopformset)

    def form_valid(self, form, cfopformset):
        self.object = form.save()
        cfopformset.instance = self.object
        cfopformset.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, cfopformset):
        return self.render_to_response(
            self.get_context_data(form=form, cfopformset=cfopformset,
                                  cfopformset_helper=forms.OperationCfopInlineHelper())
        )


class OperationLookupView(ObjectLookupView):
    model = Operation

    def lookup_filter(self, qs, term):
        return qs.filter(Q(code__istartswith=term) | Q(description__icontains=term))


class OperationList(ListView):
    model = Operation


class OperationListJson(BaseDatatableView):
    model = Operation
    columns = ['id', 'code', 'description', 'edit']
    order_columns = ['id', 'code', 'description']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    # def filter_queryset(self, qs):
    #     search_values = self.get_search_values()
    #     cols = self.get_columns()
    #     for i in range(len(search_values)):
    #         if search_values[i] != '':
    #             if cols[i] == 'name':
    #                 qs = qs.filter(name__icontains=search_values[i])
    #             if cols[i] == 'category':
    #                 qs = qs.filter(category__exact=search_values[i])
    #             if cols[i] == 'is_active':
    #                 qs = qs.filter(is_active=str2bool(search_values[i]))
    #
    #     return qs
    #
    # def get_initial_queryset(self, company=None):
    #     return super(CustomerListViewJson, self).get_initial_queryset(get_logged_company(self.request))