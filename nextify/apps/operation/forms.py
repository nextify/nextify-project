# -*- coding: utf-8 -*-
import nextify.core.util.nextforms as forms
from django.forms.models import inlineformset_factory
from nextify.core.forms import NextifyBaseInlineFormSet
from nextify.core.util.crispy_forms.helper import FormHelper
from nextify.core.util.crispy_forms.layout import Layout, Submit, Field, Div, Row, HTML, NextifyInlineField
from nextify.core.util.crispy_forms.bootstrap import FormActions

from nextify.externals.tables.cfop.widgets import CfopSelect
from nextify.externals.tables.cfop.models import Cfop

from .models import Operation, OperationCfop


class OperationForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Informações da Operação</h3>'),
            Row(
                Div(Field('code', css_class='form-control'), css_class='col-md-6'),
                Div(Field('description', css_class='form-control'), css_class='col-md-6'),
            ),
            Row(
                Div(Field('operation_category', css_class='form-control'), css_class='col-md-3'),
                Div(Field('operation_type', css_class='form-control'), css_class='col-md-3'),
                Div(Field('issuer_type', css_class='form-control'), css_class='col-md-3'),
                Div(Field('model', css_class='form-control'), css_class='col-md-3'),
            ),
            css_class='form-body',
        ),
    )

    class Meta:
        model = Operation
        fields = ['code', 'description', 'operation_type', 'operation_category', 'issuer_type', 'model', ]


class OperationCfopForm(forms.ModelForm):
    cfop = forms.ModelChoiceField(label=u'CFOP', widget=CfopSelect(),
                                                queryset=Cfop.objects.all())

    class Meta:
        model = OperationCfop
        fields = ['cfop',]

OperationCfopFormSet = inlineformset_factory(Operation, OperationCfop, form=OperationCfopForm, min_num=0, extra=1,
                                             formset=NextifyBaseInlineFormSet, fields=['cfop',])


class OperationCfopInlineHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(OperationCfopInlineHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.form_id = 'operationcfop'
        self.form_show_labels = False
        self.disable_csrf = True
        self.render_required_fields = True
        self.render_hidden_fields = True
        self.layout = Layout(
            Row(
                NextifyInlineField('DELETE'),
                NextifyInlineField('cfop', wrapper_class='col-md-12'),
            ),
        )
