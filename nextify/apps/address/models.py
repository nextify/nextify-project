from .abstract_models import AbstractAddress, AbstractCustomerAddress, AbstractSupplierAddress, \
    AbstractShippingCompanyAddress, AbstractCompanyAddress, AbstractCompanyPersonAddress


class Address(AbstractAddress):
    pass


class CustomerAddress(Address, AbstractCustomerAddress):
    pass


class SupplierAddress(Address, AbstractSupplierAddress):
    pass


class ShippingCompanyAddress(Address, AbstractShippingCompanyAddress):
    pass


class CompanyAddress(AbstractCompanyAddress):
    pass


class CompanyPersonAddress(Address, AbstractCompanyPersonAddress):
    pass
