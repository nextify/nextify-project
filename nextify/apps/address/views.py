# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from django.db.models import Q
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin

from nextify.apps.address.models import CustomerAddress, SupplierAddress, ShippingCompanyAddress, Address
from django.apps import apps
from nextify.core.views.datatables import BaseDatatableView
from nextify.core.views.generic import ObjectLookupView

class AddressListViewJson(BaseDatatableView):
    #model = CustomerAddress
    columns = ['id', 'summary', 'edit']
    order_columns = ['id', 'summary']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def render_column(self, row, column):
        """ Renders a column on a row
        """
        if column == 'summary':
            text = '<address>'
            for field in row.active_address_fields():
                text = '%s%s</br>' % (text, field)
            return '%s%s' % (text, '</address>')

        return super(AddressListViewJson, self).render_column(row, column)


class AddressCreateView(SuccessMessageMixin, CreateView):
    success_message = 'Endereço cadastrado com sucesso.'


class AddressUpdateView(SuccessMessageMixin, UpdateView):
    success_message = 'Endereço atualizado com sucesso.'


class CustomerAddressListViewJson(AddressListViewJson):
    model = CustomerAddress

    def filter_queryset(self, qs):
        qs = qs.filter(customer=self.kwargs['customer_pk'])

        return qs


class SupplierAddressListViewJson(AddressListViewJson):
    model = SupplierAddress

    def filter_queryset(self, qs):
        qs = qs.filter(supplier=self.kwargs['supplier_pk'])

        return qs


class ShippingCompanyAddressListViewJson(AddressListViewJson):
    model = ShippingCompanyAddress

    def filter_queryset(self, qs):
        qs = qs.filter(shipping_company=self.kwargs['shipping_company_pk'])

        return qs


class CustomerAddressCreateView(AddressCreateView):
    model = CustomerAddress

    def get_context_data(self, **kwargs):
        context = super(CustomerAddressCreateView, self).get_context_data(**kwargs)

        model = apps.get_model('person', 'Customer')
        customer_pk = int(self.kwargs['customer_pk'])
        customer = model.on_company.get(pk=customer_pk)
        context['customer_pk'] = customer_pk
        context['customer_code'] = customer.code
        context['customer_name'] = customer.name
        return context

    def form_valid(self, form):
        address = form.save(commit=False)
        address.customer_id = self.kwargs['customer_pk']
        return super(CustomerAddressCreateView, self).form_valid(form)


class ShippingCompanyAddressCreateView(AddressCreateView):
    model = ShippingCompanyAddress

    def get_context_data(self, **kwargs):
        context = super(ShippingCompanyAddressCreateView, self).get_context_data(**kwargs)

        model = apps.get_model('person', 'ShippingCompany')
        shipping_company_pk = int(self.kwargs['shipping_company_pk'])
        shipping_company = model.on_company.get(pk=shipping_company_pk)
        context['shipping_company_pk'] = shipping_company_pk
        context['shipping_company_code'] = shipping_company.code
        context['shipping_company_name'] = shipping_company.name
        return context

    def form_valid(self, form):
        address = form.save(commit=False)
        address.shipping_company_id = self.kwargs['shipping_company_pk']
        return super(ShippingCompanyAddressCreateView, self).form_valid(form)


class SupplierAddressCreateView(AddressCreateView):
    model = SupplierAddress

    def get_context_data(self, **kwargs):
        context = super(SupplierAddressCreateView, self).get_context_data(**kwargs)

        model = apps.get_model('person', 'Supplier')
        supplier_pk = int(self.kwargs['supplier_pk'])
        supplier = model.on_company.get(pk=supplier_pk)
        context['supplier_pk'] = supplier_pk
        context['supplier_code'] = supplier.code
        context['supplier_name'] = supplier.name
        return context

    def form_valid(self, form):
        address = form.save(commit=False)
        address.supplier_id = self.kwargs['supplier_pk']
        return super(SupplierAddressCreateView, self).form_valid(form)


class CustomerAddressUpdateView(AddressUpdateView):
    model = CustomerAddress

    def get_context_data(self, **kwargs):
        context = super(CustomerAddressUpdateView, self).get_context_data(**kwargs)

        customer = self.object.customer
        context['customer_pk'] = customer.pk
        context['customer_code'] = customer.code
        context['customer_name'] = customer.name
        return context


class SupplierAddressUpdateView(AddressUpdateView):
    model = SupplierAddress

    def get_context_data(self, **kwargs):
        context = super(SupplierAddressUpdateView, self).get_context_data(**kwargs)

        supplier = self.object.supplier
        context['supplier_pk'] = supplier.pk
        context['supplier_code'] = supplier.code
        context['supplier_name'] = supplier.name
        return context


class ShippingCompanyAddressUpdateView(AddressUpdateView):
    model = ShippingCompanyAddress

    def get_context_data(self, **kwargs):
        context = super(ShippingCompanyAddressUpdateView, self).get_context_data(**kwargs)

        shipping_company = self.object.shipping_company
        context['shipping_company_pk'] = shipping_company.pk
        context['shipping_company_code'] = shipping_company.code
        context['shipping_company_name'] = shipping_company.name
        return context


class AddressLookupView(ObjectLookupView):
    model = Address

    def lookup_filter(self, qs, term):
        return qs.filter(Q(street__icontains=term))

    def get_args(self):
        GET = self.request.GET
        return (GET.get('initial', None),
                GET.get('q', None),
                GET.get('customer', None),
                GET.get('supplier', None),
                GET.get('company', None),
                GET.get('shipping', None),
                int(GET.get('page', 1)),
                int(GET.get('page_limit', 20)))

    def get(self, request):
        self.request = request
        qs = self.get_queryset()
        addr = None
        initial, q, customer, supplier, company, shipping, page, page_limit = self.get_args()

        if initial:
            qs = self.initial_filter(qs, initial)
            more = False
        else:
            if customer:
                addr = CustomerAddress.on_company.filter(customer=customer)
            elif supplier:
                addr = SupplierAddress.on_company.filter(supplier=supplier)
            if addr is not None:
                qs = qs.filter(id__in=addr)
            if q:
                qs = self.lookup_filter(qs, q)
            qs, more = self.paginate(qs, page, page_limit)

        return HttpResponse(json.dumps({
            'results': [self.format_object(obj) for obj in qs],
            'more': more,
        }), content_type='application/json')
