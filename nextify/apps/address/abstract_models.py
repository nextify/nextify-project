# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.apps import apps
from django.utils.translation import ugettext_lazy as _

from nextify.apps.person.managers import DataCompanyManager, CompanyManager
from nextify.core.models.mixins import ModelDiffMixin
from nextify.core.enum.default import YesNo, Active
from nextify.core.enum.address import *
from nextify.core.enum.person import *

class AbstractBaseAddress(models.Model):
    title = models.SmallIntegerField(_(u"Tratamento"), choices=AddressPersonTitle.choices(),
                                     null=True, blank=True)
    first_name = models.CharField('Nome', max_length=255, blank=True)
    last_name = models.CharField('Sobrenome', max_length=255, blank=True)

    address_type = models.SmallIntegerField(_(u"TIpo do Endereço"), choices=AddressType.choices(), null=False)
    street = models.CharField("Logradouro", max_length=60, blank=False)
    number = models.CharField("Número", max_length=60, blank=False)
    complement = models.CharField("Complemento", max_length=60, blank=True)
    zipcode = models.CharField("CEP", max_length=64)
    province = models.CharField("Bairro", max_length=60, blank=False)
    city = models.ForeignKey('location.City', null=True, blank=True)
    city_name = models.CharField("Cidade", max_length=128, blank=True)
    state = models.ForeignKey('location.State', null=True, blank=True)
    state_name = models.CharField("Estado", max_length=60, blank=True)
    state_acronym = models.CharField("Sigla Estado", max_length=2, blank=True)
    country = models.ForeignKey('location.Country', null=True, blank=True)
    phone = models.CharField("Telefone", max_length=14,)
    phone2 = models.CharField("Telefone 2", max_length=14, blank=True)
    email = models.EmailField("Email", blank=True)

    is_default = models.NullBooleanField(u"Endereço padrão", default=False, choices=YesNo.choices())

    is_active = models.BooleanField('Active',
                                    default=True,
                                    choices=Active.choices(),
                                    help_text='Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.')

    date_created = models.DateTimeField(_('date joined'), auto_now_add=True, null=True)
    date_updated = models.DateTimeField(_("Date Updated"), auto_now=True, null=True)

    def __unicode__(self):
        return self.summary

    class Meta:
        abstract = True
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')

    @property
    def summary(self):
        """
        Returns a single string summary of the address,
        separating fields using commas.
        """
        return u", ".join(self.active_address_fields())


    def active_address_fields(self, include_salutation=True):
        """
        Return the non-empty components of the address, but merging the
        title, first_name and last_name into a single line.
        """
        fields = [self.street, self.number, self.complement,
                  self.city_name, self.state_name, self.zipcode]
        #if include_salutation:
        #    fields = [self.salutation] + fields
        #fields = [f.strip() for f in fields if f]
        #try:
        #    fields.append(self.country.name)
        #except exceptions.ObjectDoesNotExist:
        #    pass
        return fields

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.set_locations()
        self.set_locations_names()
        super(AbstractBaseAddress, self).save(force_insert, force_update, using, update_fields)

    def set_locations(self):
        if self.city:
            if self.city.state:
                self.state = self.city.state
            if self.state:
                self.country = self.state.country

    def set_locations_names(self):
        if self.city:
            self.city_name = self.city.name
        if self.state:
            self.state_name = self.state.name
            self.state_acronym = self.state.acronym


class AbstractAddress(AbstractBaseAddress):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


class AbstractCustomerAddress(models.Model):
    customer = models.ForeignKey('person.Customer', related_name='addresses')

    class Meta:
        abstract = True
        verbose_name = _('Customer Address')
        verbose_name_plural = _('Customer Addresses')

    def save(self, *args, **kwargs):
        if self.id:
            self._ensure_defaults_integrity()
        super(AbstractCustomerAddress, self).save(*args, **kwargs)

    def _ensure_defaults_integrity(self):
        if self.is_default:
            self.__class__._default_manager\
                .filter(customer=self.customer, is_default=True)\
                .update(is_default=False)

    @models.permalink
    def get_absolute_url(self):
        if self.customer.category == PersonCategory.FOREIGN:
            return 'address:foreign-customer-update', (), {'pk': self.pk}
        else:
            return 'address:customer-update', (), {'pk': self.pk, }


class AbstractSupplierAddress(models.Model):
    supplier = models.ForeignKey('person.Supplier', related_name='addresses')

    class Meta:
        abstract = True
        verbose_name = _('Supplier Address')
        verbose_name_plural = _('Supplier Addresses')

    def save(self, *args, **kwargs):
        if self.id:
            self._ensure_defaults_integrity()
        super(AbstractSupplierAddress, self).save(*args, **kwargs)

    def _ensure_defaults_integrity(self):
        if self.is_default:
            self.__class__._default_manager\
                .filter(supplier=self.supplier, is_default=True)\
                .update(is_default=False)

    @models.permalink
    def get_absolute_url(self):
        if self.supplier.category == PersonCategory.FOREIGN:
            return 'address:foreign-supplier-update', (), {'pk': self.pk}
        else:
            return 'address:supplier-update', (), {'pk': self.pk}


class AbstractShippingCompanyAddress(models.Model):
    shipping_company = models.ForeignKey('person.ShippingCompany', related_name='addresses')

    class Meta:
        abstract = True
        verbose_name = _('Shipping Company Address')
        verbose_name_plural = _('Shipping Company Addresses')

    def save(self, *args, **kwargs):
        if self.id:
            self._ensure_defaults_integrity()
        super(AbstractShippingCompanyAddress, self).save(*args, **kwargs)

    def _ensure_defaults_integrity(self):
        if self.is_default:
            self.__class__._default_manager\
                .filter(shipping_company=self.shipping_company, is_default=True)\
                .update(is_default=False)

    @models.permalink
    def get_absolute_url(self):
        return 'address:shippingcompany-update', (), {'pk': self.pk}


class AbstractCompanyAddress(ModelDiffMixin, AbstractBaseAddress):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, editable=False, default=settings.COMPANY_ID)

    __has_changed = False

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True
        verbose_name = _('Company Address')
        verbose_name_plural = _('Company Addresses')

    def save(self, *args, **kwargs):
        self._ensure_defaults_integrity()
        self.__has_changed = self.has_changed
        super(AbstractCompanyAddress, self).save(*args, **kwargs)
        self.create_children()

    def _ensure_defaults_integrity(self):
        if self.is_default:
            self.__class__._default_manager\
                .filter(company=self.company, is_default=True)\
                .update(is_default=False)

    def create_children(self):
        self.create_person_address()

    def create_person_address(self):
        p = apps.get_model('address', 'CompanyPersonAddress')

        try:
            obj = p.on_company.get(company_person=self.company.company_person, address_type=self.address_type)
        except p.DoesNotExist:
            o = p.on_company.create(company_person=self.company.company_person,
                                    title=self.title,
                                    first_name=self.first_name,
                                    last_name=self.last_name,
                                    address_type=self.address_type,
                                    street=self.street,
                                    number=self.number,
                                    complement=self.complement,
                                    zipcode=self.zipcode,
                                    province=self.province,
                                    city=self.city,
                                    city_name=self.city_name,
                                    state=self.state,
                                    state_name=self.state_name,
                                    state_acronym=self.state_acronym,
                                    country=self.country,
                                    phone=self.phone,
                                    phone2=self.phone2,
                                    email=self.email,
                                    is_default=self.is_default,
                                    is_active=self.is_active)
        else:
            if self.__has_changed:
                p.on_company.filter(company_person=self.company.company_person, address_type=self.address_type)\
                    .update(title=self.title,
                            first_name=self.first_name,
                            last_name=self.last_name,
                            address_type=self.address_type,
                            street=self.street,
                            number=self.number,
                            complement=self.complement,
                            zipcode=self.zipcode,
                            province=self.province,
                            city=self.city,
                            city_name=self.city_name,
                            state=self.state,
                            state_name=self.state_name,
                            state_acronym=self.state_acronym,
                            country=self.country,
                            phone=self.phone,
                            phone2=self.phone2,
                            email=self.email,
                            is_default=self.is_default,
                            is_active=self.is_active)

class AbstractCompanyPersonAddress(models.Model):
    company_person = models.ForeignKey('person.CompanyPerson', related_name='addresses')

    class Meta:
        abstract = True
        verbose_name = _('Company Person Address')
        verbose_name_plural = _('Company Person Addresses')

