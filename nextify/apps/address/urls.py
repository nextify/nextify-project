from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views
from .forms import CustomerBrazilianAddressForm, CustomerForeignAddressForm, \
    SupplierBrazilianAddressForm, SupplierForeignAddressForm, ShippingCompanyBrazilianAddressForm

urlpatterns = [
    # Customer
    url(r'^customer/(?P<customer_pk>\d+)/json/$',
        login_required(views.CustomerAddressListViewJson.as_view()),
        name='customer-list-json'),

    # Customer Create
    url(r'^customer/(?P<customer_pk>\d+)/create/$',
        login_required(views.CustomerAddressCreateView.as_view(form_class=CustomerBrazilianAddressForm)),
        name='customer-create'),
    url(r'^foreign/customer/(?P<customer_pk>\d+)/create/$',
        login_required(views.CustomerAddressCreateView.as_view(form_class=CustomerForeignAddressForm)),
        name='foreign-customer-create'),

    # Customer Update
    url(r'^customer/(?P<pk>\d+)/update/$',
        login_required(views.CustomerAddressUpdateView.as_view(form_class=CustomerBrazilianAddressForm)),
        name='customer-update'),
    url(r'^foreign/customer/(?P<pk>\d+)/update/$',
        login_required(views.CustomerAddressUpdateView.as_view(form_class=CustomerForeignAddressForm)),
        name='foreign-customer-update'),

    # Supplier
    url(r'^supplier/(?P<supplier_pk>\d+)/json/$',
        login_required(views.SupplierAddressListViewJson.as_view()),
        name='supplier-list-json'),

    # Supplier Create
    url(r'^supplier/(?P<supplier_pk>\d+)/create/$',
        login_required(views.SupplierAddressCreateView.as_view(form_class=SupplierBrazilianAddressForm)),
        name='supplier-create'),
    url(r'^foreign/supplier/(?P<supplier_pk>\d+)/create/$',
        login_required(views.SupplierAddressCreateView.as_view(form_class=SupplierForeignAddressForm)),
        name='foreign-supplier-create'),

    # Supplier Update
    url(r'^supplier/(?P<pk>\d+)/update/$',
        login_required(views.SupplierAddressUpdateView.as_view(form_class=SupplierBrazilianAddressForm)),
        name='supplier-update'),
    url(r'^foreign/supplier/(?P<pk>\d+)/update/$',
        login_required(views.SupplierAddressUpdateView.as_view(form_class=SupplierForeignAddressForm)),
        name='foreign-supplier-update'),

    # Shipping Company
    url(r'^shippingcompany/(?P<shipping_company_pk>\d+)/json/$',
        login_required(views.ShippingCompanyAddressListViewJson.as_view()),
        name='shippingcompany-list-json'),
    url(r'^shippingcompany/(?P<shipping_company_pk>\d+)/create/$',
        login_required(views.ShippingCompanyAddressCreateView.as_view(form_class=ShippingCompanyBrazilianAddressForm)),
        name='shippingcompany-create'),
    url(r'^shippingcompany/(?P<pk>\d+)/update/$',
        login_required(views.ShippingCompanyAddressUpdateView.as_view(form_class=ShippingCompanyBrazilianAddressForm)),
        name='shippingcompany-update'),

    url(r'^lookup/$', login_required(views.AddressLookupView.as_view()), name='lookup'),
]
