from django.contrib import admin
from .models import *


class AddressAdmin(admin.ModelAdmin):
    pass


admin.site.register(CompanyAddress, AddressAdmin)
admin.site.register(CustomerAddress, AddressAdmin)

