# -*- coding: utf-8 -*-
import nextify.core.util.nextforms as forms

from nextify.apps.address.models import CustomerAddress, SupplierAddress, ShippingCompanyAddress

from nextify.core.util.crispy_forms.helper import FormHelper
from nextify.core.util.crispy_forms.layout import Layout, Submit, Field, Div, Row, Colmd3, Colmd5, Colmd6, HTML, Coloff1md2
from nextify.core.util.crispy_forms.bootstrap import FormActions

from nextify.externals.tables.location.models import City, Country
from nextify.externals.tables.location.widgets import CitySelect, CountrySelect

class AbstractAddressForm(forms.ModelForm):
    is_default = forms.BooleanField(label=u'Endereço Padrão', widget=forms.CheckboxInput(), required=False)


class BrazilianAddressForm(AbstractAddressForm):
    city = forms.ModelChoiceField(label=u'Cidade', widget=CitySelect(), queryset=City.objects.all())

    helper = FormHelper()

    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Informações do Endereço</h3>'),
            Row(
                Colmd6(Field('address_type', css_class='form-control')),
                Coloff1md2(Field('is_default', css_class='form-control')),
            ),
            HTML(u'<h5 class="form-section">Contato</h3>'),
            Row(
                Colmd6(Field('title', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('first_name', css_class='form-control')),
                Colmd6(Field('last_name', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('email', css_class='form-control')),
                Colmd3(Field('phone', css_class='form-control')),
                Colmd3(Field('phone2', css_class='form-control')),
            ),

            HTML(u'<h5 class="form-section">Endereço</h3>'),
            Row(
                Colmd6(Field('street', css_class='form-control')),
                Colmd3(Field('number', css_class='form-control')),
                Colmd3(Field('complement', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('city', css_class='form-control')),
                Colmd3(Field('province', css_class='form-control')),
                Colmd3(Field('zipcode', css_class='form-control')),
            ),
            css_class='form-body',
        ),
        FormActions(Submit('submit', 'Salvar', css_class='btn green')),
    )

    class Meta:
        fields = ['title', 'address_type', 'first_name', 'last_name', 'street', 'number', 'email',
                  'complement', 'province', 'zipcode', 'phone', 'phone2', 'is_default', 'city']


class ForeignAddressForm(AbstractAddressForm):
    country = forms.ModelChoiceField(label=u'Pais', widget=CountrySelect(), queryset=Country.objects.all())
    helper = FormHelper()

    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Informações do Endereço</h3>'),
            Row(
                Colmd6(Field('address_type', css_class='form-control')),
                Coloff1md2(Field('is_default', css_class='form-control')),
            ),
            HTML(u'<h5 class="form-section">Contato</h3>'),
            Row(
                Colmd6(Field('title', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('first_name', css_class='form-control')),
                Colmd6(Field('last_name', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('email', css_class='form-control')),
                Colmd3(Field('phone', css_class='form-control')),
                Colmd3(Field('phone2', css_class='form-control')),
            ),

            HTML(u'<h5 class="form-section">Endereço</h3>'),
            Row(
                Colmd6(Field('street', css_class='form-control')),
                Colmd3(Field('number', css_class='form-control')),
                Colmd3(Field('complement', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('country', css_class='form-control')),
                Colmd3(Field('city_name', css_class='form-control')),
                Colmd3(Field('state_name', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('province', css_class='form-control')),
                Colmd3(Field('zipcode', css_class='form-control')),
            ),

            css_class='form-body',
        ),
        FormActions(Submit('submit', 'Salvar', css_class='btn green')),
    )

    class Meta:
        fields = ['title', 'address_type', 'first_name', 'last_name', 'street', 'number',
                  'complement', 'province', 'zipcode', 'phone', 'phone2', 'is_default',
                  'country', 'city_name', 'state_name', 'email']


class CustomerBrazilianAddressForm(BrazilianAddressForm):
    class Meta(BrazilianAddressForm.Meta):
        model = CustomerAddress


class CustomerForeignAddressForm(ForeignAddressForm):
    class Meta(ForeignAddressForm.Meta):
        model = CustomerAddress


class SupplierBrazilianAddressForm(BrazilianAddressForm):
    class Meta(BrazilianAddressForm.Meta):
        model = SupplierAddress


class SupplierForeignAddressForm(ForeignAddressForm):
    class Meta(ForeignAddressForm.Meta):
        model = SupplierAddress


class ShippingCompanyBrazilianAddressForm(BrazilianAddressForm):
    class Meta(BrazilianAddressForm.Meta):
        model = ShippingCompanyAddress



