from django.contrib import admin
from .models import *


class CatalogueAdmin(admin.ModelAdmin):
    pass


admin.site.register(Product, CatalogueAdmin)
admin.site.register(ItemCategory, CatalogueAdmin)

