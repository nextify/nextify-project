# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.apps import apps
from django.core.exceptions import ObjectDoesNotExist

from nextify.apps.person.managers import DataCompanyManager

from nextify.apps.sequence import get_next_code

from nextify.core.enum.sequence import *
from nextify.core.enum.product import *
from nextify.core.enum.default import *


class AbstractProduct(models.Model):
    ncm_classification = models.ForeignKey('ncm.NcmClassification', null=True, verbose_name=u'Classificação NCM')

    base_unit = models.ForeignKey('unit.Unit', verbose_name=u'Unidade')
    base_unit_ean = models.CharField(u'Código EAN', max_length=14, blank=True)
    control_unit = models.ForeignKey('unit.Unit', null=True, blank=True, related_name='item_control_unit',
                                     verbose_name=u'Unidade de Controle')
    control_unit_factor = models.DecimalField(u'Fator de conversão', max_digits=15, decimal_places=6, null=True, blank=True)
    control_unit_ean = models.CharField(u'Código EAN', max_length=14, blank=True)
    purchase_unit = models.ForeignKey('unit.Unit', null=True, blank=True, related_name='item_purchase_unit',
                                      verbose_name=u'Unidade de Compra')
    purchase_unit_factor = models.DecimalField(u'Fator de conversão', max_digits=15, decimal_places=6, null=True, blank=True)
    purchase_unit_ean = models.CharField(u'Código EAN', max_length=14, blank=True)
    sales_unit = models.ForeignKey('unit.Unit', null=True, blank=True, related_name='item_sales_unit',
                                   verbose_name=u'Unidade de Venda')
    sales_unit_factor = models.DecimalField(u'Fator de conversão', max_digits=15, decimal_places=6, null=True, blank=True)
    sales_unit_ean = models.CharField(u'Código EAN', max_length=14, blank=True)
    taxed_unit = models.ForeignKey('unit.Unit', null=True, blank=True, related_name='item_taxed_unit',
                                   verbose_name=u'Unidade Tributável')
    taxed_unit_factor = models.DecimalField(u'Fator de conversão', max_digits=15, decimal_places=6, null=True, blank=True)
    taxed_unit_ean = models.CharField(u'Código EAN', max_length=14, blank=True)

    base_net_weight = models.DecimalField(u'Peso Líquido', max_digits=15, decimal_places=3, null=True, blank=True,
                                          default=0.00)
    base_gross_weight = models.DecimalField(u'Peso Bruto', max_digits=15, decimal_places=3, null=True, blank=True,
                                            default=0.00)

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s | %s' % (self.code, self.description)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.fill_defaults()
        super(AbstractProduct, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                          update_fields=update_fields)
        self.create_children()

    @models.permalink
    def get_absolute_url(self):
        return ('product:update', (),
                {'pk': self.pk})

    @property
    def stock_quantity_display(self):
        try:
            stock = self.stock.filter(unit=self.control_unit).first()
            return '%s %s' % (str(stock.quantity), self.control_unit.code)
        except:
            return '%s %s' % ('0.00', self.control_unit.code)

    @property
    def sales_price_display(self):
        from decimal import Decimal
        model = apps.get_model('pricing', 'PriceTable')
        try:
            p = model.on_company.get(is_default=True)
            pricing = self.pricing.filter(price_table=p, unit=self.sales_unit).first()
            if pricing is None:
                pricing = self.pricing.filter(price_table=p).first()
            return 'R$ %s / %s' % \
                   (str(Decimal(pricing.value,).quantize(Decimal('1.00'))).replace('.', ','), pricing.unit.code)
        except:
            return 'R$ %s / %s' % ('0,00', self.sales_unit.code)

    def get_sales_price(self, unit):
        from decimal import Decimal
        model = apps.get_model('pricing', 'PriceTable')
        try:
            p = model.on_company.get(is_default=True)
            pricing = self.pricing.filter(price_table=p, unit=unit).first()
            if pricing is not None:
                return pricing.value
            else:
                l = self.get_product_units_list()
                for u in l:
                    pricing = self.pricing.filter(price_table=p, unit=u).first()
                    if pricing is not None:
                        return self.convert_units(unit, u, pricing.value)
        except:
            pass
        return Decimal('0.00')

    def get_product_units_list(self):
        l = [self.base_unit, self.sales_unit, self.control_unit, self.purchase_unit, self.taxed_unit]
        l = list(set(l))
        return l

    def fill_defaults(self):
        if self.control_unit is None:
            self.control_unit = self.base_unit
            self.control_unit_factor = 1
            self.control_unit_ean = self.base_unit_ean
        if self.purchase_unit is None:
            self.purchase_unit = self.base_unit
            self.purchase_unit_factor = 1
            self.purchase_unit_ean = self.base_unit_ean
        if self.sales_unit is None:
            self.sales_unit = self.base_unit
            self.sales_unit_factor = 1
            self.sales_unit_ean = self.base_unit_ean
        if self.taxed_unit is None:
            self.taxed_unit = self.base_unit
            self.taxed_unit_factor = 1
            self.taxed_unit_ean = self.base_unit_ean
        if not self.id or not self.code:
            self.code = get_next_code(SequenceType.PRODUCT, self.category.id)

    def create_children(self):
        model = apps.get_model('catalogue', 'ProductParameters')
        o, created = model._default_manager.get_or_create(product=self)

        if created:
            o.save()

    def convert_to_control_unit(self, unit_origin, quantity_origin):
        return self.convert_units(unit_origin, self.control_unit, quantity_origin)

    def convert_to_taxed_unit(self, unit_origin, quantity_origin):
        return self.convert_units(unit_origin, self.taxed_unit, quantity_origin)

    def convert_units(self, unit_origin, unit_destiny, value_origin):
        from decimal import Decimal
        origin_factor = Decimal(1)
        destiny_factor = Decimal(1)
        if unit_origin == unit_destiny:
            return value_origin
        # Verifica qual Unidade de Origem
        if unit_origin == self.control_unit:
            origin_factor = self.control_unit_factor
        if unit_origin == self.sales_unit:
            origin_factor = self.sales_unit_factor
        if unit_origin == self.purchase_unit:
            origin_factor = self.purchase_unit_factor
        if unit_origin == self.taxed_unit:
            origin_factor = self.taxed_unit_factor
        # Verifica qual Unidade de Destino
        if unit_destiny == self.control_unit:
            destiny_factor = self.control_unit_factor
        if unit_destiny == self.sales_unit:
            destiny_factor = self.sales_unit_factor
        if unit_destiny == self.purchase_unit:
            destiny_factor = self.purchase_unit_factor
        if unit_destiny == self.taxed_unit:
            destiny_factor = self.taxed_unit_factor

        quantity_destiny = Decimal((value_origin * origin_factor) / destiny_factor).quantize(Decimal('1.000000'))
        return quantity_destiny

class AbstractProductSupplier(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)
    product = models.ForeignKey('catalogue.Product', null=False)
    supplier = models.ForeignKey('person.Supplier', null=False)
    supplier_code = models.CharField(u'Codigo do Fornecedor', max_length=60, blank=False)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


class AbstractProductParameters(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)
    product = models.OneToOneField('catalogue.Product', null=False)

    origin = models.IntegerField(u'Origem', null=False, choices=ProductOrigin.choices(), default=ProductOrigin.NACIONAL)

    stock_control = models.BooleanField(u'Controle de Estoque', null=False, choices=YesNo.choices(), default=YesNo.YES)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


class AbstractProductUnits(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)
    product = models.ForeignKey('catalogue.Product', null=False)
    unit = models.ForeignKey('unit.Unit', null=False)
    factor = models.DecimalField(u'Fator de conversão', max_digits=15, decimal_places=6, null=True, blank=True)
    ean = models.CharField(u'Código EAN', max_length=14, blank=True)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True
