# -*- coding: utf-8 -*-
from django.forms.models import inlineformset_factory
from nextify.apps.unit.models import Unit

import nextify.core.util.nextforms as forms
from nextify.core.forms import NextifyBaseInlineFormSet
from nextify.core.util.crispy_forms.helper import FormHelper
from nextify.core.util.crispy_forms.layout import Layout, Field, Div, Row, Colmd6, Colmd3, HTML, Coloff1md2, NextifyInlineField
from nextify.externals.tables.ncm.widgets import NcmSelect
from nextify.externals.tables.ncm.models import NcmClassification
from nextify.apps.catalogue.models import Product, ProductSupplier, ItemCategory
from nextify.apps.catalogue.product.widgets import ProductUnitSelect
from nextify.apps.person.models import Supplier
from nextify.apps.person.supplier.widgets import SupplierSelect
from nextify.apps.unit.widgets import UnitSelect
from nextify.apps.unit.models import Unit
from nextify.apps.pricing.models import PriceTable, PriceTableLine
from nextify.apps.pricing.widgets import PriceTableSelect


class ProductForm(forms.ModelForm):
    control_unit_base = forms.BooleanField(label=u'Igual à unidade base', widget=forms.CheckboxInput(), required=False)
    purchase_unit_base = forms.BooleanField(label=u'Igual à unidade base', widget=forms.CheckboxInput(), required=False)
    sales_unit_base = forms.BooleanField(label=u'Igual à unidade base', widget=forms.CheckboxInput(), required=False)
    # taxed_unit_base = forms.BooleanField(label=u'Igual à unidade base', widget=forms.CheckboxInput(), required=False)
    ncm_classification = forms.ModelChoiceField(label=u'Class. NCM', widget=NcmSelect(),
                                                queryset=NcmClassification.objects.all())

    base_unit = forms.ModelChoiceField(label=u'Unidade Base',
                                       widget=UnitSelect(attrs={'placeholder': 'Selecione uma unidade ...',
                                                                'data-min-input-length': '0'}),
                                       queryset=Unit.on_company.all(),
                                       required=True)

    control_unit = forms.ModelChoiceField(label=u'Unidade de Controle',
                                          widget=UnitSelect(attrs={'placeholder': 'Selecione uma unidade ...',
                                                                   'data-min-input-length': '0'}),
                                          queryset=Unit.on_company.all(),
                                          required=False)

    sales_unit = forms.ModelChoiceField(label=u'Unidade de Venda',
                                        widget=UnitSelect(attrs={'placeholder': 'Selecione uma unidade ...',
                                                                 'data-min-input-length': '0'}),
                                        queryset=Unit.on_company.all(),
                                        required=False)

    purchase_unit = forms.ModelChoiceField(label=u'Unidade de Compra',
                                           widget=UnitSelect(attrs={'placeholder': 'Selecione uma unidade ...',
                                                                    'data-min-input-length': '0'}),
                                           queryset=Unit.on_company.all(),
                                           required=False)

    # taxed_unit = forms.ModelChoiceField(label=u'Unidade Tributável',
    #                                     widget=UnitSelect(attrs={'placeholder': 'Selecione uma unidade ...',
    #                                                              'data-min-input-length': '0'}),
    #                                     queryset=Unit.on_company.all(),
    #                                     required=False)

    helper = FormHelper()
    helper.form_tag = False

    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Informações do Produto</h3>'),
            Row(
                Colmd6(Field('code', readonly=True)),
                Colmd6(Field('description')),
            ),
            Row(
                Colmd6(Field('type', css_class='form-control')),
                Colmd6(Field('category', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('ncm_classification', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('base_unit', css_class='form-control')),
                Colmd6(Field('base_unit_ean')),
            ),
            Row(
                Colmd6(Field('base_net_weight', css_class='form-control')),
                Colmd6(Field('base_gross_weight', css_class='form-control')),
            ),
            HTML(u'<h5 class="form-section">Unidade de Controle</h5>'),
            Row(
                Div(Field('control_unit', css_class='form-control'), css_class='col-md-4'),
                Div(Field('control_unit_factor', css_class='form-control'), css_class='col-md-4'),
                Div(Field('control_unit_ean'), css_class='col-md-4'),
            ),
            HTML(u'<h5 class="form-section">Unidade de Compra</h5>'),
            Row(
                Div(Field('purchase_unit', css_class='form-control'), css_class='col-md-4'),
                Div(Field('purchase_unit_factor', css_class='form-control'), css_class='col-md-4'),
                Div(Field('purchase_unit_ean'), css_class='col-md-4'),
            ),
            HTML(u'<h5 class="form-section">Unidade de Venda</h5>'),
            Row(
                Div(Field('sales_unit', css_class='form-control'), css_class='col-md-4'),
                Div(Field('sales_unit_factor', css_class='form-control'), css_class='col-md-4'),
                Div(Field('sales_unit_ean'), css_class='col-md-4'),
            ),
            # HTML(u'<h5 class="form-section">Unidade Tributável</h5>'),
            # Row(
            #     Colmd3(Field('taxed_unit', css_class='form-control')),
            #     Colmd3(Field('taxed_unit_factor', css_class='form-control')),
            #     Colmd3(Field('taxed_unit_ean')),
            # ),
            css_class='form-body',
        ),
       # FormActions(Submit('submit', 'Salvar', css_class='btn green')),
    )

    class Meta:
        model = Product

        fields = ['code', 'description', 'type', 'ncm_classification',
                  'base_unit', 'base_unit_ean', 'category',
                  'control_unit', 'control_unit_factor', 'control_unit_ean',
                  'purchase_unit', 'purchase_unit_factor', 'purchase_unit_ean',
                  'sales_unit', 'sales_unit_factor', 'sales_unit_ean',
                  # 'taxed_unit', 'taxed_unit_factor', 'taxed_unit_ean',
                  'base_net_weight', 'base_gross_weight']

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['category'].widget = forms.ChoiceDayLabelInput()
            self.fields['category'].queryset = ItemCategory.on_company.all()
            self.fields['category'].widget.attrs.update({'disabled': 'disabled'})

    def clean(self):
        cleaned_data = super(ProductForm, self).clean()
        error = False
        if cleaned_data.get('control_unit') and cleaned_data.get('control_unit_factor') is None:
            msg = 'Ao preencher a Unidade de Controle, o fator de conversão é obrigatório.'
            self.add_error('control_unit_factor', msg)
            error = True

        if cleaned_data.get('purchase_unit') and cleaned_data.get('purchase_unit_factor') is None:
            msg = 'Ao preencher a Unidade de Compra, o fator de conversão é obrigatório.'
            self.add_error('purchase_unit_factor', msg)
            error = True

        if cleaned_data.get('sales_unit') and cleaned_data.get('sales_unit_factor') is None:
            msg = 'Ao preencher a Unidade de Venda, o fator de conversão é obrigatório.'
            self.add_error('sales_unit_factor', msg)
            error = True

        if error:
            return cleaned_data

        if cleaned_data.get('control_unit') == cleaned_data.get('purchase_unit'):
            msg = 'Unidade de Compra igual a Unidade de Controle. O fator de conversão deve ser o mesmo.'
            if cleaned_data.get('control_unit_factor') != cleaned_data.get('purchase_unit_factor'):
                self.add_error('purchase_unit_factor', msg)
                return cleaned_data

        if cleaned_data.get('control_unit') == cleaned_data.get('sales_unit'):
            msg = 'Unidade de Venda igual a Unidade de Controle. O fator de conversão deve ser o mesmo.'
            if cleaned_data.get('control_unit_factor') != cleaned_data.get('sales_unit_factor'):
                self.add_error('sales_unit_factor', msg)
                return cleaned_data

        if cleaned_data.get('purchase_unit') == cleaned_data.get('sales_unit'):
            msg = 'Unidade de Venda igual a Unidade de Compra. O fator de conversão deve ser o mesmo.'
            if cleaned_data.get('purchase_unit_factor') != cleaned_data.get('sales_unit_factor'):
                self.add_error('sales_unit_factor', msg)
                return cleaned_data

        return cleaned_data


class ProductSupplierForm(forms.ModelForm):
    supplier = forms.ModelChoiceField(label=u'Fornecedor',
                                      widget=SupplierSelect(attrs={'placeholder': 'Selecione um fornecedor ...',
                                                                   'data-min-input-length': '1'}),
                                      queryset=Supplier.on_company.editable())

    class Meta:
        model = ProductSupplier
        fields = ['supplier', 'supplier_code']

ProductSupplierFormSet = inlineformset_factory(Product, ProductSupplier, form=ProductSupplierForm, min_num=0, extra=1,
                                               formset=NextifyBaseInlineFormSet, fields=['supplier', 'supplier_code'])


class ProductSupplierInlineHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(ProductSupplierInlineHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.form_id = 'product_supplier'
        self.form_show_labels = False
        self.disable_csrf = True
        self.render_required_fields = True
        self.render_hidden_fields = True
        self.layout = Layout(
            Row(
                NextifyInlineField('DELETE'),
                NextifyInlineField('supplier', wrapper_class='col-md-6'),
                NextifyInlineField('supplier_code', wrapper_class='col-md-6'),
            ),
        )


class PriceTableLineForm(forms.ModelForm):
    price_table = forms.ModelChoiceField(label=u'Tabela de Preços',
                                         widget=PriceTableSelect(),
                                         queryset=PriceTable.on_company.all())
    unit = forms.ModelChoiceField(label=u'Unidade',
                                  widget=ProductUnitSelect(),
                                  queryset=Unit.on_company.all())

    def __init__(self, *args, **kwargs):
        super(PriceTableLineForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['unit'].widget = forms.ChoiceDayLabelText()
            self.fields['price_table'].widget = forms.ChoiceDayLabelText()
            self.fields['unit'].queryset = Unit.on_company.all()
            self.fields['price_table'].queryset = PriceTable.on_company.all()

    def clean(self):
        cleaned_data = super(PriceTableLineForm, self).clean()
        if not self.instance.pk:
            if 'price_table' in cleaned_data \
                    and 'product' in cleaned_data \
                    and 'unit' in cleaned_data:
                if PriceTableLine.on_company.filter(price_table=cleaned_data['price_table'],
                                                    product=cleaned_data['product'],
                                                    unit=cleaned_data['unit']).exists():
                    msg = 'Tabela já existente para esse produto e unidade.'
                    self.add_error('price_table', msg)

        return cleaned_data


    class Meta:
        model = PriceTableLine
        fields = ['price_table', 'unit', 'value']


PriceTableLineFormSet = inlineformset_factory(Product, PriceTableLine, PriceTableLineForm, min_num=0, extra=1,
                                              formset=NextifyBaseInlineFormSet, fields=['price_table', 'unit', 'value'])


class PriceTableLineInlineHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(PriceTableLineInlineHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.form_id = 'pricetableline'
        self.render_required_fields = True
        self.form_show_labels = False
        self.disable_csrf = True
        self.render_required_fields = True
        self.render_hidden_fields = True
        self.layout = Layout(
            Row(
                NextifyInlineField('DELETE'),
                NextifyInlineField('price_table', css_class='product_text', wrapper_class='col-md-4'),
                NextifyInlineField('unit', css_class='product_text', wrapper_class='col-md-4'),
                NextifyInlineField('value', css_class='form-control', wrapper_class='col-md-4'),
            ),
        )