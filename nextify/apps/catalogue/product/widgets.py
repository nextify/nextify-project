from django.core.urlresolvers import reverse_lazy

from nextify.core.forms.widgets import RemoteSelect


class ProductSelect(RemoteSelect):
    lookup_url = reverse_lazy('product:lookup')


class ProductUnitSelect(RemoteSelect):
    lookup_url = reverse_lazy('product:unit-lookup')
