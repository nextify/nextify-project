import json
from itertools import chain
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Q
from django.views.generic.edit import CreateView, UpdateView
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse

from nextify.apps.catalogue.models import Product, ProductUnits
from nextify.apps.unit.models import Unit
from nextify.core.views.datatables import BaseDatatableView
from nextify.core.views.generic import ObjectLookupView

from .forms import *
#from .forms import ProductForm, ProductSupplierFormSet, ProductSupplierInlineHelper


class ProductCreate(SuccessMessageMixin, CreateView):
    model = Product
    form_class = ProductForm
    success_message = 'Produto cadastrado com sucesso.'

    valid_dict = {
        'form': 'active',
        'formset_suppliers': '',
        'formset_pricing': ''
    }

    def get_success_url(self):
        return reverse('product:update', kwargs={'pk': self.object.pk, })

    def get_context_data(self, **kwargs):
        context = super(ProductCreate, self).get_context_data(**kwargs)
        context['active'] = self.valid_dict
        return context


class ProductUpdate(UpdateView):
    model = Product
    form_class = ProductForm
    formset_suppliers = ProductSupplierFormSet
    formset_pricing = PriceTableLineFormSet
    success_message = 'Produto atualizado com sucesso!'

    valid_dict = {
        'form': '',
        'formset_suppliers': '',
        'formset_pricing': ''
    }

    def get_success_url(self):
        return reverse('product:update', kwargs={'pk': self.object.pk, })

    def get_context_data(self, **kwargs):
        context = super(ProductUpdate, self).get_context_data(**kwargs)
        if self.request.POST:
            context['form'] = self.form_class(self.request.POST, instance=self.object)
            context['formset_suppliers'] = self.formset_suppliers(self.request.POST, instance=self.object)
            context['formset_pricing'] = self.formset_pricing(self.request.POST, instance=self.object)
            context['suppliers_helper'] = ProductSupplierInlineHelper()
            context['pricing_helper'] = PriceTableLineInlineHelper()
        else:
            self.valid_dict['form'] = 'active'
            self.valid_dict['formset_suppliers'] = ''
            self.valid_dict['formset_pricing'] = ''
            context['form'] = self.form_class(instance=self.object)
            context['formset_suppliers'] = self.formset_suppliers(instance=self.object)
            context['formset_pricing'] = self.formset_pricing(instance=self.object)
            context['suppliers_helper'] = ProductSupplierInlineHelper()
            context['pricing_helper'] = PriceTableLineInlineHelper()
        context['active'] = self.valid_dict
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_suppliers = self.formset_suppliers(self.request.POST, instance=self.object)
        formset_pricing = self.formset_pricing(self.request.POST, instance=self.object)
        if self.valid_forms(form, formset_suppliers, formset_pricing):
            return self.form_valid(form, formset_suppliers, formset_pricing)
        else:
            return self.form_invalid(form, formset_suppliers, formset_pricing)

    def form_valid(self, form, formset_suppliers, formset_pricing):
        self.object = form.save()
        formset_suppliers.instance = self.object
        formset_pricing.instance = self.object
        formset_suppliers.save()
        formset_pricing.save()
        messages.add_message(self.request, messages.SUCCESS, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, formset_suppliers, formset_pricing):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset_suppliers=formset_suppliers,
                                  formset_pricing=formset_pricing,
                                  suppliers_helper=ProductSupplierInlineHelper(),
                                  pricing_helper=PriceTableLineInlineHelper(),
                                  )
        )

    def valid_forms(self, form, formset_suppliers, formset_pricing):
        self.valid_dict['form'] = ''
        self.valid_dict['formset_suppliers'] = ''
        self.valid_dict['formset_pricing'] = ''
        if not form.is_valid():
            self.valid_dict['form'] = 'active'
        elif not formset_suppliers.is_valid():
            self.valid_dict['formset_suppliers'] = 'active'
        elif not formset_pricing.is_valid():
            self.valid_dict['formset_pricing'] = 'active'
        else:
            self.valid_dict['form'] = 'active'
            return True
        return False


class ProductList(ListView):
    model = Product

class ProductListJson(BaseDatatableView):
    model = Product
    columns = ['id', 'code', 'description', 'stock_quantity_display', 'sales_price_display', 'edit']
    order_columns = ['id', 'code', 'description', 'stock_quantity_display', 'sales_price_display']
    search_columns = ['product_code', 'product_description']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def filter_queryset(self, qs):
        request = self.request
        for column in self.search_columns:
            try:
                value = request.POST.get(column, '')
                if column == 'product_description':
                    qs = qs.filter(description__icontains=value)
                if column == 'product_code':
                    qs = qs.filter(code__icontains=value)
            except ValueError:
                pass
        return qs

    # def filter_queryset(self, qs):
    #     search_values = self.get_search_values()
    #     cols = self.get_columns()
    #     for i in range(len(search_values)):
    #         if search_values[i] != '':
    #             if cols[i] == 'name':
    #                 qs = qs.filter(name__icontains=search_values[i])
    #             if cols[i] == 'category':
    #                 qs = qs.filter(category__exact=search_values[i])
    #             if cols[i] == 'is_active':
    #                 qs = qs.filter(is_active=str2bool(search_values[i]))
    #
    #     return qs
    #
    # def get_initial_queryset(self, company=None):
    #     return super(CustomerListViewJson, self).get_initial_queryset(get_logged_company(self.request))


class ProductLookupView(ObjectLookupView):
    model = Product

    def lookup_filter(self, qs, term):
        return qs.filter(Q(code__istartswith=term) | Q(description__icontains=term))


class ProductUnitLookupView(ObjectLookupView):
    model = Unit

    def lookup_filter(self, qs, term):
        return qs.filter(Q(code__istartswith=term) | Q(description__icontains=term))

    def get_args(self):
        GET = self.request.GET
        return (GET.get('initial', None),
                GET.get('q', None),
                GET.get('p', None),
                int(GET.get('page', 1)),
                int(GET.get('page_limit', 20)))

    def get(self, request):
        self.request = request
        qs = self.get_queryset()

        initial, q, p, page, page_limit = self.get_args()

        if initial:
            qs = self.initial_filter(qs, initial)
            more = False
        else:
            if p:
                prod = Product.on_company.get(pk=p)
                l = [prod.base_unit.pk, prod.sales_unit.pk, prod.control_unit.pk, prod.purchase_unit.pk, prod.taxed_unit.pk]
                l = list(set(l))
                qs = qs.filter(pk__in=l)
                #set(chain(Unit.on_company.all().values_list('id', flat=True), l))
            if q:
                qs = self.lookup_filter(qs, q)
            qs, more = self.paginate(qs, page, page_limit)

        return HttpResponse(json.dumps({
            'results': [self.format_object(obj) for obj in qs],
            'more': more,
        }), content_type='application/json')
