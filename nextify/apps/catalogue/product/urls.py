from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^create/$', login_required(views.ProductCreate.as_view()), name='create'),
    url(r'^(?P<pk>\d+)/update/$', login_required(views.ProductUpdate.as_view()), name='update'),
    url(r'^$', login_required(views.ProductList.as_view()), name='list'),
    url(r'^json/$', login_required(views.ProductListJson.as_view()), name='list-json'),
    url(r'^lookup/$', login_required(views.ProductLookupView.as_view()), name='lookup'),
    url(r'^unit/lookup/$', login_required(views.ProductUnitLookupView.as_view()), name='unit-lookup'),
]
