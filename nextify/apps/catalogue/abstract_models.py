# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings

from nextify.apps.person.managers import DataCompanyManager
from nextify.core.enum.item import *
from nextify.core.enum.default import *

class AbstractBaseItem(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)

    is_active = models.BooleanField('Active',
                                    default=True,
                                    choices=Active.choices(),
                                    help_text='Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.')

    date_created = models.DateTimeField('Date created', auto_now_add=True, null=True)
    date_updated = models.DateTimeField('Date updated', auto_now=True, null=True)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


class AbstractItem(AbstractBaseItem):
    code = models.CharField(u'Código', max_length=60, blank=True)
    description = models.CharField(u'Descrição', max_length=255, blank=True)
    type = models.IntegerField(u'Tipo', choices=ItemType.choices(), null=False)
    category = models.ForeignKey('catalogue.ItemCategory', null=False, verbose_name=u'Categoria')

    class Meta:
        abstract = True

