from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^create/$', login_required(views.ItemCategoryCreate.as_view()), name='create'),
    url(r'^(?P<pk>\d+)/update/$', login_required(views.ItemCategoryUpdate.as_view()), name='update'),
    url(r'^$', login_required(views.ItemCategoryList.as_view()), name='list'),
    url(r'^json/$', login_required(views.ItemCategoryListJson.as_view()), name='list-json'),
]
