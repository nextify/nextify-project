from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse

from nextify.apps.catalogue.models import ItemCategory
from nextify.core.views.datatables import BaseDatatableView

from .forms import ItemCategoryForm

class ItemCategoryCreate(SuccessMessageMixin, CreateView):
    model = ItemCategory
    form_class = ItemCategoryForm
    success_message = 'Categoria de Produto cadastrada com sucesso.'

    def get_success_url(self):
        return reverse('itemcategory:update', kwargs={'pk': self.object.pk, })



class ItemCategoryUpdate(SuccessMessageMixin, UpdateView):
    model = ItemCategory
    form_class = ItemCategoryForm
    success_message = 'Categoria de Produto atualizada com sucesso.'

    def get_success_url(self):
        return reverse('itemcategory:update', kwargs={'pk': self.object.pk, })


class ItemCategoryList(ListView):
    model = ItemCategory


class ItemCategoryListJson(BaseDatatableView):
    model = ItemCategory
    columns = ['id', 'code', 'description', 'edit']
    order_columns = ['id', 'code', 'description']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    # def filter_queryset(self, qs):
    #     search_values = self.get_search_values()
    #     cols = self.get_columns()
    #     for i in range(len(search_values)):
    #         if search_values[i] != '':
    #             if cols[i] == 'name':
    #                 qs = qs.filter(name__icontains=search_values[i])
    #             if cols[i] == 'category':
    #                 qs = qs.filter(category__exact=search_values[i])
    #             if cols[i] == 'is_active':
    #                 qs = qs.filter(is_active=str2bool(search_values[i]))
    #
    #     return qs
    #
    # def get_initial_queryset(self, company=None):
    #     return super(CustomerListViewJson, self).get_initial_queryset(get_logged_company(self.request))