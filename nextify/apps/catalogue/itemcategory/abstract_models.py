# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.apps import apps

from nextify.apps.person.managers import DataCompanyManager
from nextify.apps.sequence import get_next_code
from nextify.core.enum.item import *
from nextify.core.enum.sequence import *
#from nextify.apps.sequence.models import Sequence


class AbstractItemCategory(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)
    genre = models.IntegerField(u'Gênero', choices=ItemGenre.choices(), null=False)
    code = models.CharField(u'Código da Categoria', max_length=10, blank=True)
    description = models.CharField(u'Descrição', max_length=255, blank=True)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s - %s' % (self.code, self.description)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id or not self.code:
            self.code = get_next_code(SequenceType.ITEM_CATEGORY)
        super(AbstractItemCategory, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                               update_fields=update_fields)
        self.create_sequence()

    @models.permalink
    def get_absolute_url(self):
        return ('itemcategory:update', (),
                {'pk': self.pk})


    def create_sequence(self):
        base = apps.get_model('sequence', 'GenericSequence')
        model = apps.get_model('sequence', 'GenericSequence')
        o, created = model.on_company.get_or_create(sequence_type=SequenceType.PRODUCT, item_category=self)

        if created:
            o_base = base.on_company.get(sequence_type=SequenceType.BASE_PRODUCT)

            o.prefix = o_base.prefix
            o.suffix = o_base.suffix
            o.sequence_expression = o_base.sequence_expression
            o.layout = o_base.layout

            o.save()



