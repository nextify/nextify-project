# -*- coding: utf-8 -*-
#from django import forms
import nextify.core.util.nextforms as forms
from nextify.core.util.crispy_forms.helper import FormHelper
from nextify.core.util.crispy_forms.layout import Layout, Submit, Field, Div, Row, Colmd6, Colmd3, HTML, Coloff1md2
from nextify.core.util.crispy_forms.bootstrap import FormActions

from nextify.apps.catalogue.models import ItemCategory

class ItemCategoryForm(forms.ModelForm):

    helper = FormHelper()

    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Informações da Categoria</h3>'),
            Row(
                Colmd6(Field('code', readonly=True)),
                Colmd6(Field('description')),
            ),
            Row(
                Colmd6(Field('genre', css_class='form-control')),
            ),
            css_class='form-body',
        ),
        FormActions(Submit('submit', 'Salvar', css_class='btn green')),
    )

    class Meta:
        model = ItemCategory

        fields = ['code', 'description', 'genre']
