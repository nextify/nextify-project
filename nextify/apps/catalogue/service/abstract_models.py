from django.db import models


class AbstractService(models.Model):

    class Meta:
        abstract = True

