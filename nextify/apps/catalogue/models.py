from .abstract_models import *
from .product.abstract_models import AbstractProduct, AbstractProductSupplier, AbstractProductUnits, \
    AbstractProductParameters
from .service.abstract_models import AbstractService
from .itemcategory.abstract_models import *

class Item(AbstractItem):
    pass

class ItemCategory(AbstractItemCategory):
    pass

class Product(Item, AbstractProduct):
    pass

class ProductSupplier(AbstractProductSupplier):
    pass

class ProductUnits(AbstractProductUnits):
    pass

class ProductParameters(AbstractProductParameters):
    pass

class Service(Item, AbstractService):
    pass

