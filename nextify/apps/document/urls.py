from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from serializer import NFeQueueList, NFeQueueDetail, NFeWsStatusDetail

urlpatterns = [
    url(r'^nfe/$', NFeQueueList.as_view()),
    url(r'^nfe/(?P<pk>[0-9]+)/$', NFeQueueDetail.as_view()),
    url(r'^nfestatusservico/$', NFeWsStatusDetail.as_view()),
]
