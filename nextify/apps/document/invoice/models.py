from .abstract_models import AbstractInvoice, AbstractInvoiceDuplicate, AbstractBankFile, AbstractBankOcurrence


class Invoice(AbstractInvoice):
    pass


class InvoiceDuplicate(AbstractInvoiceDuplicate):
    pass


class BankFile(AbstractBankFile):
    pass


class BankOcurrence(AbstractBankOcurrence):
    pass