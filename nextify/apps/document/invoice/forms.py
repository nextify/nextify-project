# -*- coding: utf-8 -*-
from collections import OrderedDict
from django.forms.models import inlineformset_factory
from django.forms.models import BaseInlineFormSet

import nextify.core.util.nextforms as forms
from nextify.core.util.crispy_forms.helper import FormHelper
from nextify.core.util.crispy_forms.layout import Layout, Submit, Field, Div, Row, Colmd6, Colmd3, HTML, Coloff1md2, \
    NextifyInlineField
from nextify.core.forms.widgets import ModelLinkWidget
from nextify.core.util.crispy_forms.bootstrap import FormActions
from nextify.apps.document.invoice.models import Invoice, InvoiceDuplicate, BankFile
from nextify.apps.person.customer.widgets import CustomerSelect
from nextify.apps.person.models import Customer


class InvoiceForm(forms.ModelForm):
    person = forms.ModelChoiceField(label=u'Destinatário',
                                    widget=CustomerSelect(
                                        attrs={'placeholder': 'Selecione um Destinatário ...',
                                               'data-min-input-length': '1'}),
                                    queryset=Customer.on_company.editable())

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Informações da Categoria</h3>'),
            Row(
                Colmd3(Field('person', css_class='form-control')),
                Colmd3(Field('invoice_number', css_class='form-control')),
                Colmd3(Field('bank', css_class='form-control')),
                Colmd3(Field('bank_account', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('net_value', css_class='form-control')),
                Colmd6(Field('status', css_class='form-control')),
            ),
            css_class='form-body',
        ),
    )

    class Meta:
        model = Invoice
        fields = ['person', 'invoice_number', 'bank', 'bank_account', 'net_value', 'status']


class MyBaseInlineFormSet(BaseInlineFormSet):
    def add_fields(self, form, index):
        super(MyBaseInlineFormSet, self).add_fields(form, index)
        # Pop the delete field out of the OrderedDict
        delete_field = form.fields.pop('DELETE')
        # Add it at the start
        form.fields = OrderedDict(
                      [('DELETE', delete_field)] + form.fields.items())
        form.fields['DELETE'].label = 'Exc.'


class InvoiceDuplicateForm(forms.ModelForm):
    sequence = forms.IntegerField(label=u'#', widget=forms.DayLabelText(), required=False)

    invoice_due_date = forms.DateTimeField(label=u'Data de Vencimento',
                                           input_formats=['%d/%m/%Y'],
                                           widget=forms.NextifyDateInput(attrs={'data-date-format': 'dd/mm/yyyy'},
                                                                         format='%d/%m/%Y'))
    link = forms.CharField(label='', required=False)

    class Meta:
        model = InvoiceDuplicate
        fields = ['invoice_due_date', 'gross_value', 'discount_value', 'net_value', 'sequence', ]

    def __init__(self, *args, **kwargs):
        super(InvoiceDuplicateForm, self).__init__(*args, **kwargs)
        # instance is always available, it just does or doesn't have pk.
        self.fields['link'].widget = ModelLinkWidget(self.instance, pdf=True, label=u'Gerar Boleto')


InvoiceDuplicateFormSet = inlineformset_factory(Invoice, InvoiceDuplicate, form=InvoiceDuplicateForm, min_num=0, extra=1,
                                                formset=MyBaseInlineFormSet,
                                                fields=['sequence', 'invoice_due_date', 'gross_value',
                                                        'discount_value', 'net_value', ]
                                                )


class InvoiceDuplicateFormInlineHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(InvoiceDuplicateFormInlineHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.form_id = 'invoiceduplicate'
        self.form_show_labels = False
        self.disable_csrf = True
        self.render_required_fields = True
        self.render_hidden_fields = True

        self.layout = Layout(
            NextifyInlineField('DELETE'),
            NextifyInlineField('sequence'),
            NextifyInlineField('invoice_due_date', css_class='form-control dateinput',
                               wrapper_class='col-md-3', readonly=True),
            NextifyInlineField('gross_value', wrapper_class='col-md-3', css_class='form-control'),
            NextifyInlineField('discount_value', wrapper_class='col-md-3', css_class='form-control'),
            NextifyInlineField('net_value', wrapper_class='col-md-3', css_class='form-control', readonly=True),
            NextifyInlineField('link'),
        )


class BankFileForm(forms.ModelForm):
    helper = FormHelper()
    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Informações da Categoria</h3>'),
            Row(
                Colmd6(Field('bank_account', css_class='form-control')),
            ),
            css_class='form-body',
        ),
        FormActions(Submit('save', 'Salvar', css_class='btn green')),
    )

    class Meta:
        model = BankFile
        fields = ['bank_account', ]