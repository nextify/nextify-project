# -*- coding: utf-8 -*-
from datetime import datetime, date
from decimal import Decimal
from django.db import models, transaction
from django.db.models import Sum
from django.conf import settings
from django.apps import apps
from nextify.apps.person.managers import DataCompanyManager
from nextify.core.fields import NextifyPositionField
from nextify.core.enum.banking import Bank, InvoiceStatus, BankEvents, BankInstructions, BankFileStatus
from nextify.core.models.mixins import ModelDiffMixin
from nextify.core.enum.sequence import SequenceType
from nextify.apps.sequence import get_next_code


class AbstractInvoice(ModelDiffMixin, models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)
    person = models.ForeignKey('person.Person', null=False)

    invoice_number = models.IntegerField(u'Num. Fatura', null=False)

    bank = models.IntegerField(u'Banco', choices=Bank.choices(), null=True)

    document = models.ForeignKey('document.FiscalDocument', null=True)

    bank_account = models.ForeignKey('banking.BankAccount', null=False)

    gross_value = models.DecimalField(u'Valor Bruto', max_digits=15, decimal_places=2, null=False, default=0.00)

    net_value = models.DecimalField(u'Valor Líquido', max_digits=15, decimal_places=2, null=False, default=0.00)

    discount_value = models.DecimalField(u'Valor Desconto', max_digits=15, decimal_places=2, null=False, default=0.00)

    fines_value = models.DecimalField(u'Multa', max_digits=15, decimal_places=2, null=False, default=0.00)

    interest_value = models.DecimalField(u'Juros', max_digits=15, decimal_places=2, null=False, default=0.00)

    status = models.IntegerField(u'Situação', choices=InvoiceStatus.choices(), null=False, default=InvoiceStatus.OPEN)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    @models.permalink
    def get_absolute_url(self):
        return ('invoice:update', (),
                {'pk': self.pk})

    def calculate(self):
        if self.duplicates.all().count() > 0:
            totals = self.duplicates.all().aggregate(Sum('gross_value'),
                                                     Sum('net_value'),
                                                     Sum('discount_value'))
            self.gross_value = totals['gross_value__sum']
            self.net_value = totals['net_value__sum']
            self.discount_value = totals['discount_value__sum']


class AbstractInvoiceDuplicate(ModelDiffMixin, models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)

    invoice = models.ForeignKey('invoice.Invoice', null=False, related_name='duplicates')

    sequence = NextifyPositionField(collection='invoice')

    bank_number = models.CharField(u'Nosso Numero', max_length=25, blank=True)

    bank_number_dv = models.CharField(u'Nosso Numero DV', max_length=1, blank=True)

    invoice_date = models.DateField(u'Data', null=True, auto_now_add=True, editable=False)

    invoice_due_date = models.DateField(u'Data de Vencimento', null=True)

    gross_value = models.DecimalField(u'Valor Bruto', max_digits=15, decimal_places=2, null=False, default=0.00)

    net_value = models.DecimalField(u'Valor Líquido', max_digits=15, decimal_places=2, null=False, default=0.00)

    discount_value = models.DecimalField(u'Valor Desconto', max_digits=15, decimal_places=2, null=False, default=0.00)

    fines_value = models.DecimalField(u'Multa', max_digits=15, decimal_places=2, null=False, default=0.00)

    interest_value = models.DecimalField(u'Juros', max_digits=15, decimal_places=2, null=False, default=0.00)

    status = models.IntegerField(u'Situação', choices=InvoiceStatus.choices(), null=False, default=InvoiceStatus.OPEN)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    @models.permalink
    def get_absolute_url(self):
        return ('invoice:duplicate-pdf', (),
                {'pk': self.pk})

    @models.permalink
    def get_pdf_url(self):
        return ('invoice:duplicate-pdf', (),
                {'pk': self.pk})

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.calculate_bank_number()
        self.calculate_net_value()
        super(AbstractInvoiceDuplicate, self).save(force_insert, force_update, using, update_fields)
        self.calculate_invoice_totals()
        self.create_bank_ocurrence()

    def delete(self, using=None, keep_parents=False):
        super(AbstractInvoiceDuplicate, self).delete(using, keep_parents)
        self.calculate_invoice_totals()

    def calculate_net_value(self):
        self.net_value = self.gross_value - self.discount_value

    def calculate_invoice_totals(self):
        self.invoice.calculate()
        self.invoice.save()

    def calculate_bank_number(self):
        if not self.id or not self.bank_number:
            self.bank_number = get_next_code(SequenceType.BANK_NUMBER, self.invoice.bank_account_id)
            if self.bank_number:
                self.bank_number_dv = pin_dv_gen_mod11(self.bank_number)

    def create_bank_ocurrence(self):
        model = apps.get_model('invoice', 'BankOcurrence')
        o, created = model.on_company.get_or_create(duplicate=self, bank_event=BankEvents.ENTRADA_TITULO,
                                                    bank_first_instruction=BankInstructions.PROTESTAR,
                                                    bank_second_instruction=BankInstructions.NAO_HA_INSTRUCOES)
        if created:
            o.save()

    @property
    def barcode(self):
        cod_barras_p1 = ''
        cod_barras_p2 = ''
        cod_barras_dv = ''
        cod_barras_p1 += '0339'
        cod_barras_p2 += str(calc_fator_vencimento_353(self.invoice_due_date))
        cod_barras_p2 += decimal_format(self.net_value)[3:13]
        cod_barras_p2 += '9'
        cod_barras_p2 += str(self.invoice.bank_account.bank_recipient_account)[0:7]
        cod_barras_p2 += '00000'
        cod_barras_p2 += self.bank_number
        cod_barras_p2 += self.bank_number_dv
        cod_barras_p2 += '0'
        cod_barras_p2 += '101'

        cod_barras_dv = pin_dv_gen_mod11_max_weight(cod_barras_p1 + cod_barras_p2)

        print('%s%s%s' % (cod_barras_p1, cod_barras_dv, cod_barras_p2))
        return '%s%s%s' % (cod_barras_p1, cod_barras_dv, cod_barras_p2)


    @property
    def digitable_line(self):
        barcode = self.barcode
        # Grupo 1
        linha = ''
        grupo1 = ''
        grupo1 += barcode[0:3]
        grupo1 += barcode[3:4]
        grupo1 += '9'
        grupo1 += barcode[20:24]
        grupo1 += '.'+pin_dv_gen_mod10(grupo1)

        # Grupo 2
        grupo2 = ''
        grupo2 += barcode[24:28]
        grupo2 += barcode[28:34]
        grupo2 += '.' + pin_dv_gen_mod10(grupo2)

        # Grupo 3
        grupo3 = ''
        grupo3 += barcode[34:40]
        grupo3 += barcode[40:41]
        grupo3 += barcode[41:44]
        grupo3 += '.' + pin_dv_gen_mod10(grupo3)

        # Grupo 4
        grupo4 = ''
        grupo4 += barcode[4:5]

        # Grupo 5
        grupo5 = ''
        grupo5 += barcode[5:9]
        grupo5 += barcode[9:19]

        linha = '%s %s %s %s %s' % (grupo1, grupo2, grupo3, grupo4, grupo5)

        return linha


class AbstractBankFile(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)

    bank_account = models.ForeignKey('banking.BankAccount', null=False)

    file_name = models.CharField(u'Nome do Arquivo', blank=True, max_length=255)

    file_date = models.DateTimeField(u'Data do Arquivo', null=False, auto_now_add=True)

    file_status = models.IntegerField(u'Situação do Arquivo', null=False,
                                      choices=BankFileStatus.choices(), default=BankFileStatus.NO_FILE)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.get_file_name()
        super(AbstractBankFile, self).save(force_insert, force_update, using, update_fields)

    @models.permalink
    def get_download_url(self):
        return ('invoice:bankfile-download', (),
                {'pk': self.pk})

    @models.permalink
    def get_generate_file_url(self):
        return ('invoice:bankfile-generate', (),
                {'pk': self.pk})

    @models.permalink
    def get_bankfile_url(self):
        if self.file_status == BankFileStatus.FILE_CREATED:
            return ('invoice:bankfile-download', (), {'pk': self.pk})
        else:
            return ('invoice:bankfile-generate', (), {'pk': self.pk})

    def select_ocurrences_to_file(self):
        with transaction.atomic():
            m = apps.get_model('invoice', 'BankFile')
            f = m.on_company.get(id=self.id)

            if f.file_status == BankFileStatus.NO_FILE:
                model = apps.get_model('invoice', 'BankOcurrence')
                ocurrences = model.on_company.filter(bank_file=None)

                if len(ocurrences) > 0:
                    f.file_status = BankFileStatus.FILE_CREATED
                    f.save()

                    for ocurrence in ocurrences:
                        ocurrence.bank_file = self
                        ocurrence.save()



    def get_file_name(self):
        if not self.id or not self.file_name:
            self.file_name = get_next_code(SequenceType.BANK_FILE, self.bank_account_id)


class AbstractBankOcurrence(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)

    duplicate = models.ForeignKey('invoice.InvoiceDuplicate', null=False)

    bank_file = models.ForeignKey('invoice.BankFile', null=True, related_name='ocurrences')

    bank_event = models.IntegerField(u'Ocorrencia de Remessa', null=False, choices=BankEvents.choices())

    bank_first_instruction = models.IntegerField(u'Primeira Instrução de Remessa', null=False,
                                                 choices=BankInstructions.choices(),
                                                 default=BankInstructions.NAO_HA_INSTRUCOES)
    bank_second_instruction = models.IntegerField(u'Primeira Instrução de Remessa', null=False,
                                                  choices=BankInstructions.choices(),
                                                  default=BankInstructions.NAO_HA_INSTRUCOES)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


def pin_dv_gen_mod10(digs):
    l = len(digs)
    d = 0
    s = 0
    weight = 2

    for i in range(1,l+1):
        s = int(digs[-i]) * weight
        if s >= 10:
            s = int(str(s)[0]) + int(str(s)[1])
        d += s

        if weight == 2:
            weight = 1
        else:
            weight = 2

    dv = d % 10
    if dv == 0:
        return '0'
    return str(10-dv)


def pin_dv_gen_mod11(digs):
    l = len(digs)
    d = 0
    for i in range(1, l+1):
        d += int(digs[-i])*(i+1)
    dv = d*10 % 11
    if 10 == dv:
        return '0'
    return str(dv)


def pin_dv_gen_mod11_max_weight(digs, max=9):
    l = len(digs)
    d = 0
    weight = 2

    for i in range(1, l+1):
        d = d + (int(digs[-i]) * weight)
        weight += 1
        if weight > max:
            weight = 2
    d = d * 10
    dv = d % 11
    if dv in (0, 1, 10):
        return '1'
    return str(dv)


def calc_fator_vencimento_353(venc_date):
    a = datetime.strptime('07101997', '%d%m%Y').date()
    b = venc_date
    delta = b - a
    return int(delta.days)


def decimal_format(value):
    if not isinstance(value, Decimal):
        raise Exception

    aux = unicode(value).replace('.', '')
    chars_faltantes = 13 - len(aux)
    _valor = (u'0' * chars_faltantes) + aux
    return _valor
