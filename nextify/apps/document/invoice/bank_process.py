# -*- coding: utf-8 -*-
from pycnab.interface import CNABInterface, CNABLine
from datetime import datetime
from decimal import Decimal


class BankServices(object):

    def __init__(self):
        self.cnab = CNABInterface()
        self.company = None
        self.bank_account = None
        self.invoice = None
        self.duplicate = None
        self.bank_file = None

    def process(self):
        if self.invoice is not None:
            self.company = self.invoice.company
            self.bank_account = self.invoice.bank_account
            self.set_header_data()
            self.process_invoice()
        elif self.duplicate is not None:
            self.company = self.duplicate.invoice.company
            self.bank_account = self.duplicate.invoice.bank_account
            self.set_header_data()
            self.proccess_duplicate()
        elif self.bank_file is not None:
            self.company = self.bank_file.company
            self.bank_account = self.bank_file.bank_account
            self.set_header_data()
            self.proccess_bankfile()
        else:
            raise Exception

    def set_header_data(self):
        company = self.company
        bank_account = self.bank_account
        self.cnab.nome_beneficiario = company.name
        self.cnab.cnpj_cpf_beneficiario = str(company.document_number)
        self.cnab.cod_agencia_beneficiario = bank_account.bank_agency
        self.cnab.conta_movimento_beneficiario = bank_account.bank_recipient_account
        self.cnab.conta_cobranca_beneficiario = bank_account.bank_account
        self.cnab.cod_transmissao = bank_account.cnab_transmission_code

    def process_invoice(self):
        duplicates = self.invoice.duplicates.all()
        for duplicate in duplicates:
            self.proccess_duplicate(duplicate)

    def proccess_bankfile(self):
        ocurrences = self.bank_file.ocurrences.all()

        for ocurrence in ocurrences:
            self.proccess_duplicate(ocurrence.duplicate)

    def proccess_duplicate(self, duplicate=None):
        d = None
        bank_account = self.bank_account
        if duplicate is None:
            d = self.duplicate
        else:
            d = duplicate

        address = None
        if d.invoice.person.has_customer():
            customer = d.invoice.person.customer
            address = customer.billing_address()
        line = CNABLine()
        line.num_controle_participante_p1 = d.entity.id
        line.num_controle_participante_p2 = d.company.id
        line.num_controle_participante_p3 = d.id
        line.nosso_numero = d.bank_number
        line.nosso_numero_dv = d.bank_number_dv
        line.data_segundo_desconto = '000000'
        line.seu_numero = d.invoice.invoice_number
        line.seu_numero_seq = d.sequence
        line.data_venc_titulo = d.invoice_due_date
        line.vlr_titulo = d.net_value
        line.cod_agencia_cobradora = bank_account.bank_agency
        line.data_emissao_titulo = d.invoice_date
        line.tipo_inscricao_pagador = 1
        line.cnpj_cpf_pagador = int(d.invoice.person.document_number)
        line.nome_pagador = d.invoice.person.name
        line.endereco_pagador = '%s,%s - %s' % (address.street, address.number, address.complement)
        line.bairro_pagador = address.province
        line.cep_pagador = address.zipcode
        line.municipio_pagador = address.city_name
        line.estado_pagador = address.state_acronym
        line.num_dias_protesto = 5
        line.boleto_aceite = u'NÃO'
        line.boleto_especie = u'REAL'
        line.boleto_carteira = u'RCR'
        line.boleto_especie_doc = u'DM'
        line.boleto_codigo_barras = d.barcode
        line.boleto_linha_digitavel = d.digitable_line

        self.cnab.add_boleto(line)

    def gerar_boleto(self):
        return self.cnab.return_boletos()

    def salvar_boletos(self, filename):
        return self.cnab.salvar_boletos(filename)

    def generate_bankfile(self):
        self.cnab.process_file()
        return self.cnab.txt
