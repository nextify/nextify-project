# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse
from django.views.generic import RedirectView, DetailView
from django.http import HttpResponseRedirect, HttpResponse
from nextify.apps.document.invoice.models import Invoice, InvoiceDuplicate, BankFile
from nextify.apps.document.invoice import forms

from nextify.core.views.datatables import BaseDatatableView
from nextify.core.views.generic import ObjectLookupView


class InvoiceListViewJson(BaseDatatableView):
    columns = ['id', 'bank', 'invoice_number', 'net_value', 'child', 'edit']
    order_columns = ['id', 'bank', 'invoice_number', 'net_value']
    model = Invoice

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def render_column(self, row, column):
        """ Renders a column on a row
        """
        if column == 'child':
            text = 'Boletos'
            return text
        if column == 'child':
            return '<a href="%s" class="btn btn-sm btn-default btn-circle btn-editable"><i class="fa fa-pencil">' \
                   '</i> %s</a>' % (row.get_absolute_url(), "Listar Parcelas")

        return super(InvoiceListViewJson, self).render_column(row, column)


class InvoiceCreateView(SuccessMessageMixin, CreateView):
    model = Invoice
    form_class = forms.InvoiceForm
    success_message = 'Fatura cadastrada com sucesso.'
    template_name = 'document/invoice/invoice_form.html'

    valid_dict = {
        'form': 'active',
        'formset_duplicates': ''
    }

    def get_success_url(self):
        return reverse('invoice:update', kwargs={'pk': self.object.pk, })

    def get_context_data(self, **kwargs):
        context = super(InvoiceCreateView, self).get_context_data(**kwargs)
        context['active'] = self.valid_dict
        return context


class InvoiceUpdateView(SuccessMessageMixin, UpdateView):
    model = Invoice
    form_class = forms.InvoiceForm
    success_message = 'Fatura atualizada com sucesso.'
    template_name = 'document/invoice/invoice_form.html'
    formset_invoiceduplicates = forms.InvoiceDuplicateFormSet

    valid_dict = {
        'form': 'active',
        'formset_duplicates': ''
    }

    def get_success_url(self):
        return reverse('invoice:update', kwargs={'pk': self.object.pk, })

    def get_context_data(self, **kwargs):
        context = super(InvoiceUpdateView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['form'] = self.form_class(self.request.POST, instance=self.object)
            context['formset_duplicates'] = self.formset_invoiceduplicates(self.request.POST, instance=self.object)
            context['invoiceduplicates_helper'] = forms.InvoiceDuplicateFormInlineHelper()
        else:
            self.valid_dict['form'] = 'active'
            self.valid_dict['formset_duplicates'] = ''
            context['form'] = self.form_class(instance=self.object)
            context['formset_duplicates'] = self.formset_invoiceduplicates(instance=self.object)
            context['invoiceduplicates_helper'] = forms.InvoiceDuplicateFormInlineHelper()
        context['active'] = self.valid_dict
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_duplicates = self.formset_invoiceduplicates(self.request.POST, instance=self.object)
        if self.valid_forms(form, formset_duplicates):
            return self.form_valid(form, formset_duplicates)
        else:
            return self.form_invalid(form, formset_duplicates)

    def form_valid(self, form, formset_duplicates):
        self.object = form.save()
        formset_duplicates.instance = self.object
        formset_duplicates.save()
        messages.add_message(self.request, messages.SUCCESS, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, formset_duplicates):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset_duplicates=formset_duplicates,
                                  invoiceduplicates_helper=forms.InvoiceDuplicateFormInlineHelper(),
                                  )
        )

    def valid_forms(self, form, formset_duplicates):
        self.valid_dict['form'] = ''
        self.valid_dict['formset_duplicates'] = ''
        if not form.is_valid():
            self.valid_dict['form'] = 'active'
        elif not formset_duplicates.is_valid():
            self.valid_dict['formset_duplicates'] = 'active'
        else:
            self.valid_dict['form'] = 'active'
            return True
        return False


class InvoiceList(ListView):
    model = Invoice
    template_name = 'document/invoice/invoice_list.html'


class BoletoPDF(DetailView):
    model = InvoiceDuplicate

    def render_to_response(self, context, **response_kwargs):
        from .bank_process import BankServices

        pk = self.kwargs.get('pk')
        duplicate = InvoiceDuplicate.on_company.get(pk=pk)
        b = BankServices()
        b.duplicate = duplicate
        b.process()
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % 'boleto'
        response.content = b.gerar_boleto()

        return response


class BoletoPDFAll(DetailView):
    model = Invoice

    def render_to_response(self, context, **response_kwargs):
        from .bank_process import BankServices

        pk = self.kwargs.get('pk')
        invoice = Invoice.on_company.get(pk=pk)
        b = BankServices()
        b.invoice = invoice
        b.process()
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % 'boleto'
        response.content = b.gerar_boleto()

        return response

class BankFileListViewJson(BaseDatatableView):
    columns = ['id', 'file_name', 'file']
    order_columns = ['id', 'file_name']
    model = BankFile

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def render_column(self, row, column):
        """ Renders a column on a row
        """
        if column == 'file':
            return '<a href="%s" class="btn btn-sm btn-default btn-circle btn-editable"><i class="fa fa-pencil">' \
                   '</i> %s</a>' % (row.get_bankfile_url(), "Gerar Arquivo")

        return super(BankFileListViewJson, self).render_column(row, column)


class BankFileCreateView(SuccessMessageMixin, CreateView):
    model = BankFile
    form_class = forms.BankFileForm
    success_message = 'Remessa cadastrada com sucesso.'
    template_name = 'document/invoice/bankfile_form.html'

    def get_success_url(self):
        return reverse('invoice:bankfile-list')


class BankFileList(ListView):
    model = BankFile
    template_name = 'document/invoice/bankfile_list.html'


class BankFileSelectOcurrences(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        pk = self.kwargs.get('pk')
        file = BankFile.on_company.get(pk=pk)
        file.select_ocurrences_to_file()
        return reverse('invoice:bankfile-list')


class BankFileDownload(DetailView):
    model = BankFile

    def render_to_response(self, context, **response_kwargs):
        from .bank_process import BankServices

        pk = self.kwargs.get('pk')
        bank_file = BankFile.on_company.get(pk=pk)
        b = BankServices()
        b.bank_file = bank_file
        b.process()
        response = HttpResponse(content_type='text/html; charset=utf-8')
        response['Content-Disposition'] = 'attachment; filename="%s.txt"' % 'boleto'
        response.content = b.generate_bankfile()

        return response