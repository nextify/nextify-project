from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^create/$', login_required(views.InvoiceCreateView.as_view()), name='create'),
    url(r'^(?P<pk>\d+)/update/$', login_required(views.InvoiceUpdateView.as_view()), name='update'),
    url(r'^$', login_required(views.InvoiceList.as_view()), name='list'),
    url(r'^json/$', login_required(views.InvoiceListViewJson.as_view()), name='list-json'),
    url(r'^(?P<pk>[\w]+)/pdf/$', login_required(views.BoletoPDFAll.as_view()), name='invoice-pdf'),
    url(r'^(?P<pk>[\w]+)/duplicate/pdf/$', login_required(views.BoletoPDF.as_view()), name='duplicate-pdf'),

    # BankFile
    url(r'^bankfile/$', login_required(views.BankFileList.as_view()), name='bankfile-list'),
    url(r'^bankfile/create/$', login_required(views.BankFileCreateView.as_view()), name='bankfile-create'),
    url(r'^bankfile/json/$', login_required(views.BankFileListViewJson.as_view()), name='bankfile-list-json'),
    url(r'^bankfile/(?P<pk>[\w]+)/generate/$', login_required(views.BankFileSelectOcurrences.as_view()), name='bankfile-generate'),
    url(r'^bankfile/(?P<pk>[\w]+)/dowload/$', login_required(views.BankFileDownload.as_view()), name='bankfile-download'),
]
