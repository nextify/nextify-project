from .abstract_models import AbstractDocument, AbstractFiscalDocument, AbstractDocumentIssuer, \
    AbstractFiscalDocumentIssuer, AbstractFiscalDocumentReceiver, AbstractFiscalLine, AbstractFiscalDocumentInvoice,\
    AbstractFiscalDocumentInvoiceTotals, AbstractFiscalDocumentTotals, AbstractFiscalTaxLineIcmsSN, \
    AbstractFiscalTaxLineIpi, AbstractFiscalTaxLinePis, AbstractFiscalTaxLineCofins, AbstractFiscalDocumentTaxTotals, \
    AbstractDocumentComplementaryInformation, AbstractFiscalDocumentShipping, AbstractFiscalDocumentShippingVolume


class Document(AbstractDocument):
    pass


class FiscalDocument(AbstractFiscalDocument):
    pass


class DocumentIssuer(AbstractDocumentIssuer):
    pass


class FiscalDocumentIssuer(AbstractFiscalDocumentIssuer):
    pass


class FiscalDocumentReceiver(AbstractFiscalDocumentReceiver):
    pass


class FiscalLine(AbstractFiscalLine):
    pass


class FiscalDocumentInvoice(AbstractFiscalDocumentInvoice):
    pass


class FiscalDocumentInvoiceTotals(AbstractFiscalDocumentInvoiceTotals):
    pass


# Taxes
class FiscalTaxLineIcmsSN(AbstractFiscalTaxLineIcmsSN):
    pass


class FiscalTaxLineIpi(AbstractFiscalTaxLineIpi):
    pass


class FiscalTaxLinePis(AbstractFiscalTaxLinePis):
    pass


class FiscalTaxLineCofins(AbstractFiscalTaxLineCofins):
    pass


# Totals
class FiscalDocumentTotals(AbstractFiscalDocumentTotals):
    pass


class FiscalDocumentTaxTotals(AbstractFiscalDocumentTaxTotals):
    pass


class FiscalDocumentShipping(AbstractFiscalDocumentShipping):
    pass


class FiscalDocumentShippingVolume(AbstractFiscalDocumentShippingVolume):
    pass


class FiscalDocumentComplementaryInformation(AbstractDocumentComplementaryInformation):
    pass
