# -*- coding: utf-8 -*-
import arrow
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from nextify.apps.person.managers import CompanyManager
from nextify.core.models.mixins import ModelDiffMixin
from nextify.core.enum.document import *
from nextify.core.enum.person import *
from nextify.core.enum.operation import *
from nextify.core.enum.taxes import *


class AbstractBaseDocument(ModelDiffMixin, models.Model):
    document_issuer = models.ForeignKey('person.Person', null=True, blank=True, related_name='%(class)s_issuer')
    document_receiver = models.ForeignKey('person.Person', null=True, blank=True, related_name='%(class)s_receiver')
    document_type = models.IntegerField(_("Type"), choices=DocumentType.choices())
    operation = models.ForeignKey('operation.Operation', null=False)
    operation_type = models.IntegerField(_("Operation Type"), choices=OperationType.choices(), editable=False)
    issuer_type = models.IntegerField(u'Tipo Emissão', choices=OperationIssuer.choices(), editable=False)

    price_table = models.ForeignKey('pricing.PriceTable', null=True)

    document_date = models.DateTimeField(u'Data de Emissão', null=False)
    document_release_date = models.DateTimeField(_("Document Release Date"), null=True, blank=True)
    document_status = models.IntegerField(u"Document Status", choices=DocumentStatus.choices(),
                                          default=DocumentStatus.TYPING)
    document_status_date = models.DateTimeField(u"Document Status Date", blank=True, auto_now_add=True)

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.set_initial_values()
        super(AbstractBaseDocument, self).save(force_insert, force_update, using,
                                           update_fields)

    # @property
    # def third_info(self):
    #     third = None
    #     if self.issuer_type == OperationIssuer.SELF:
    #         third = self.document_receiver
    #     else:
    #         third = self.document_issuer
    #     if third is None:
    #         return u'Consumidor não Identificado'
    #     return '%s - %s' % (cnpj_cpf(third.document_number), third.name)

    def get_document_release_date_formatted(self):
        return str(arrow.get(self.document_release_date).to('Brazil/East').format('DD/MM/YYYY HH:mm:ss'))

    def set_initial_values(self):
        operation = self.operation
        if operation is not None or 'operation' in self.changed_fields:
            self.operation_type = operation.operation_type
            self.issuer_type = operation.issuer_type


class AbstractDocument(AbstractBaseDocument):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


class AbstractFiscalDocument(AbstractBaseDocument):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)
    model = models.IntegerField(u'Modelo', choices=DocumentModel.choices(), null=False)
    series = models.CharField(u'Série', max_length=6, blank=False)
    number = models.IntegerField(u'Núm. Documento', null=False, blank=True)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def set_initial_values(self):
        super(AbstractFiscalDocument, self).set_initial_values()
        operation = self.operation
        if operation is not None or 'operation' in self.changed_fields:
            self.model = operation.model

    @property
    def number_series(self):
        return '%s-%s' % (self.number, self.series)


class AbstractBaseDocumentIssuer(ModelDiffMixin, models.Model):

    document_number = models.CharField(u'CPF / CNPJ', max_length=14, blank=True)
    name = models.CharField(u'Nome', max_length=60, blank=True)
    fancy_name = models.CharField(u'Nome Fantasia', max_length=100, blank=True)

    issuer_address = models.ForeignKey('address.Address', null=True, blank=True)

    street = models.CharField(u'Logradouro', max_length=60, blank=True)
    number = models.CharField(u'Número', max_length=60, blank=True)
    complement = models.CharField(u'Complemento', max_length=60, blank=True)
    province = models.CharField(u'Bairro', max_length=60, blank=True)
    city = models.ForeignKey('location.City', null=True)
    city_ibge_code = models.CharField(_("City Code"), max_length=7, blank=True)
    city_name = models.CharField(_("City Name"), max_length=60, blank=True)
    state_ibge_code = models.CharField(_("State Code"), max_length=2, blank=True)
    state_acronym = models.CharField(_("State Acronym"), max_length=2, blank=True)
    state_name = models.CharField(_("State Name"), max_length=60, blank=True)
    zipcode = models.CharField(u'CEP', max_length=64, blank=True)
    country_bacen_code = models.CharField(_("Country Code"), max_length=4, blank=True)
    country_name = models.CharField(_("Country Name"), max_length=60, blank=True)
    phone = models.CharField(u'Telefone', max_length=14, null=True)
    state_registry = models.CharField(max_length=14, blank=True)
    state_registry_st = models.CharField(max_length=14, blank=True)
    city_registry = models.CharField(max_length=15, blank=True)
    crt = models.IntegerField(choices=PersonCRT.choices(), null=True)

    class Meta:
        abstract = True


class AbstractDocumentIssuer(AbstractBaseDocumentIssuer):

    class Meta:
        abstract = True


class AbstractFiscalDocumentIssuer(AbstractBaseDocumentIssuer):

    class Meta:
        abstract = True


class AbstractBaseDocumentReceiver(ModelDiffMixin, models.Model):

    category = models.SmallIntegerField(
        choices=PersonCategory.choices(), verbose_name=u'Tipo Pessoa', null=True)
    document_number = models.CharField(max_length=20, blank=True)
    name = models.CharField(max_length=60, blank=False)
    fancy_name = models.CharField(u'Nome Fantasia', max_length=100, blank=True)

    receiver_address = models.ForeignKey('address.Address', null=True)

    street = models.CharField(_("Street"), max_length=60, blank=False)
    number = models.CharField(_("Number"), max_length=60, blank=False)
    complement = models.CharField(_("Complement"), max_length=60, blank=True)
    province = models.CharField(_("Province"), max_length=60, blank=False)
    city = models.ForeignKey('location.City', null=True)
    city_ibge_code = models.CharField(_("City Code"), max_length=7, blank=True)
    city_name = models.CharField(_("City Name"), max_length=60, blank=True)
    state_ibge_code = models.CharField(_("State Code"), max_length=2, blank=True)
    state_acronym = models.CharField(_("State Acronym"), max_length=2, blank=True)
    state_name = models.CharField(_("State Name"), max_length=60, blank=True)
    zipcode = models.CharField(_("ZipCode"), max_length=64)
    country_bacen_code = models.CharField(_("Country Code"), max_length=4, blank=True)
    country_name = models.CharField(_("Country Name"), max_length=60, blank=True)
    phone = models.CharField(_("Phone Number"), max_length=14, null=True)
    state_registry_id = models.SmallIntegerField(
        choices=PersonIE.choices(), verbose_name=_("State Registry ID"), null=False, default=PersonIE.EXEMPT_TAXPAYER)
    state_registry = models.CharField(max_length=14, blank=True)
    city_registry = models.CharField(max_length=15, blank=True)
    suframa_registry = models.CharField(max_length=9, blank=True)
    crt = models.IntegerField(choices=PersonCRT.choices(), null=True)
    email = models.EmailField(max_length=100, blank=True)

    class Meta:
        abstract = True


class AbstractFiscalDocumentReceiver(AbstractBaseDocumentReceiver):

    class Meta:
        abstract = True


class AbstractBaseDocumentShipping(ModelDiffMixin, models.Model):
    shipping_company = models.ForeignKey('person.Person', null=True, blank=True, related_name='%(class)s_receiver')

    category = models.SmallIntegerField(
        choices=PersonCategory.choices(), verbose_name=_("Category"), null=True)

    document_number = models.CharField(u'CPF/CNPJ', max_length=20, blank=True)
    name = models.CharField(max_length=60, blank=True)
    state_registry_id = models.SmallIntegerField(
        choices=PersonIE.choices(), verbose_name=_("State Registry ID"), null=True,
        default=PersonIE.EXEMPT_TAXPAYER)
    state_registry = models.CharField(u'Inscrição Estadual', max_length=14, blank=True)

    shipping_company_address = models.ForeignKey('address.Address', null=True)

    address_sumary = models.CharField(u'Endereço Completo', max_length=60, blank=True)
    city = models.ForeignKey('location.City', null=True, blank=True)
    city_ibge_code = models.CharField(_("City Code"), max_length=7, blank=True)
    city_name = models.CharField(_("City Name"), max_length=60, blank=True)
    state_ibge_code = models.CharField(_("State Code"), max_length=2, blank=True)
    state_acronym = models.CharField(_("State Acronym"), max_length=2, blank=True)
    state_name = models.CharField(_("State Name"), max_length=60, blank=True)

    vehicle_license_plate = models.CharField(u'Placa do Veículo', max_length=11, blank=True)
    vehicle_state = models.ForeignKey('location.State', verbose_name=u'UF do Veículo', null=True, blank=True)
    vehicle_rntc = models.CharField(u'RNTC', max_length=20, blank=True)

    class Meta:
        abstract = True


class AbstractFiscalDocumentShipping(AbstractBaseDocumentShipping):
    pass

    class Meta:
        abstract = True


class AbstractBaseDocumentShippingVolume(ModelDiffMixin, models.Model):
    volume_quantity = models.IntegerField(u'Número de Volumes', null=True, blank=True)
    volume_especies = models.CharField(u'Espécie do Volume', max_length=60, blank=True)
    volume_mark = models.CharField(u'Marca do Volume', max_length=60, blank=True)
    volume_number = models.CharField(u'Numeração do Volume', max_length=60, blank=True)
    net_weight = models.DecimalField(u'Peso Líquido', max_digits=15, decimal_places=3, null=True, blank=True)
    gross_weight = models.DecimalField(u'Peso Bruto', max_digits=15, decimal_places=3, null=True, blank=True)

    class Meta:
        abstract = True


class AbstractFiscalDocumentShippingVolume(AbstractBaseDocumentShippingVolume):
    pass

    class Meta:
        abstract = True


class AbstractBaseLine(ModelDiffMixin, models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)

    product = models.ForeignKey('catalogue.Product', null=True)
    item_code = models.CharField(_("Item code"), max_length=60, blank=True)
    description = models.CharField(_("Description"), max_length=255, blank=True)
    complementary_description = models.CharField(u'Descrição Complementar', max_length=500, blank=True)
    unit = models.ForeignKey('unit.Unit', null=True)
    unit_code = models.CharField('Unit Code', blank=True, max_length=6)
    ncm_classification = models.ForeignKey('ncm.NcmClassification', null=True)
    ncm_code = models.CharField('NCM Code', max_length=8, blank=True)

    quantity = models.DecimalField(u'Quantidade',
                                   max_digits=15, decimal_places=4, null=False, default=0.00)
    unitary_value = models.DecimalField(u'Valor Unitário',
                                        max_digits=17, decimal_places=6, null=False, default=0.00)
    ean_code = models.CharField(_('EAN Code'), max_length=14, blank=True)

    goods_value = models.DecimalField(u'Valor dos Produtos',
                                      max_digits=15, decimal_places=2, null=False, blank=True, default=0.00)
    services_value = models.DecimalField(_("Services"),
                                         max_digits=15, decimal_places=2, null=False, blank=True, default=0.00)
    deductions_value = models.DecimalField(_("Deductions"),
                                           max_digits=15, decimal_places=2, null=False, default=0.00)
    discount_value = models.DecimalField(u'Valor do Desconto',
                                         max_digits=15, decimal_places=2, null=False, default=0.00)
    conditional_discount_value = models.DecimalField(_("Conditional discount"),
                                                     max_digits=15, decimal_places=2, null=False, default=0.00)
    insurance_value = models.DecimalField(u'Valor do Seguro',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    freight_value = models.DecimalField(u'Valor do Frete',
                                        max_digits=15, decimal_places=2, null=False, default=0.00)
    expenses_value = models.DecimalField(u'Outras Despesas',
                                         max_digits=15, decimal_places=2, null=False, default=0.00)

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.set_goods_value()
        super(AbstractBaseLine, self).save(force_insert, force_update, using, update_fields)

    def set_goods_value(self):
        if not self.id or \
                        'unitary_value' in self.changed_fields or \
                        'quantity' in self.changed_fields:
            from decimal import Decimal
            if self.unitary_value > 0 and self.quantity > 0:
                self.goods_value = Decimal(self.unitary_value * self.quantity).quantize(Decimal('1.00'))
            else:
                self.goods_value = Decimal('0.00')


class AbstractLine(AbstractBaseLine):

    class Meta:
        abstract = True


class AbstractFiscalLine(AbstractBaseLine):
    cfop = models.ForeignKey('cfop.Cfop', null=False)
    cfop_code = models.CharField('CFOP Code', blank=True, max_length=4)
    taxed_unit = models.ForeignKey('unit.Unit', null=True, blank=True, related_name='taxed_unit')
    taxed_unit_code = models.CharField('Unit Code', max_length=6, blank=True)
    taxed_quantity = models.DecimalField(_("Taxed Quantity"),
                                         max_digits=15, decimal_places=4, null=False, default=0.00)
    taxed_unitary_value = models.DecimalField(_("Taxed Unitary value"),
                                              max_digits=21, decimal_places=10, null=False, default=0.00)
    taxed_ean_code = models.CharField(_('Taxed EAN Code'), max_length=14, blank=True)

    taxes_value = models.DecimalField(u'Total dos Tributos', max_digits=15, decimal_places=2, null=True, default=0.00)

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.set_initial_data()
        self.set_defaults()
        super(AbstractFiscalLine, self).save(force_insert, force_update, using, update_fields)

    def set_initial_data(self):
        item = self.product
        if item is not None:
            if self.taxed_unit is None:
                self.taxed_unit = self.product.taxed_unit
        super(AbstractFiscalLine, self).set_initial_data()

    def set_defaults(self):
        cfop = self.cfop
        unit = self.taxed_unit
        if cfop is not None:
            self.cfop_code = cfop.code
        if unit is not None:
            self.taxed_unit_code = unit.code


class AbstractFiscalDocumentInvoice(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)

    invoice_number = models.CharField(u'Num. da Duplicata', max_length=60, blank=True)
    invoice_due_date = models.DateField(u'Data de Vencimento', null=True)
    original_value = models.DecimalField(u'Valor Original', max_digits=15, decimal_places=2, null=False)
    discount_value = models.DecimalField(u'Valor Desconto', max_digits=15, decimal_places=2, null=True, default=0.00)
    net_value = models.DecimalField(u'Valor Líquido', max_digits=15, decimal_places=2, null=True, default=0.00)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.calculate_totals()
        super(AbstractFiscalDocumentInvoice, self).save(force_insert, force_update, using, update_fields)

    def calculate_totals(self):
        if self.discount_value is not None:
            self.net_value = self.original_value - self.discount_value


class AbstractFiscalDocumentInvoiceTotals(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)

    invoice_number = models.CharField(u'Num. da Fatura', max_length=60, blank=True)
    original_value = models.DecimalField(u'Valor Original', max_digits=15, decimal_places=2, null=False)
    discount_value = models.DecimalField(u'Valor Desconto', max_digits=15, decimal_places=2, null=True, default=0.00)
    net_value = models.DecimalField(u'Valor Líquido', max_digits=15, decimal_places=2, null=True, default=0.00)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


class AbstractBaseTotals(models.Model):
    goods_total = models.DecimalField(_("Total Goods"),
                                      max_digits=15, decimal_places=2, null=False, default=0.00)
    services_total = models.DecimalField(_("Total Services"),
                                         max_digits=15, decimal_places=2, null=False, default=0.00)
    freight_total = models.DecimalField(_("Total Freight"),
                                        max_digits=15, decimal_places=2, null=False, default=0.00)
    insurance_total = models.DecimalField(_("Total Insurance"),
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    deductions_total = models.DecimalField(_("Total Deductions"),
                                           max_digits=15, decimal_places=2, null=False, default=0.00)
    discount_total = models.DecimalField(_("Total discount"),
                                         max_digits=15, decimal_places=2, null=False, default=0.00)
    conditional_discount_total = models.DecimalField(_("Total conditional discount"),
                                                     max_digits=15, decimal_places=2, null=False, default=0.00)
    expenses_total = models.DecimalField(_("Total Expenses"),
                                         max_digits=15, decimal_places=2, null=False, default=0.00)
    document_total = models.DecimalField(_("Total Document"),
                                         max_digits=15, decimal_places=2, null=False, default=0.00)

    class Meta:
        abstract = True


class AbstractBaseTaxTotals(models.Model):
    icms_base_total = models.DecimalField(u'Base de ICMS',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    icms_value_total = models.DecimalField(u'Valor de ICMS',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    icmsst_base_total = models.DecimalField(u'Base de ICMS ST',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    icmsst_value_total = models.DecimalField(u'Valor de ICMS ST',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    ipi_base_total = models.DecimalField(u'Base de IPI',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    ipi_value_total = models.DecimalField(u'Valor de IPI',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    pis_base_total = models.DecimalField(u'Base de PIS',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    pis_value_total = models.DecimalField(u'Valor de PIS',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    cofins_base_total = models.DecimalField(u'Base de COFINS',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    cofins_value_total = models.DecimalField(u'Valor de COFINS',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    iss_base_total = models.DecimalField(u'Base de ISS',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    iss_value_total = models.DecimalField(u'Valor de ISS',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    ii_base_total = models.DecimalField(u'II Base',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)
    ii_value_total = models.DecimalField(u'II Value',
                                          max_digits=15, decimal_places=2, null=False, default=0.00)

    class Meta:
        abstract = True


class AbstractFiscalDocumentTaxTotals(AbstractBaseTaxTotals):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


class AbstractBaseTaxLine(ModelDiffMixin, models.Model):
    tax_base_value = models.DecimalField(u'Base de Cálculo', max_digits=15, decimal_places=2, null=False, default=0.00)
    tax_rate = models.DecimalField(u'Alíquota', max_digits=7, decimal_places=4, null=False, default=0.00)
    tax_value = models.DecimalField(u'Valor do Imposto', max_digits=15, decimal_places=2, null=False, default=0.00)

    class Meta:
        abstract = True




class AbstractFiscalTaxLine(AbstractBaseTaxLine):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

class AbstractFiscalTaxLineIcmsSN(AbstractFiscalTaxLine):
    csosn = models.IntegerField('CSOSN', null=False, choices=TaxIcmsSNType.choices())
    tax_base_st_value = models.DecimalField(u'Base de Cálculo ST', max_digits=15, decimal_places=2, null=False, default=0.00)
    tax_base_mva = models.DecimalField(u'M.V.A.', max_digits=7, decimal_places=4, null=False, default=0.0000)
    tax_rate_st = models.DecimalField(u'Alíquota', max_digits=7, decimal_places=4, null=False, default=0.0000)
    tax_value_st = models.DecimalField(u'Valor do Imposto', max_digits=15, decimal_places=2, null=False, default=0.00)

    class Meta:
        abstract = True


class AbstractFiscalTaxLineIpi(AbstractFiscalTaxLine):
    cst = models.CharField('CST', blank=False, max_length=2, choices=TaxIpiType.choices())

    class Meta:
        abstract = True


class AbstractFiscalTaxLinePis(AbstractFiscalTaxLine):
    cst = models.CharField('CST', blank=False, max_length=2, choices=TaxPisType.choices())

    class Meta:
        abstract = True


class AbstractFiscalTaxLineCofins(AbstractFiscalTaxLine):
    cst = models.CharField('CST', blank=False, max_length=2, choices=TaxCofinsType.choices())

    class Meta:
        abstract = True
#
# class AbstractTaxLineIbpt(models.Model):
#     ibpt = models.ForeignKey('taxes.Ibpt', null=True)
#
#     class Meta:
#         abstract = True
#
#
# class AbstractTaxLineRetained(models.Model):
#
#     class Meta:
#         abstract = True


class AbstractFiscalDocumentTotals(AbstractBaseTotals):
    pass

    class Meta:
        abstract = True


class AbstractDocumentComplementaryInformation(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)

    tax_additional_information = models.TextField(u'Informações Complementares de Interesse do Fisco',
                                                  max_length=2000,
                                                  blank=True)
    additional_information = models.TextField(u'Informações Complementares de Interesse do Contribuinte',
                                              max_length=5000,
                                              blank=True)
    additional_information_auto = models.TextField(u'Informações Complementares (geradas automaticamente)',
                                                   max_length=5000,
                                                   blank=True)

    on_company = CompanyManager()
    objects = models.Manager()
    
    class Meta:
        abstract = True
