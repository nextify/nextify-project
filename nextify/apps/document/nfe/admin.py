from django.contrib import admin
from .models import *

class NFeLineInLine(admin.TabularInline):
    model = NFeLine


class NFeAdmin(admin.ModelAdmin):
    inlines = [
        NFeLineInLine,
    ]


admin.site.register(NFe, NFeAdmin)

