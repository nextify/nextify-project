from django.core.management.base import BaseCommand, CommandError
import os
from os.path import abspath, dirname
from datetime import datetime
from pysped.nfe import ProcessadorNFe
from pysped.nfe.webservices_flags import (UF_CODIGO,
                                          WS_NFE_CONSULTA_RECIBO)
import django
from django.conf import settings

from nextify.apps.document.nfe.models import NFe
from nextify.apps.document.nfe.serializer import NfeSerializer

from pysped.nfe.leiaute import NFe_310, ProtNFe_310
from pysped.nfe.danfe import DANFE

class Command(BaseCommand):

    def handle(self, *args, **options):
        proc = ProcessadorNFe()
        proc.versao = '3.10'
        proc.estado = 'PR'
        proc.certificado.arquivo = '10393451.pfx'
        proc.certificado.senha = '123456'
        proc.ambiente = 2

        proc.salva_arquivos = True
        proc.contingencia_SCAN = False
        proc.caminho = ''

        #
        # Instancia uma NF-e
        #
        nfe = NFe.on_company.get(pk=3)
        s = NfeSerializer(nfe=nfe)

        n = s.nfe_310
        n.gera_nova_chave()
        print(n.xml.encode('utf-8'))
        print(n.validar())


        danfe = DANFE()
        danfe.NFe = n
        danfe.salvar_arquivo = True
        danfe.gerar_danfe()

