# -*- coding: utf-8 -*-
import pytz
import sendgrid
from datetime import datetime
from django.conf import settings
from django.apps import apps

# from pysped.nfe.NFeProcessor import ProcessadorNFe
from nextify.apps.document.nfe.serializer import NfeSerializer
from nextify.apps.document.invoice.models import Invoice
from nextify.apps.document.invoice.bank_process import BankServices


def webservice_status_query(webservice, model=55):
    m = apps.get_model('nfe', 'NFeWebserviceStatus')
    exists = True
    try:
        o = m.objects.get(webservice=webservice, environment=settings.NFE_AMBIENTE, model=model)
    except m.DoesNotExist:
        exists = False

    if exists:
        start = o.query_date
        end = datetime.utcnow().replace(tzinfo=pytz.UTC)
        if getTimeDifferenceFromNow(start, end) < 15:
            date = o.query_date
            status_dict = {
                'status': o.status.code,
                'reason': o.status_description,
                'date': date
            }
            return status_dict

    # p = ProcessadorNFe()
    # p.versao = '3.10'
    # p.estado = webservice
    # p.ambiente = settings.NFE_AMBIENTE
    # p.certificado.arquivo = settings.CERT_PATH + '13073206/' + settings.CERT_FILE
    # p.certificado.senha = settings.CERT_PASSWORD
    # processo = p.consultar_servico()
    #
    # query_date = processo.resposta.dhRecbto.valor

    # if not exists:
    #     o = m(webservice=webservice, environment=settings.NFE_AMBIENTE, model=model,
    #           status_id=processo.resposta.cStat.valor,
    #           status_description=processo.resposta.xMotivo.valor,
    #           query_date=query_date)
    #     o.save()
    # else:
    #     o.status_id = processo.resposta.cStat.valor
    #     o.status_description = processo.resposta.xMotivo.valor
    #     o.query_date = query_date
    #     o.save()
    #
    # status_dict = {
    #     'status': o.status.code,
    #     'reason': o.status_description,
    #     'date': o.query_date
    # }
    # return status_dict


def query_nfe_by_receipt():
    m = apps.get_model('nfe', 'NFe')

    nfe_list = m.on_company.filter(nfe_status_id__in=[3, 103, 104, 105])

    # for nfe in nfe_list:
    #     p = ProcessadorNFe()
    #     p.versao = '3.10'
    #     p.estado = 'PR'
    #     p.ambiente = settings.NFE_AMBIENTE
    #     p.certificado.arquivo = settings.CERT_PATH + '13073206/' + settings.CERT_FILE
    #     p.certificado.senha = settings.CERT_PASSWORD
    #     p.salvar_arquivos = True
    #     s = NfeSerializer(nfe=nfe)
    #     n = s.nfe_310
    #     proc_recibo = p.process_nfe_query_receipt([n], nfe.nfe_receipt_number)
    #     if proc_recibo.resposta.cStat.valor == '104':
    #         dic = proc_recibo.resposta.dic_protNFe[nfe.nfe_access_key]
    #         nfe.nfe_status_id = dic.infProt.cStat.valor
    #         nfe.nfe_status_description = dic.infProt.cStat.valor
    #         nfe.nfe_authorization_protocol = dic.infProt.nProt.valor
    #         nfe.nfe_authorization_date = dic.infProt.dhRecbto.valor
    #         nfe.save()
    #         if dic.infProt.cStat.valor in ['100', '101', ]:
    #             insert_nfe_mail_queue(nfe=nfe)


def query_nfe_by_access_key(nfe):
    pass
    # p = ProcessadorNFe()
    # p.versao = '3.10'
    # p.estado = 'PR'
    # p.ambiente = settings.NFE_AMBIENTE
    # p.certificado.arquivo = settings.CERT_PATH + '13073206/' + settings.CERT_FILE
    # p.certificado.senha = settings.CERT_PASSWORD
    # p.salvar_arquivos = True
    # s = NfeSerializer(nfe=nfe)
    # n = s.nfe_310
    # proc = p.consultar_nota(ambiente=settings.NFE_AMBIENTE, chave_nfe=nfe.nfe_access_key, nfe=n)
    # print(proc.resposta.cStat.valor)
    # nfe.nfe_status_id = proc.resposta.cStat.valor
    # nfe.nfe_status_description = proc.resposta.infProt.xMotivo.valor
    # nfe.save()


def cancel_nfe(nfe_event):
    nfe = nfe_event.nfe

    try:
        q = apps.get_model('nfe', 'NFeQueue')

        o = q(nfe=nfe.id, operacao=7, status=0, chave_acesso=nfe.nfe_access_key,
              cnpj=nfe.issuer.document_number, protocolo=nfe.nfe_authorization_protocol,
              modelo=nfe.model, justificativa=nfe_event.message, evento=nfe_event.id,
              sequencia_evento=nfe_event.sequence
              )
        o.save()
        return True
    except:
        return False


def nfe_list():
    m = apps.get_model('nfe', 'NFe')
    nfe_list = m.on_company.filter(nfe_status_id=2)


def send_nfe():
    m = apps.get_model('nfe', 'NFe')

    nfe_list = m.on_company.filter(nfe_status_id=2)

    for nfe in nfe_list:
        q = apps.get_model('nfe', 'NFeQueue')

        s = NfeSerializer(nfe=nfe)
        nfe.nfe_access_key = s.nfe_400.chave

        o = q(nfe=nfe.id, operacao=0, status=0, chave_acesso=nfe.nfe_access_key,
              cnpj=nfe.issuer.document_number,
              modelo=nfe.model
              )
        o.save()

        nfe.nfe_status_id = 3
        nfe.save()


# def send_nfe():
#     m = apps.get_model('nfe', 'NFe')
#
#     nfe_list = m.on_company.filter(nfe_status_id=2)
#
#     for nfe in nfe_list:
#         p = ProcessadorNFe()
#         p.versao = '3.10'
#         p.estado = 'PR'
#         p.ambiente = settings.NFE_AMBIENTE
#         p.certificado.arquivo = settings.CERT_PATH + '13073206/' + settings.CERT_FILE
#         p.certificado.senha = settings.CERT_PASSWORD
#         p.salvar_arquivos = True
#         s = NfeSerializer(nfe=nfe)
#         n = s.nfe_310
#         nfe_batch_number, ret_envi_nfe = p.process_nfe_send([n])
#         nfe.nfe_access_key = n.chave
#         nfe.nfe_batch_number = nfe_batch_number
#         if ret_envi_nfe:
#             nfe.nfe_receipt_number = ret_envi_nfe.infRec.nRec.valor
#             nfe.nfe_status_id = ret_envi_nfe.cStat.valor
#             nfe.nfe_status_description = ret_envi_nfe.cStat.valor
#         nfe.save()
def generate_invoice_pdf(nfe_id):

    try:
        invoice = Invoice.on_company.get(document_id=nfe_id)
    except Invoice.DoesNotExist:
        return False
    b = BankServices()
    b.invoice = invoice
    b.process()
    b.salvar_boletos('/home/nextify/teste_nfe_invoice.pdf')
    #b.salvar_boletos('/home/carlos/teste_nfe_invoice.pdf')


    return True

def send_mail():
    m = apps.get_model('nfe', 'NFeSendMailQueue')
    mail_list = m.on_company.filter(mail_sent=False, attempt__lte=settings.MAIL_MAX_RETRIES)

    for mail in mail_list:
        sg = sendgrid.SendGridClient(settings.SENDGRID_API_KEY)
        message = sendgrid.Mail()
        message.add_to(mail.mail_to)
        if settings.MAIL_CCO != '':
            message.add_bcc(settings.MAIL_CCO)
        message.set_from(mail.mail_from)
        message.set_subject(mail.subject)
        message.set_html(mail.message)

        if mail.nfe_pdf_path:
            message.add_attachment('%s.pdf' % mail.nfe.nfe_access_key, open(mail.nfe_pdf_path, 'rb'))
        if mail.nfe_xml_path:
            message.add_attachment('%s.xml' % mail.nfe.nfe_access_key, open(mail.nfe_xml_path, 'rb'))

        if generate_invoice_pdf(mail.nfe_id):
            #message.add_attachment('boleto.pdf', open('/home/carlos/teste_nfe_invoice.pdf', 'rb'))
            message.add_attachment('boleto.pdf', open('/home/nextify/teste_nfe_invoice.pdf', 'rb'))

        try:
            status, msg = sg.send(message)
        except:
            continue

        mail.attempt = mail.attempt + 1
        mail.return_status = status
        mail.return_message = msg
        mail.mail_sent = True
        mail.save()


def insert_nfe_mail_queue(nfe):
    s = NfeSerializer(nfe=nfe)
    m = apps.get_model('nfe', 'NFeSendMailQueue')
    if settings.NFE_AMBIENTE == 2:
        mail_to = settings.MAIL_TO_HOMOLOG
    else:
        mail_to = s.nfe_310.infNFe.dest.email.valor

    pdf_path = ''
    if s.pdf_path is not None:
        pdf_path = s.pdf_path
    xml_path = ''
    if s.xml_path is not None:
        xml_path = s.xml_path

    if mail_to not in ['', None]:
        o = m(nfe=nfe, mail_to=mail_to, mail_from=settings.MAIL_FROM,
              message=settings.MAIL_MESSAGE, subject=settings.MAIL_SUBJECT,
              nfe_pdf_path=pdf_path, nfe_xml_path=xml_path)
        o.save()




def getTimeDifferenceFromNow(start_time, end_time):
    time_diff = end_time - start_time

    return time_diff.total_seconds() / 60
