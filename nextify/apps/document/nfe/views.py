# -*- coding: utf-8 -*-
import pytz
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import UpdateView
from django.views.generic import RedirectView, TemplateView, CreateView, DetailView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.apps import apps
from django.contrib import messages
from django.conf import settings

from nextify.core.views.datatables import BaseDatatableView
from nextify.core.enum.document import DocumentStatus
from nextify.core.enum.nfe import NFeEventType
from nextify.core.enum.operation import OperationType, OperationIssuer

from .models import NFe, NFeLine, NFeEvent
from . import forms
from .webservice import webservice_status_query, \
    query_nfe_by_receipt, send_mail, \
    query_nfe_by_access_key, send_nfe, cancel_nfe


class NFeIssue(TemplateView):
    template_name = 'document/nfe/nfe_create.html'
    nfeoperation_form_class = forms.NFeOperationSelectForm

    def get_context_data(self, **kwargs):
        ctx = super(NFeIssue, self).get_context_data(**kwargs)
        ctx['nfeoperation_form'] = self.nfeoperation_form_class()

        return ctx


class NFeSendQueryUpdate(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        # query_nfe_by_receipt()
        send_nfe()
        # send_mail()
        return reverse('nfe:list-self')

    def query_nfe(self):
        pass


class NFePdfDANFE(DetailView):
    model = NFe

    def render_to_response(self, context, **response_kwargs):
        from nextify.apps.document.nfe.serializer import NfeSerializer

        nfe_pk = self.kwargs.get('pk')
        nfe = NFe.on_company.get(pk=nfe_pk)
        s = NfeSerializer(nfe=nfe)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % s.access_key
        response.content = s.get_danfe_content()

        return response


class NFeCreateRedirect(RedirectView):
    permanent = False
    operation_form_class = forms.NFeOperationSelectForm

    def get_nfe_create_url(self, operation):
        if operation.operation_type == OperationType.IN:
            return reverse('nfe:in-create', kwargs={'operation_pk': operation.pk})
        elif operation.operation_type == OperationType.OUT:
            if operation.model == 55:
                return reverse('nfe:out-create', kwargs={'operation_pk': operation.pk})
            elif operation.model == 65:
                return reverse('nfce:out-create', kwargs={'operation_pk': operation.pk})

    def get_invalid_nfe_operation_url(self):
        return reverse('nfe:issue')

    def get_redirect_url(self, **kwargs):
        form = self.operation_form_class(self.request.POST)
        if form.is_valid():
            operation = form.cleaned_data['operation']
            return self.get_nfe_create_url(operation)
        else:
            return self.get_invalid_nfe_operation_url()


class NFeCreate(CreateView):
    form_class = forms.NFeThirdInCreate
    template_name = 'document/nfe/nfe_form.html'

    valid_dict = {
        'form': 'active',
        'formset_lines': '',
        'formset_invoices': '',
        'form_person': '',
    }

    def get_context_data(self, **kwargs):
        context = super(NFeCreate, self).get_context_data(**kwargs)
        context['operation_pk'] = self.kwargs.get('operation_pk')
        context['active'] = self.valid_dict
        return context

    def get_initial(self):
        initials = super(NFeCreate, self).get_initial()
        operation_pk = self.kwargs.get('operation_pk')
        initials['operation'] = operation_pk
        return initials


class NFeInCreate(NFeCreate):
    form_class = forms.NFeThirdInCreate


class NFeOutCreate(NFeCreate):
    form_class = forms.NFeOutCreate

    def get_initial(self):
        initials = super(NFeCreate, self).get_initial()
        operation_pk = self.kwargs.get('operation_pk')
        model = apps.get_model('operation', 'Operation')
        try:
            o = model.on_company.get(pk=operation_pk)
        except model.DoesNotExist:
            pass
        else:
            initials['series'] = '0'
            initials['number'] = '0'
        initials['operation'] = operation_pk
        return initials


class NFeEventCreateRedirect(RedirectView):
    permanent = False
    form_class = forms.NFeEventForm

    def get_nfe_event_create_url(self, nfe_id, event_type):
        model = apps.get_model('nfe', 'NFeEvent')

        try:
            o = model.on_company.get(nfe_id=nfe_id, event_type=event_type, event_status_id=12)
        except model.DoesNotExist:
            if event_type == NFeEventType.CANCEL_EVENT:
                return reverse('nfe:cancel', kwargs={'nfe_pk': nfe_id, })
            else:
                return reverse('nfe:cce', kwargs={'nfe_pk': nfe_id, })
        else:
            if event_type == NFeEventType.CANCEL_EVENT:
                return reverse('nfe:cancel-confirm', kwargs={'pk': o.id, })
            else:
                return reverse('nfe:cce-confirm', kwargs={'pk': o.id, })

    def get_invalid_nfe_event_url(self, nfe_id):
        return reverse('nfe:out-update', kwargs={'pk': nfe_id, })

    def get_redirect_url(self, **kwargs):
        form = self.form_class(self.request.POST)
        if form.is_valid():
            nfe_id = form.cleaned_data['nfe_id']
            if 'cancel' in self.request.POST:
                event_type = NFeEventType.CANCEL_EVENT
            if 'cce' in self.request.POST:
                event_type = NFeEventType.CCE_EVENT
            return self.get_nfe_event_create_url(nfe_id, event_type)
        else:
            nfe_id = form.cleaned_data['nfe_id']
            messages.add_message(self.request, messages.ERROR, u'Ocorreu um erro ao tentar criar um evento.')
            return self.get_invalid_nfe_event_url(nfe_id)


class NFeEventCreate(CreateView):
    model = NFeEvent
    form_class = forms.NFeCancelForm
    template_name = 'document/nfe/nfeevent_form.html'

    def get_context_data(self, **kwargs):
        context = super(NFeEventCreate, self).get_context_data(**kwargs)
        context['nfe_pk'] = self.kwargs.get('nfe_pk')
        return context


class NFeEventUpdate(UpdateView):
    model = NFeEvent
    form_class = forms.NFeCancelForm
    template_name = 'document/nfe/nfeeventconfirm_form.html'


class NFeCancelCreate(NFeEventCreate):
    model = NFeEvent
    form_class = forms.NFeCancelForm
    template_name = 'document/nfe/nfeevent_form.html'

    def get_success_url(self):
        return reverse('nfe:cancel-confirm', kwargs={'pk': self.object.pk, })

    def form_valid(self, form):
        nfe_event = form.save(commit=False)
        nfe_event.nfe_id = self.kwargs['nfe_pk']
        nfe_event.event_type = NFeEventType.CANCEL_EVENT
        return super(NFeCancelCreate, self).form_valid(form)
    
    def get_context_data(self, **kwargs):
        context = super(NFeCancelCreate, self).get_context_data(**kwargs)
        nfe_pk = self.kwargs['nfe_pk']
        nfe = NFe.on_company.get(pk=nfe_pk)
        context['nfe'] = nfe
        return context


class NFeCancelUpdate(NFeEventUpdate):
    model = NFeEvent
    form_class = forms.NFeCancelConfirmForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            if cancel_nfe(self.object):
                messages.add_message(self.request, messages.SUCCESS, u'A NFe foi cancelada com Sucesso.')
            else:
                messages.add_message(self.request, messages.ERROR, u'Ocorreu um problema ao cancelar a NFe.')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('nfe:cancel-confirm', kwargs={'pk': self.object.pk, })

    def get_context_data(self, **kwargs):
        context = super(NFeCancelUpdate, self).get_context_data(**kwargs)
        nfe_pk = self.object.nfe_id
        nfe = NFe.on_company.get(pk=nfe_pk)
        context['nfe'] = nfe
        return context



class NFeCceCreate(NFeEventCreate):
    model = NFeEvent
    form_class = forms.NFeCancelForm
    template_name = 'document/nfe/nfeevent_form.html'

    def get_success_url(self):
        return reverse('nfe:cancel-confirm', kwargs={'pk': self.object.pk, })


class NFeCceUpdate(NFeEventUpdate):
    model = NFeEvent
    form_class = forms.NFeCancelForm
    template_name = 'document/nfe/nfeevent_form.html'


class NFeUpdateFinalize(UpdateView):
    model = NFe
    form_class = forms.NFeUpdateFinalize

    def get_success_url(self):
        nfe = self.object
        if nfe.issuer_type == OperationIssuer.THIRD:
            return reverse('nfe:in-update', kwargs={'pk': self.object.pk, })
        else:
            return reverse('nfe:out-update', kwargs={'pk': self.object.pk, })

    def form_valid(self, form):
        instance = form.save(commit=False)
        if 'finalize' in self.request.POST:
            instance.finalize()
        if 'reopen' in self.request.POST:
            instance.reopen()
        return super(NFeUpdateFinalize, self).form_valid(form)


class NFeUpdate(UpdateView):
    model = NFe
    form_class = None
    form_lines = None
    form_invoices = None
    form_person = None
    formset_shipping = None
    form_complementary_information = None
    formset_volumes = None
    template_name = 'document/nfe/nfe_form.html'
    success_message = 'NFe atualizada com sucesso!'

    valid_dict = {
        'form': 'active',
        'formset_lines': '',
        'formset_invoices': '',
        'formset_volumes': '',
        'formset_shipping': '',
        'form_person': '',
        'form_complementary_information': '',
    }

    def get_context_data(self, **kwargs):
        context = super(NFeUpdate, self).get_context_data(**kwargs)
        context['nfe_pk'] = self.object.pk
        if self.request.POST:
            context['form'] = self.form_class(self.request.POST, instance=self.object)
            context['form_lines'] = self.form_lines(self.request.POST, instance=self.object)
            context['form_invoices'] = self.form_invoices(self.request.POST, instance=self.object)
            context['form_person'] = self.form_person(self.request.POST, instance=self.object)
            context['formset_volumes'] = self.formset_volumes(self.request.POST, instance=self.object)
            if self.form_complementary_information is not None:
                context['form_complementary_information'] = self.form_complementary_information(self.request.POST,
                                                                                                instance=self.object)
            if self.formset_shipping is not None:
                context['formset_shipping'] = self.formset_shipping(self.request.POST, instance=self.object)

            if self.formset_volumes is not None:
                context['formset_volumes'] = self.formset_volumes(self.request.POST, instance=self.object)
        else:
            self.valid_dict['form'] = 'active'
            self.valid_dict['formset_lines'] = ''
            self.valid_dict['formset_invoices'] = ''
            self.valid_dict['formset_shipping'] = ''
            self.valid_dict['formset_volumes'] = ''
            self.valid_dict['form_person'] = ''
            self.valid_dict['form_complementary_information'] = ''
            context['form'] = self.form_class(instance=self.object)
            context['form_lines'] = self.form_lines(instance=self.object)
            context['form_invoices'] = self.form_invoices(instance=self.object)
            context['form_person'] = self.form_person(instance=self.object)
            if self.form_complementary_information is not None:
                context['form_complementary_information'] = self.form_complementary_information(instance=self.object)
            if self.formset_shipping is not None:
                context['formset_shipping'] = self.formset_shipping(instance=self.object)
            if self.formset_volumes is not None:
                context['formset_volumes'] = self.formset_volumes(instance=self.object)

        context['active'] = self.valid_dict
        context['lines_helper'] = forms.NFeLineInlineHelper()
        context['invoices_helper'] = forms.NFeInvoiceInlineHelper()
        context['volumes_helper'] = forms.NFeShippingVolumeInlineHelper()
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form_lines = self.form_lines(self.request.POST, instance=self.object)
        form_invoices = self.form_invoices(self.request.POST, instance=self.object)
        form_person = self.form_person(self.request.POST, instance=self.object)
        form_complementary_information = None
        formset_shipping = None
        formset_volumes = None
        if self.form_complementary_information is not None:
            form_complementary_information = self.form_complementary_information(self.request.POST, instance=self.object)

        if self.formset_shipping is not None:
            formset_shipping = self.formset_shipping(self.request.POST, instance=self.object)

        if self.formset_volumes is not None:
            formset_volumes = self.formset_volumes(self.request.POST, instance=self.object)

        if self.valid_forms(form=form, formset_lines=form_lines,
                            form_person=form_person, formset_invoices=form_invoices,
                            form_complementary_information=form_complementary_information,
                            formset_shipping=formset_shipping,
                            formset_volumes=formset_volumes):
            return self.form_valid(form, form_lines, form_person, form_invoices,
                                   form_complementary_information,
                                   formset_shipping,
                                   formset_volumes)
        else:
            return self.form_invalid(form, form_lines, form_person, form_invoices,
                                     form_complementary_information,
                                     formset_shipping,
                                     formset_volumes)

    def form_valid(self, form, form_lines, form_person, form_invoices,
                   form_complementary_information=None,
                   formset_shipping=None,
                   formset_volumes=None):
        self.object = form.save()
        form_lines.instance = self.object
        form_lines.save()
        form_person.save()
        form_invoices.save()
        if form_complementary_information is not None:
            form_complementary_information.save()
        if formset_shipping is not None:
            formset_shipping.save()
        if formset_volumes is not None:
            formset_volumes.save()
        messages.add_message(self.request, messages.SUCCESS, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, form_lines, form_person, form_invoices,
                     form_complementary_information=None,
                     formset_shipping=None,
                     formset_volumes=None):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  form_lines=form_lines,
                                  form_person=form_person,
                                  form_invoices=form_invoices,
                                  form_complementary_information=form_complementary_information,
                                  formset_shipping=formset_shipping,
                                  formset_volumes=formset_volumes,
                                  lines_helper=forms.NFeLineInlineHelper(),
                                  invoices_helper=forms.NFeInvoiceInlineHelper(),
                                  volumes_helper=forms.NFeShippingVolumeInlineHelper())
        )

    def valid_forms(self, form, formset_lines, form_person, formset_invoices,
                    form_complementary_information=None,
                    formset_shipping=None,
                    formset_volumes=None):
        self.valid_dict['form'] = ''
        self.valid_dict['formset_lines'] = ''
        self.valid_dict['form_person'] = ''
        self.valid_dict['formset_invoices'] = ''
        self.valid_dict['formset_shipping'] = ''
        self.valid_dict['formset_volumes'] = ''
        if not form.is_valid():
            self.valid_dict['form'] = 'active'
        elif not formset_lines.is_valid():
            self.valid_dict['formset_lines'] = 'active'
        elif not form_person.is_valid():
            self.valid_dict['form_person'] = 'active'
        elif not formset_invoices.is_valid():
            self.valid_dict['formset_invoices'] = 'active'
        else:
            if form_complementary_information is not None:
                if not form_complementary_information.is_valid():
                    self.valid_dict['form_complementary_information'] = 'active'
                    return False
            if formset_shipping is not None:
                if not formset_shipping.is_valid():
                    self.valid_dict['formset_shipping'] = 'active'
                    return False
            if formset_volumes is not None:
                if not formset_volumes.is_valid():
                    self.valid_dict['formset_volumes'] = 'active'
                    return False
            self.valid_dict['form'] = 'active'
            return True
        return False


class NFeInUpdateView(NFeUpdate):
    form_class = forms.NFeThirdInCreate
    form_lines = forms.NFeLineFormSet
    form_invoices = forms.NFeInvoiceFormSet
    form_person = forms.NFeIssuerFormSet

    def get_success_url(self):
        return reverse('nfe:in-update', kwargs={'pk': self.object.pk, })


class NFeOutUpdateView(NFeUpdate):
    form_class = forms.NFeOutCreate
    form_lines = forms.NFeLineFormSet
    form_invoices = forms.NFeInvoiceFormSet
    form_person = forms.NFeReceiverFormSet
    formset_shipping = forms.NFeShippingFormSet
    form_complementary_information = forms.NFeComplementaryInformationFormSet
    formset_volumes = forms.NFeShippingVolumeInlineFormSet

    def get_success_url(self):
        return reverse('nfe:out-update', kwargs={'pk': self.object.pk, })


class NFeLineUpdate(UpdateView):
    model = NFeLine
    form_class = forms.NFeLineThirdInCreate
    formset_icmssn = forms.NFeTaxLineIcmsSNFormSet
    formset_ipi = forms.NFeTaxLineIpiFormSet
    formset_pis = forms.NFeTaxLinePisFormSet
    formset_cofins = forms.NFeTaxLineCofinsFormSet
    template_name = 'document/nfe/nfelinein_form.html'
    show_taxes_forms = False

    valid_dict = {
        'form': 'active',
        'formset_icmssn': '',
        'formset_ipi': '',
        'formset_piscofins': ''
    }

    def get_context_data(self, **kwargs):
        context = super(NFeLineUpdate, self).get_context_data(**kwargs)
        context['nfe_pk'] = self.object.nfe.pk
        if self.request.POST:
            context['form'] = forms.NFeLineThirdInCreate(self.request.POST, instance=self.object)
            if self.show_taxes_forms:
                context['icmssn'] = self.formset_icmssn(self.request.POST, instance=self.object)
                context['ipi'] = self.formset_ipi(self.request.POST, instance=self.object)
                context['pis'] = self.formset_pis(self.request.POST, instance=self.object)
                context['cofins'] = self.formset_cofins(self.request.POST, instance=self.object)

        else:
            self.valid_dict['form'] = 'active'
            self.valid_dict['formset_icmssn'] = ''
            self.valid_dict['formset_ipi'] = ''
            self.valid_dict['formset_piscofins'] = ''
            context['form'] = forms.NFeLineThirdInCreate(instance=self.object)
            if self.show_taxes_forms:
                context['icmssn'] = self.formset_icmssn(instance=self.object)
                context['ipi'] = self.formset_ipi(instance=self.object)
                context['pis'] = self.formset_pis(instance=self.object)
                context['cofins'] = self.formset_cofins(instance=self.object)
        context['active'] = self.valid_dict
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if self.show_taxes_forms:
            icmssn_formset = self.formset_icmssn(self.request.POST, instance=self.object)
            ipi_formset = self.formset_ipi(self.request.POST, instance=self.object)
            pis_formset = self.formset_pis(self.request.POST, instance=self.object)
            cofins_formset = self.formset_cofins(self.request.POST, instance=self.object)
            if self.valid_forms(form, icmssn_formset, ipi_formset, pis_formset, cofins_formset):
                return self.form_valid(form, icmssn_formset, ipi_formset, pis_formset, cofins_formset)
            else:
                return self.form_invalid(form, icmssn_formset, ipi_formset, pis_formset, cofins_formset)
        else:
            if self.valid_forms(form):
                return self.form_valid(form)
            else:
                return self.form_invalid(form)

    def form_valid(self, form, icmssn_formset=None, ipi_formset=None, pis_formset=None, cofins_formset=None):
        self.object = form.save()
        if self.show_taxes_forms:
            icmssn_formset.instance = self.object
            ipi_formset.instance = self.object
            pis_formset.instance = self.object
            cofins_formset.instance = self.object
            icmssn_formset.save()
            ipi_formset.save()
            pis_formset.save()
            cofins_formset.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, icmssn_formset=None, ipi_formset=None, pis_formset=None, cofins_formset=None):
        return self.render_to_response(
            self.get_context_data(form=form, icmssn_formset=icmssn_formset,
                                  ipi_formset=ipi_formset,
                                  pis_formset=pis_formset,
                                  cofins_formset=cofins_formset)
        )

    def valid_forms(self, form, icmssn_formset=None, ipi_formset=None, pis_formset=None,
                    cofins_formset=None):
        self.valid_dict['form'] = ''
        self.valid_dict['formset_icmssn'] = ''
        self.valid_dict['formset_ipi'] = ''
        self.valid_dict['formset_piscofins'] = ''
        if not self.show_taxes_forms:
            self.valid_dict['form'] = 'active'
            return form.is_valid()
        else:
            if not form.is_valid():
                self.valid_dict['form'] = 'active'
            elif not icmssn_formset.is_valid():
                self.valid_dict['formset_icmssn'] = 'active'
            elif not ipi_formset.is_valid():
                self.valid_dict['formset_ipi'] = 'active'
            elif not pis_formset.is_valid():
                self.valid_dict['formset_piscofins'] = 'active'
            elif not cofins_formset.is_valid():
                self.valid_dict['formset_piscofins'] = 'active'
            else:
                self.valid_dict['form'] = 'active'
                return True
            return False


class NFeInLineUpdate(NFeLineUpdate):
    template_name = 'document/nfe/nfelinein_form.html'
    show_taxes_forms = True

    def get_success_url(self):
        return reverse('nfe:in-line-update', kwargs={'pk': self.object.pk, })


class NFeOutLineUpdate(NFeLineUpdate):
    template_name = 'document/nfe/nfelineout_form.html'
    show_taxes_forms = False

    def get_success_url(self):
        return reverse('nfe:out-line-update', kwargs={'pk': self.object.pk, })


class NFeListThird(TemplateView):
    nfeoperation_form_class = forms.NFeOperationSelectForm
    template_name = 'document/nfe/nfe_list_third.html'

    def get_context_data(self, **kwargs):
        ctx = super(NFeListThird, self).get_context_data(**kwargs)
        ctx['nfeoperation_form'] = self.nfeoperation_form_class()

        return ctx


class NFeListSelf(TemplateView):
    nfeoperation_form_class = forms.NFeOperationSelectForm
    template_name = 'document/nfe/nfe_list_self.html'

    def get_context_data(self, **kwargs):
        ctx = super(NFeListSelf, self).get_context_data(**kwargs)
        ctx['nfeoperation_form'] = self.nfeoperation_form_class()
        request = self.request
        status = webservice_status_query(webservice='PR')
        br_tz = pytz.timezone('America/Sao_Paulo')
        #date = status['date'].astimezone(br_tz)
        #date = date.strftime('%d/%m/%Y %H:%M:%S')
        msg = u''
        if settings.NFE_AMBIENTE == 2:
            msg = u'EMISSÃO EM HOMOLOGAÇÃO. '
        #msg = msg + u'%s | Última consulta em %s' % (status['reason'], str(date))
        messages.add_message(request, messages.ERROR, msg)

        return ctx


class NFeListThirdJson(BaseDatatableView):
    model = NFe
    columns = ['id', 'number_series', 'document_release_date', 'third_info', 'document_status', 'edit', ]
    order_columns = ['id', 'number_series', 'document_release_date', 'third_info', 'document_status', ]

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def filter_queryset(self, qs):
        qs = qs.filter(issuer_type=OperationIssuer.THIRD)

        return qs

    # def filter_queryset(self, qs):
    #     search_values = self.get_search_values()
    #     cols = self.get_columns()
    #     for i in range(len(search_values)):
    #         if search_values[i] != '':
    #             if cols[i] == 'name':
    #                 qs = qs.filter(name__icontains=search_values[i])
    #             if cols[i] == 'category':
    #                 qs = qs.filter(category__exact=search_values[i])
    #             if cols[i] == 'is_active':
    #                 qs = qs.filter(is_active=str2bool(search_values[i]))
    #
    #     return qs
    #
    def render_column(self, row, column):
        if column == 'document_status':
            if row.document_status == DocumentStatus.FINALIZED:
                info = 'success'
            elif row.document_status == DocumentStatus.TYPING:
                info = 'info'
            else:
                info = 'danger'
            return '<span class="label label-%s">%s</span>' % (info, super(NFeListThirdJson, self).render_column(row, column))
        else:
            return super(NFeListThirdJson, self).render_column(row, column)


class NFeListSelfJson(BaseDatatableView):
    model = NFe
    columns = ['id', 'number_series', 'document_release_date', 'third_info', 'nfe_status', 'danfe', 'edit', ]
    order_columns = ['id', 'number_series', 'document_release_date', 'third_info', 'nfe_status', ]

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def filter_queryset(self, qs):
        qs = qs.filter(model=55)
        qs = qs.filter(issuer_type=OperationIssuer.SELF).order_by('-document_date', '-number')

        return qs

    # def filter_queryset(self, qs):
    #     search_values = self.get_search_values()
    #     cols = self.get_columns()
    #     for i in range(len(search_values)):
    #         if search_values[i] != '':
    #             if cols[i] == 'name':
    #                 qs = qs.filter(name__icontains=search_values[i])
    #             if cols[i] == 'category':
    #                 qs = qs.filter(category__exact=search_values[i])
    #             if cols[i] == 'is_active':
    #                 qs = qs.filter(is_active=str2bool(search_values[i]))
    #
    #     return qs
    #
    def render_column(self, row, column):
        if column == 'document_status':
            if row.document_status == DocumentStatus.FINALIZED:
                info = 'success'
            elif row.document_status == DocumentStatus.TYPING:
                info = 'info'
            else:
                info = 'danger'
            return '<span class="label label-%s">%s</span>' % (info, super(NFeListSelfJson, self).render_column(row, column))
        if column == 'danfe':
            html = '<a href="%s" class="btn btn-sm btn-default"><i class="fa fa-print"></i> Danfe</a>' % row.get_danfe_url()
            return html
        if column == 'edit':
            html = '<a href="%s" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i> Editar</a>' % row.get_absolute_url()
            return html
            # return '<a href="%s" class="btn btn-sm btn-default btn-circle btn-editable"><i class="fa fa-pencil">' \
            #        '</i> %s</a>' % (row.get_danfe_url(), "DANFE")
        return super(NFeListSelfJson, self).render_column(row, column)

