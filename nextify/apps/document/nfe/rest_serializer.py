from django.conf.urls import url, include
from models import NFeQueue
from rest_framework import routers, serializers, viewsets
from django.http import HttpResponse, JsonResponse

class NFeSerializer(serializers.ModelSerializer):
    class Meta:
        model = NFeQueue
        fields = ('id', 'nfe', 'nfe_operation')


def nfe_list(request):
    nfes = NFeQueue.objects.all()
    serializer = NFeSerializer(nfes, many=True)
    return JsonResponse(serializer.data, safe=False)

class NFeViewSet(viewsets.ModelViewSet):
    queryset = NFeQueue.objects.all()
    serializer_class = NFeSerializer


router = routers.DefaultRouter()
router.register(r'nfe', NFeViewSet)