from __future__ import division, print_function, unicode_literals
from django.test import TestCase, SimpleTestCase
from os.path import abspath, dirname
from datetime import datetime
from pysped.nfe import ProcessadorNFe
from pysped.nfe.webservices_flags import (UF_CODIGO,
                                          WS_NFE_CONSULTA_RECIBO)
from nextify.apps.document.nfe.models import NFe
from nextify.apps.document.nfe.serializer import NfeSerializer

class NFeTests(SimpleTestCase):

    def test_danfe_xml(self):
        proc = ProcessadorNFe()
        proc.versao = '3.10'
        proc.estado = 'PR'
        proc.certificado.arquivo = '10393451.pfx'
        proc.certificado.senha = '123456'
        proc.ambiente = 2

        proc.salva_arquivos = True
        proc.contingencia_SCAN = False
        proc.caminho = ''

        #
        # Instancia uma NF-e
        #
        nfe = NFe.on_company.get(pk=7)
        s = NfeSerializer(nfe=nfe)
        self.assertEqual(1, 1, 'OK')
