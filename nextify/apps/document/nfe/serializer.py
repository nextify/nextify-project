# -*- coding: utf-8 -*-
import os
from django.conf import settings
from nextify.core.enum.person import *
from nextify.core.enum.nfe import *

from pysped.nfe.leiaute import NFe_400, ProtNFe_310, Det_400, Vol_400, Dup_400, ProcEventoCancNFe_100, Pag_400, DetPag_400
from pysped.nfe.danfe import DANFE


class NfeSerializer(object):

    def __init__(self, nfe):
        nfe.refresh_from_db()
        self.nfe = nfe
        self.nfe_400 = NFe_400()
        self.protnfe_400 = ProtNFe_310()
        self.proc_evento_canc_nfe = ProcEventoCancNFe_100()
        self.danfe_content = None
        self.danfe = DANFE()
        self.access_key = None
        self.xml_path = None
        self.pdf_path = None
        self.environment = settings.NFE_AMBIENTE
        self.initial_path = settings.NFE_FILE_PATH
        self.infNFe()


    def get_danfe_content(self):
        danfe = self.danfe
        danfe.NFe = self.nfe_400
        danfe.protNFe = self.protnfe_400
        danfe.procEventoCancNFe = self.proc_evento_canc_nfe
        danfe.salvar_arquivo = False
        danfe.gerar_danfe()
        self.danfe_content = danfe.conteudo_pdf
        return self.danfe_content

    def infNFe(self):
        nfe = self.nfe
        xml = self.nfe_400

        if nfe.nfe_status_id in [100, ]:
            nfe_path = self.set_nfe_path(self.environment, nfe.nfe_access_key)
            nfe_xml_file = nfe_path + nfe.nfe_access_key + '-nfeProc.xml'
            nfe_pdf_file = nfe_path + nfe.nfe_access_key + '.pdf'
            self.xml_path = nfe_xml_file
            self.pdf_path = nfe_pdf_file
            if os.path.isfile(nfe_xml_file):
                self.nfe_400.xml = nfe_xml_file
                self.protnfe_400.xml = nfe_xml_file
                danfe = self.danfe
                danfe.NFe = self.nfe_400
                danfe.logo = settings.NFE_LOGO
                danfe.protNFe = self.protnfe_400
                danfe.gerar_danfe()
                self.access_key = self.nfe_400.chave
                self.danfe_content = danfe.conteudo_pdf

                return

        if nfe.nfe_status_id in [3, 101, 103, 104, 105, ]:
            nfe_xml_file = self.set_nfe_path(self.environment, nfe.nfe_access_key)
            nfe_xml_file = nfe_xml_file + nfe.nfe_access_key + '.xml'
            if nfe.nfe_status_id == 101:
                canc_xml = self.set_nfe_path(self.environment, nfe.nfe_access_key)
                canc_xml = canc_xml + nfe.nfe_access_key + '_110111-procEventoNFe.xml'
                self.proc_evento_canc_nfe.xml = canc_xml
            self.xml_path = nfe_xml_file
            if os.path.isfile(nfe_xml_file):
                self.nfe_400.xml = nfe_xml_file
                self.protnfe_400.xml = nfe_xml_file
                self.nfe_400.monta_chave()
                self.access_key = self.nfe_400.chave
                return
        self.ide()
        self.emit()
        self.dest()
        for line in nfe.lines.all():
            l = self.det(line=line)
            xml.infNFe.det.append(l)
        self.icmstotal()
        self.transporte()
        self.cobranca()
        self.pag()
        self.infAdic()
        self.nfe_400.gera_nova_chave()
        self.access_key = self.nfe_400.chave

    def ide(self):
        xml = self.nfe_400
        nfe = self.nfe
        xml.infNFe.ide.cUF.valor = nfe.issuer.state_ibge_code
        xml.infNFe.ide.natOp.valor = nfe.operation.description
        xml.infNFe.ide.indPag.valor = nfe.nfe_payment_method.payment_form
        xml.infNFe.ide.mod.valor = nfe.model
        xml.infNFe.ide.serie.valor = nfe.series
        xml.infNFe.ide.nNF.valor = nfe.number
        xml.infNFe.ide.dhEmi.valor = nfe.document_date
        xml.infNFe.ide.dSaiEnt.valor = nfe.document_release_date
        xml.infNFe.ide.tpNF.valor = nfe.operation_type
        xml.infNFe.ide.idDest.valor = str(nfe.nfe_destination)
        xml.infNFe.ide.cMunFG.valor = nfe.issuer.city_ibge_code
        xml.infNFe.ide.tpImp.valor = nfe.nfe_danfe_print_type
        xml.infNFe.ide.tpEmis.valor = nfe.nfe_issue_type
        xml.infNFe.ide.tpAmb.valor = str(self.environment)
        xml.infNFe.ide.finNFe.valor = nfe.nfe_finality
        xml.infNFe.ide.indFinal.valor = str(nfe.nfe_final_consumer)
        xml.infNFe.ide.indPres.valor = str(nfe.nfe_consumer_presence)
        xml.infNFe.ide.procEmi.valor = 0
        xml.infNFe.ide.verProc.valor = nfe.nfe_application_version

    def emit(self):
        xml = self.nfe_400
        nfe = self.nfe
        xml.infNFe.emit.CNPJ.valor = nfe.issuer.document_number
        xml.infNFe.emit.xNome.valor = nfe.issuer.name
        xml.infNFe.emit.xFant.valor = nfe.issuer.fancy_name
        xml.infNFe.emit.IE.valor = nfe.issuer.state_registry
        xml.infNFe.emit.IEST.valor = nfe.issuer.state_registry_st
        xml.infNFe.emit.IM.valor = nfe.issuer.city_registry
        xml.infNFe.emit.CRT.valor = nfe.issuer.crt
        self.enderEmit()

    def enderEmit(self):
        xml = self.nfe_400
        nfe = self.nfe
        xml.infNFe.emit.enderEmit.xLgr.valor = nfe.issuer.street
        xml.infNFe.emit.enderEmit.nro.valor = nfe.issuer.number
        xml.infNFe.emit.enderEmit.xCpl.valor = nfe.issuer.complement
        xml.infNFe.emit.enderEmit.xBairro.valor = nfe.issuer.province
        xml.infNFe.emit.enderEmit.cMun.valor = nfe.issuer.city_ibge_code
        xml.infNFe.emit.enderEmit.xMun.valor = nfe.issuer.city_name
        xml.infNFe.emit.enderEmit.UF.valor = nfe.issuer.state_acronym
        xml.infNFe.emit.enderEmit.CEP.valor = nfe.issuer.zipcode
        xml.infNFe.emit.enderEmit.cPais.valor = nfe.issuer.country_bacen_code
        xml.infNFe.emit.enderEmit.xPais.valor = nfe.issuer.country_name
        xml.infNFe.emit.enderEmit.fone.valor = nfe.issuer.phone

    def dest(self):
        xml = self.nfe_400
        nfe = self.nfe
        if settings.NFE_AMBIENTE == NFeEnvironment.TEST:
            xml.infNFe.dest.CNPJ.valor = settings.NFE_HOMOLOG_CNPJ
        else:
            if nfe.document_receiver.category == PersonCategory.INDIVIDUAL:
                xml.infNFe.dest.CPF.valor = nfe.receiver.document_number
            elif nfe.document_receiver.category == PersonCategory.CORPORATE:
                xml.infNFe.dest.CNPJ.valor = nfe.receiver.document_number
            else:
                xml.infNFe.dest.idEstrangeiro.valor = nfe.receiver.document_number
        xml.infNFe.dest.xNome.valor = nfe.receiver.name
        xml.infNFe.dest.indIEDest.valor = str(nfe.receiver.state_registry_id)
        xml.infNFe.dest.IE.valor = nfe.receiver.state_registry
        xml.infNFe.dest.ISUF.valor = nfe.receiver.suframa_registry
        xml.infNFe.dest.IM.valor = nfe.receiver.city_registry
        xml.infNFe.dest.email.valor = nfe.receiver.email
        self.enderDest()

    def enderDest(self):
        xml = self.nfe_400
        nfe = self.nfe
        xml.infNFe.dest.enderDest.xLgr.valor = nfe.receiver.street
        xml.infNFe.dest.enderDest.nro.valor = nfe.receiver.number
        xml.infNFe.dest.enderDest.xCpl.valor = nfe.receiver.complement
        xml.infNFe.dest.enderDest.xBairro.valor = nfe.receiver.province
        xml.infNFe.dest.enderDest.cMun.valor = nfe.receiver.city_ibge_code
        xml.infNFe.dest.enderDest.xMun.valor = nfe.receiver.city_name
        xml.infNFe.dest.enderDest.UF.valor = nfe.receiver.state_acronym
        xml.infNFe.dest.enderDest.CEP.valor = nfe.receiver.zipcode
        xml.infNFe.dest.enderDest.cPais.valor = nfe.receiver.country_bacen_code
        xml.infNFe.dest.enderDest.xPais.valor = nfe.receiver.country_name
        xml.infNFe.dest.enderDest.fone.valor = nfe.receiver.phone

    def det(self, line):
        l = Det_400()
        l.nItem.valor = line.sequence
        l.infAdProd.valor = line.complementary_description
        l.prod.cProd.valor = line.item_code
        l.prod.cEAN.valor = 'SEM GTIN'
        l.prod.xProd.valor = line.description
        l.prod.NCM.valor = line.ncm_code
        #l.prod.NVE.valor = ''
        #l.prod.EXTIPI.valor = ''
        l.prod.CFOP.valor = line.cfop_code
        l.prod.uCom.valor = line.unit_code
        l.prod.qCom.valor = line.quantity
        l.prod.vUnCom.valor = line.nfe_unitary_value
        l.prod.vProd.valor = line.goods_value
        # l.prod.cEANTrib.valor = line.taxed_ean_code
        # l.prod.uTrib.valor = line.taxed_unit_code
        # l.prod.qTrib.valor = line.taxed_quantity
        # l.prod.vUnTrib.valor = line.nfe_taxed_unitary_value
        l.prod.cEANTrib.valor = 'SEM GTIN'
        l.prod.uTrib.valor = line.unit_code
        l.prod.qTrib.valor = line.quantity
        l.prod.vUnTrib.valor = line.nfe_unitary_value
        l.prod.vFrete.valor = line.freight_value
        l.prod.vSeg.valor = line.insurance_value
        l.prod.vDesc.valor = line.discount_value
        l.prod.vOutro.valor = line.expenses_value
        l.prod.indTot.valor = line.ind_tot
        self.imposto(line=l)

        return l

    def imposto(self, line):
        l = line
        self.icmssn(line=l)
        self.pis(line=l)
        self.cofins(line=l)
        l.imposto.vTotTrib.valor = ''

    def icmssn(self, line):
        l = line
        l.imposto.ICMS.orig.valor = '0'
        l.imposto.ICMS.CSOSN.valor = '103'

    def pis(self, line):
        l = line
        l.imposto.PIS.CST.valor = '99'
        l.imposto.PIS.qBCProd.valor = 0
        l.imposto.PIS.vAliqProd.valor = 0
        l.imposto.PIS.vPIS.valor = 0

    def cofins(self, line):
        l = line
        l.imposto.COFINS.CST.valor = '99'
        l.imposto.COFINS.qBCProd.valor = 0
        l.imposto.COFINS.vAliqProd.valor = 0
        l.imposto.COFINS.vCOFINS.valor = 0

    def icmstotal(self):
        xml = self.nfe_400
        nfe = self.nfe

        xml.infNFe.total.ICMSTot.vBC.valor = nfe.tax_totals.icms_base_total
        xml.infNFe.total.ICMSTot.vICMS.valor = nfe.tax_totals.icms_value_total
        xml.infNFe.total.ICMSTot.vBCST.valor = nfe.tax_totals.icmsst_base_total
        xml.infNFe.total.ICMSTot.vST.valor = nfe.tax_totals.icmsst_value_total
        xml.infNFe.total.ICMSTot.vProd.valor = nfe.totals.goods_total
        xml.infNFe.total.ICMSTot.vFrete.valor = nfe.totals.freight_total
        xml.infNFe.total.ICMSTot.vSeg.valor = nfe.totals.insurance_total
        xml.infNFe.total.ICMSTot.vDesc.valor = nfe.totals.discount_total
        xml.infNFe.total.ICMSTot.vII.valor = nfe.tax_totals.ii_value_total
        xml.infNFe.total.ICMSTot.vIPI.valor = nfe.tax_totals.ipi_value_total
        xml.infNFe.total.ICMSTot.vPIS.valor = nfe.tax_totals.pis_value_total
        xml.infNFe.total.ICMSTot.vCOFINS.valor = nfe.tax_totals.cofins_value_total
        xml.infNFe.total.ICMSTot.vOutro.valor = nfe.totals.expenses_total
        xml.infNFe.total.ICMSTot.vNF.valor = nfe.totals.document_total

    def transporte(self):
        xml = self.nfe_400
        nfe = self.nfe

        xml.infNFe.transp.modFrete.valor = nfe.nfe_freight
        if hasattr(nfe, 'shipping'):
            shipping = nfe.shipping
            if shipping.category == PersonCategoryBR.CORPORATE:
                xml.infNFe.transp.transporta.CNPJ.valor = shipping.document_number
            else:
                xml.infNFe.transp.transporta.CPF.valor = shipping.document_number
            xml.infNFe.transp.transporta.xNome.valor = shipping.name
            xml.infNFe.transp.transporta.IE.valor = shipping.state_registry
            xml.infNFe.transp.transporta.xEnder.valor = shipping.address_sumary
            xml.infNFe.transp.transporta.xMun.valor = shipping.city_name
            xml.infNFe.transp.transporta.UF.valor = shipping.state_acronym

            if hasattr(nfe, 'shipping_volumes'):
                for volume in nfe.shipping_volumes.all():
                    v = self.vol(volume=volume)
                    xml.infNFe.transp.vol.append(v)

    def vol(self, volume):
        v = Vol_400()
        v.qVol.valor = volume.volume_quantity
        v.esp.valor = volume.volume_especies
        v.marca.valor = volume.volume_mark
        v.nVol.valor = volume.volume_number
        v.pesoB.valor = volume.gross_weight
        v.pesoL.valor = volume.net_weight
        return v

    def cobranca(self):
        xml = self.nfe_400
        nfe = self.nfe
        if hasattr(nfe, 'invoice_totals'):
            xml.infNFe.cobr.fat.nFat.valor = nfe.invoice_totals.invoice_number
            xml.infNFe.cobr.fat.vOrig.valor = nfe.invoice_totals.original_value
            xml.infNFe.cobr.fat.vDesc.valor = nfe.invoice_totals.discount_value
            xml.infNFe.cobr.fat.vLiq.valor = nfe.invoice_totals.net_value

            for invoice in nfe.invoices.all():
                i = self.dup(invoice=invoice)
                xml.infNFe.cobr.dup.append(i)

    def dup(self, invoice):
        i = Dup_400()
        i.nDup.valor = invoice.invoice_number
        i.dVenc.valor = invoice.invoice_due_date
        i.vDup.valor = invoice.net_value
        return i


    def pag(self):
        xml = self.nfe_400
        nfe = self.nfe
        p1 = Pag_400()
        p1.detPag = DetPag_400()
        p1.detPag.tPag.valor = '99'
        p1.detPag.vPag.valor = nfe.totals.document_total
        # p1.vPag.valor = nfe.totals.document_total
        xml.infNFe.pag.append(p1)





    def infAdic(self):
        xml = self.nfe_400
        nfe = self.nfe
        if hasattr(nfe, 'complementary_information'):
            compl_info = nfe.complementary_information
            info = u'%s. %s' % (compl_info.additional_information, compl_info.additional_information_auto)
            xml.infNFe.infAdic.infCpl.valor = str(info)


    def set_nfe_path(self, environment, nfe_access_key):
        path = self.initial_path

        if environment == 1:
            path = os.path.join(path, 'producao/')
        else:
            path = os.path.join(path, 'homologacao/')

        #data = '20' + nfe_access_key[2:4] + '-' + nfe_access_key[4:6]
        cnpj = nfe_access_key[6:20]
        serie = nfe_access_key[22:25]
        numero = nfe_access_key[25:34]

        #path = os.path.join(path, data + '/')
        path = os.path.join(path, cnpj + '/')
        #path = os.path.join(path, serie + '-' + numero + '/')

        try:
            os.makedirs(path)
        except:
            pass

        return path


    # def __init__(self, nfe=None, xml=None, access_key=None):
    #     self.nfe = nfe
    #     self.operation = nfe.operation
    #     self.access_key = access_key
    #     self._parser = etree.XMLParser(recover=True)
    #     super(NfeSerializer, self).__init__()
    #
    #     if nfe is not None:
    #         self._nfe()
    #     # elif lote is not None:
    #     #     self._serialize_lote()
    #     # el
    #     if xml is not None:
    #         self.root_xml = etree.fromstring(xml.encode('utf-8'), parser=self._parser)
    #
    # def __unicode__(self):
    #     return etree.tostring(self.root_xml, encoding='utf-8')
    #
    # def _nfe(self):
    #     root = etree.Element('NFe', xmlns="http://www.portalfiscal.inf.br/nfe")
    #     Id = 'NFe' + str(self.access_key)
    #     root.append(self.infNFe(id))
    #     #root.append(self.signature())
    #
    #     s = signature.AssinaturaA1()
    #
    #     xml = etree.tostring(root, encoding='utf-8')
    #     doctype = u'<!DOCTYPE NFe [<!ATTLIST infNFe Id ID #IMPLIED>]>'
    #     xml = doctype + xml
    #
    #     self.root_xml = root
    #
    #
    #
    #     root = s.sign(xml, Id)
    #
    #     self.root_xml = etree.fromstring(root)
    #
    #
    # def infNFe(self, Id):
    #     root = etree.Element('infNFe', Id=str(Id), versao='3.10')
    #
    #     root.append(self.ide())
    #     root.append(self.emit())
    #     root.append(self.dest())
    #     _produto = self.produto()
    #     for child in _produto:
    #         root.append(child)
    #     root.append(self.total())
    #     root.append(self.transp())
    #
    #     return root
    #
    # def ide(self):
    #     root = etree.Element('ide')
    #     nfe = self.nfe
    #     operation = self.operation
    #     issuer = nfe.issuer
    #
    #     etree.SubElement(root, 'cUF').text = str(issuer.state_ibge_code)
    #     etree.SubElement(root, 'cNF').text = str(nfe.nfe_number)
    #     etree.SubElement(root, 'natOp').text = str(operation.description)
    #     etree.SubElement(root, 'indPag').text = u'0'  # FIX
    #     etree.SubElement(root, 'mod').text = str(nfe.model)
    #     etree.SubElement(root, 'serie').text = str(nfe.series_code)
    #     etree.SubElement(root, 'nNF').text = str(nfe.document_number)
    #     etree.SubElement(root, 'dhEmi').text = str(arrow.get(nfe.document_date).to('Brazil/East').format('YYYY-MM-DDTHH:mm:ssZZ'))
    #     etree.SubElement(root, 'dhSaiEnt').text = str(arrow.get(nfe.document_release_date).to('Brazil/East').format('YYYY-MM-DDTHH:mm:ssZZ'))
    #     etree.SubElement(root, 'tpNF').text = str(nfe.operation_type)
    #     etree.SubElement(root, 'idDest').text = str(nfe.nfe_destination)
    #     etree.SubElement(root, 'cMunFG').text = str(issuer.city_ibge_code)
    #     etree.SubElement(root, 'tpImp').text = u'1'
    #     etree.SubElement(root, 'tpEmis').text = u'1'
    #     etree.SubElement(root, 'cDV').text = str(nfe.nfe_access_key_dv)
    #     etree.SubElement(root, 'tpAmb').text = u'2'  # 2 - Homologação  #FIX
    #     etree.SubElement(root, 'finNFe').text = str(nfe.nfe_finality)
    #     etree.SubElement(root, 'indFinal').text = str(nfe.nfe_final_consumer)
    #     etree.SubElement(root, 'indPres').text = str(nfe.nfe_consumer_presence)
    #     etree.SubElement(root, 'procEmi').text = u'0'  # 0 - Fixo
    #     etree.SubElement(root, 'verProc').text = u'0.0.1'
    #
    #     # Grupo Opcional (Somente em caso de contingencia)
    #     #etree.SubElement(root, 'dhCont').text = u''
    #     #etree.SubElement(root, 'xJust').text = u''
    #
    #     return root
    #
    # def emit(self):
    #     root = etree.Element('emit')
    #     nfe = self.nfe
    #     issuer = nfe.issuer
    #
    #     etree.SubElement(root, 'CNPJ').text = str(issuer.document_number)
    #     # etree.SubElement(root, 'CPF').text = '' # Nao Existe Emitente Pessoa Fisica
    #     etree.SubElement(root, 'xNome').text = str(issuer.name)
    #     etree.SubElement(root, 'xFant').text = str(issuer.fancy_name)
    #
    #     root.append(self.enderEmit())
    #
    #     etree.SubElement(root, 'IE').text = str(issuer.state_registry)
    #     # etree.SubElement(root, 'IEST').text = ''
    #     # etree.SubElement(root, 'IM').text = '' # OP
    #     # etree.SubElement(root, 'CNAE').text = '' # OP
    #
    #     etree.SubElement(root, 'CRT').text = str(issuer.crt)
    #
    #     return root
    #
    # def enderEmit(self):
    #     root = etree.Element('enderEmit')
    #     nfe = self.nfe
    #     issuer = nfe.issuer
    #
    #     etree.SubElement(root, 'xLgr').text = str(issuer.street)
    #     etree.SubElement(root, 'nro').text = str(issuer.number)
    #     if issuer.complement:
    #         etree.SubElement(root, 'xCpl').text = str(issuer.complement)
    #     etree.SubElement(root, 'xBairro').text = str(issuer.province)
    #     etree.SubElement(root, 'cMun').text = str(issuer.city_ibge_code)
    #     etree.SubElement(root, 'xMun').text = str(issuer.city_name)
    #     etree.SubElement(root, 'UF').text = str(issuer.state_acronym)
    #     etree.SubElement(root, 'CEP').text = str(issuer.zipcode)
    #     etree.SubElement(root, 'cPais').text = str(issuer.country_bacen_code)
    #     etree.SubElement(root, 'xPais').text = str(issuer.country_name)
    #     etree.SubElement(root, 'fone').text = str(issuer.phone)
    #
    #     return root
    #
    # def dest(self):
    #     root = etree.Element('dest')
    #     nfe = self.nfe
    #
    #     receiver = nfe.receiver
    #
    #     if receiver.category == PersonCategory.CORPORATE:
    #         etree.SubElement(root, 'CNPJ').text = str(receiver.document_number)
    #     elif receiver.category == PersonCategory.INDIVIDUAL:
    #         etree.SubElement(root, 'CPF').text = str(receiver.document_number)
    #     elif receiver.category == PersonCategory.FOREIGN:
    #         etree.SubElement(root, 'idEstrangeiro').text = str(receiver.document_number)
    #     etree.SubElement(root, 'xNome').text = str(receiver.name)
    #
    #     root.append(self.enderDest())
    #
    #     etree.SubElement(root, 'indIEDest').text = str(receiver.state_registry_id)
    #     if receiver.state_registry_id == PersonIE.TAXPAYER:
    #         etree.SubElement(root, 'IE').text = str(receiver.state_registry)
    #     if receiver.suframa_registry:
    #         etree.SubElement(root, 'ISUF').text = str(receiver.suframa_registry)
    #     if receiver.city_registry:
    #         etree.SubElement(root, 'IM').text = str(receiver.city_registry)
    #     if receiver.email:
    #         etree.SubElement(root, 'email').text = str(receiver.email)
    #
    #     return root
    #
    # def enderDest(self):
    #     root = etree.Element('enderDest')
    #     nfe = self.nfe
    #     receiver = nfe.receiver
    #
    #     etree.SubElement(root, 'xLgr').text = str(receiver.street)
    #     etree.SubElement(root, 'nro').text = str(receiver.number)
    #     if receiver.complement:
    #         etree.SubElement(root, 'xCpl').text = str(receiver.complement)
    #     etree.SubElement(root, 'xBairro').text = str(receiver.province)
    #     etree.SubElement(root, 'cMun').text = str(receiver.city_ibge_code)
    #     etree.SubElement(root, 'xMun').text = str(receiver.city_name)
    #     etree.SubElement(root, 'UF').text = str(receiver.state_acronym)
    #     etree.SubElement(root, 'CEP').text = str(receiver.zipcode)
    #     etree.SubElement(root, 'cPais').text = str(receiver.country_bacen_code)
    #     etree.SubElement(root, 'xPais').text = str(receiver.country_name)
    #     etree.SubElement(root, 'fone').text = str(receiver.phone)
    #
    #     return root
    #
    # def produto(self):
    #     nfe = self.nfe
    #     root = etree.Element('root')
    #     for prod in nfe.lines.all():
    #         root.append(self.det(prod))
    #
    #     return root
    #
    # def det(self, det):
    #     #root = etree.Element('det', nItem=str(det.sequence))
    #     root = etree.Element('det', nItem='1')
    #     root.append(self.prod(line=det))
    #     root.append(self.imposto(line=det))
    #
    #     return root
    #
    # def prod(self, line):
    #     root = etree.Element('prod')
    #     etree.SubElement(root, 'cProd').text = line.item_code
    #     etree.SubElement(root, 'cEAN').text = line.ean_code
    #     etree.SubElement(root, 'xProd').text = line.description
    #     etree.SubElement(root, 'NCM').text = line.ncm_code
    #     #etree.SubElement(root, 'NVE').text = ''
    #     #etree.SubElement(root, 'EX_TIPI').text = ''
    #     etree.SubElement(root, 'CFOP').text = line.cfop_code
    #     etree.SubElement(root, 'uCom').text = line.unit_code
    #     etree.SubElement(root, 'qCom').text = str('{0:.4f}'.format(line.quantity))
    #     etree.SubElement(root, 'vUnCom').text = str('{0:.10f}'.format(line.unitary_value))
    #     etree.SubElement(root, 'vProd').text = str('{0:.2f}'.format(line.goods_value))
    #     etree.SubElement(root, 'cEANTrib').text = line.taxed_ean_code
    #     etree.SubElement(root, 'uTrib').text = line.taxed_unit_code
    #     etree.SubElement(root, 'qTrib').text = str('{0:.4f}'.format(line.taxed_quantity))
    #     etree.SubElement(root, 'vUnTrib').text = str('{0:.10f}'.format(line.taxed_unitary_value))
    #     if line.freight_value > 0:
    #         etree.SubElement(root, 'vFrete').text = str('{0:.2f}'.format(line.freight_value))
    #     if line.insurance_value > 0:
    #         etree.SubElement(root, 'vSeg').text = str('{0:.2f}'.format(line.insurance_value))
    #     if line.discount_value > 0:
    #         etree.SubElement(root, 'vDesc').text = str('{0:.2f}'.format(line.discount_value))
    #     if line.expenses_value > 0:
    #         etree.SubElement(root, 'vOutro').text = str('{0:.2f}'.format(line.expenses_value))
    #     etree.SubElement(root, 'indTot').text = str(line.ind_tot)
    #
    #     return root
    #
    # def imposto(self, line):
    #     root = etree.Element('imposto')
    #
    #     # etree.SubElement(root, 'vTotTrib').text = '' # FIX
    #     root.append(self.icms(line))
    #     #root.append(self.ipi(line))
    #     root.append(self.pis(line))
    #     root.append(self.cofins(line))
    #
    #     return root
    #
    # def icms(self, line):
    #     root = etree.Element('ICMS')
    #     # Fixo para ICMSSN102
    #     root.append(self.icmssn(line))
    #
    #     return root
    #
    # def icmssn(self, line):
    #     # Fixo para ICMSSN102
    #     root = etree.Element('ICMSSN102')
    #     etree.SubElement(root, 'orig').text = '0'  # Nacional
    #     etree.SubElement(root, 'CSOSN').text = '102'  # SN sem permissao de credito
    #
    #     return root
    #
    # def ipi(self, line):
    #     root = etree.Element('IPI')
    #
    #     return root
    #
    # def pis(self, line):
    #     root = etree.Element('PIS')
    #     root.append(self.pisoutr(line))
    #
    #     return root
    #
    # def pisoutr(self, line):
    #     root = etree.Element('PISOutr')
    #     etree.SubElement(root, 'CST').text = '99'
    #     etree.SubElement(root, 'qBCProd').text = '0'
    #     etree.SubElement(root, 'vAliqProd').text = '0'
    #     etree.SubElement(root, 'vPIS').text = '0.00'
    #
    #     return root
    #
    # def cofins(self, line):
    #     root = etree.Element('COFINS')
    #     root.append(self.cofinsoutr(line))
    #
    #     return root
    #
    # def cofinsoutr(self, line):
    #     root = etree.Element('COFINSOutr')
    #     etree.SubElement(root, 'CST').text = '99'
    #     etree.SubElement(root, 'qBCProd').text = '0'
    #     etree.SubElement(root, 'vAliqProd').text = '0'
    #     etree.SubElement(root, 'vCOFINS').text = '0.00'
    #
    #     return root
    #
    # def total(self):
    #     nfe = self.nfe
    #     root = etree.Element('total')
    #
    #     root.append(self.icmstotal())
    #
    #     return root
    #
    # def icmstotal(self):
    #     nfe = self.nfe
    #     root = etree.Element('ICMSTot')
    #     totals = nfe.totals
    #
    #     etree.SubElement(root, 'vBC').text = '0.00'  # FIX
    #     etree.SubElement(root, 'vICMS').text = '0.00'  # FIX
    #     etree.SubElement(root, 'vICMSDeson').text = '0.00'  # FIX
    #     etree.SubElement(root, 'vBCST').text = '0.00'  # FIX
    #     etree.SubElement(root, 'vST').text = '0.00'  # FIX
    #     etree.SubElement(root, 'vProd').text = str('{0:.2f}'.format(totals.goods_total))
    #     etree.SubElement(root, 'vFrete').text = str('{0:.2f}'.format(totals.freight_total))
    #     etree.SubElement(root, 'vSeg').text = str('{0:.2f}'.format(totals.insurance_total))
    #     etree.SubElement(root, 'vDesc').text = str('{0:.2f}'.format(totals.discount_total))
    #     etree.SubElement(root, 'vII').text = '0.00'  # FIX
    #     etree.SubElement(root, 'vIPI').text = '0.00'  # FIX
    #     etree.SubElement(root, 'vPIS').text = '0.00'  # FIX
    #     etree.SubElement(root, 'vCOFINS').text = '0.00'  # FIX
    #     etree.SubElement(root, 'vOutro').text = str('{0:.2f}'.format(totals.expenses_total))
    #     etree.SubElement(root, 'vNF').text = str('{0:.2f}'.format(totals.document_total))
    #     etree.SubElement(root, 'vTotTrib').text = '0.00'  # FIX
    #
    #     return root
    #
    # def transp(self):
    #     nfe = self.nfe
    #     root = etree.Element('transp')
    #
    #     etree.SubElement(root, 'modFrete').text = str(nfe.nfe_freight)
    #
    #     return root
    #
    # def signature(self):
    #     s = signature.AssinaturaA1()
    #     nfe = self.nfe
    #
    #     return s.template_signature(ref_uri=nfe.nfe_access_key)
    #
