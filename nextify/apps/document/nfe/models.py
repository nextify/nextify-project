from .abstract_models import *


class NFe(AbstractNFe):
    pass


class NFeIssuer(AbstractNFeIssuer):
    pass


class NFeReceiver(AbstractNFeReceiver):
    pass


class NFeLine(AbstractNFeLine):
    pass


class NFeInvoice(AbstractNFeInvoice):
    pass


class NFeInvoiceTotals(AbstractNFeInvoiceTotals):
    pass


# Taxes
class NFeTaxLineIcmsSN(AbstractNFeTaxLineIcmsSN):
    pass


class NFeTaxLineIpi(AbstractNFeTaxLineIpi):
    pass


class NFeTaxLinePis(AbstractNFeTaxLinePis):
    pass


class NFeTaxLineCofins(AbstractNFeTaxLineCofins):
    pass


# Totals
class NFeTotals(AbstractNFeTotals):
    pass


class NFeTaxTotals(AbstractNFeTaxTotals):
    pass


class NFeStatus(AbstractNFeStatus):
    pass


class NFeShipping(AbstractNFeShipping):
    pass


class NFeShippingVolume(AbstractNFeShippingVolume):
    pass


class NFeComplementaryInformation(AbstractNFeComplementaryInformation):
    pass


class NFeEvent(AbstractNFeEvent):
    pass


class NFeWebserviceStatus(AbstractNFeWebserviceStatus):
    pass


class NFeSendMailQueue(AbstractNFeSendMailQueue):
    pass


class NFeQueue(AbstractNFeQueue):
    pass