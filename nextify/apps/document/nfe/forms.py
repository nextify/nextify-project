# -*- coding: utf-8 -*-
from collections import OrderedDict
from django.forms.models import inlineformset_factory
from django.forms.models import BaseInlineFormSet

import nextify.core.util.nextforms as forms
from nextify.core.util.crispy_forms.helper import FormHelper
from nextify.core.util.crispy_forms.layout import Layout, Submit, Field, Div, Row, \
    Colmd1, Colmd2, Colmd4, Colmd3, Colmd6, HTML, NextifyInlineField, Hidden

from nextify.core.forms.widgets import ModelLinkWidget
from nextify.core.enum.nfe import NFeEventType
from nextify.apps.address.widgets import AddressSelect
from nextify.apps.address.models import Address
from nextify.apps.person.supplier.widgets import SupplierSelect
from nextify.apps.person.customer.widgets import CustomerSelect
from nextify.apps.person.shippingcompany.widgets import ShippingCompanySelect
from nextify.apps.person.models import Supplier, Customer, ShippingCompany
from nextify.apps.catalogue.models import Product
from nextify.apps.unit.models import Unit
from nextify.externals.tables.cfop.models import Cfop
from nextify.externals.tables.ncm.models import NcmClassification
from nextify.externals.tables.cfop.widgets import CfopSelect
from nextify.externals.tables.ncm.widgets import NcmSelect
from nextify.apps.catalogue.product.widgets import ProductSelect, ProductUnitSelect

from nextify.apps.operation.widgets import OperationSelect
from nextify.externals.tables.location.models import City, State
from nextify.externals.tables.location.widgets import CitySelect, StateSelect
from nextify.apps.payment.widgets import PaymentMethodSelect
from nextify.apps.operation.models import Operation
from nextify.apps.payment.models import PaymentMethod
from nextify.apps.pricing.models import PriceTable
from nextify.apps.pricing.widgets import PriceTableSelect
from nextify.core.util.validations import validate_cnpj, validate_cpf
from nextify.core.util.validations.brazilian_numbers import validate_nfe_access_key

from .models import NFe, NFeLine, NFeIssuer, NFeReceiver, NFeInvoice, NFeTaxLineIcmsSN, NFeTaxLineIpi, \
    NFeTaxLinePis, NFeTaxLineCofins, NFeComplementaryInformation, NFeShipping, NFeShippingVolume, NFeEvent, \
    NFeStatus


class NFeOperationSelectForm(forms.Form):
    """
    Form which is used before creating a NFe to select it's operation
    """
    operation = forms.ModelChoiceField(label=u'Operação',
                                       widget=OperationSelect(),
                                       queryset=Operation.on_company.all())


class NFeEventForm(forms.Form):
    """
    Form which is used before creating a NFe to select it's operation
    """
    nfe_id = forms.IntegerField(widget=forms.HiddenInput(), required=True)


class NFeUpdateFinalize(forms.ModelForm):

    class Meta:
        model = NFe
        fields = ['id', ]

class NFeThirdInCreate(forms.ModelForm):
    operation = forms.ModelChoiceField(label=u'Operação', widget=forms.ChoiceDayLabelText(), queryset=Operation.on_company.all())
    number = forms.IntegerField(label=u'Número',)

    document_issuer = forms.ModelChoiceField(label=u'Remetente',
                                             widget=SupplierSelect(attrs={'placeholder': 'Selecione um Remetente ...',
                                                                          'data-min-input-length': '1'}),
                                             queryset=Supplier.on_company.editable())


    nfe_payment_method = forms.ModelChoiceField(label=u'Método de Pagamento',
                                                widget=PaymentMethodSelect(),
                                                queryset=PaymentMethod.on_company.all())

    document_date = forms.DateTimeField(label=u'Data de Emissão',
                                        input_formats=['%d/%m/%Y'],
                                        widget=forms.NextifyDateInput(attrs={'data-date-format': 'dd/mm/yyyy'},
                                                                      format='%d/%m/%Y'))
    document_release_date = forms.DateTimeField(label=u'Data de Entrada',
                                                input_formats=['%d/%m/%Y'],
                                                widget=forms.NextifyDateInput(attrs={'data-date-format': 'dd/mm/yyyy'},
                                                                              format='%d/%m/%Y'))


    helper = FormHelper()
    helper.form_tag = False

    helper.layout = Layout(
        Div(
            #HTML(u'<h3 class="form-section">Emissão de NFe de Entrada</h3>'),
            Row(
                Colmd6(Field('operation', css_class='form-control-static')),
                Colmd6(Field('document_issuer', css_class='form-control')),
            ),
            Row(
                Colmd1(Field('number', css_class='form-control')),
                Colmd1(Field('series', css_class='form-control')),
                Colmd2(Field('document_date', css_class='form-control dateinput', readonly=True)),
                Colmd2(Field('document_release_date', css_class='form-control dateinput', readonly=True)),
                Colmd6(Field('nfe_access_key', css_class='form-control')),
            ),
            Row(
                # Colmd4(Field('price_table', css_class='form-control')),
                Colmd3(Field('nfe_payment_method', css_class='form-control')),
                Colmd3(Field('nfe_freight', css_class='form-control')),
            ),
            css_class='form-body',
        ),
        #FormActions(Submit('submit', 'Salvar', css_class='btn green')),
    )

    class Meta:
        model = NFe
        fields = ['operation', 'number', 'series', 'document_issuer', 'document_date', 'document_release_date',
                  'nfe_freight', 'nfe_payment_method', 'nfe_access_key', ]

    def clean(self):
        cleaned_data = super(NFeThirdInCreate, self).clean()
        document_issuer = cleaned_data.get('document_issuer')
        access_key = cleaned_data.get('nfe_access_key')
        if not access_key:
            msg = u'Este campo é obrigatório.'
            self.add_error('nfe_access_key', msg)
        else:
            if not str(access_key).isdigit():
                msg = u'Chave de Acesso inválida. Campo deve conter somente números.'
                self.add_error('nfe_access_key', msg)
            else:
                if len(access_key) != 44:
                    msg = u'Chave de Acesso inválida. A chave deve possuir 44 dígitos.'
                    self.add_error('nfe_access_key', msg)
                elif not validate_nfe_access_key(access_key=access_key):
                    msg = u'Chave de Acesso inválida. O cálculo do dígito verificador falhou.'
                    self.add_error('nfe_access_key', msg)

        if not document_issuer.has_billing_or_default_address():
            msg = u'Remetente não possui endereço de faturamento ou um endereço padrão cadastrado.'
            self.add_error('document_issuer', msg)

        return cleaned_data


class NFeOutCreate(forms.ModelForm):
    operation = forms.ModelChoiceField(label=u'Operação', widget=forms.ChoiceDayLabelInput(), queryset=Operation.on_company.all())
    number = forms.Field(label=u'Número', widget=forms.NumberInput())

    document_receiver = forms.ModelChoiceField(label=u'Destinatário',
                                             widget=CustomerSelect(attrs={'placeholder': 'Selecione um Destinatário ...',
                                                                          'data-min-input-length': '1'}),
                                             queryset=Customer.on_company.editable())


    nfe_payment_method = forms.ModelChoiceField(label=u'Método de Pagamento',
                                                widget=PaymentMethodSelect(),
                                                queryset=PaymentMethod.on_company.all())

    price_table = forms.ModelChoiceField(label=u'Tabela de Preços',
                                                widget=PriceTableSelect(),
                                                queryset=PriceTable.on_company.all())


    document_date = forms.DateTimeField(label=u'Data de Emissão',
                                        input_formats=['%d/%m/%Y'],
                                        widget=forms.NextifyDateInput(attrs={'data-date-format': 'dd/mm/yyyy'},
                                                                      format='%d/%m/%Y'))
    document_release_date = forms.DateTimeField(label=u'Data de Saída',
                                                input_formats=['%d/%m/%Y'],
                                                widget=forms.NextifyDateInput(attrs={'data-date-format': 'dd/mm/yyyy'},
                                                                              format='%d/%m/%Y'))
    helper = FormHelper()
    helper.form_tag = False

    helper.layout = Layout(
        Div(
            #HTML(u'<h3 class="form-section">Emissão de NFe de Entrada</h3>'),
            Row(
                Colmd6(Field('operation', css_class='form-control', disabled='disabled')),
            ),
            Row(
                Colmd6(Field('document_receiver', css_class='form-control')),
            ),
            Row(
                Colmd1(Field('number', css_class='form-control', readonly=True)),
                Colmd1(Field('series', css_class='form-control', readonly=True)),
                Colmd2(Field('document_date', css_class='form-control dateinput', readonly=True)),
                Colmd2(Field('document_release_date', css_class='form-control dateinput', readonly=True)),
                Colmd3(Field('price_table', css_class='form-control')),
                Colmd3(Field('nfe_payment_method', css_class='form-control')),
            ),
            Row(
                Colmd4(Field('nfe_freight', css_class='form-control')),
                Colmd4(Field('nfe_final_consumer', css_class='form-control')),
                Colmd4(Field('nfe_consumer_presence', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('nfe_access_key', css_class='form-control', readonly=True)),
            ),
            css_class='form-body',
        ),
    )

    class Meta:
        model = NFe
        fields = ['operation', 'number', 'series', 'document_receiver', 'document_date', 'document_release_date',
                  'nfe_freight', 'nfe_payment_method', 'nfe_access_key', 'price_table', 'nfe_final_consumer',
                  'nfe_consumer_presence', ]


class NFeLineThirdInCreate(forms.ModelForm):
    product = forms.ModelChoiceField(label=u'Produto',
                                     widget=ProductSelect(attrs={'placeholder': 'Selecione um Produto ...',
                                                                 'data-min-input-length': '1'}),
                                     queryset=Product.on_company.all())

    cfop = forms.ModelChoiceField(label=u'CFOP',
                                  required=True,
                                  widget=CfopSelect(attrs={'placeholder': 'Selecione uma CFOP ...'}),
                                  queryset=Cfop.objects.all())

    ncm_classification = forms.ModelChoiceField(label=u'Class. NCM',
                                                widget=NcmSelect(attrs={'placeholder': 'Selecione uma NCM ...',
                                                                 'data-min-input-length': '1'}),
                                                queryset=NcmClassification.objects.all())

    unit = forms.ModelChoiceField(label=u'Unidade', required=False,
                                     widget=ProductUnitSelect(attrs={'placeholder': 'Selecione uma Unidade ...'}),
                                     queryset=Unit.on_company.all())

    helper = FormHelper()
    helper.form_tag = False

    helper.layout = Layout(
        Div(
            Row(
                Div(Field('product', css_class='form-control'), css_class='col-md-6'),
                Div(Field('cfop', css_class='form-control'), css_class='col-md-6'),
            ),
            Row(
                Div(Field('ncm_classification', css_class='form-control'), css_class='col-md-6'),
                Div(Field('complementary_description', css_class='form-control'), css_class='col-md-6'),
            ),
            Row(
                Div(Field('quantity', css_class='form-control'), css_class='col-md-3'),
                Div(Field('unit', css_class='form-control'), css_class='col-md-3'),
                Div(Field('unitary_value', css_class='form-control'), css_class='col-md-3'),
                Div(Field('discount_value', css_class='form-control'), css_class='col-md-3'),
            ),
            Row(
                Div(Field('insurance_value', css_class='form-control'), css_class='col-md-3'),
                Div(Field('freight_value', css_class='form-control'), css_class='col-md-3'),
                Div(Field('expenses_value', css_class='form-control'), css_class='col-md-3'),
                Div(Field('goods_value', css_class='form-control', readonly=True), css_class='col-md-3'),
            ),
            css_class='form-body'
        ),
    )

    class Meta:
        model = NFeLine
        fields = ['product', 'cfop', 'ncm_classification', 'complementary_description',
                  'quantity', 'unit', 'unitary_value', 'goods_value',
                  'discount_value', 'insurance_value', 'freight_value', 'expenses_value',]


class MyBaseInlineFormSet(BaseInlineFormSet):
    def add_fields(self, form, index):
        super(MyBaseInlineFormSet, self).add_fields(form, index)
        # Pop the delete field out of the OrderedDict
        delete_field = form.fields.pop('DELETE')
        # Add it at the start
        form.fields = OrderedDict(
                      [('DELETE', delete_field)] + form.fields.items())
        form.fields['DELETE'].label = 'Exc.'


class NFeLineForm(forms.ModelForm):
    sequence = forms.IntegerField(label=u'#', widget=forms.DayLabelText(), required=False)
    product = forms.ModelChoiceField(label=u'Produto',
                                     widget=ProductSelect(attrs={'placeholder': 'Selecione um Produto ...',
                                                                 'data-min-input-length': '1'}),
                                     queryset=Product.on_company.all())

    cfop = forms.ModelChoiceField(label=u'CFOP', required=True,
                                  widget=CfopSelect(attrs={'placeholder': 'Selecione uma CFOP ...'}),
                                  queryset=Cfop.objects.all())

    unit = forms.ModelChoiceField(label=u'Unidade', required=False,
                                     widget=ProductUnitSelect(attrs={'placeholder': 'Selecione uma Unidade ...'}),
                                     queryset=Unit.on_company.all())

    link = forms.CharField(label='', required=False)

    class Meta:
        model = NFeLine
        fields = ['sequence', 'product', 'cfop', 'quantity', 'unit', 'unitary_value', 'goods_value']

    def __init__(self, *args, **kwargs):
        super(NFeLineForm, self).__init__(*args, **kwargs)
        # instance is always available, it just does or doesn't have pk.
        self.fields['link'].widget = ModelLinkWidget(self.instance)

NFeLineFormSet = inlineformset_factory(NFe, NFeLine, form=NFeLineForm,  min_num=0, extra=1, formset=MyBaseInlineFormSet,
                                       fields=['sequence', 'product', 'cfop', 'quantity', 'unit',
                                               'unitary_value', 'goods_value'])


class NFeLineInlineHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(NFeLineInlineHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.form_id = 'nfeline'
        self.form_show_labels = False
        self.disable_csrf = True
        self.layout = Layout(
            NextifyInlineField('DELETE'),
            NextifyInlineField('sequence'),
            NextifyInlineField('product', wrapper_class='col-md-3'),
            NextifyInlineField('cfop', wrapper_class='col-md-3'),
            NextifyInlineField('quantity', wrapper_class='col-md-1', css_class='form-control'),
            NextifyInlineField('unit', wrapper_class='col-md-2'),
            NextifyInlineField('unitary_value', wrapper_class='col-md-2', css_class='form-control'),
            NextifyInlineField('goods_value', wrapper_class='col-md-1', css_class='form-control', readonly=True),
            NextifyInlineField('link'),
        )
        self.render_required_fields = True
        self.render_hidden_fields = True


class NFeIssuerForm(forms.ModelForm):
    issuer_address = forms.ModelChoiceField(label=u'Endereço',
                                            widget=AddressSelect(attrs={'placeholder': 'Selecione um Endereço ...',
                                                                 'data-min-input-length': '0'}),
                                            queryset=Address.on_company.all())
    city = forms.ModelChoiceField(label=u'Cidade',
                                     widget=CitySelect(attrs={'placeholder': 'Selecione uma Cidade ...',
                                                                 'data-min-input-length': '1'}),
                                     queryset=City.objects.all())

    helper = FormHelper()
    helper.form_tag = False
    helper.render_required_fields = True
    helper.render_hidden_fields = True
    helper.disable_csrf = True
    helper.layout = Layout(
        HTML(u'<h3 class="form-section">Dados do Remetente</h3>'),
        Row(
            Colmd6(Field('document_number', readonly=True))
        ),
        Row(
            Colmd6(Field('name')),
            Colmd6(Field('fancy_name')),
        ),
        HTML(u'<h3 class="form-section">Endereço do Remetente</h3>'),
        Row(
            Colmd6(Field('issuer_address', css_class='issuer_address'))
        ),
        Row(
            Colmd6(Field('street')),
            Colmd3(Field('number')),
            Colmd3(Field('complement')),
        ),
        Row(
            Colmd6(Field('city')),
            Colmd3(Field('province')),
            Colmd3(Field('zipcode')),
        ),
    )


    class Meta:
        model = NFeIssuer
        fields = ['document_number', 'name', 'fancy_name',
                  'issuer_address', 'street', 'number', 'complement', 'province', 'zipcode', 'city']

NFeIssuerFormSet = inlineformset_factory(NFe, NFeIssuer, form=NFeIssuerForm, min_num=0, max_num=1, can_delete=False)


class NFeReceiverForm(forms.ModelForm):
    receiver_address = forms.ModelChoiceField(label=u'Endereço',
                                              widget=AddressSelect(attrs={'placeholder': 'Selecione um Endereço ...',
                                                                          'data-min-input-length': '0',
                                                                          }),
                                              queryset=Address.on_company.all())
    city = forms.ModelChoiceField(label=u'Cidade',
                                     widget=CitySelect(attrs={'placeholder': 'Selecione uma Cidade ...',
                                                                 'data-min-input-length': '1'}),
                                     queryset=City.objects.all())

    helper = FormHelper()
    helper.form_tag = False
    helper.render_required_fields = True
    helper.render_hidden_fields = True
    helper.disable_csrf = True
    helper.layout = Layout(
        HTML(u'<h3 class="form-section">Dados do Destinatário</h3>'),
        Row(
            Colmd6(Field('document_number', readonly=True))
        ),
        Row(
            Colmd6(Field('name')),
            Colmd6(Field('fancy_name')),
        ),
        HTML(u'<h3 class="form-section">Endereço do Remetente</h3>'),
        Row(
            Colmd6(Field('receiver_address', css_class='receiver_address'))
        ),
        Row(
            Colmd6(Field('street')),
            Colmd3(Field('number')),
            Colmd3(Field('complement')),
        ),
        Row(
            Colmd6(Field('city')),
            Colmd3(Field('province')),
            Colmd3(Field('zipcode')),
        ),
    )


    class Meta:
        model = NFeReceiver
        fields = ['document_number', 'name', 'fancy_name',
                  'receiver_address', 'street', 'number', 'complement', 'province', 'zipcode', 'city']

NFeReceiverFormSet = inlineformset_factory(NFe, NFeReceiver, form=NFeReceiverForm, min_num=0, max_num=1, can_delete=False)


class NFeInvoiceForm(forms.ModelForm):
    sequence = forms.IntegerField(label=u'#', widget=forms.DayLabelText(), required=False)

    invoice_due_date = forms.DateTimeField(label=u'Data de Vencimento',
                                           input_formats=['%d/%m/%Y'],
                                           widget=forms.NextifyDateInput(attrs={'data-date-format': 'dd/mm/yyyy'},
                                                                         format='%d/%m/%Y'))

    class Meta:
        model = NFeInvoice
        fields = ['invoice_due_date', 'original_value', 'discount_value', 'net_value', 'sequence', ]


NFeInvoiceFormSet = inlineformset_factory(NFe, NFeInvoice, form=NFeInvoiceForm, min_num=0, extra=1,
                                          formset=MyBaseInlineFormSet,
                                          fields=['sequence', 'invoice_due_date', 'original_value', 'discount_value', 'net_value', ]
                                          )


class NFeInvoiceInlineHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(NFeInvoiceInlineHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.form_id = 'nfeinvoice'
        self.form_show_labels = False
        self.disable_csrf = True

        self.layout = Layout(
            NextifyInlineField('DELETE'),
            NextifyInlineField('sequence'),
            NextifyInlineField('invoice_due_date', css_class='form-control dateinput',
                               wrapper_class='col-md-3', readonly=True),
            NextifyInlineField('original_value', wrapper_class='col-md-3', css_class='form-control'),
            NextifyInlineField('discount_value', wrapper_class='col-md-3', css_class='form-control'),
            NextifyInlineField('net_value', wrapper_class='col-md-3', css_class='form-control', readonly=True),
        )
        self.render_required_fields = True
        self.render_hidden_fields = True


class NFeTaxLineIcmsSNForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_tag = False
    helper.render_required_fields = True
    helper.render_hidden_fields = True
    helper.disable_csrf = True
    helper.layout = Layout(
        HTML(u'<h3 class="form-section">ICMS Simples Nacional</h3>'),
        Row(
            Div(Field('csosn', css_class='form-control'), css_class='col-md-6')
        ),
        Row(
            Div(Field('tax_base_value', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_rate', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_value', css_class='form-control'), css_class='col-md-3'),
        ),
        HTML(u'<h3 class="form-section">ICMS ST</h3>'),
        Row(
            Div(Field('tax_base_st_value', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_base_mva', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_rate_st', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_value_st', css_class='form-control'), css_class='col-md-3'),
        ),
    )

    class Meta:
        model = NFeTaxLineIcmsSN
        fields = ['csosn', 'tax_base_value', 'tax_rate', 'tax_value',
                  'tax_base_st_value', 'tax_base_mva', 'tax_rate_st', 'tax_value_st', ]

NFeTaxLineIcmsSNFormSet = inlineformset_factory(NFeLine, NFeTaxLineIcmsSN, form=NFeTaxLineIcmsSNForm,
                                         min_num=0, max_num=1, can_delete=False)


class NFeTaxLineIpiForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_tag = False
    helper.render_required_fields = True
    helper.render_hidden_fields = True
    helper.disable_csrf = True
    helper.layout = Layout(
        HTML(u'<h3 class="form-section">IPI</h3>'),
        Row(
            Div(Field('cst', css_class='form-control'), css_class='col-md-6')
        ),
        Row(
            Div(Field('tax_base_value', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_rate', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_value', css_class='form-control'), css_class='col-md-3'),
        ),
    )

    class Meta:
        model = NFeTaxLineIpi
        fields = ['cst', 'tax_base_value', 'tax_rate', 'tax_value', ]

NFeTaxLineIpiFormSet = inlineformset_factory(NFeLine, NFeTaxLineIpi, form=NFeTaxLineIpiForm,
                                             min_num=0, max_num=1, can_delete=False)


class NFeTaxLinePisForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_tag = False
    helper.render_required_fields = True
    helper.render_hidden_fields = True
    helper.disable_csrf = True
    helper.layout = Layout(
        HTML(u'<h3 class="form-section">PIS</h3>'),
        Row(
            Div(Field('cst', css_class='form-control'), css_class='col-md-6')
        ),
        Row(
            Div(Field('tax_base_value', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_rate', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_value', css_class='form-control'), css_class='col-md-3'),
        ),
    )

    class Meta:
        model = NFeTaxLineIpi
        fields = ['cst', 'tax_base_value', 'tax_rate', 'tax_value', ]

NFeTaxLinePisFormSet = inlineformset_factory(NFeLine, NFeTaxLinePis, form=NFeTaxLinePisForm,
                                             min_num=0, max_num=1, can_delete=False)


class NFeTaxLineCofinsForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_tag = False
    helper.render_required_fields = True
    helper.render_hidden_fields = True
    helper.disable_csrf = True
    helper.layout = Layout(
        HTML(u'<h3 class="form-section">COFINS</h3>'),
        Row(
            Div(Field('cst', css_class='form-control'), css_class='col-md-6')
        ),
        Row(
            Div(Field('tax_base_value', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_rate', css_class='form-control'), css_class='col-md-3'),
            Div(Field('tax_value', css_class='form-control'), css_class='col-md-3'),
        ),
    )

    class Meta:
        model = NFeTaxLineCofins
        fields = ['cst', 'tax_base_value', 'tax_rate', 'tax_value', ]

NFeTaxLineCofinsFormSet = inlineformset_factory(NFeLine, NFeTaxLineCofins, form=NFeTaxLineCofinsForm,
                                             min_num=0, max_num=1, can_delete=False)


class NFeComplementaryInformationForm(forms.ModelForm):
    additional_information = forms.CharField(label=u'Informações Complementares',
                                             widget=forms.Textarea(attrs={'rows': '4'}), required=False)
    additional_information_auto = forms.CharField(label=u'Informações Complemetares (Geradas pelo sistema)',
                                                  widget=forms.Textarea(attrs={'rows': '4', 'readonly': True}),
                                                  required=False)

    helper = FormHelper()
    helper.form_tag = False
    helper.render_required_fields = True
    helper.render_hidden_fields = True
    helper.disable_csrf = True
    helper.layout = Layout(
        #HTML(u'<h3 class="form-section">Informações Complementares</h3>'),
        Row(
            Div(Field('additional_information', css_class='form-control'), css_class='col-md-6'),
            Div(Field('additional_information_auto', css_class='form-control'), css_class='col-md-6'),
        ),
    )

    class Meta:
        model = NFeComplementaryInformation
        fields = ['additional_information', 'additional_information_auto', ]

NFeComplementaryInformationFormSet = inlineformset_factory(NFe, NFeComplementaryInformation,
                                                           form=NFeComplementaryInformationForm,
                                                           min_num=0, max_num=1, can_delete=False)


class NFeShippingForm(forms.ModelForm):
    shipping_company = forms.ModelChoiceField(label=u'Transportadora',
                                              widget=ShippingCompanySelect(attrs={'placeholder': 'Selecione uma Transportadora ...',
                                                                                  'data-min-input-length': '1'}),
                                              queryset=ShippingCompany.on_company.editable(),
                                              required=False)

    city = forms.ModelChoiceField(label=u'Cidade/Estado',
                                  widget=CitySelect(attrs={'placeholder': 'Selecione uma Cidade ...',
                                                           'data-min-input-length': '1'}),
                                  queryset=City.objects.all(),
                                  required=False)

    vehicle_state = forms.ModelChoiceField(label=u'Estado',
                                           widget=StateSelect(attrs={'placeholder': 'Selecione um Estado ...',
                                                                    'data-min-input-length': '1'}),
                                           queryset=State.objects.all(),
                                           required=False)

    vehicle_license_plate = forms.CharField(label=u'Placa do Veículo',
                                            help_text=u'Formatos aceitos: XXX9999, XXX999, XX9999 ou XXXX999.',
                                            required=False)

    helper = FormHelper()
    helper.form_tag = False
    helper.render_required_fields = True
    helper.render_hidden_fields = True
    helper.disable_csrf = True
    helper.layout = Layout(
        HTML(u'<h3 class="form-section">Transportadora</h3>'),
        Row(
            Div(Field('shipping_company', css_class='form-control'), css_class='col-md-6'),

        ),
        Row(
            Div(Field('document_number', css_class='form-control'), css_class='col-md-6'),
            Div(Field('name', css_class='form-control'), css_class='col-md-6'),
        ),
        Row(
            Div(Field('address_sumary', css_class='form-control'), css_class='col-md-6'),
            Div(Field('state_registry', css_class='form-control'), css_class='col-md-3'),
            Div(Field('city', css_class='form-control'), css_class='col-md-3'),
        ),
        HTML(u'<h3 class="form-section">Veículo</h3>'),
        Row(
            Div(Field('vehicle_license_plate', css_class='form-control'), css_class='col-md-4'),
            Div(Field('vehicle_state', css_class='form-control'), css_class='col-md-4'),
            Div(Field('vehicle_rntc', css_class='form-control'), css_class='col-md-4'),
        ),
    )

    class Meta:
        model = NFeShipping
        fields = ['shipping_company', 'document_number', 'name', 'state_registry',
                  'address_sumary', 'city', 'vehicle_license_plate', 'vehicle_state', 'vehicle_rntc', ]

    def clean(self):
        cleaned_data = super(NFeShippingForm, self).clean()
        document_number = cleaned_data.get('document_number')
        city = cleaned_data.get('city')
        state_registry = cleaned_data.get('state_registry')

        if document_number:
            if not validate_cpf(document_number) and not validate_cnpj(document_number):
                msg = u'CPF/CNPJ Inválido.'
                self.add_error('document_number', msg)

        if state_registry and not city:
            msg = u'Obrigatório preencher Cidade/Estado ao preencher a Incrição Estadual.'
            self.add_error('city', msg)

        return cleaned_data

    def clean_document_number(self):
        data = self.cleaned_data.get('document_number')
        if data:
            if len(data) <= 11:
                data = str(data).zfill(11)
            elif len(data) <= 14:
                data = str(data).zfill(14)
        return data

NFeShippingFormSet = inlineformset_factory(NFe, NFeShipping, form=NFeShippingForm,
                                           min_num=0, max_num=1, can_delete=True)

class NFeShippingVolumeForm(forms.ModelForm):

    class Meta:
        model = NFeShipping
        fields = ['shipping_company', 'document_number', 'name', 'state_registry',
                  'address_sumary', 'city', 'vehicle_license_plate', 'vehicle_state', 'vehicle_rntc', ]


NFeShippingVolumeInlineFormSet = inlineformset_factory(NFe, NFeShippingVolume, form=NFeShippingVolumeForm,
                                                       min_num=0, extra=1, formset=MyBaseInlineFormSet,
                                                       fields=['volume_quantity', 'volume_especies', 'volume_mark',
                                                               'volume_number', 'net_weight', 'gross_weight', ]
                                                       )


class NFeShippingVolumeInlineHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(NFeShippingVolumeInlineHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.form_id = 'nfe_shipping_volumes'
        self.form_show_labels = False
        self.disable_csrf = True
        self.render_required_fields = True
        self.render_hidden_fields = True
        self.layout = Layout(
            NextifyInlineField('DELETE'),
            NextifyInlineField('volume_quantity', wrapper_class='col-md-2', css_class='form-control'),
            NextifyInlineField('volume_especies', wrapper_class='col-md-2', css_class='form-control'),
            NextifyInlineField('volume_mark', wrapper_class='col-md-2', css_class='form-control'),
            NextifyInlineField('volume_number', wrapper_class='col-md-2', css_class='form-control'),
            NextifyInlineField('net_weight', wrapper_class='col-md-2', css_class='form-control'),
            NextifyInlineField('gross_weight', wrapper_class='col-md-2', css_class='form-control'),
        )


class NFeCancelForm(forms.ModelForm):
    message = forms.CharField(label=u'Justificativa para o cancelamento',
                              widget=forms.Textarea(attrs={'rows': '4'}),
                              required=True)

    helper = FormHelper()
    helper.render_hidden_fields = True
    helper.render_required_fields = True
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Row(
                Div(Field('message', css_class='form-control'), css_class='col-md-6'),
            ),
            css_class='form-body'
        ),
    )

    class Meta:
        model = NFeEvent
        fields = ['message', ]

    # def clean(self):
    #     cleaned_data = super(NFeCancelForm, self).clean()
    #     message = cleaned_data.get('message')
    #
    #     if len(message) < 15:
    #         msg = u'A Justificativa deve possuir no mínimo 15 caracteres.'
    #         self.add_error('message', msg)
    #
    #     if len(message) > 255:
    #         msg = u'A Justificativa deve possuir no máximo 255 caracteres.'
    #         self.add_error('message', msg)
    #
    #     return cleaned_data


class NFeCancelConfirmForm(forms.ModelForm):
    event_type = forms.CharField(label=u'Tipo do Evento',
                                 widget=forms.ChoiceDayLabelInput(choices=NFeEventType.choices(),
                                                                  attrs={'readonly': True}))
    message = forms.CharField(label=u'Justificativa',
                              widget=forms.Textarea(attrs={'rows': '4'}),
                              required=True)

    event_status = forms.ModelChoiceField(label=u'Situação',
                                          widget=forms.ChoiceDayLabelInput(choices=NFeEventType.choices(),
                                                                           attrs={'readonly': True}),
                                          queryset=NFeStatus.objects.all())

    helper = FormHelper()
    helper.render_hidden_fields = True
    helper.render_required_fields = True
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Row(
                Div(Field('event_type', css_class='form-control'), css_class='col-md-3'),
            ),
            Row(
                Div(Field('event_date', css_class='form-control', readonly=True), css_class='col-md-3'),
                Div(Field('sequence', css_class='form-control', readonly=True), css_class='col-md-3'),
            ),
            Row(
                Div(Field('event_registry_date', css_class='form-control', readonly=True), css_class='col-md-3'),
                Div(Field('protocol_number', css_class='form-control', readonly=True), css_class='col-md-3'),
            ),
            Row(
                Div(Field('message', css_class='form-control'), css_class='col-md-6'),
            ),
            Row(
                Div(Field('event_status', css_class='form-control'), css_class='col-md-6'),
            ),
            css_class='form-body'
        ),
    )

    class Meta:
        model = NFeEvent
        fields = ['message', 'event_status', 'event_date', 'event_registry_date',
                  'protocol_number', 'sequence', 'event_type']

    # def clean(self):
    #     cleaned_data = super(NFeCancelForm, self).clean()
    #     message = cleaned_data.get('message')
    #
    #     if len(message) < 15:
    #         msg = u'A Justificativa deve possuir no mínimo 15 caracteres.'
    #         self.add_error('message', msg)
    #
    #     if len(message) > 255:
    #         msg = u'A Justificativa deve possuir no máximo 255 caracteres.'
    #         self.add_error('message', msg)
    #
    #     return cleaned_data


class NFeCceForm(forms.ModelForm):
    message = forms.CharField(label=u'Correção',
                              widget=forms.Textarea(attrs={'rows': '4'}),
                              required=True)

    helper = FormHelper()
    helper.render_hidden_fields = True
    helper.render_required_fields = True
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Row(
                Div(Field('message', css_class='form-control'), css_class='col-md-6'),
            ),
            css_class='form-body'
        ),
    )

    class Meta:
        model = NFeEvent
        fields = ['message', ]

    def clean(self):
        cleaned_data = super(NFeCancelForm, self).clean()
        message = cleaned_data.get('message')

        if len(message) < 15:
            msg = u'A Correção deve possuir no mínimo 15 caracteres.'
            self.add_error('message', msg)

        if len(message) > 1000:
            msg = u'A Correção deve possuir no máximo 1000 caracteres.'
            self.add_error('message', msg)

        return cleaned_data

