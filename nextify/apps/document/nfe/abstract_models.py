# -*- coding: utf-8 -*-
from datetime import datetime
from django.db import models, transaction
from django.db.models import Sum
from django.apps import apps
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from nextify.apps.document.models import *
from nextify.apps.sequence import get_next_model_sequence
from ibptws.calculadoras import DeOlhoNoImposto
from nextify.apps.person.managers import CompanyManager

from nextify.core.enum.nfe import *
from nextify.core.enum.person import *
from nextify.core.enum.operation import *
from nextify.core.enum.document import *
from nextify.core.enum.stock import *
from nextify.core.fields import NextifyPositionField
from nextify.core.util.filters import *


class AbstractNFe(FiscalDocument):

    nfe_number = models.TextField("Random NFe Number", max_length=8,
                                  default='00000000', blank=False)

    nfe_finality = models.SmallIntegerField(_('Nfe Finality'), null=False, choices=NFeFinality.choices(),
                                            editable=False)
    nfe_freight = models.SmallIntegerField(u'Mod. Frete', choices=NFeModFrete.choices(), null=True)

    nfe_payment_method = models.ForeignKey('payment.PaymentMethod', null=False)

    nfe_final_consumer = models.IntegerField(u'Ind. Consumidor final',
                                                  choices=NFeIndFinalConsumer.choices(), null=True)
    nfe_consumer_presence = models.IntegerField(u'Ind. Presenca Consumidor',
                                                     choices=NFeIndConsumerPresence.choices(), null=True)
    nfe_access_key = models.CharField(u'Chave de Acesso', max_length=44, blank=True)
    nfe_access_key_dv = models.CharField('NFe Access Key DV', max_length=1, blank=True)
    nfe_layout_version = models.CharField('NFe Layout Version', max_length=4,
                                          default=settings.NFE_VERSAO_LAYOUT, editable=False)
    nfe_application_version = models.CharField('NFe App. Version', max_length=20,
                                               default=settings.NFE_VERSAO_APP, editable=False)
    nfe_issuer_state_code = models.CharField('Issuer State Code', max_length=2, blank=False, editable=False)
    nfe_issuer_city_code = models.CharField('Issuer City Code', max_length=7, blank=False, editable=False)
    nfe_danfe_print_type = models.IntegerField('DANFE Print Type',
                                               choices=NFeDANFEPrintType.choices(), null=False,
                                               default=NFeDANFEPrintType.PORTRAIT, editable=False)
    nfe_issue_type = models.IntegerField('Nfe Issue Type',
                                         choices=NFeIssueType.choices(), null=False,
                                         default=NFeIssueType.NORMAL, editable=False)

    nfe_status = models.ForeignKey('nfe.NFeStatus', null=True)
    nfe_status_description = models.CharField(u'Descrição Status NFe', blank=True, max_length=500)
    nfe_status_date = models.DateTimeField(u'Data do Status NFe', null=True)
    nfe_digest_value = models.CharField(u'Digest Value', max_length=255, blank=True)

    # Controle de Envio de NF
    nfe_batch_number = models.CharField(u'Lote de NFe', max_length=255, blank=True)
    nfe_receipt_number = models.CharField(u'Recibo do Lote', max_length=255, blank=True)
    nfe_authorization_protocol = models.CharField(u'Protocolo de Autorização', max_length=255, blank=True)
    nfe_authorization_date = models.DateTimeField(u'Data de Autorização', null=True)

    nfe_ibpt_message = models.CharField(u'Total Tributos', max_length=255, blank=True)


    __person_changed = False

    class Meta:
        abstract = True

    def __unicode__(self):
        return 'NFe%s' % self.nfe_access_key

    @property
    def portlet(self):
        operation = self.operation
        if operation.issuer_type == OperationIssuer.THIRD:
            return 'NFe #%s | %s' % (self.number_series, self.document_issuer.document_number, self.document_issuer.name)
        else:
            receiver = self.third_info
            if operation.model == 65:
                return 'NFCe #%s | %s' % (self.number_series, receiver)
            else:
                return 'NFe #%s | %s' % (self.number_series, receiver)

    @models.permalink
    def get_absolute_url(self):
        if self.operation_type == OperationType.IN:
            return ('nfe:in-update', (), {'pk': self.pk})
        elif self.operation_type == OperationType.OUT:
            if self.model == 55:
                return ('nfe:out-update', (), {'pk': self.pk})
            if self.model == 65:
                return ('nfce:out-update', (), {'pk': self.pk})

    @models.permalink
    def get_danfe_url(self):
        if self.model == 55:
            return 'nfe:danfe', (), {'pk': self.pk}
        if self.model == 65:
            return 'nfce:danfe', (), {'pk': self.pk}

    @property
    def third_info(self):
        if self.operation.issuer_type == OperationIssuer.THIRD:
            third = u'Emissor não Identificado'
            if hasattr(self, 'issuer'):
                if self.issuer.document_number and len(self.issuer.document_number):
                    third = u'%s' % cnpj_cpf(self.issuer.document_number)
                    if self.issuer.name and len(self.issuer.name):
                        third += u' - %s' % self.issuer.name
        else:
            third = u'Consumidor não Identificado'
            if hasattr(self, 'receiver'):
                if self.receiver.document_number and len(self.receiver.document_number):
                    third = u'%s' % cnpj_cpf(self.receiver.document_number)
                    if self.receiver.name and len(self.receiver.name):
                        third += u' - %s' % self.receiver.name

        return third

    def finalize(self):
        with transaction.atomic():
            self.document_status = DocumentStatus.FINALIZED
            self.set_number_series()
            self.finalize_invoice()
            # Efetua algumas operacoes diferentes para nota de emissao propria
            if self.issuer_type == OperationIssuer.SELF:
                self.set_legal_text()
                # Calcula Total de Tributos para consumidor final
                if self.nfe_final_consumer == NFeIndFinalConsumer.FINAL_CONSUMER and \
                                self.operation_type == OperationType.OUT:
                    self.set_taxes_value_ibpt()
                # Seta STATUS da NFe para "Pronto para envio"
                self.nfe_status_id = NFeStatusEnum.STAT002

            for line in self.lines.all():
                line.finalize()
                line.save()

    def reopen(self):
        with transaction.atomic():
            self.document_status = DocumentStatus.TYPING
            self.reopen_invoice()
            if self.issuer_type == OperationIssuer.SELF:
                # Seta STATUS da NFe para "NFe em digitação"
                self.nfe_status_id = NFeStatusEnum.STAT001
            for line in self.lines.all():
                line.reopen()
                line.save()

    def reopen_invoice(self):
        if hasattr(self, 'invoice_totals') and hasattr(self, 'invoices'):
            m_invoice = apps.get_model('invoice', 'Invoice')
            m_duplicates = apps.get_model('invoice', 'InvoiceDuplicate')
            m_ocurrences = apps.get_model('invoice', 'BankOcurrence')

            inv = m_invoice.on_company.filter(invoice_number=self.number,
                                              person=self.document_receiver,
                                              bank=self.operation.bank_account.bank,
                                              bank_account=self.operation.bank_account).first()


    def finalize_invoice(self):
        if hasattr(self, 'invoice_totals') and hasattr(self, 'invoices'):
            if self.operation.bank_account is not None:
                model = apps.get_model('invoice', 'Invoice')
                total = self.invoice_totals

                o, created = model.on_company.get_or_create(document=self,
                                                            invoice_number=self.number,
                                                            person=self.document_receiver,
                                                            bank=self.operation.bank_account.bank,
                                                            bank_account=self.operation.bank_account)

                if created:
                    o.gross_value = total.original_value
                    o.net_value = total.net_value
                    o.discount_value = total.discount_value
                    o.save()

                model = apps.get_model('invoice', 'InvoiceDuplicate')
                duplicates = self.invoices.all()

                for duplicate in duplicates:
                    d, created = model.on_company.get_or_create(invoice=o,
                                                                invoice_date=self.document_date,
                                                                invoice_due_date=duplicate.invoice_due_date)

                    if created:
                        d.gross_value = duplicate.original_value
                        d.net_value = duplicate.net_value
                        d.discount_value = duplicate.discount_value
                        d.save()






    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if 'document_issuer' in self.changed_fields or 'document_receiver' in self.changed_fields:
            self.__person_changed = True
        super(AbstractNFe, self).save(force_insert, force_update, using, update_fields)
        self.create_children()

    def set_initial_values(self):
        super(AbstractNFe, self).set_initial_values()
        operation = self.operation
        if not self.pk:
            self.document_type = DocumentType.NFE
            if self.operation_type == OperationType.OUT:
                self.nfe_status_id = NFeStatusEnum.STAT001
        if operation is not None or 'operation' in self.changed_fields:
            self.nfe_finality = operation.nfe.nfe_finality
        if operation.model == 65:
            self.initial_data_nfce()

    def initial_data_nfce(self):
        self.document_date = datetime.now()
        self.document_release_date = datetime.now()
        self.nfe_freight = NFeModFrete.NONE
        self.nfe_final_consumer = NFeIndFinalConsumer.FINAL_CONSUMER
        self.nfe_consumer_presence = NFeIndConsumerPresence.FACE_OPERATION

    def create_children(self):
        self.create_issuer()
        self.create_receiver()
        self.create_totals()
        self.create_tax_totals()

    def create_complementary_information(self, information, reset=False):
        model = apps.get_model('nfe', 'NFeComplementaryInformation')

        o, created = model.on_company.get_or_create(nfe=self.nfe)
        previous_info = o.additional_information_auto
        if not reset:
            information = '%s. %s.' % (previous_info, information)
            o.additional_information_auto = information
        else:
            o.additional_information_auto = information
        o.save()

    def create_issuer(self):
        model = apps.get_model('nfe', 'NFeIssuer')
        operation = self.operation
        person = None
        address = None

        if operation.issuer_type == OperationIssuer.SELF:
            address = self.company.company_person.billing_address()
            person = self.company.company_person
        elif operation.issuer_type == OperationIssuer.THIRD:
            if self.document_issuer.has_customer():
                person = self.document_issuer.customer
            elif self.document_issuer.has_supplier():
                person = self.document_issuer.supplier
            address = person.billing_address()

        if address is None:
            return

        defaults = {'issuer_address': address}
        nfe_issuer, created = model._default_manager.get_or_create(nfe=self, defaults=defaults)

        if created or self.__person_changed:
            if self.__person_changed:
                nfe_issuer.issuer_address = address
            nfe_issuer.document_number = person.document_number
            nfe_issuer.name = person.name
            nfe_issuer.fancy_name = person.fancy_name
            nfe_issuer.state_registry = person.state_registry
            nfe_issuer.city_registry = person.city_registry
            nfe_issuer.crt = person.crt
            nfe_issuer.save()

    def create_receiver(self):
        model = apps.get_model('nfe', 'NFeReceiver')
        operation = self.operation
        address = None
        person = None

        if operation.issuer_type == OperationIssuer.SELF:
            if self.document_receiver is not None:
                if self.document_receiver.has_customer():
                    person = self.document_receiver.customer
                elif self.document_receiver.has_supplier():
                    person = self.document_receiver.supplier
                address = person.billing_address()
        elif operation.issuer_type == OperationIssuer.THIRD:
            address = self.company.company_person.billing_address()
            person = self.company.company_person

        if address is None:
            return

        defaults = {'receiver_address': address}
        nfe_receiver, created = model._default_manager.get_or_create(nfe=self, defaults=defaults)

        if created or self.__person_changed:
            if self.__person_changed:
                nfe_receiver.receiver_address = address
            if settings.NFE_AMBIENTE == NFeEnvironment.TEST and operation.model == 55:
                nfe_receiver.category = PersonCategory.CORPORATE
                nfe_receiver.document_number = settings.NFE_HOMOLOG_CNPJ
                nfe_receiver.name = settings.NFE_HOMOLOG_RAZAO_SOCIAL
                nfe_receiver.state_registry = ''
            else:
                nfe_receiver.category = person.category
                nfe_receiver.document_number = person.document_number
                nfe_receiver.name = person.name
                nfe_receiver.state_registry = person.state_registry

            nfe_receiver.fancy_name = person.fancy_name
            nfe_receiver.city_registry = person.city_registry
            nfe_receiver.crt = person.crt
            nfe_receiver.save()

    def create_totals(self):
        model = apps.get_model('nfe', 'NFeTotals')
        nfe_totals, created = model._default_manager.get_or_create(nfe=self)
        nfe_totals.calculate()
        nfe_totals.save()


    def create_tax_totals(self):
        model = apps.get_model('nfe', 'NFeTaxTotals')
        nfe_totals, created = model._default_manager.get_or_create(nfe=self)
        nfe_totals.calculate()
        nfe_totals.save()

    def set_taxes_value_ibpt(self):
        calc = DeOlhoNoImposto()
        for line in self.lines.all():
            calc.produto(line.ncm_code, 0, line.net_value)
            line.ind_taxes_value = calc.total_tributos()
            line.save()
            calc.reiniciar()
        for line in self.lines.all():
            calc.produto(line.ncm_code, 0, line.net_value)

        obs = calc.observacao_complementar()

        if self.model == 55:
            obs = calc.observacao_complementar()
            self.create_complementary_information(obs, False)
        elif self.model == 65:
            obs = calc.observacao_complementar_nfce()
        self.nfe_ibpt_message = obs

    def set_legal_text(self):
        if self.model == 55:
            if settings.LEGAL_TEXT:
                self.create_complementary_information(settings.LEGAL_TEXT, True)

    def set_number_series(self):
        if self.number == 0:
            self.series, self.number = get_next_model_sequence(self.model)

    def get_nfe_status_display(self):
        return '%s - %s' % (self.nfe_status.code, self.nfe_status.description)

    @property
    def nfe_destination(self):
        issuer_address = self.issuer.issuer_address

        if not hasattr(self, 'receiver'):
            return NFeDestination.STATE
        receiver_address = self.receiver.receiver_address

        if receiver_address is None:
            return NFeDestination.STATE
        if receiver_address.state.acronym == 'EX':
            return NFeDestination.INTERNATIONAL
        if issuer_address.state_acronym == receiver_address.state_acronym:
            return NFeDestination.STATE
        else:
            return NFeDestination.INTERSTATE




class AbstractNFeIssuer(FiscalDocumentIssuer):
    nfe = models.OneToOneField('nfe.NFe', null=False, related_name='issuer')

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id or 'issuer_address' in self.changed_fields:
            self.set_defaults()
            self.set_location_fields()
        if 'city' in self.changed_fields:
            self.set_location_fields()
        super(AbstractNFeIssuer, self).save(force_insert, force_update, using, update_fields)

    def set_defaults(self):
        if self.issuer_address is None:
            return
        self.street = self.issuer_address.street
        self.number = self.issuer_address.number
        self.complement = self.issuer_address.complement
        self.province = self.issuer_address.province
        self.city = self.issuer_address.city
        self.zipcode = self.issuer_address.zipcode
        self.phone = self.issuer_address.phone

    def set_location_fields(self):
        if self.city is None:
            return
        state = self.city.state
        country = state.country
        self.city_ibge_code = self.city.ibge_code
        self.city_name = self.city.name
        self.state_ibge_code = state.ibge_code
        self.state_acronym = state.acronym
        self.state_name = state.name
        self.country_bacen_code = country.bacen_code
        self.country_name = country.name


class AbstractNFeReceiver(FiscalDocumentReceiver):
    nfe = models.OneToOneField('nfe.NFe', null=False, related_name='receiver')

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id or 'receiver_address' in self.changed_fields:
            self.set_defaults()
            self.set_location_fields()
        if 'city' in self.changed_fields:
            self.set_location_fields()
        super(AbstractNFeReceiver, self).save(force_insert, force_update, using, update_fields)

    def set_defaults(self):
        if self.receiver_address is None:
            return
        self.street = self.receiver_address.street
        self.number = self.receiver_address.number
        self.complement = self.receiver_address.complement
        self.province = self.receiver_address.province
        self.city = self.receiver_address.city
        self.zipcode = self.receiver_address.zipcode
        self.phone = self.receiver_address.phone

    def set_location_fields(self):
        if self.city is None:
            return
        state = self.city.state
        country = state.country
        self.city_ibge_code = self.city.ibge_code
        self.city_name = self.city.name
        self.state_ibge_code = state.ibge_code
        self.state_acronym = state.acronym
        self.state_name = state.name
        self.country_bacen_code = country.bacen_code
        self.country_name = country.name


class AbstractNFeQueue(models.Model):
    nfe = models.IntegerField('NFe', null=False)
    cnpj = models.CharField('', max_length=14, null=True)
    modelo = models.IntegerField('Modelo', null=True)
    operacao = models.IntegerField('', null=False)
    ambiente = models.IntegerField('', default=settings.NFE_AMBIENTE, null=False)
    chave_acesso = models.CharField('', max_length=44, null=True)
    status = models.IntegerField('', null=False, blank=True)
    mensagem = models.CharField('', max_length=255, blank=True)
    justificativa = models.CharField('', max_length=255, blank=True)
    protocolo = models.CharField('', max_length=255, blank=True)
    recibo = models.CharField('', max_length=255, blank=True)
    processada = models.CharField('', max_length=1, default='N')
    evento = models.IntegerField('Evento ID', null=True)
    sequencia_evento = models.IntegerField('Seq. Evento', null=True)
    data_processamento = models.DateTimeField('data proc.', null=True)

    class Meta:
        abstract = True


class AbstractNFeLine(FiscalLine):
    nfe = models.ForeignKey('nfe.NFe', null=False, related_name='lines')
    sequence = NextifyPositionField(collection='nfe')
    nfe_unitary_value = models.DecimalField(u'Valor Unitário NFe', editable=False,
                                            max_digits=21, decimal_places=10, null=False, default=0.0000000000)
    nfe_taxed_unitary_value = models.DecimalField(u'Valor Unitário NFe', editable=False,
                                                  max_digits=21, decimal_places=10, null=False, default=0.0000000000)
    ind_taxes_value = models.DecimalField(u'Total dos Tributos',
                                                  max_digits=15, decimal_places=2, null=False, default=0.00)
    ind_tot = models.SmallIntegerField('', null=False, choices=NFeIndTot.choices(), default=NFeIndTot.YES)

    class Meta:
        abstract = True

    def save(self):
        nfe = self.nfe
        self.set_units()
        self.set_initial_data()
        if nfe.issuer_type == OperationIssuer.SELF:
            self.set_pricing()
        self.set_goods_value()

        self.set_unitary_value()
        super(AbstractNFeLine, self).save()
        self.nfe.create_tax_totals()
        self.nfe.create_totals()

    @property
    def portlet(self):
        operation = self.nfe.operation
        nfe = self.nfe
        if operation.issuer_type == OperationIssuer.THIRD:
            return 'NFe #%s | %s - %s' % (
            nfe.number_series, nfe.document_issuer.document_number, nfe.document_issuer.name)
        else:
            return 'NFe #%s | %s - %s' % (
            nfe.number_series, nfe.document_receiver.document_number, nfe.document_receiver.name)


    @property
    def line_cost(self):
        return self.goods_value - self.discount_value

    @property
    def net_value(self):
        return self.goods_value - self.discount_value

    @models.permalink
    def get_absolute_url(self):
        operation = self.nfe.operation
        if operation.issuer_type == OperationIssuer.THIRD:
            return ('nfe:in-line-update', (), {'pk': self.pk, })
        else:
            if self.nfe.model == 55:
                return ('nfe:out-line-update', (), {'pk': self.pk, })
            if self.nfe.model == 65:
                return ('nfce:out-line-update', (), {'pk': self.pk,})

    def set_initial_data(self):
        item = None
        unit = None
        ncm = None
        if not self.id or 'product' in self.changed_fields:
            item = self.product
            if item is not None:
                self.item_code = item.code
                self.description = item.description
            else:
                return
            if 'unit' in self.changed_fields:
                unit = self.unit
            if unit is None:
                if self.nfe.operation.operation_type == OperationType.IN:
                    unit = self.product.purchase_unit
                elif self.nfe.operation.operation_type == OperationType.OUT:
                    unit = item.sales_unit
                else:
                    unit = item.base_unit
            if 'ncm_classification' in self.changed_fields:
                ncm = self.ncm_classification
            if ncm is None:
                ncm = item.ncm_classification
        if unit is not None:
            self.unit = unit
        if ncm is not None:
            self.ncm_classification = ncm
        if self.unit is not None:
            self.unit_code = self.unit.code
        if self.ncm_classification is not None:
            self.ncm_code = self.ncm_classification.code

    def set_units(self):
        from decimal import Decimal
        nfe = self.nfe
        product = self.product
        if self.unit is None:
            if nfe.operation_type == OperationType.IN:
                self.unit = self.product.purchase_unit
            else:
                self.unit = self.product.sales_unit

        if self.taxed_unit is None or \
                        'unit' in self.changed_fields or \
                        'product' in self.changed_fields:
            self.taxed_unit = self.product.taxed_unit
        if self.quantity > 0:
            self.taxed_quantity = product.convert_to_taxed_unit(unit_origin=self.unit, quantity_origin=self.quantity)
        else:
            self.taxed_quantity = Decimal('0.00')

        if self.unitary_value > 0:
            self.taxed_unitary_value = product.convert_to_taxed_unit(unit_origin=self.unit, quantity_origin=self.unitary_value)
        else:
            self.taxed_unitary_value = Decimal('0.00')

        def convert_to_control_unit(self, unit_origin, quantity_origin):
            return self.convert_units(unit_origin, self.control_unit, quantity_origin)

    def set_pricing(self):
        if 'unit' in self.changed_fields and 'unitary_value' not in self.changed_fields:
            self.unitary_value = self.product.get_sales_price(self.unit)
        if self.unitary_value == 0:
            self.unitary_value = self.product.get_sales_price(self.unit)

    def set_unitary_value(self):
        from decimal import Decimal
        if not self.id or \
                        'unitary_value' in self.changed_fields or \
                        'quantity' in self.changed_fields or \
                        'taxed_unitary_value' in self.changed_fields or \
                        'taxed_quantity' in self.changed_fields:
            if self.unitary_value > 0 and self.quantity > 0:
                self.nfe_unitary_value = Decimal(self.goods_value / self.quantity)\
                    .quantize(Decimal('1.0000000000'))
            if self.taxed_unitary_value > 0 and self.taxed_quantity > 0:
                self.nfe_taxed_unitary_value = Decimal(self.goods_value / self.taxed_quantity)\
                    .quantize(Decimal('1.0000000000'))

    def set_goods_value(self):
        super(AbstractNFeLine, self).set_goods_value()

    def finalize(self):
        self.stock_transaction()

    def reopen(self):
        model = apps.get_model('stock', 'StockTransaction')

        try:
            stock_transaction = model._default_manager.get(document_type=self.nfe.document_type,
                                                           document_id=self.nfe_id,
                                                           document_line_sequence=self.sequence,
                                                           stock_status=StockLineStatus.NORMAL)
        except model.DoesNotExist:
            pass
        else:
            model.on_company.stock_calculate(stock_transaction, stock_status=StockLineStatus.OPEN)

    def stock_transaction(self):
        model = apps.get_model('stock', 'StockTransaction')
        control_quantity = self.product.convert_to_control_unit(self.unit, self.quantity)
        cost = 0.00
        calculate_cost = True
        if self.nfe.operation_type == OperationType.IN:
            cost = self.line_cost
            calculate_cost = False

        defaults = {'operation_type': self.nfe.operation_type,
                    'stock_status': StockLineStatus.OPEN,
                    'stock_date': self.nfe.document_release_date,
                    'product': self.product,
                    'unit': self.product.control_unit,
                    'quantity': control_quantity,
                    'cost': cost,
                    'calculate_cost': calculate_cost,
                    'previous_quantity': 0.000000,
                    'previous_cost': 0.000000,
                    'stock_history': ''
                    }
        stock_transaction, created = model._default_manager.get_or_create(document_type=self.nfe.document_type,
                                                                          document_id=self.nfe_id,
                                                                          document_line_sequence=self.sequence,
                                                                          defaults=defaults)
        if not created:
            stock_transaction.operation_type = self.nfe.operation_type
            stock_transaction.stock_status = StockLineStatus.OPEN
            stock_transaction.stock_date = self.nfe.document_release_date
            stock_transaction.product = self.product
            stock_transaction.unit = self.product.control_unit
            stock_transaction.quantity = control_quantity
            stock_transaction.cost = cost
            stock_transaction.calculate_cost = calculate_cost
            stock_transaction.previous_quantity = 0.000000
            stock_transaction.previous_cost = 0.000000
            stock_transaction.stock_history = ''
        stock_transaction.save()
        model.on_company.stock_calculate(stock_transaction, stock_status=StockLineStatus.NORMAL)



class AbstractNFeInvoice(FiscalDocumentInvoice):
    nfe = models.ForeignKey('nfe.NFe', null=False, related_name='invoices')
    sequence = NextifyPositionField('Sequencia', collection='nfe', null=False)

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.invoice_number = '%s-%s' % (str(self.nfe.number), str(self.sequence))
        super(AbstractNFeInvoice, self).save(force_insert, force_update, using, update_fields)
        self.calculate_invoice_totals()

    def delete(self, using=None, keep_parents=False):
        super(AbstractNFeInvoice, self).delete(using, keep_parents)
        self.calculate_invoice_totals()

    def calculate_invoice_totals(self):
        model = apps.get_model('nfe', 'NFeInvoiceTotals')
        nfe_totals, created = model._default_manager.get_or_create(nfe=self.nfe)
        nfe_totals.calculate()
        nfe_totals.save()


class AbstractNFeInvoiceTotals(FiscalDocumentInvoiceTotals):
    nfe = models.OneToOneField('nfe.NFe', null=False, related_name='invoice_totals')

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.calculate()
        super(AbstractNFeInvoiceTotals, self).save(force_insert, force_update, using, update_fields)

    def calculate(self):
        if self.nfe.invoices.all().count() > 0:
            totals = self.nfe.invoices.all().aggregate(Sum('original_value'),
                                                       Sum('discount_value'),
                                                       Sum('net_value'))
            self.original_value = totals['original_value__sum']
            self.discount_value = totals['discount_value__sum']
            self.net_value = totals['net_value__sum']


class AbstractNFeTaxLineIcmsSN(FiscalTaxLineIcmsSN):
    nfe = models.ForeignKey('nfe.NFe', null=False, related_name='icms_lines')
    nfe_line = models.OneToOneField('nfe.NFeLine', null=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id:
            self.nfe = self.nfe_line.nfe
        self.tax_calculation()
        super(AbstractNFeTaxLineIcmsSN, self).save(force_insert, force_update, using, update_fields)
        self.nfe.create_tax_totals()

    class Meta:
        abstract = True

    def tax_calculation(self):
        # Calculo ICMS Normal
        if self.tax_base_value == 0 and self.tax_rate == 0:
            return
        if self.tax_base_value == 0:
            self.tax_base_value = self.nfe_line.goods_value
        tax_base = self.tax_base_value
        from decimal import Decimal, ROUND_UP
        tax_value = (Decimal(tax_base) * Decimal(self.tax_rate)) / Decimal('100.00')
        self.tax_value = tax_value.quantize(Decimal('1.00'), ROUND_UP)

        # Calculo ICMS ST
        if self.tax_rate_st == 0:
            self.tax_base_st_value = 0
            self.tax_base_mva = 0
            return

        tax_base_mva = (Decimal(self.tax_base_mva) / Decimal('100.00')) + Decimal('1.00')
        tax_base_st = Decimal(self.tax_base_value) * Decimal(tax_base_mva)
        self.tax_base_st_value = tax_base_st.quantize(Decimal('1.00'), ROUND_UP)
        tax_value_st = ((Decimal(tax_base_st) * Decimal(self.tax_rate_st)) / Decimal('100.00')) - Decimal(tax_value)
        self.tax_value_st = tax_value_st.quantize(Decimal('1.00'), ROUND_UP)


class AbstractNFeTaxLineIpi(FiscalTaxLineIpi):
    nfe = models.ForeignKey('nfe.NFe', null=False, related_name='ipi_lines')
    nfe_line = models.OneToOneField('nfe.NFeLine', null=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id:
            self.nfe = self.nfe_line.nfe
        self.tax_calculation()
        super(AbstractNFeTaxLineIpi, self).save(force_insert, force_update, using, update_fields)
        self.nfe.create_tax_totals()

    class Meta:
        abstract = True

    def tax_calculation(self):
        if self.tax_base_value == 0 and self.tax_rate == 0:
            return
        if self.tax_base_value == 0:
            self.tax_base_value = self.nfe_line.goods_value
        tax_base = self.tax_base_value
        from decimal import Decimal, ROUND_UP
        tax_value = (Decimal(tax_base) * Decimal(self.tax_rate)) / Decimal('100.00')
        self.tax_value = tax_value.quantize(Decimal('1.00'), ROUND_UP)


class AbstractNFeTaxLinePis(FiscalTaxLinePis):
    nfe = models.ForeignKey('nfe.NFe', null=False, related_name='pis_lines')
    nfe_line = models.OneToOneField('nfe.NFeLine', null=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id:
            self.nfe = self.nfe_line.nfe
        self.tax_calculation()
        super(AbstractNFeTaxLinePis, self).save(force_insert, force_update, using, update_fields)
        self.nfe.create_tax_totals()

    class Meta:
        abstract = True

    def tax_calculation(self):
        if self.tax_base_value == 0 and self.tax_rate == 0:
            return
        if self.tax_base_value == 0:
            self.tax_base_value = self.nfe_line.goods_value
        tax_base = self.tax_base_value
        from decimal import Decimal, ROUND_UP
        tax_value = (Decimal(tax_base) * Decimal(self.tax_rate)) / Decimal('100.00')
        self.tax_value = tax_value.quantize(Decimal('1.00'), ROUND_UP)


class AbstractNFeTaxLineCofins(FiscalTaxLineCofins):
    nfe = models.ForeignKey('nfe.NFe', null=False, related_name='cofins_lines')
    nfe_line = models.OneToOneField('nfe.NFeLine', null=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id:
            self.nfe = self.nfe_line.nfe
        self.tax_calculation()
        super(AbstractNFeTaxLineCofins, self).save(force_insert, force_update, using, update_fields)
        self.nfe.create_tax_totals()

    class Meta:
        abstract = True

    def tax_calculation(self):
        if self.tax_base_value == 0 and self.tax_rate == 0:
            return
        if self.tax_base_value == 0:
            self.tax_base_value = self.nfe_line.goods_value
        tax_base = self.tax_base_value
        from decimal import Decimal, ROUND_UP
        tax_value = (Decimal(tax_base) * Decimal(self.tax_rate)) / Decimal('100.00')
        self.tax_value = tax_value.quantize(Decimal('1.00'), ROUND_UP)


class AbstractNFeTotals(FiscalDocumentTotals):
    nfe = models.OneToOneField('nfe.NFe', null=False, related_name='totals')
    taxes_value_total = models.DecimalField(u'Total de Tributos (Fonte: IBPT)',
                                            max_digits=15, decimal_places=2, null=False, default=0.00)

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.calculate()
        super(AbstractNFeTotals, self).save(force_insert, force_update, using, update_fields)

    def calculate(self):
        if self.nfe.lines.all().count() <= 0:
            return
        tax_totals = self.nfe.tax_totals
        totals = self.nfe.lines.all().aggregate(Sum('goods_value'),
                                                Sum('services_value'),
                                                Sum('conditional_discount_value'),
                                                Sum('deductions_value'),
                                                Sum('discount_value'),
                                                Sum('insurance_value'),
                                                Sum('freight_value'),
                                                Sum('expenses_value'),
                                                Sum('ind_taxes_value'))
        self.goods_total = totals['goods_value__sum']
        self.services_total = totals['services_value__sum']
        self.freight_total = totals['freight_value__sum']
        self.insurance_total = totals['insurance_value__sum']
        self.discount_total = totals['discount_value__sum']
        self.deductions_total = totals['deductions_value__sum']
        self.conditional_discount_total = totals['conditional_discount_value__sum']
        self.expenses_total = totals['expenses_value__sum']
        self.taxes_value_total = totals['ind_taxes_value__sum']
        self.document_total = self.goods_total + \
                              self.services_total + \
                              self.freight_total + \
                              self.insurance_total + \
                              self.expenses_total - \
                              self.discount_total

        if tax_totals is not None:
            self.document_total += tax_totals.icmsst_value_total + tax_totals.ipi_value_total


class AbstractNFeTaxTotals(FiscalDocumentTaxTotals):
    nfe = models.OneToOneField('nfe.NFe', null=False, related_name='tax_totals')

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.calculate()
        super(AbstractNFeTaxTotals, self).save(force_insert, force_update, using, update_fields)

    def calculate(self):
        icms_totals = None
        ipi_totals = None
        pis_totals = None
        cofins_totals = None
        if self.nfe.icms_lines.all().count() > 0:
            icms_totals = self.nfe.icms_lines.all().aggregate(Sum('tax_base_value'),
                                                              Sum('tax_value'),
                                                              Sum('tax_base_st_value'),
                                                              Sum('tax_value_st'))
        if self.nfe.ipi_lines.all().count() > 0:
            ipi_totals = self.nfe.ipi_lines.all().aggregate(Sum('tax_base_value'),
                                                            Sum('tax_value'))

        if self.nfe.pis_lines.all().count() > 0:
            pis_totals = self.nfe.pis_lines.all().aggregate(Sum('tax_base_value'),
                                                            Sum('tax_value'))

        if self.nfe.cofins_lines.all().count() > 0:
            cofins_totals = self.nfe.cofins_lines.all().aggregate(Sum('tax_base_value'),
                                                                  Sum('tax_value'))

        if icms_totals is not None:
            self.icms_base_total = icms_totals['tax_base_value__sum']
            self.icms_value_total = icms_totals['tax_value__sum']
            self.icmsst_base_total = icms_totals['tax_base_st_value__sum']
            self.icmsst_value_total = icms_totals['tax_value_st__sum']
        if ipi_totals is not None:
            self.ipi_base_total = ipi_totals['tax_base_value__sum']
            self.ipi_value_total = ipi_totals['tax_value__sum']
        if pis_totals is not None:
            self.pis_base_total = pis_totals['tax_base_value__sum']
            self.pis_value_total = pis_totals['tax_value__sum']
        if cofins_totals is not None:
            self.cofins_base_total = cofins_totals['tax_base_value__sum']
            self.cofins_value_total = cofins_totals['tax_value__sum']


class AbstractNFeComplementaryInformation(FiscalDocumentComplementaryInformation):
    nfe = models.OneToOneField('nfe.NFe', null=False, related_name='complementary_information')

    class Meta:
        abstract = True


class AbstractNFeShipping(FiscalDocumentShipping):
    nfe = models.OneToOneField('nfe.NFe', null=False, related_name='shipping')

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.fill_defaults()
        super(AbstractNFeShipping, self).save(*args, **kwargs)

    def fill_defaults(self):
        shipping_company = self.shipping_company
        if 'shipping_company' in self.changed_fields and shipping_company is not None:
            address = self.shipping_company.billing_address()
            self.category = shipping_company.category
            self.document_number = shipping_company.document_number
            self.name = shipping_company.name
            self.state_registry_id = shipping_company.state_registry_id
            self.state_registry = shipping_company.state_registry

            if address is not None:
                if address.city is not None:
                    self.city = address.city
                    self.city_ibge_code = address.city.ibge_code
                    self.city_name = address.city_name
                    state = self.city.state
                    self.state_ibge_code = state.ibge_code
                    self.state_acronym = state.acronym
                    self.state_name = state.name
                    self.address_sumary = address.summary[0:60]


class AbstractNFeShippingVolume(FiscalDocumentShippingVolume):
    nfe = models.ForeignKey('nfe.NFe', null=False, related_name='shipping_volumes')

    class Meta:
        abstract = True


class AbstractNFeStatus(models.Model):
    code = models.IntegerField(u'Status NFe', null=False)
    description = models.CharField(u'Descrição Status NFe', blank=False, max_length=500)

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s - %s' % (self.code, self.description)


class AbstractNFeWebserviceStatus(models.Model):
    webservice = models.CharField(u'Webservice', max_length=10, blank=False)
    model = models.IntegerField(u'Modelo', choices=DocumentModel.choices(), null=False)
    environment = models.IntegerField(u'Ambiente', choices=NFeEnvironment.choices(), null=False)
    status = models.ForeignKey('nfe.NFeStatus', null=False)
    status_description = models.CharField(u'Descrição Status NFe', blank=False, max_length=500)
    query_date = models.DateTimeField(u'Data Autorizacao', null=False)

    class Meta:
        abstract = True


class AbstractNFeSendMailQueue(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)
    nfe = models.ForeignKey('nfe.NFe', null=False, related_name='mail_queue')
    mail_to = models.CharField(u'Mail To', max_length=255, blank=False)
    mail_from = models.CharField(u'Mail To', max_length=255, blank=False)
    message = models.TextField(u'Mensagem', blank=True)
    subject = models.TextField(u'Mensagem', blank=True)
    mail_sent = models.BooleanField(u'Enviado?', default=False)
    nfe_pdf_path = models.CharField(u'PDF', max_length=500, blank=True)
    nfe_xml_path = models.CharField(u'XML', max_length=500, blank=True)
    attempt = models.IntegerField(u'Tentativa No.', null=False, default=0)
    return_status = models.IntegerField(u'Return Status', null=True)
    return_message = models.CharField(u'Return Message', max_length=500, blank=True)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


class AbstractNFeEvent(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID, editable=False)
    nfe = models.ForeignKey('nfe.NFe', null=False, related_name='events')
    event_type = models.CharField(u'Tipo do Evento', max_length=50, blank=True)
    event_version = models.CharField(u'Versao do Evento', max_length=10, blank=True)
    sequence = NextifyPositionField(u'Sequência do Evento', collection=('nfe', 'event_type'))
    message = models.TextField(u'Justificativa do Evento', blank=True)
    protocol_number = models.CharField(u'Protocolo', max_length=15, blank=True)
    event_status = models.ForeignKey('nfe.NFeStatus', verbose_name='Situação', null=True)
    event_status_message = models.CharField(u'Descricao do Status', blank=True, max_length=500)
    event_date = models.DateTimeField(u'Data/Hora do Evento', null=True, blank=True)
    event_registry_date = models.DateTimeField(u'Data/Hora do Registro do Evento', null=True, blank=True)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def portlet(self):
        return self.nfe.portlet

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id:
            self.event_status_id = 12
            self.event_date = datetime.now()
        super(AbstractNFeEvent, self).save(force_insert, force_update, using, update_fields)
