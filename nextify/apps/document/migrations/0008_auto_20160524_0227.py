# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-24 02:27
from __future__ import unicode_literals

from django.db import migrations
import nextify.apps.person.managers


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0007_remove_fiscaldocumenttaxtotals_taxes_value_total'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='fiscaldocumentcomplementaryinformation',
            managers=[
                ('on_company', nextify.apps.person.managers.CompanyManager()),
            ],
        ),
    ]
