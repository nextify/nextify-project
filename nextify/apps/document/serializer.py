from django.conf.urls import url, include
from nfe.models import NFeQueue, NFe, NFeWebserviceStatus, NFeEvent
from rest_framework import routers, serializers, viewsets
from django.http import HttpResponse, JsonResponse, Http404
from django.apps import apps
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class NFeSerializer(serializers.ModelSerializer):
    class Meta:
        model = NFeQueue
        fields = ('id', 'nfe', 'cnpj', 'modelo', 'ambiente', 'operacao', 'chave_acesso', 'status', 'mensagem', 'justificativa', 'protocolo', 'recibo', 'data_processamento', 'sequencia_evento')

class NFeWsStatus(serializers.ModelSerializer):
    class Meta:
        model = NFeWebserviceStatus
        fields = ('webservice', 'model', 'environment', 'status_description', 'query_date', 'status' )

from rest_framework.views import APIView

class NFeQueueList(APIView):
    def get(self, request):
        nfe_list = NFeQueue.objects.filter(operacao__in=[0, 3, 5, 6, 7], processada='N')
        for nfe in nfe_list:
            nfe.status = 3
            nfe.save()
        serializer = NFeSerializer(nfe_list, many=True)

        return JsonResponse(serializer.data, safe=False)


    def post(self, request, format=None):
        serializer = NFeSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NFeQueueDetail(APIView):
    """
    Retrieve, update or delete
    """

    def get_object(self, pk):
        try:
            return NFeQueue.objects.get(pk=pk)
        except NFeQueue.DoesNotExist:
            raise Http404

    def get_nfe(self, nfe_id):
        try:
            return NFe.on_company.get(pk=nfe_id)
        except NFe.DoesNotExist:
            raise Http404

    def get_nfe_event(self, event_id):
        try:
            return NFeEvent.on_company.get(pk=event_id)
        except NFe.DoesNotExist:
            raise Http404

    def proccess(self, pk):
        q = self.get_object(pk)

        nfe = self.get_nfe(q.nfe)
        if q.operacao in [0, 3, 5]:
            nfe.nfe_status_id = q.status
            nfe.nfe_status_description = q.mensagem
            if q.status in [103, 105]:
                nfe.nfe_receipt_number = q.recibo
                q.processada = 'S'
                self.inclui_consulta_nfe(q)
            elif q.status in [100, 101, 110, 150, 151]:
                nfe.nfe_access_key = q.chave_acesso
                if q.status == 100:
                    nfe.nfe_receipt_number = q.recibo
                    nfe.nfe_authorization_protocol = q.protocolo
                    nfe.nfe_authorization_date = q.data_processamento
                q.processada = 'S'
            elif q.status in [106]:
                q.status = 0
            else:
                q.processada = 'S'

        if q.operacao in [6, 7]:
            evt = self.get_nfe_event(q.evento)
            evt.event_status_id = q.status
            evt.event_status_message = q.mensagem
            q.processada = 'S'
            if q.status in [135, 136]:
                evt.protocol_number = q.protocolo
                evt.event_registry_date = q.data_processamento
                self.inclui_consulta_nfe(q)
            evt.save()
        nfe.save()
        q.save()

    def inclui_consulta_nfe(self, q):

        try:
            m = apps.get_model('nfe', 'NFeQueue')

            o = m(nfe=q.nfe, operacao=3, status=0, chave_acesso=q.chave_acesso,
                  cnpj=q.cnpj, protocolo=q.protocolo, processada='N', ambiente=q.ambiente,
                  modelo=q.modelo)
            o.save()
            return True
        except:
            return False

    def get(self, request, pk, format=None):
        queue = self.get_object(pk)
        serializer = NFeSerializer(queue)

        return Response(serializer.data)


    def put(self, request, pk, format=None):
        queue = self.get_object(pk)
        serializer = NFeSerializer(queue, data=request.data)

        if serializer.is_valid():
            serializer.save()
            self.proccess(pk)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NFeWsStatusDetail(APIView):

    def post(self, request):
        serializer = NFeWsStatus(data=request.data)
        if serializer.is_valid():
            try:
                o = NFeWebserviceStatus.objects.get(webservice=serializer.validated_data['webservice'],
                                                model=serializer.validated_data['model'],
                                                environment=serializer.validated_data['environment'])
                o.status_id = serializer.data['status']
                o.status_description = serializer.data['status_description']
                o.query_date = serializer.data['query_date']

                o.save()
            except NFeWebserviceStatus.DoesNotExist:
                serializer.save()
            return Response(serializer.data, status.HTTP_200_OK)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


