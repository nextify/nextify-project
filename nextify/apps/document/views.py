from django.shortcuts import render
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse
# Create your views here.


class NFeSendQueryUpdate(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        #send_nfe()
        return reverse('nfe:list-self')

    def query_nfe(self):
        pass

