# -*- coding: utf-8 -*-
import pytz
import sendgrid
from datetime import datetime
from django.conf import settings
from django.apps import apps

# from pysped.nfe.NFCeProcessor import ProcessadorNFe
from pysped.nfe.leiaute import NFCe_310
from nextify.apps.document.nfce.serializer import NfeSerializer


def webservice_status_query(webservice, model=65):
    m = apps.get_model('nfe', 'NFeWebserviceStatus')
    exists = True
    try:
        o = m.objects.get(webservice=webservice, environment=settings.NFE_AMBIENTE, model=model)
    except m.DoesNotExist:
        exists = False

    status_dict = {
        'status': o.status.code,
        'reason': o.status_description,
        'date': o.query_date
    }
    return status_dict


def query_nfe_by_receipt_bkp():
    pass
    # m = apps.get_model('nfe', 'NFe')
    #
    # nfe_list = m.on_company.filter(nfe_status_id__in=[3, 103, 104, 105])
    #
    # for nfe in nfe_list:
    #     p = ProcessadorNFe()
    #     p.versao = '3.10'
    #     p.estado = 'PR'
    #     p.ambiente = settings.NFE_AMBIENTE
    #     p.certificado.arquivo = settings.CERT_PATH + '13073206/' + settings.CERT_FILE
    #     p.certificado.senha = settings.CERT_PASSWORD
    #     p.salvar_arquivos = True
    #     s = NfeSerializer(nfe=nfe)
    #     n = NFCe_310()
    #     n = s.nfe_310
    #     n.gera_url_xml()
    #
    #     arq = open(s.xml_path + '.teste', 'w')
    #     arq.write(n.xml.encode('utf-8'))
    #     arq.close()


def query_nfe_by_receipt():
    pass
    # m = apps.get_model('nfe', 'NFe')
    #
    # nfe_list = m.on_company.filter(nfe_status_id__in=[3, 103, 104, 105])
    #
    # for nfe in nfe_list:
    #     p = ProcessadorNFe()
    #     p.versao = '3.10'
    #     p.estado = 'PR'
    #     p.ambiente = settings.NFE_AMBIENTE
    #     p.certificado.arquivo = settings.CERT_PATH + '13073206/' + settings.CERT_FILE
    #     p.certificado.senha = settings.CERT_PASSWORD
    #     p.salvar_arquivos = True
    #     s = NfeSerializer(nfe=nfe)
    #     n = s.nfe_310
    #     proc_recibo = p.process_nfe_query_receipt([n], nfe.nfe_receipt_number)
    #     if proc_recibo.resposta.cStat.valor == '104':
    #         dic = proc_recibo.resposta.dic_protNFe[nfe.nfe_access_key]
    #         nfe.nfe_status_id = dic.infProt.cStat.valor
    #         nfe.nfe_status_description = dic.infProt.cStat.valor
    #         nfe.nfe_authorization_protocol = dic.infProt.nProt.valor
    #         nfe.nfe_authorization_date = dic.infProt.dhRecbto.valor
    #         nfe.save()
    #         if dic.infProt.cStat.valor in ['100', '101', ]:
    #             insert_nfe_mail_queue(nfe=nfe)


def query_nfe_by_access_key(nfe):
    pass
    # p = ProcessadorNFe()
    # p.versao = '3.10'
    # p.estado = 'PR'
    # p.ambiente = settings.NFE_AMBIENTE
    # p.certificado.arquivo = settings.CERT_PATH + '13073206/' + settings.CERT_FILE
    # p.certificado.senha = '123456'
    # p.salvar_arquivos = True
    # s = NfeSerializer(nfe=nfe)
    # n = s.nfe_310
    # proc = p.consultar_nota(ambiente=settings.NFE_AMBIENTE, chave_nfe=nfe.nfe_access_key, nfe=n)
    # print(proc.resposta.cStat.valor)
    # nfe.nfe_status_id = proc.resposta.cStat.valor
    # nfe.nfe_status_description = proc.resposta.infProt.xMotivo.valor
    # nfe.save()


def cancel_nfe(nfe_event):
    nfe = nfe_event.nfe

    try:
        q = apps.get_model('nfe', 'NFeQueue')

        o = q(nfe=nfe.id, operacao=7, status=0, chave_acesso=nfe.nfe_access_key,
              cnpj=nfe.issuer.document_number, protocolo=nfe.nfe_authorization_protocol,
              modelo=nfe.model, justificativa=nfe_event.message, evento=nfe_event.id,
              sequencia_evento=nfe_event.sequence
              )
        o.save()
        return True
    except:
        return False

def send_nfe():
    m = apps.get_model('nfe', 'NFe')

    nfe_list = m.on_company.filter(nfe_status_id=2)

    for nfe in nfe_list:
        q = apps.get_model('nfe', 'NFeQueue')

        s = NfeSerializer(nfe=nfe)
        nfe.nfe_access_key = s.nfe_400.chave

        o = q(nfe=nfe.id, operacao=0, status=0, chave_acesso=nfe.nfe_access_key,
              cnpj=nfe.issuer.document_number,
              modelo=nfe.model
              )
        o.save()

        nfe.nfe_status_id = 3
        nfe.save()


def send_mail():
    pass
    # m = apps.get_model('nfe', 'NFeSendMailQueue')
    # mail_list = m.on_company.filter(mail_sent=False, attempt__lte=settings.MAIL_MAX_RETRIES)
    #
    # for mail in mail_list:
    #     sg = sendgrid.SendGridClient(settings.SENDGRID_API_KEY)
    #     message = sendgrid.Mail()
    #     message.add_to(mail.mail_to)
    #     if settings.MAIL_CCO != '':
    #         message.add_bcc(settings.MAIL_CCO)
    #     message.set_from(mail.mail_from)
    #     message.set_subject(mail.subject)
    #     message.set_html(mail.message)
    #     if mail.nfe_pdf_path:
    #         message.add_attachment('%s.pdf' % mail.nfe.nfe_access_key, open(mail.nfe_pdf_path, 'rb'))
    #     if mail.nfe_xml_path:
    #         message.add_attachment('%s.xml' % mail.nfe.nfe_access_key, open(mail.nfe_xml_path, 'rb'))
    #     try:
    #         status, msg = sg.send(message)
    #     except:
    #         continue
    #
    #     mail.attempt = mail.attempt + 1
    #     mail.return_status = status
    #     mail.return_message = msg
    #     maifinalizel.mail_sent = True
    #     mail.save()


def insert_nfe_mail_queue(nfe):
    s = NfeSerializer(nfe=nfe)
    m = apps.get_model('nfe', 'NFeSendMailQueue')
    if settings.NFE_AMBIENTE == 2:
        mail_to = settings.MAIL_TO_HOMOLOG
    else:
        mail_to = s.nfe_310.infNFe.dest.email.valor

    pdf_path = ''
    if s.pdf_path is not None:
        pdf_path = s.pdf_path
    xml_path = ''
    if s.xml_path is not None:
        xml_path = s.xml_path

    if mail_to not in ['', None]:
        o = m(nfe=nfe, mail_to=mail_to, mail_from=settings.MAIL_FROM,
              message=settings.MAIL_MESSAGE, subject=settings.MAIL_SUBJECT,
              nfe_pdf_path=pdf_path, nfe_xml_path=xml_path)
        o.save()




def getTimeDifferenceFromNow(start_time, end_time):
    time_diff = end_time - start_time

    return time_diff.total_seconds() / 60
