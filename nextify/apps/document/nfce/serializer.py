# -*- coding: utf-8 -*-
import os
from datetime import datetime
from django.conf import settings
from nextify.core.enum.person import *
from nextify.core.enum.nfe import *

from pysped.nfe.leiaute import NFCe_400 as NFe_400, ProtNFe_310, Det_400, Vol_400, Dup_400, ProcEventoCancNFe_100, Pag_400, DetPag_400
from pysped.nfe.danfe import DANFE_NFCE as DANFE


class NfeSerializer(object):

    def __init__(self, nfe):
        nfe.refresh_from_db()
        self.nfe = nfe
        self.nfe_400 = NFe_400()
        self.protnfe_400 = ProtNFe_310()
        self.proc_evento_canc_nfe = ProcEventoCancNFe_100()
        self.danfe_content = None
        self.danfe = DANFE()
        self.access_key = None
        self.xml_path = None
        self.pdf_path = None
        self.environment = settings.NFE_AMBIENTE
        self.initial_path = settings.NFE_FILE_PATH
        self.infNFe()

    def get_danfe_content(self):
        danfe = self.danfe
        danfe.nfe_model = self.nfe
        danfe.NFe = self.nfe_400
        danfe.protNFe = self.protnfe_400
        danfe.procEventoCancNFe = self.proc_evento_canc_nfe
        danfe.salvar_arquivo = False
        danfe.gerar_danfe()
        self.danfe_content = danfe.conteudo_pdf
        return self.danfe_content

    def infNFe(self):
        nfe = self.nfe
        xml = self.nfe_400

        if nfe.nfe_status_id in [100, ]:
            nfe_path = self.set_nfe_path(self.environment, nfe.nfe_access_key)
            nfe_xml_file = nfe_path + nfe.nfe_access_key + '-nfeProc.xml'
            nfe_pdf_file = nfe_path + nfe.nfe_access_key + '.pdf'
            self.xml_path = nfe_xml_file
            self.pdf_path = nfe_pdf_file
            if os.path.isfile(nfe_xml_file):
                self.nfe_400.xml = nfe_xml_file
                self.protnfe_400.xml = nfe_xml_file
                danfe = self.danfe
                danfe.NFe = self.nfe_400
                danfe.logo = settings.NFE_LOGO
                danfe.protNFe = self.protnfe_400
                danfe.gerar_danfe()
                self.access_key = self.nfe_400.chave
                self.danfe_content = danfe.conteudo_pdf

                return

        if nfe.nfe_status_id in [3, 101, 103, 104, 105, ]:
            nfe_xml_file = self.set_nfe_path(self.environment, nfe.nfe_access_key)
            nfe_xml_file = nfe_xml_file + nfe.nfe_access_key + '.xml'
            if nfe.nfe_status_id == 101:
                canc_xml = self.set_nfe_path(self.environment, nfe.nfe_access_key)
                canc_xml = canc_xml + nfe.nfe_access_key + '_110111-procEventoNFe.xml'
                self.proc_evento_canc_nfe.xml = canc_xml
            self.xml_path = nfe_xml_file
            if os.path.isfile(nfe_xml_file):
                self.nfe_400.xml = nfe_xml_file
                self.protnfe_400.xml = nfe_xml_file
                self.nfe_400.monta_chave()
                self.access_key = self.nfe_400.chave
                return
        self.ide()
        self.emit()
        self.dest()
        for line in nfe.lines.all():
            l = self.det(line=line)
            xml.infNFe.det.append(l)
        self.icmstotal()
        self.transporte()
        self.cobranca()
        self.pag()
        self.infAdic()
        # self.nfe_310.infNFeSupl.qrCode.valor = ''
        self.nfe_400.gera_nova_chave()
        self.access_key = self.nfe_400.chave

    def ide(self):
        xml = self.nfe_400
        nfe = self.nfe
        xml.infNFe.ide.cUF.valor = nfe.issuer.state_ibge_code
        xml.infNFe.ide.natOp.valor = nfe.operation.description
        xml.infNFe.ide.indPag.valor = nfe.nfe_payment_method.payment_form
        xml.infNFe.ide.mod.valor = nfe.model
        xml.infNFe.ide.serie.valor = nfe.series
        xml.infNFe.ide.nNF.valor = nfe.number
        xml.infNFe.ide.dhEmi.valor = datetime.now()
        xml.infNFe.ide.dSaiEnt.valor = datetime.now()
        xml.infNFe.ide.tpNF.valor = nfe.operation_type
        xml.infNFe.ide.idDest.valor = str(nfe.nfe_destination)
        xml.infNFe.ide.cMunFG.valor = nfe.issuer.city_ibge_code
        xml.infNFe.ide.tpImp.valor = '4' #nfe.nfe_danfe_print_type
        xml.infNFe.ide.tpEmis.valor = nfe.nfe_issue_type
        xml.infNFe.ide.tpAmb.valor = str(self.environment)
        xml.infNFe.ide.finNFe.valor = nfe.nfe_finality
        xml.infNFe.ide.indFinal.valor = str(nfe.nfe_final_consumer)
        xml.infNFe.ide.indPres.valor = str(nfe.nfe_consumer_presence)
        xml.infNFe.ide.procEmi.valor = 0
        xml.infNFe.ide.verProc.valor = nfe.nfe_application_version

    def emit(self):
        xml = self.nfe_400
        nfe = self.nfe
        xml.infNFe.emit.CNPJ.valor = nfe.issuer.document_number
        xml.infNFe.emit.xNome.valor = nfe.issuer.name
        xml.infNFe.emit.xFant.valor = nfe.issuer.fancy_name
        xml.infNFe.emit.IE.valor = nfe.issuer.state_registry
        xml.infNFe.emit.IEST.valor = nfe.issuer.state_registry_st
        xml.infNFe.emit.IM.valor = nfe.issuer.city_registry
        xml.infNFe.emit.CRT.valor = nfe.issuer.crt
        self.enderEmit()

    def enderEmit(self):
        xml = self.nfe_400
        nfe = self.nfe
        xml.infNFe.emit.enderEmit.xLgr.valor = nfe.issuer.street
        xml.infNFe.emit.enderEmit.nro.valor = nfe.issuer.number
        xml.infNFe.emit.enderEmit.xCpl.valor = nfe.issuer.complement
        xml.infNFe.emit.enderEmit.xBairro.valor = nfe.issuer.province
        xml.infNFe.emit.enderEmit.cMun.valor = nfe.issuer.city_ibge_code
        xml.infNFe.emit.enderEmit.xMun.valor = nfe.issuer.city_name
        xml.infNFe.emit.enderEmit.UF.valor = nfe.issuer.state_acronym
        xml.infNFe.emit.enderEmit.CEP.valor = nfe.issuer.zipcode
        xml.infNFe.emit.enderEmit.cPais.valor = nfe.issuer.country_bacen_code
        xml.infNFe.emit.enderEmit.xPais.valor = nfe.issuer.country_name
        xml.infNFe.emit.enderEmit.fone.valor = nfe.issuer.phone

    def dest(self):
        xml = self.nfe_400
        nfe = self.nfe
        if not hasattr(nfe, 'receiver'):
            return

        if nfe.receiver.category == PersonCategory.INDIVIDUAL:
            xml.infNFe.dest.CPF.valor = nfe.receiver.document_number
        elif nfe.receiver.category == PersonCategory.CORPORATE:
            xml.infNFe.dest.CNPJ.valor = nfe.receiver.document_number
        else:
            xml.infNFe.dest.idEstrangeiro.valor = nfe.receiver.document_number

        if settings.NFE_AMBIENTE == NFeEnvironment.TEST:
            xml.infNFe.dest.xNome.valor = settings.NFE_HOMOLOG_RAZAO_SOCIAL
        else:
            xml.infNFe.dest.xNome.valor = nfe.receiver.name
        xml.infNFe.dest.email.valor = nfe.receiver.email
        self.enderDest()

    def enderDest(self):
        xml = self.nfe_400
        nfe = self.nfe
        if xml.infNFe.dest.enderDest.xLgr.valor and len(xml.infNFe.dest.enderDest.xLgr.valor):
            xml.infNFe.dest.enderDest.xLgr.valor = nfe.receiver.street
            xml.infNFe.dest.enderDest.nro.valor = nfe.receiver.number
            xml.infNFe.dest.enderDest.xCpl.valor = nfe.receiver.complement
            xml.infNFe.dest.enderDest.xBairro.valor = nfe.receiver.province
            xml.infNFe.dest.enderDest.cMun.valor = nfe.receiver.city_ibge_code
            xml.infNFe.dest.enderDest.xMun.valor = nfe.receiver.city_name
            xml.infNFe.dest.enderDest.UF.valor = nfe.receiver.state_acronym
            xml.infNFe.dest.enderDest.CEP.valor = nfe.receiver.zipcode
            xml.infNFe.dest.enderDest.cPais.valor = nfe.receiver.country_bacen_code
            xml.infNFe.dest.enderDest.xPais.valor = nfe.receiver.country_name
            xml.infNFe.dest.enderDest.fone.valor = nfe.receiver.phone

    def det(self, line):
        l = Det_400()
        l.nItem.valor = line.sequence
        l.infAdProd.valor = line.complementary_description
        l.prod.cProd.valor = line.item_code
        l.prod.cEAN.valor = 'SEM GTIN' #line.ean_code
        if settings.NFE_AMBIENTE == NFeEnvironment.TEST:
            l.prod.xProd.valor = settings.NFCE_HOMOLOG_ITEM
        else:
            l.prod.xProd.valor = line.description
        l.prod.NCM.valor = line.ncm_code
        l.prod.CFOP.valor = line.cfop_code
        l.prod.uCom.valor = line.unit_code
        l.prod.qCom.valor = line.quantity
        l.prod.vUnCom.valor = line.nfe_unitary_value
        l.prod.vProd.valor = line.goods_value
        # l.prod.cEANTrib.valor = line.taxed_ean_code
        # l.prod.uTrib.valor = line.taxed_unit_code
        # l.prod.qTrib.valor = line.taxed_quantity
        # l.prod.vUnTrib.valor = line.nfe_taxed_unitary_value
        l.prod.cEANTrib.valor = 'SEM GTIN' #line.ean_code
        l.prod.uTrib.valor = line.unit_code
        l.prod.qTrib.valor = line.quantity
        l.prod.vUnTrib.valor = line.nfe_unitary_value
        l.prod.vFrete.valor = line.freight_value
        l.prod.vSeg.valor = line.insurance_value
        l.prod.vDesc.valor = line.discount_value
        l.prod.vOutro.valor = line.expenses_value
        l.prod.indTot.valor = line.ind_tot
        self.imposto(line=l)

        return l

    def imposto(self, line):
        l = line
        self.icmssn(line=l)
        self.pis(line=l)
        self.cofins(line=l)
        l.imposto.vTotTrib.valor = ''

    def icmssn(self, line):
        l = line
        l.imposto.ICMS.orig.valor = '0'
        l.imposto.ICMS.CSOSN.valor = '103'

    def pis(self, line):
        l = line
        l.imposto.PIS.CST.valor = '99'
        l.imposto.PIS.qBCProd.valor = 0
        l.imposto.PIS.vAliqProd.valor = 0
        l.imposto.PIS.vPIS.valor = 0

    def cofins(self, line):
        l = line
        l.imposto.COFINS.CST.valor = '99'
        l.imposto.COFINS.qBCProd.valor = 0
        l.imposto.COFINS.vAliqProd.valor = 0
        l.imposto.COFINS.vCOFINS.valor = 0

    def icmstotal(self):
        xml = self.nfe_400
        nfe = self.nfe

        xml.infNFe.total.ICMSTot.vBC.valor = nfe.tax_totals.icms_base_total
        xml.infNFe.total.ICMSTot.vICMS.valor = nfe.tax_totals.icms_value_total
        xml.infNFe.total.ICMSTot.vBCST.valor = nfe.tax_totals.icmsst_base_total
        xml.infNFe.total.ICMSTot.vST.valor = nfe.tax_totals.icmsst_value_total
        xml.infNFe.total.ICMSTot.vProd.valor = nfe.totals.goods_total
        xml.infNFe.total.ICMSTot.vFrete.valor = nfe.totals.freight_total
        xml.infNFe.total.ICMSTot.vSeg.valor = nfe.totals.insurance_total
        xml.infNFe.total.ICMSTot.vDesc.valor = nfe.totals.discount_total
        xml.infNFe.total.ICMSTot.vII.valor = nfe.tax_totals.ii_value_total
        xml.infNFe.total.ICMSTot.vIPI.valor = nfe.tax_totals.ipi_value_total
        xml.infNFe.total.ICMSTot.vPIS.valor = nfe.tax_totals.pis_value_total
        xml.infNFe.total.ICMSTot.vCOFINS.valor = nfe.tax_totals.cofins_value_total
        xml.infNFe.total.ICMSTot.vOutro.valor = nfe.totals.expenses_total
        xml.infNFe.total.ICMSTot.vNF.valor = nfe.totals.document_total

    def transporte(self):
        xml = self.nfe_400
        nfe = self.nfe

        xml.infNFe.transp.modFrete.valor = nfe.nfe_freight
        if hasattr(nfe, 'shipping'):
            shipping = nfe.shipping
            if shipping.category == PersonCategoryBR.CORPORATE:
                xml.infNFe.transp.transporta.CNPJ.valor = shipping.document_number
            else:
                xml.infNFe.transp.transporta.CPF.valor = shipping.document_number
            xml.infNFe.transp.transporta.xNome.valor = shipping.name
            xml.infNFe.transp.transporta.IE.valor = shipping.state_registry
            xml.infNFe.transp.transporta.xEnder.valor = shipping.address_sumary
            xml.infNFe.transp.transporta.xMun.valor = shipping.city_name
            xml.infNFe.transp.transporta.UF.valor = shipping.state_acronym

            if hasattr(nfe, 'shipping_volumes'):
                for volume in nfe.shipping_volumes.all():
                    v = self.vol(volume=volume)
                    xml.infNFe.transp.vol.append(v)

    def vol(self, volume):
        v = Vol_400()
        v.qVol.valor = volume.volume_quantity
        v.esp.valor = volume.volume_especies
        v.marca.valor = volume.volume_mark
        v.nVol.valor = volume.volume_number
        v.pesoB.valor = volume.gross_weight
        v.pesoL.valor = volume.net_weight
        return v

    def cobranca(self):
        xml = self.nfe_400
        nfe = self.nfe
        if hasattr(nfe, 'invoice_totals'):
            xml.infNFe.cobr.fat.nFat.valor = nfe.invoice_totals.invoice_number
            xml.infNFe.cobr.fat.vOrig.valor = nfe.invoice_totals.original_value
            xml.infNFe.cobr.fat.vDesc.valor = nfe.invoice_totals.discount_value
            xml.infNFe.cobr.fat.vLiq.valor = nfe.invoice_totals.net_value

            for invoice in nfe.invoices.all():
                i = self.dup(invoice=invoice)
                xml.infNFe.cobr.dup.append(i)

    def dup(self, invoice):
        i = Dup_400()
        i.nDup.valor = invoice.invoice_number
        i.dVenc.valor = invoice.invoice_due_date
        i.vDup.valor = invoice.net_value
        return i

    def pag(self):
        xml = self.nfe_400
        nfe = self.nfe
        p1 = Pag_400()
        p1.detPag = DetPag_400()
        p1.detPag.tPag.valor = '99'
        p1.detPag.vPag.valor = nfe.totals.document_total
        # p1.vPag.valor = nfe.totals.document_total
        xml.infNFe.pag.append(p1)


    def infAdic(self):
        xml = self.nfe_400
        nfe = self.nfe
        if hasattr(nfe, 'complementary_information'):
            compl_info = nfe.complementary_information
            info = u'%s. %s' % (compl_info.additional_information, compl_info.additional_information_auto)
            xml.infNFe.infAdic.infCpl.valor = str(info)


    def set_nfe_path(self, environment, nfe_access_key):
        path = self.initial_path

        if environment == 1:
            path = os.path.join(path, 'producao/')
        else:
            path = os.path.join(path, 'homologacao/')

        #data = '20' + nfe_access_key[2:4] + '-' + nfe_access_key[4:6]
        cnpj = nfe_access_key[6:20]
        serie = nfe_access_key[22:25]
        numero = nfe_access_key[25:34]

        #path = os.path.join(path, data + '/')
        path = os.path.join(path, cnpj + '/')
        #path = os.path.join(path, serie + '-' + numero + '/')

        try:
            os.makedirs(path)
        except:
            pass

        return path
