from django.conf import settings
from pysped.nfe.NFCeProcessor import ProcessadorNFe

def status_context_processor(request):
    p = ProcessadorNFe()
    p.versao = '3.10'
    p.estado = 'PR'
    p.certificado.arquivo = settings.CERT_PATH + '13073206/' + '10393451.pfx'
    p.certificado.senha = '123456'
    processo = p.consultar_servico()

    status = processo.resposta.cStat.valor
    motivo = processo.resposta.xMotivo.valor

    my_dict = {
        'status': status,
        'motivo': motivo
    }

    return my_dict



#
#
# from __future__ import division, print_function, unicode_literals
#
# from os.path import abspath, dirname
# from pysped.nfe import ProcessadorNFe
#
#
# FILE_DIR = abspath(dirname(__file__))
#
#
# if __name__ == '__main__':
#     p = ProcessadorNFe()
#     p.versao              = '3.10'
#     p.estado              = 'PR'
#     p.certificado.arquivo = '10393451.pfx'
#     p.certificado.senha   = '123456'
#
#     #
#     # arquivo 'certificado_caminho.txt' deve conter o caminho para o 'certificado.pfx'
#     #
#     #p.certificado.arquivo = open(FILE_DIR+'/certificado_caminho.txt').read().strip()
#
#     #
#     # arquivo 'certificado_senha.txt' deve conter a senha para o 'certificado.pfx'
#     #
#     #p.certificado.senha   = open(FILE_DIR+'/certificado_senha.txt').read().strip()
#
#     p.salva_arquivos      = True
#     p.contingencia_SCAN   = False
#     p.caminho = ''
#
#     #
#     # com as seguintes propriedades
#     #  .webservice - o webservice que foi consultado
#     #  .envio - o objeto da classe XMLNFE enviado
#     #  .envio.original - o texto do xml (envelope SOAP) enviado ao webservice
#     #  .resposta - o objeto da classe XMLNFE retornado
#     #  .resposta.version - version da HTTPResponse
#     #  .resposta.status - status da HTTPResponse
#     #  .resposta.reason - reason da HTTPResponse
#     #  .resposta.msg - msg da HTTPResponse
#     #  .resposta.original - o texto do xml (SOAP) recebido do webservice
#     #
#     processo = p.consultar_servico()
#
#     print(processo)
#     print()
#     print(processo.envio.xml)
#     print()
#     print(processo.envio.original)
#     print()
#     print(processo.resposta.xml)
#     print()
#     print(processo.resposta.original)
#     print()
#     print(processo.resposta.reason)
