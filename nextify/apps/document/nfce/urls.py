from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    url(r'^issue/$', login_required(views.NFeIssue.as_view()), name='issue'),
    url(r'^create/$', login_required(views.NFeCreateRedirect.as_view()), name='create'),
    url(r'^send/$', login_required(views.NFeSendQueryUpdate.as_view()), name='send-query'),

    url(r'^in/create/(?P<operation_pk>[\w]+)/$', login_required(views.NFeInCreate.as_view()), name='in-create'),
    url(r'^out/create/(?P<operation_pk>[\w]+)/$', login_required(views.NFeOutCreate.as_view()), name='out-create'),
    url(r'^(?P<pk>[\w]+)/in/update/$', login_required(views.NFeInUpdateView.as_view()), name='in-update'),
    url(r'^(?P<pk>[\w]+)/out/update/$', login_required(views.NFeOutUpdateView.as_view()), name='out-update'),
    url(r'^(?P<pk>[\w]+)/finalize/$', login_required(views.NFeUpdateFinalize.as_view()), name='finalize'),
    url(r'^(?P<pk>[\w]+)/in/line/update/$', login_required(views.NFeInLineUpdate.as_view()), name='in-line-update'),
    url(r'^(?P<pk>[\w]+)/out/line/update/$', login_required(views.NFeOutLineUpdate.as_view()), name='out-line-update'),
    url(r'^(?P<pk>[\w]+)/danfe/$', login_required(views.NFePdfDANFE.as_view()), name='danfe'),

    url(r'^third/$', login_required(views.NFeListThird.as_view()), name='list-third'),
    url(r'^self/$', login_required(views.NFeListSelf.as_view()), name='list-self'),
    url(r'^json/third/$', login_required(views.NFeListThirdJson.as_view()), name='list-third-json'),
    url(r'^json/self/$', login_required(views.NFeListSelfJson.as_view()), name='list-self-json'),

    # Eventos
    url(r'^event/$', login_required(views.NFeEventCreateRedirect.as_view()), name='event'),
    url(r'^(?P<nfe_pk>[\w]+)/cancel/$', login_required(views.NFeCancelCreate.as_view()), name='cancel'),
    url(r'^(?P<pk>[\w]+)/cancel/confirm/$', login_required(views.NFeCancelUpdate.as_view()), name='cancel-confirm'),
    url(r'^(?P<nfe_pk>[\w]+)/cce/$', login_required(views.NFeCceCreate.as_view()), name='cce'),
    url(r'^(?P<pk>[\w]+)/cce/confirm/$', login_required(views.NFeCceUpdate.as_view()), name='cce-confirm'),

    # API
    url(r'^nfce_api/(?P<pk>[\w]+)/$', views.APINFe.as_view(), name='api_nfce'),
]
