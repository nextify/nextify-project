# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings

from nextify.apps.person.managers import DataCompanyManager
from nextify.core.enum.payment import PaymentType, PaymentForm, PaymentCardFlag


class AbstractPaymentMethod(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)

    code = models.CharField(u'Código', max_length=6, blank=False)
    description = models.CharField(u'Descrição', max_length=255, blank=False)

    payment_form = models.IntegerField(u'Forma de Pagamento', null=False, choices=PaymentForm.choices())

    payment_type = models.CharField(u'Tipo de Pagamento', blank=True, max_length=2,
                                    choices=PaymentType.choices())

    payment_card_flag = models.CharField(u'Bandeira do Cartão', blank=True, max_length=2,
                                         choices=PaymentCardFlag.choices())

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s - %s' % (self.code, self.description)

    @models.permalink
    def get_absolute_url(self):
        return ('payment:update', (),
                {'pk': self.pk})
