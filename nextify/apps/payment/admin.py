from django.contrib import admin
from .models import PaymentMethod


class PaymentAdmin(admin.ModelAdmin):
    pass


admin.site.register(PaymentMethod, PaymentAdmin)

