# -*- coding: utf-8 -*-
import nextify.core.util.nextforms as forms
from nextify.core.util.crispy_forms.helper import FormHelper
from nextify.core.util.crispy_forms.layout import Layout, Submit, Field, Div, Row, Colmd6, HTML
from nextify.core.util.crispy_forms.bootstrap import FormActions

from .models import PaymentMethod


class PaymentMethodForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_class = 'form-horizontal'

    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Informações da Unidade</h3>'),
            Row(
                Colmd6(Field('code')),
                Colmd6(Field('description')),
            ),
            css_class='form-body',
        ),
        FormActions(Submit('submit', 'Salvar', css_class='btn green')),
    )

    class Meta:
        model = PaymentMethod

        fields = ['code', 'description']
