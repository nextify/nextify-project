# -*- coding: utf-8 -*-
from django.db.models import Q
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse

from nextify.core.views.datatables import BaseDatatableView
from nextify.core.views.generic import ObjectLookupView

from .models import PaymentMethod
from .forms import PaymentMethodForm


class PaymentMethodCreate(SuccessMessageMixin, CreateView):
    model = PaymentMethod
    form_class = PaymentMethodForm
    success_message = 'Unidade de Medida cadastrada com sucesso.'

    def get_success_url(self):
        return reverse('payment:update', kwargs={'pk': self.object.pk, })



class PaymentMethodUpdate(SuccessMessageMixin, UpdateView):
    model = PaymentMethod
    form_class = PaymentMethodForm
    success_message = 'Unidade de Medida atualizada com sucesso.'

    def get_success_url(self):
        return reverse('payment:update', kwargs={'pk': self.object.pk, })


class PaymentMethodList(ListView):
    model = PaymentMethod


class PaymentMethodListJson(BaseDatatableView):
    model = PaymentMethod
    columns = ['id', 'code', 'description', 'edit']
    order_columns = ['id', 'code', 'description']

    max_display_length = 500


class PaymentMethodLookupView(ObjectLookupView):
    model = PaymentMethod

    def lookup_filter(self, qs, term):
        return qs.filter(Q(code__istartswith=term) | Q(description__icontains=term))
