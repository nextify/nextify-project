from .abstract_models import AbstractStockTransaction, AbstractStock


class StockTransaction(AbstractStockTransaction):
    pass


class Stock(AbstractStock):
    pass


