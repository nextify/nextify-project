# -*- coding: utf-8 -*-
from django.db import models, transaction
from django.conf import settings
from django.apps import apps

from nextify.apps.person.managers import CompanyManager
from nextify.core.enum.product import ProductOrigin
from nextify.core.enum.stock import StockType, StockLineStatus
from nextify.core.enum.operation import OperationType
from nextify.core.enum.document import DocumentType
from nextify.core.enum.default import YesNo


class StockManager(CompanyManager):

    def stock_calculate(self, base_transaction, stock_status):
        with transaction.atomic():
            model = apps.get_model('stock', 'StockTransaction')
            if stock_status == StockLineStatus.NORMAL:
                model.on_company.filter(id=base_transaction.pk).update(stock_status=stock_status)
            elif stock_status == StockLineStatus.OPEN or stock_status == StockLineStatus.CANCELED:
                model.on_company.filter(id=base_transaction.pk).update(stock_status=stock_status,
                                                                       previous_cost=0,
                                                                       previous_quantity=0)
            self.stock_average_cost(base_transaction=base_transaction)


    def stock_average_cost(self, base_transaction):
        with transaction.atomic():
            from decimal import Decimal
            model = apps.get_model('stock', 'StockTransaction')

            product = base_transaction.product
            unit = base_transaction.unit
            stock_date = base_transaction.stock_date
            o = model.on_company.filter(product=product, unit=unit, stock_date__lte=stock_date,
                                        stock_status=StockLineStatus.NORMAL) \
                .order_by('stock_date', 'id') \
                .first()

            objs = None
            actual_cost = Decimal('0.00')
            actual_quantity = Decimal('0.00')
            average_cost = Decimal('0.00')
            if o is None:
                objs = model.on_company\
                    .filter(product=product, unit=unit, stock_date__gte=stock_date,
                            stock_status=StockLineStatus.NORMAL) \
                    .order_by('stock_date', 'id')
            else:
                if o.operation_type == OperationType.IN:
                    actual_cost = o.previous_cost + o.cost
                    actual_quantity = o.previous_quantity + o.quantity
                else:
                    actual_cost = o.previous_cost - o.cost
                    actual_quantity = o.previous_quantity - o.quantity
                objs = model.on_company \
                    .filter(product=product, unit=unit, stock_date__gte=o.stock_date,
                            stock_status=StockLineStatus.NORMAL) \
                    .exclude(id=o.id) \
                    .order_by('stock_date', 'id')

            for obj in objs:
                obj.previous_cost = actual_cost
                obj.previous_quantity = actual_quantity
                if actual_cost == 0 or actual_quantity == 0:
                    average_cost = Decimal('0.00')
                else:
                    average_cost = actual_cost / actual_quantity
                if obj.calculate_cost:
                    obj.cost = average_cost
                obj.save()

                if obj.operation_type == OperationType.IN:
                    actual_cost = actual_cost + obj.cost
                    actual_quantity = actual_quantity + obj.quantity
                else:
                    actual_cost = actual_cost - obj.cost
                    actual_quantity = actual_quantity - obj.quantity

            self.update_cost(base_transaction=base_transaction, quantity=actual_quantity, cost=actual_cost)

    def update_cost(self, base_transaction, quantity, cost):
        with transaction.atomic():
            model = apps.get_model('stock', 'Stock')

            defaults = {'quantity': quantity,
                        'cost': cost}
            stock, created = model._default_manager.get_or_create(product=base_transaction.product,
                                                                  unit=base_transaction.unit,
                                                                  defaults=defaults)

            if not created:
                stock.quantity = quantity
                stock.cost = cost
            stock.save()




class AbstractStockTransaction(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID)

    operation_type = models.IntegerField('Operation Type', choices=OperationType.choices(), null=False)
    stock_status = models.IntegerField('Operation Type', choices=StockLineStatus.choices(), null=False)
    stock_date = models.DateField(u'Data do estoque', null=True)
    stock_history = models.CharField(u'Historico', max_length=500, blank=True)

    product = models.ForeignKey('catalogue.Product', null=False)
    origin = models.IntegerField(u'Origem', null=False, choices=ProductOrigin.choices(), default=ProductOrigin.NACIONAL)
    stock_type = models.IntegerField(u'Tipo de Estoque', null=False, choices=StockType.choices(),
                                     default=StockType.OWN_STOCK)
    unit = models.ForeignKey('unit.Unit', null=False)
    quantity = models.DecimalField(u'Quantidade', max_digits=15, decimal_places=6, default=0, null=False)
    cost = models.DecimalField(u'Custo', max_digits=15, decimal_places=6, default=0, null=False)
    previous_quantity = models.DecimalField(u'Quantidade Anterior', max_digits=15, decimal_places=6, default=0, null=False)
    previous_cost = models.DecimalField(u'Custo Anterior', max_digits=15, decimal_places=6, default=0, null=False)
    calculate_cost = models.BooleanField(u'Calcular Custo', choices=YesNo.choices(), default=YesNo.NO)

    document_type = models.IntegerField('Document Type', choices=DocumentType.choices(), editable=False)
    document_id = models.IntegerField('ID do Documento', null=True)
    document_line_sequence = models.IntegerField('Sequencia do Item no Documento', null=True)

    date_created = models.DateTimeField(u'Data de Cadastro', auto_now_add=True, null=True)
    date_updated = models.DateTimeField(u'Data de Atualização', auto_now=True, null=True)

    on_company = StockManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s' % str(self.pk)

    # def save(self, force_insert=False, force_update=False, using=None,
    #          update_fields=None):
    #     # if self.document_id != 0:
    #     #     self.create_initial_transaction()
    #     super(AbstractStockTransaction, self).save(force_insert, force_update, using, update_fields)

    def create_initial_transaction(self):
        model = self.__class__
        if model.on_company.filter(product=self.product, unit=self.unit).count() > 0:
            return

        defaults = {
            'operation_type': OperationType.IN,
            'stock_status': StockLineStatus.NORMAL,
            'stock_date': self.stock_date,
            'stock_history': u'Implantação Inicial de Estoque',
            'quantity': 0.000000,
            'cost': 0.000000,
            'previous_quantity': 0.000000,
            'previous_cost': 0.000000,
            'document_type': self.document_type,
            'document_id': 0,
            'document_line_sequence': 0,
        }
        obj, created = model.on_company.get_or_create(product=self.product, unit=self.unit, defaults=defaults)

        if created:
            obj.save()


class AbstractStock(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, default=settings.COMPANY_ID)

    product = models.ForeignKey('catalogue.Product', null=False, related_name='stock')
    origin = models.IntegerField(u'Origem', null=False, choices=ProductOrigin.choices(), default=ProductOrigin.NACIONAL)
    stock_type = models.IntegerField(u'Tipo de Estoque', null=False, choices=StockType.choices(),
                                     default=StockType.OWN_STOCK)
    unit = models.ForeignKey('unit.Unit', null=False)
    quantity = models.DecimalField(u'Custo', max_digits=15, decimal_places=6, default=0, null=False)
    cost = models.DecimalField(u'Custo', max_digits=15, decimal_places=6, default=0, null=False)

    date_created = models.DateTimeField(u'Data de Cadastro', auto_now_add=True, null=True)
    date_updated = models.DateTimeField(u'Data de Atualização', auto_now=True, null=True)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True