__author__ = 'carlos'
from django.db import transaction
from nextify.core.enum.sequence import *

def get_next_value(id):
    from .models import BaseSequence

    with transaction.atomic():
        sequence = BaseSequence.on_entity.select_for_update().get(pk=id)
        sequence.last += 1

        sequence.save()

        return sequence.last


def get_next_code(sequence_type, sub_sequence_id=None):
    from .models import GenericSequence

    o = None
    if sequence_type == SequenceType.PRODUCT:
        o = GenericSequence.on_company.get(sequence_type=sequence_type, item_category_id=sub_sequence_id)
    elif sequence_type == SequenceType.BANK_NUMBER:
        o = GenericSequence.on_company.get(sequence_type=sequence_type, bank_account_id=sub_sequence_id)
    else:
        o = GenericSequence.on_company.get(sequence_type=sequence_type)

    if o is not None:
        return build_code(o.id)
    else:
        return False


def get_next_model_sequence(model):
    from .models import ModelSequence

    try:
        o = ModelSequence.on_company.get(model=model, is_default=True)
    except ModelSequence.DoesNotExist:
        return False
    else:
        return o.series, get_next_value(o.base_sequence_id)


def build_sequence(expr, sequence):
    expr = expr[0:len(expr)-len(str(sequence))]
    return '%s%s' % (expr.replace('#', '0'), str(sequence))


def build_code(id):
    from .models import GenericSequence

    code = ''
    o = GenericSequence.on_company.get(pk=id)
    sequence = get_next_value(o.base_sequence.id)
    code = build_sequence(o.sequence_expression, sequence)
    layout = o.layout

    if o.prefix != '':
        layout = layout.replace('#PREFIX#', o.prefix)

    if o.suffix != '':
        layout = layout.replace('#SUFFIX#', o.suffix)

    if o.sequence_type == SequenceType.PRODUCT:
        if o.item_category is not None:
            layout = layout.replace('#ITEM_CATEGORY#', o.item_category.code)
        layout = layout.replace('#ITEM_GENRE#', str(o.item_category.genre))

    layout = layout.replace('#EXPRESSION#', code)

    return layout


