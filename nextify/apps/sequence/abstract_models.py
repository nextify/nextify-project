# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.apps import apps

from nextify.apps.person.managers import CompanyManager, EntityManager, DataCompanyManager
from nextify.apps.sequence import get_next_code
from nextify.core.enum.document import *
from nextify.core.enum.sequence import *
from nextify.core.enum.default import YesNo


class AbstractBaseSequence(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, editable=False)
    code = models.CharField('Code', max_length=100, blank=True)
    last = models.PositiveIntegerField('Last Sequence', null=False, default=0)

    on_entity = EntityManager()
    on_company = CompanyManager()
    objects = models.Manager()

    def __unicode__(self):
        return '%s' % self.code

    class Meta:
        abstract = True


class AbstractModelSequence(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, editable=False, default=settings.COMPANY_ID)
    model = models.IntegerField("Model", choices=DocumentModel.choices(), null=False)
    series = models.CharField('Series', max_length=6, blank=False)
    base_sequence = models.ForeignKey('sequence.BaseSequence', null=False, editable=False)
    is_default = models.NullBooleanField(u'Sequencia padrão', default=YesNo.NO, choices=YesNo.choices())

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.create_sequence()
        if not self.id:
            self.is_default = self.exists_default()
        else:
            self.ensure_defaults_integrity()
        super(AbstractModelSequence, self).save(force_insert, force_update, using, update_fields)

    def create_sequence(self):
        code = 'Model_%s_-_Series_%s' % (str(self.model), self.series)
        model = apps.get_model('sequence', 'BaseSequence')
        o, created = model.on_entity.get_or_create(company=self.company, code=code)

        if created:
            o.save()

        self.base_sequence = o

    def exists_default(self):
        model = apps.get_model('sequence', 'ModelSequence')
        count = model.on_company.filter(model=self.model, is_default=YesNo.YES).count()
        if count > 0:
            return YesNo.NO
        return YesNo.YES

    def ensure_defaults_integrity(self):
        if self.is_default:
            self.__class__._default_manager \
                .filter(model=self.model, is_default=True) \
                .update(is_default=False)


class AbstractGenericSequence(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, editable=False, default=settings.DATA_COMPANY_ID)
    sequence_type = models.CharField(u'Tipo da Sequencia', blank=False, max_length=20, choices=SequenceType.choices())
    prefix = models.CharField(u'Prefixo', max_length=20, blank=True)
    suffix = models.CharField(u'Sufixo', max_length=20, blank=True)
    sequence_expression = models.CharField(u'Expressão da Sequencia', max_length=10, blank=False)
    layout = models.CharField(u'Layout', max_length=100, blank=False)
    base_sequence = models.ForeignKey('sequence.BaseSequence', null=True, editable=False)
    item_category = models.ForeignKey('catalogue.ItemCategory', null=True, editable=False)
    bank_account = models.ForeignKey('banking.BankAccount', null=True, editable=False)

    on_company = DataCompanyManager()
    objects = models.Manager()


    def __unicode__(self):
        return '%s (%s)' % (self.sequence_type, self.prefix)

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.sequence_type != SequenceType.BASE_PRODUCT:
            self.create_sequence()
        super(AbstractGenericSequence, self).save(force_insert, force_update, using, update_fields)

    def create_sequence(self):
        code = ''
        if self.sequence_type == SequenceType.PRODUCT:
            code = 'Product_Category_%s' % (self.item_category.code)
        if self.sequence_type == SequenceType.ITEM_CATEGORY:
            code = 'Item_Category'
        if self.sequence_type == SequenceType.CUSTOMER:
            code = 'Customer'
        if self.sequence_type == SequenceType.SUPPLIER:
            code = 'Supplier'
        if self.sequence_type == SequenceType.BANK_NUMBER:
            code = 'Bank_Number'
        if self.sequence_type == SequenceType.BANK_FILE:
            code = 'Bank_File'


        model = apps.get_model('sequence', 'BaseSequence')
        o, created = model.on_entity.get_or_create(company=self.company, code=code)

        if created:
            o.save()
            self.base_sequence = o





