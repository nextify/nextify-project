from django.contrib import admin
from .models import *


class SequenceAdmin(admin.ModelAdmin):
    pass


admin.site.register(BaseSequence, SequenceAdmin)
admin.site.register(ModelSequence, SequenceAdmin)
admin.site.register(GenericSequence, SequenceAdmin)

