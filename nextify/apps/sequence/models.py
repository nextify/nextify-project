from .abstract_models import AbstractBaseSequence, AbstractModelSequence, AbstractGenericSequence


class BaseSequence(AbstractBaseSequence):
    pass


class ModelSequence(AbstractModelSequence):
    pass


class GenericSequence(AbstractGenericSequence):
     pass