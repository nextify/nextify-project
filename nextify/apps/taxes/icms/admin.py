from django.contrib import admin
from .models import *


class TaxAdmin(admin.ModelAdmin):
    pass


admin.site.register(IcmsSN, TaxAdmin)

