from django.db import models
from django.utils.translation import ugettext_lazy as _
from nextify.core.enum.Taxes import *
from nextify.apps.taxes.models import Tax

class AbstractIcmsSN(Tax):
    csosn = models.IntegerField('CSOSN', null=False, choices=TaxIcmsSNType.choices())

    is_active = models.BooleanField(_("Active?"), null=False, default=True)

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.tax_type = TaxType.ICMS
        super(AbstractIcmsSN, self).save(force_insert, force_update, using,update_fields)
