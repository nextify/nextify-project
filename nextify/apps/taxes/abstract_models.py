from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from nextify.core.enum.Taxes import *
from nextify.apps.person.managers import CompanyManager


class AbstractTax(models.Model):
    entity = models.ForeignKey('person.Entity', default=settings.ENTITY_ID, null=False, editable=False)
    company = models.ForeignKey('person.Company', default=settings.COMPANY_ID, null=False, editable=False)
    tax_type = models.IntegerField(_("Tax Type"), choices=TaxType.choices(), editable=False)
    code = models.CharField(_("Tax Code"), max_length=50, blank=False)
    description = models.TextField(_("Description"), max_length=255, blank=False)

    on_company = CompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True