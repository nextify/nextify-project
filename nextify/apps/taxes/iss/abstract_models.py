from django.db import models
from django.utils.translation import ugettext_lazy as _

from ..models import *
from nextify.apps.person.models import CurrentEntityManager
from nextify.externals.tables.Nbs.models import NbsClassification
from nextify.externals.tables.Ncm.models import NcmClassification
from nextify.externals.tables.ServiceCode.models import ServiceCodeList
from ..enum import *


class IssManager(CurrentEntityManager):

    def select_iss(self, company, service_code=None, nbs=None, operation=None):
        l = self.select_all(company, service_code, nbs, operation)
        if len(l) > 0:
            return l[0]
        return None

    def select_all(self, company, service_code=None, nbs=None, operation=None):
        l = []
        if operation is not None and service_code is not None:
            l = self.filter(company=company, related_operations=operation,
                            related_service_code=service_code, is_active=True)
            if len(l) > 0:
                return l
        if operation is not None and nbs is not None:
            l = self.filter(company=company, related_operations=operation,
                            related_nbs=nbs, is_active=True)
            if len(l) > 0:
                return l
        if service_code is not None:
            l = self.filter(company=company, related_service_code=service_code, is_active=True)
            if len(l) > 0:
                return l
        if nbs is not None:
            l = self.filter(company=company, related_nbs=nbs, is_active=True)
            if len(l) > 0:
                return l
        if operation is not None:
            l = self.filter(company=company, related_operations=operation, is_active=True)
            if len(l) > 0:
                return l
        return l


class AbstractIss(Tax):
    tax_rate = models.DecimalField(_("Tax rate"), max_digits=7, decimal_places=4, null=False)
    tax_table_type = models.IntegerField(_("Tax Table Type"), choices=TaxTableType.choices())
    taxation_type = models.IntegerField(_("Taxation type"), choices=IssTaxationType.choices())

    related_operations = models.ManyToManyField(
        'operation.Operation', related_name='operations', blank=True,
        verbose_name=_("Related Operations"))

    related_nbs = models.ManyToManyField(NbsClassification, related_name='nbs', blank=True,
                                         verbose_name=_("Related NBS\'s"))

    related_service_code = models.ManyToManyField(ServiceCodeList, related_name='service_code', blank=True,
                                                  verbose_name=_("Related Service Codes"))

    is_active = models.BooleanField(_("Active?"), null=False, default=True)

    objects = models.Manager()
    on_entity = IssManager()

    class Meta:
        abstract = True
        app_label = 'taxes'
