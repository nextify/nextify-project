# -*- coding: utf-8 -*-
from django_enumfield import enum
from django.utils.translation import ugettext_lazy as _


TABLE_TYPE_GENERIC = 'GN'
TABLE_TYPE_OPERATION = 'OP'
TABLE_TYPE_NBS = 'NS'
TABLE_TYPE_SERVICE_CODE = 'SC'
TABLE_TYPE_OPERATION_NBS = 'ON'
TABLE_TYPE_OPERATION_SERVICE_CODE = 'OS'

class TaxTableType(enum.Enum):
    GENERIC = 0
    OPERATION = 1
    NBS = 2
    SERVICE_CODE = 3
    OPERATION_NBS = 4
    OPERATION_SERVICE_CODE = 5

    labels = {
        GENERIC: _("Generic"),
        OPERATION: _("Operation"),
        NBS: _("NBS"),
        SERVICE_CODE: _("Service Code"),
        OPERATION_NBS: _("Operation/NBS"),
        OPERATION_SERVICE_CODE: _("Operation/Service Code"),
    }

class TaxType(enum.Enum):
    ISS = 0
    PIS = 1
    COFINS = 2
    ICMS = 3
    IPI = 4
    IBPT = 9

    labels = {
        ISS: 'Iss',
        PIS: 'Pis',
        COFINS: 'Cofins',
        ICMS: 'Icms',
        IPI: 'Ipi',
        IBPT: 'Ibpt',
    }

class IssTaxationType(enum.Enum):
    COUNTY = 1
    OUTSIDE_COUNTY = 2
    EXEMPT = 3
    IMMUNE = 4
    SUSPENDED_ADM = 5
    SUSPENDED_JUD = 6

    labels = {
        COUNTY: _(u'Tributação no município'),
        OUTSIDE_COUNTY: _(u'Tributação fora do município'),
        EXEMPT: _(u'Isenção'),
        IMMUNE: _(u'Imune'),
        SUSPENDED_ADM: _(u'Exigibilidade suspensa (Decisão Judicial)'),
        SUSPENDED_JUD: _(u'Exigibilidade suspensa (Procedimento Administrativo)'),
    }
