from django.db import models
from django.db.models.loading import get_model
from django.utils.translation import ugettext_lazy as _

from nextify.extensions.nextcsv import model as csvmodel, fields as csvfield

class IbptCSV(csvmodel.CsvModel):
    codigo = csvfield.CharField()
    ex = csvfield.CharField()
    tabela = csvfield.CharField()
    descricao = csvfield.CharField()
    aliqNac = csvfield.FloatField()
    aliqImp = csvfield.FloatField()
    versao = csvfield.CharField()

    class Meta:
        delimiter = ';'


class IbptManager(models.Manager):

    def import_csv(self, csv_file):
        data = IbptCSV.import_data(data=open(csv_file))

        for item in data:
            model = None

            if item.tabela == '0':
                if len(item.codigo) != 8:
                    continue
                model = get_model('core', 'NcmClassification')
            elif item.tabela == '1':
                if len(item.codigo) != 9:
                    continue
                model = get_model('core', 'NbsClassification')
            elif item.tabela == '2':
                model = get_model('core', 'ServiceCodeList')
            try:
                obj = model.objects.get(code=item.codigo)
            except model.DoesNotExist:
                print(item.codigo)

            else:
                t = self.model(code=item.codigo, description=obj.description, exception=item.ex,
                               national_rate=item.aliqNac, international_rate=item.aliqImp, version='0.0.2')
                if item.tabela == '0':
                    t.ncm = obj
                elif item.tabela == '1':
                    t.nbs = obj
                elif item.tabela == '2':
                    t.service_code = obj
                t.save(using=self._db)

    def select_ibpt(self, ncm=None, nbs=None, service_code=None):
        obj = None
        if ncm is not None:
            obj = self.filter(ncm=ncm)[0]
        elif service_code is not None:
            obj = self.filter(service_code=service_code)[0]
        elif nbs is not None:
            obj = self.filter(nbs=nbs)[0]
        return obj


class AbstractIbpt(models.Model):
    code = models.CharField(_("Code"), max_length=10)
    description = models.CharField(_("Description"), max_length=255)
    ncm = models.ForeignKey('core.NcmClassification', null=True)
    nbs = models.ForeignKey('core.NbsClassification', null=True)
    service_code = models.ForeignKey('core.ServiceCodeList', null=True)
    exception = models.CharField(_("NCM Exception"), max_length=3, blank=True)
    national_rate = models.DecimalField(_("National rate"), max_digits=15, decimal_places=2)
    international_rate = models.DecimalField(_("National rate"), max_digits=15, decimal_places=2)
    version = models.CharField(_("Version"), max_length=10, blank=False)

    objects = IbptManager()
    on_entity = IbptManager()

    class Meta:
        abstract = True
        app_label = 'taxes'
