from .abstract_models import *


class Series(AbstractSeries):
    pass


class SeriesSequence(AbstractSeriesSequence):
    pass
