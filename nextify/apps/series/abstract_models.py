from django.db import models
from django.db.models import ObjectDoesNotExist
from django.apps import apps
from django.conf import settings

from nextify.apps.person.managers import CompanyManager


class AbstractSeries(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False,
                               default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company')
    code = models.CharField(blank=False, max_length=6)
    description = models.CharField(blank=False, max_length=100)

    on_company = CompanyManager()
    objects = models.Manager()

    def __unicode__(self):
        return '%s' % self.code

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(AbstractSeries, self).save(force_insert, force_update, using,
                                         update_fields)
        if not self.exists_sequence:
            model = apps.get_model('series', 'SeriesSequence')
            model.on_entity.create(company=self.company, series=self, sequence=0)

    @property
    def exists_sequence(self):
        model = apps.get_model('series', 'SeriesSequence')
        try:
            param = model.on_entity.get(company=self.company, series=self)
        except ObjectDoesNotExist:
            return False
        else:
            return True

    @property
    def next_sequence(self):
        model = apps.get_model('sequences', 'SeriesSequence')
        return model.on_entity.get_serie_sequence(company=self.company, series=self)

class SeriesSequenceManager(CompanyManager):
    def get_current(self, series):
        obj = self\
            .select_for_update()\
            .filter(series=series)[0]
        obj.sequence += 1
        obj.save()
        return obj

class AbstractSeriesSequence(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False,
                               default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company')
    series = models.ForeignKey('series.Series')
    sequence = models.IntegerField(null=False)

    objects = models.Manager()
    on_entity = SeriesSequenceManager()

    class Meta:
        abstract = True

#
# class SequenceManager(CurrentEntityManager):
#     def get_current(self, sequence_type):
#         obj = self\
#             .select_for_update()\
#             .filter(type=sequence_type)[0]
#         obj.sequence += 1
#         obj.save()
#         return obj
#
#     def get_company_serie_current(self, series, company):
#         obj = self\
#             .select_for_update()\
#             .filter(series=series,
#                     company=company)[0]
#         obj.sequence += 1
#         obj.save()
#         return obj
#
#     def get_company_current(self, sequence_type, company):
#         obj = self\
#             .select_for_update()\
#             .filter(type=sequence_type,
#                     company=company)[0]
#         obj.sequence += 1
#         obj.save()
#         return obj
#
#     def get_sequence(self, sequence_type, company):
#         seq = self.get_company_current(sequence_type, company)
#         return seq.sequence
#
#     def get_serie_sequence(self, series, company):
#         seq = self.get_company_serie_current(series, company)
#         return seq.sequence
#
#     def get_code(self, sequence_type, obj, template = None):
#         ctx = Context()
#         seq = self.get_current(sequence_type=sequence_type)
#         ctx['sequence'] = seq
#         ctx['obj'] = obj
#         ctx['code'] = build_sequence(seq.expr, seq.sequence)
#         return render_to_string('management/settings/%s_code.txt' % (template or sequence_type), ctx)
#
#
# class AbstractBaseSequence(models.Model):
#     entity = models.ForeignKey('person.Entity', null=False, editable=False,
#                                default=settings.ENTITY_ID)
#     type = models.CharField(max_length=20, blank=False)
#     sequence = models.IntegerField(null=False)
#
#     objects = models.Manager()
#     on_entity = SequenceManager()
#
#     class Meta:
#         abstract = True
#
#
#
# class AbstractSeriesSequence(AbstractBaseSequence):
#     company = models.ForeignKey('person.Company', null=False)
#     series = models.ForeignKey('series.Series', null=False)
#
#     class Meta:
#         abstract = True
#
#     def save(self, force_insert=False, force_update=False, using=None,
#              update_fields=None):
#         self.type = 'series'
#         super(AbstractSeriesSequence, self).save(force_insert, force_update, using,
#                                                 update_fields)
