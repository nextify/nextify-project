# -*- coding: utf-8 -*-
from nextify.apps.person.models import Customer
from nextify.apps.person import forms

from nextify.core.enum.person import PersonCategory


class CustomerForm(forms.PersonForm):
    class Meta(forms.PersonForm.Meta):
        model = Customer
