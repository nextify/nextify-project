# -*- coding: utf-8 -*-
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView, TemplateView
from django.core.urlresolvers import reverse
from django.db.models import Q

from nextify.apps.person.models import Customer
from nextify.apps.person.views import PersonCreate
from nextify.core.views.datatables import BaseDatatableView
from nextify.core.views.generic import ObjectLookupView
from nextify.core.enum.person import PersonCategory

from .forms import *


class CustomerCreate(SuccessMessageMixin, PersonCreate):
    form_class = CustomerForm
    model = Customer
    success_message = 'Cliente cadastrado com sucesso.'

    def get_success_url(self):
        return reverse('customer:update', kwargs={'pk': self.object.pk, })


class CustomerUpdate(SuccessMessageMixin, UpdateView):
    form_class = CustomerForm
    model = Customer
    success_message = 'Cliente Atualizado com sucesso.'

    def get_success_url(self):
        return reverse('customer:update', kwargs={'pk': self.object.pk, })

    def get_context_data(self, **kwargs):
        context = super(CustomerUpdate, self).get_context_data(**kwargs)
        context['customer_pk'] = self.object.pk
        context['customer_code'] = self.object.code
        context['customer_name'] = self.object.name
        return context


class CustomerList(TemplateView):
    template_name = 'person/customer_list.html'


class CustomerListJson(BaseDatatableView):
    model = Customer
    columns = ['id', 'document_number', 'name', 'edit']
    order_columns = ['id', 'document_number', 'name']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def filter_queryset(self, qs):
        return qs.filter(editable=True)

    # def filter_queryset(self, qs):
    #     search_values = self.get_search_values()
    #     cols = self.get_columns()
    #     for i in range(len(search_values)):
    #         if search_values[i] != '':
    #             if cols[i] == 'name':
    #                 qs = qs.filter(name__icontains=search_values[i])
    #             if cols[i] == 'category':
    #                 qs = qs.filter(category__exact=search_values[i])
    #             if cols[i] == 'is_active':
    #                 qs = qs.filter(is_active=str2bool(search_values[i]))
    #
    #     return qs
    #
    # def get_initial_queryset(self, company=None):
    #     return super(CustomerListViewJson, self).get_initial_queryset(get_logged_company(self.request))

class CustomerLookupView(ObjectLookupView):
    model = Customer

    def lookup_filter(self, qs, term):
        qs = qs.filter(editable=True)
        return qs.filter(Q(document_number__istartswith=term) | Q(name__icontains=term) | Q(fancy_name__icontains=term))
