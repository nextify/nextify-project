# -*- coding: utf-8 -*-
from django.views.generic.edit import UpdateView
from django.db.models import Q
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse

from nextify.apps.person.models import Supplier
from nextify.apps.person.views import PersonCreate
from nextify.core.views.datatables import BaseDatatableView
from nextify.core.views.generic import ObjectLookupView
from .forms import *


class SupplierCreate(SuccessMessageMixin, PersonCreate):
    form_class = SupplierForm
    model = Supplier
    success_message = 'Fornecedor cadastrado com sucesso!'

    def get_success_url(self):
        return reverse('supplier:update', kwargs={'pk': self.object.pk, })


class SupplierUpdate(SuccessMessageMixin, UpdateView):
    form_class = SupplierForm
    model = Supplier
    success_message = 'Fornecedor atualizado com sucesso!'

    def get_context_data(self, **kwargs):
        context = super(SupplierUpdate, self).get_context_data(**kwargs)
        context['supplier_pk'] = self.object.pk
        context['supplier_code'] = self.object.code
        context['supplier_name'] = self.object.name
        return context


class SupplierList(TemplateView):
    template_name = 'person/supplier_list.html'


class SupplierListJson(BaseDatatableView):
    model = Supplier
    columns = ['id', 'document_number', 'name', 'edit']
    order_columns = ['id', 'document_number', 'name']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def filter_queryset(self, qs):
        return qs.filter(editable=True)

    # def filter_queryset(self, qs):
    #     search_values = self.get_search_values()
    #     cols = self.get_columns()
    #     for i in range(len(search_values)):
    #         if search_values[i] != '':
    #             if cols[i] == 'name':
    #                 qs = qs.filter(name__icontains=search_values[i])
    #             if cols[i] == 'category':
    #                 qs = qs.filter(category__exact=search_values[i])
    #             if cols[i] == 'is_active':
    #                 qs = qs.filter(is_active=str2bool(search_values[i]))
    #
    #     return qs


class SupplierLookupView(ObjectLookupView):
    model = Supplier

    def lookup_filter(self, qs, term):
        qs = qs.filter(editable=True)
        return qs.filter(Q(document_number__istartswith=term) | Q(name__icontains=term) | Q(fancy_name__icontains=term))
