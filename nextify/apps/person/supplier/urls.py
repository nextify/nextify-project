from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^create/$', login_required(views.SupplierCreate.as_view()), name='create'),
    url(r'^(?P<pk>\d+)/update/$', login_required(views.SupplierUpdate.as_view()), name='update'),

    url(r'^$', login_required(views.SupplierList.as_view()), name='list'),
    url(r'^json/$', login_required(views.SupplierListJson.as_view()), name='list-json'),
    url(r'^lookup/$', login_required(views.SupplierLookupView.as_view()), name='lookup'),
]
