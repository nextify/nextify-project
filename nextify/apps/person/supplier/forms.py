# -*- coding: utf-8 -*-
from nextify.apps.person.models import Supplier
from nextify.apps.person import forms


class SupplierForm(forms.PersonForm):
    class Meta(forms.PersonForm.Meta):
        model = Supplier

