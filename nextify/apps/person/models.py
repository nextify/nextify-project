from nextify.apps.person.abstract_models import *

from .company.abstract_models import AbstractCompany, AbstractCompanyPerson
from .customer.abstract_models import AbstractCustomer
from .supplier.abstract_models import AbstractSupplier
from .shippingcompany.abstract_models import AbstractShippingCompany


class Entity(AbstractEntity):
    pass

def get_current_entity(request):
    """
    Checks if company is installed and returns the current
    ``Company`` object.
    """
    current_entity = Entity.objects.get_current()

    return current_entity


class Person(AbstractBasePerson):
    pass


class Company(AbstractCompany):
    pass


class CompanyPerson(AbstractCompanyPerson, Person):
    pass


class Customer(AbstractCustomer, Person):
    pass


class Supplier(AbstractSupplier, Person):
    pass


class ShippingCompany(AbstractShippingCompany, Person):
    pass

