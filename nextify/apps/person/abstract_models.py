# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from nextify.apps.person.managers import DataCompanyManager
from django.db import models
from django.db.models import ObjectDoesNotExist
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from nextify.core.models.mixins import ModelDiffMixin
from nextify.core.enum.person import *
from nextify.core.enum.default import Active, YesNo
from nextify.core.util.filters import cnpj_cpf


ENTITY_CACHE = {}

class CurrentEntityManager(models.Manager):

    def get_current(self):
        """
        Returns the current ``Entity`` based on the ENTITY_ID in the
        project's settings. The ``Company`` object is cached the first
        time it's retrieved from the database.
        """
        try:
            sid = settings.ENTITY_ID
        except AttributeError:
            from django.core.exceptions import ImproperlyConfigured
            raise ImproperlyConfigured("You're using the NEXTIFY \"entity framework\" without having set the ENTITY_ID setting. Create a entty in your database and set the ENTITY_ID setting to fix this error.")
        try:
            current_entity = ENTITY_CACHE[sid]
        except KeyError:
            current_entity = self.get(pk=sid)
            ENTITY_CACHE[sid] = current_entity
        return current_entity

    def clear_cache(self):
        """Clears the ``Site`` object cache."""
        global ENTITY_CACHE
        ENTITY_CACHE = {}


class AbstractEntity(models.Model):
    name = models.CharField(_("Name"), max_length=60)
    document_number = models.CharField(max_length=20)

    objects = CurrentEntityManager()

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(AbstractEntity, self).save(*args, **kwargs)
        # Cached information will likely be incorrect now.
        if self.id in ENTITY_CACHE:
            del ENTITY_CACHE[self.id]

    def delete(self):
        pk = self.pk
        super(AbstractEntity, self).delete()
        try:
            del ENTITY_CACHE[pk]
        except KeyError:
            pass


class AbstractBasePerson(ModelDiffMixin, models.Model):
    code = models.CharField(u'Código', max_length=60, blank=True)
    category = models.SmallIntegerField(choices=PersonCategory.choices(), verbose_name='Tipo Pessoa')
    document_number = models.CharField('CPF/CNPJ', max_length=20, blank=True)
    name = models.CharField('Nome', max_length=60, blank=False)
    fancy_name = models.CharField('Nome Fantasia', max_length=100, blank=True)
    state_registry_id = models.SmallIntegerField(choices=PersonIE.choices(), verbose_name='Tipo Contribuinte',
                                                 null=False, default=PersonIE.EXEMPT_TAXPAYER)
    state_registry = models.CharField('Inscrição Estadual', max_length=14, blank=True)
    city_registry = models.CharField('Inscrição Municipal', max_length=15, blank=True)
    suframa_registry = models.CharField('Inscrição Suframa', max_length=9, blank=True)
    crt = models.IntegerField('Regime Tributário', choices=PersonCRT.choices(), null=True)
    email = models.EmailField('Email', max_length=100, blank=True)

    is_active = models.BooleanField(_('Active'),
                                    default=True,
                                    choices=Active.choices(),
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))

    date_created = models.DateTimeField(_('date joined'), auto_now_add=True, null=True)
    date_updated = models.DateTimeField(_("Date Updated"), auto_now=True, null=True)

    def __unicode__(self):
        return '%s - %s' % (cnpj_cpf(self.document_number), self.name)

    class Meta:
        abstract = True
        app_label = 'person'

    def has_customer(self):
        return hasattr(self, 'customer')

    def has_supplier(self):
        return hasattr(self, 'supplier')

    def has_shippingcompany(self):
        return hasattr(self, 'shippingcompany1')

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self._set_defaults()
        super(AbstractBasePerson, self).save(force_insert, force_update, using, update_fields)

    def set_active(self):
        if self.is_active:
            return
        self.is_active = True
        self.save()
    set_active.alters_data = True

    def set_inactive(self):
        if not self.is_active:
            return
        self.is_active = False
        self.save()
    set_inactive.alters_data = True

    def _set_defaults(self):
        if self.category in (PersonCategory.INDIVIDUAL, PersonCategory.FOREIGN):
            self.fancy_name = self.name
            self.crt = PersonCRT.CONSUMER

    def is_tax_regime_simple(self):
        if self.crt == PersonCRT.SIMPLE or self.crt == PersonCRT.SIMPLE_SUB:
            return True
        return False

    def is_tax_regime_normal(self):
        if self.crt == PersonCRT.NORMAL:
            return True
        return False

    def is_tax_regime_consumer(self):
        if self.crt == PersonCRT.CONSUMER:
            return True
        return False


class AbstractPerson(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, related_name='%(app_label)s_%(class)s_entity',
                               default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, related_name='%(app_label)s_%(class)s_company',
                                default=settings.DATA_COMPANY_ID)
    editable = models.BooleanField(u'Editable', choices=YesNo.choices(), default=YesNo.YES, editable=False)

    type = models.IntegerField(u'Tipo', choices=PersonType.choices(), null=True, editable=False)

    on_company = DataCompanyManager()
    objects = models.Manager()

    def __unicode__(self):
        return '%s - %s' % (cnpj_cpf(self.document_number), self.name)

    def get_document_number_display(self):
        return cnpj_cpf(self.document_number)

    def get_input_id(self):
        return self.id

    class Meta:
        abstract = True


