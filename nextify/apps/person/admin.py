from django.contrib import admin
from .models import *


class PersonAdmin(admin.ModelAdmin):
    pass


admin.site.register(Entity, PersonAdmin)
admin.site.register(Company, PersonAdmin)
admin.site.register(Customer, PersonAdmin)
