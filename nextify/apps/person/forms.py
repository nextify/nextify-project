# -*- coding: utf-8 -*-
import nextify.core.util.nextforms as forms
from nextify.core.util.crispy_forms.helper import FormHelper
from nextify.core.util.crispy_forms.layout import Layout, Submit, Field, Div, Row, Colmd6, HTML
from nextify.core.util.crispy_forms.bootstrap import FormActions

from nextify.core.enum.person import PersonCategory, PersonIE
from nextify.core.util.validations import validate_cnpj, validate_cpf, validate_email


class PersonForm(forms.ModelForm):
    helper = FormHelper()
    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Dados Principais</h3>'),
            Row(
                Div(Field('code', readonly=True), css_class='col-md-6'),
            ),
            Row(
                Div(Field('category', css_class='form-control'), css_class='col-md-3'),
                Div(Field('document_number', css_class='form-control'), css_class='col-md-3'),
                Div(Field('name', css_class='form-control'), css_class='col-md-6'),
            ),
            HTML(u'<h3 class="form-section">Dados Adicionais</h3>'),

            Row(
                Colmd6(Field('fancy_name', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('state_registry_id', css_class='form-control')),
                Colmd6(Field('state_registry', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('city_registry', css_class='form-control')),
                Colmd6(Field('suframa_registry', css_class='form-control')),
            ),
            Row(
                Colmd6(Field('crt', css_class='form-control')),
                Colmd6(Field('email', css_class='form-control')),
            ),
            css_class='form-body',
        ),
        FormActions(Submit('submit', 'Salvar', css_class='btn green')),
    )

    class Meta:
        fields = ('code', 'document_number', 'name', 'fancy_name', 'state_registry_id',
                  'state_registry', 'city_registry', 'suframa_registry', 'crt',
                  'email', 'category')

    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['category'].widget = forms.ChoiceDayLabelInput()
            self.fields['category'].choices = PersonCategory.choices()
            self.fields['category'].widget.attrs.update({'disabled': 'disabled'})
            self.fields['document_number'].widget.attrs.update({'readonly': True})

    def clean(self):
        cleaned_data = super(PersonForm, self).clean()
        category = cleaned_data.get('category')
        document_number = cleaned_data.get('document_number')
        state_registry_id = cleaned_data.get('state_registry_id')
        state_registry = cleaned_data.get('state_registry')
        email = cleaned_data.get('email')

        if category == PersonCategory.INDIVIDUAL:
            if not validate_cpf(document_number):
                msg = u'CPF Inválido.'
                self.add_error('document_number', msg)
        if category == PersonCategory.CORPORATE:
            if not validate_cnpj(document_number):
                msg = u'CNPJ Inválido.'
                self.add_error('document_number', msg)
            if state_registry_id == PersonIE.TAXPAYER and not state_registry:
                msg = u'Contribuinte do ICMS deve possuir Inscrição Estadual.'
                self.add_error('state_registry', msg)
        if email:
            if not validate_email(email):
                msg = u'Email inválido ou inexistente.'
                self.add_error('email', msg)

        return cleaned_data

    def clean_document_number(self):
        data = self.cleaned_data.get('document_number')
        if len(data) <= 11:
            data = str(data).zfill(11)
        elif len(data) <= 14:
            data = str(data).zfill(14)
        return data
