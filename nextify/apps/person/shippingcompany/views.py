# -*- coding: utf-8 -*-
from django.views.generic.edit import UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse
from django.db.models import Q

from nextify.apps.person.models import ShippingCompany
from nextify.apps.person.views import PersonCreate
from nextify.core.views.datatables import BaseDatatableView
from nextify.core.views.generic import ObjectLookupView

from .forms import *


class ShippingCompanyCreate(SuccessMessageMixin, PersonCreate):
    form_class = ShippingCompanyForm
    model = ShippingCompany
    success_message = 'Transportadora cadastrada com sucesso.'

    def get_success_url(self):
        return reverse('shippingcompany:update', kwargs={'pk': self.object.pk, })


class ShippingCompanyUpdate(SuccessMessageMixin, UpdateView):
    form_class = ShippingCompanyForm
    model = ShippingCompany
    success_message = 'Transportadora atualizada com sucesso.'

    def get_context_data(self, **kwargs):
        context = super(ShippingCompanyUpdate, self).get_context_data(**kwargs)
        context['shippingcompany_pk'] = self.object.pk
        context['shippingcompany_code'] = self.object.code
        context['shippingcompany_name'] = self.object.name
        return context

    def get_success_url(self):
        return reverse('shippingcompany:update', kwargs={'pk': self.object.pk, })


class ShippingCompanyList(TemplateView):
    template_name = 'person/shippingcompany_list.html'


class ShippingCompanyListJson(BaseDatatableView):
    model = ShippingCompany
    columns = ['id', 'document_number', 'name', 'edit']
    order_columns = ['id', 'document_number', 'name']

    max_display_length = 500


class ShippingCompanyLookupView(ObjectLookupView):
    model = ShippingCompany

    def lookup_filter(self, qs, term):
        qs = qs.filter(editable=True)
        return qs.filter(Q(document_number__istartswith=term) | Q(name__icontains=term) | Q(fancy_name__icontains=term))