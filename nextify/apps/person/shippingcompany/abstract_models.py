# -*- coding: utf-8 -*-
from django.db import models

from nextify.apps.person.abstract_models import AbstractPerson
from nextify.core.enum.address import AddressType
from nextify.core.enum.person import PersonType
from nextify.core.enum.sequence import SequenceType
from nextify.apps.sequence import get_next_code


class AbstractShippingCompany(AbstractPerson):

    class Meta:
        abstract = True

    def billing_address(self):
        return self.addresses.filter(address_type=AddressType.BILLING).first()

    def shipping_address(self):
        return self.addresses.filter(address_type=AddressType.SHIPPING).first()

    @models.permalink
    def get_absolute_url(self):
        return 'shippingcompany:update', (), {'pk': self.pk}

    @property
    def portlet(self):
        return u'#%s | %s - %s' % (self.code, self.get_document_number_display(), self.name)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id or not self.code:
            self.code = get_next_code(SequenceType.SHIPPING_COMPANY)
            self.type = PersonType.SHIPPING_COMPANY
        super(AbstractShippingCompany, self).save(force_insert, force_update, using, update_fields)