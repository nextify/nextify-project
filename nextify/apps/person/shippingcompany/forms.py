# -*- coding: utf-8 -*-
from nextify.apps.person.models import ShippingCompany
from nextify.apps.person.forms import PersonForm
from nextify.core.enum.person import PersonCategoryBR


class ShippingCompanyForm(PersonForm):
    class Meta(PersonForm.Meta):
        model = ShippingCompany
