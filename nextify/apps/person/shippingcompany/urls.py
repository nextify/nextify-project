from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    url(r'^create/$', login_required(views.ShippingCompanyCreate.as_view()), name='create'),
    url(r'^(?P<pk>\d+)/update/$', login_required(views.ShippingCompanyUpdate.as_view()), name='update'),
    url(r'^$', login_required(views.ShippingCompanyList.as_view()), name='list'),
    url(r'^json/$', login_required(views.ShippingCompanyListJson.as_view()), name='list-json'),
    url(r'^lookup/$', login_required(views.ShippingCompanyLookupView.as_view()), name='lookup'),
]
