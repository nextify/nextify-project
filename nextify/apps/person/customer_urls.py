from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^create/$', views.CustomerCreate.as_view(), name='create'),
    url(r'^(?P<pk>\d+)/update/$', views.CustomerUpdate.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/detail/$', views.CustomerDetail.as_view(), name='detail')
]
