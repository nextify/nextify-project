from django.views.generic.edit import CreateView


class PersonCreate(CreateView):
    def get_context_data(self, **kwargs):
        ctx = super(PersonCreate, self).get_context_data(**kwargs)
        ctx['category'] = self.kwargs.get('category')
        return ctx

    def get_initial(self):
        initials = super(PersonCreate, self).get_initial()
        initials['category'] = self.kwargs.get('category')
        return initials
