# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.apps import apps
from nextify.apps.person.managers import EntityManager
from nextify.core.enum.address import AddressType
from nextify.core.enum.person import PersonType
from nextify.apps.person.abstract_models import AbstractBasePerson, AbstractPerson

COMPANY_CACHE = {}

class CurrentCompanyManager(models.Manager):

    def get_current(self):
        """
        Returns the current ``Entity`` based on the ENTITY_ID in the
        project's settings. The ``Company`` object is cached the first
        time it's retrieved from the database.
        """
        try:
            sid = settings.COMPANY_ID
        except AttributeError:
            from django.core.exceptions import ImproperlyConfigured
            raise ImproperlyConfigured("You're using the NEXTIFY \"company framework\" without having set the COMPANY_ID setting. Create a company in your database and set the COMPANY_ID setting to fix this error.")
        try:
            current_company = COMPANY_CACHE[sid]
        except KeyError:
            current_company = self.get(pk=sid)
            COMPANY_CACHE[sid] = current_company
        return current_company

    def clear_cache(self):
        """Clears the ``Site`` object cache."""
        global COMPANY_CACHE
        COMPANY_CACHE = {}


class AbstractCompany(AbstractBasePerson):
    entity = models.ForeignKey('person.Entity', null=True, related_name='entity', default=settings.ENTITY_ID)

    company_person = models.ForeignKey('person.CompanyPerson', null=True, blank=True, related_name='company_person')

    on_entity = EntityManager()
    objects = CurrentCompanyManager()

    __has_changed = False

    def save(self, *args, **kwargs):
        # Ensure that each company only has one default establishment
        self.__has_changed = self.has_changed
        super(AbstractCompany, self).save(*args, **kwargs)
        self.create_children()

    def billing_address(self):
        return self.company_addresses.filter(address_type=AddressType.BILLING).first()

    def shipping_address(self):
        return self.company_addresses.filter(address_type=AddressType.SHIPPING).first()

    def create_children(self):
        self.create_person()

    def create_person(self):
        p = apps.get_model('person', 'CompanyPerson')
        if not self.company_person:
            o = p.on_company.create(code=self.document_number,
                                    category=self.category,
                                    document_number=self.document_number,
                                    name=self.name,
                                    fancy_name=self.fancy_name,
                                    state_registry_id=self.state_registry_id,
                                    state_registry=self.state_registry,
                                    city_registry=self.city_registry,
                                    suframa_registry=self.suframa_registry,
                                    crt=self.crt,
                                    email=self.email,
                                    is_active=self.is_active,
                                    editable=False)
            self.__class__._default_manager\
                .filter(pk=self.pk)\
                .update(company_person=o)
        else:
            if self.__has_changed:
                p.on_company.filter(pk=self.company_person_id)\
                    .update(code=self.document_number,
                            category=self.category,
                            document_number=self.document_number,
                            name=self.name,
                            fancy_name=self.fancy_name,
                            state_registry_id=self.state_registry_id,
                            state_registry=self.state_registry,
                            city_registry=self.city_registry,
                            suframa_registry=self.suframa_registry,
                            crt=self.crt,
                            email=self.email,
                            is_active=self.is_active,
                            editable=False)


    class Meta:
        abstract = True


class AbstractCompanyPerson(AbstractPerson):

    class Meta:
        abstract = True

    def billing_address(self):
        return self.addresses.filter(address_type=AddressType.BILLING).first()

    def shipping_address(self):
        return self.addresses.filter(address_type=AddressType.SHIPPING).first()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id or not self.code:
            self.type = PersonType.COMPANY
        super(AbstractCompanyPerson, self).save(force_insert, force_update, using, update_fields)