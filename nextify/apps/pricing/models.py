# -*- coding: utf-8 -*-
from .abstract_models import AbstractPriceTable, AbstractPriceTableLine


class PriceTable(AbstractPriceTable):
    pass


class PriceTableLine(AbstractPriceTableLine):
    pass

    class Meta:
        unique_together = ('entity', 'company', 'price_table', 'product', 'unit')
