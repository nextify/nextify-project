# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings

from nextify.apps.person.managers import DataCompanyManager
from nextify.core.enum.default import Active, YesNo



class AbstractPriceTable(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID)

    code = models.CharField(u'Código', max_length=10, blank=False)
    description = models.CharField(u'Descrição', max_length=255, blank=False)

    is_default = models.NullBooleanField(u'Tabela padrão', default=True, choices=YesNo.choices())

    is_active = models.BooleanField('Active',
                                    default=True,
                                    choices=Active.choices(),
                                    help_text='Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.')

    date_created = models.DateTimeField(u'Data de Cadastro', auto_now_add=True, null=True)
    date_updated = models.DateTimeField(u'Data de Atualização', auto_now=True, null=True)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s - %s' % (self.code, self.description)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.ensure_defaults_integrity()
        super(AbstractPriceTable, self).save(force_insert, force_update, using,update_fields)

    def ensure_defaults_integrity(self):
        if self.is_default:
            self.__class__.on_company\
                .filter(is_default=True)\
                .update(is_default=False)


    @models.permalink
    def get_absolute_url(self):
        return ('pricing:update', (),
                {'pk': self.pk})


class AbstractPriceTableLine(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, editable=False, default=settings.ENTITY_ID)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID)
    price_table = models.ForeignKey('pricing.PriceTable', null=False)
    product = models.ForeignKey('catalogue.Product', null=False, related_name='pricing')
    unit = models.ForeignKey('unit.Unit', null=False)
    value = models.DecimalField(u'Preço', max_digits=15, decimal_places=6, null=False)

    date_created = models.DateTimeField(u'Data de Cadastro', auto_now_add=True, null=True)
    date_updated = models.DateTimeField(u'Data de Atualização', auto_now=True, null=True)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True


