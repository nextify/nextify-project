# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.core.urlresolvers import reverse
from django.db.models import Q
from nextify.core.views.datatables import BaseDatatableView
from nextify.core.views.generic import ObjectLookupView

from .models import PriceTable
from .forms import PriceTableForm


class PriceTableCreate(SuccessMessageMixin, CreateView):
    model = PriceTable
    form_class = PriceTableForm
    success_message = u'Tabela de Preços cadastrada com sucesso!'

    def get_success_url(self):
        if 'save' in self.request.POST:
            return reverse('pricing:update', kwargs={'pk': self.object.pk, })
        if 'save-add' in self.request.POST:
            return reverse('pricing:create')


class PriceTableUpdate(SuccessMessageMixin, UpdateView):
    model = PriceTable
    form_class = PriceTableForm
    success_message = u'Tabela de Preços atualizada com sucesso!'

    def get_success_url(self):
        if 'save' in self.request.POST:
            return reverse('pricing:update', kwargs={'pk': self.object.pk, })
        if 'save-add' in self.request.POST:
            return reverse('pricing:create')


class PriceTableList(ListView):
    model = PriceTable


class PriceTableListJson(BaseDatatableView):
    model = PriceTable
    columns = ['id', 'code', 'description', 'edit']
    order_columns = ['id', 'code', 'description']

    max_display_length = 500


class PriceTableLookupView(ObjectLookupView):
    model = PriceTable

    def lookup_filter(self, qs, term):
        return qs.filter(Q(code__istartswith=term) | Q(description__icontains=term))
