# -*- coding: utf-8 -*-
import nextify.core.util.nextforms as forms
from nextify.core.util.crispy_forms.helper import FormHelper
from nextify.core.util.crispy_forms.layout import Layout, Submit, Field, Div, Row, HTML
from nextify.core.util.crispy_forms.bootstrap import FormActions

from .models import PriceTable


class PriceTableForm(forms.ModelForm):
    helper = FormHelper()
    helper.layout = Layout(
        Div(
            HTML(u'<h3 class="form-section">Informações da Tabela de Preços</h3>'),
            Row(
                Div(Field('code'), css_class='col-md-6'),
                Div(Field('description'), css_class='col-md-6'),
            ),
            css_class='form-body',
        ),
        FormActions(Submit('save', 'Salvar', css_class='btn green'),
                    Submit('save-add', 'Salvar e Cadastrar um novo', css_class='btn blue-hoki')
                    ),
    )

    class Meta:
        model = PriceTable
        fields = ['code', 'description']
