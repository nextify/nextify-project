# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings

from nextify.apps.person.managers import DataCompanyManager

class AbstractUnit(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)

    code = models.CharField(u'Sigla', max_length=6, blank=False)
    description = models.CharField(u'Descrição', max_length=255, blank=False)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s - %s' % (self.code, self.description)

    @models.permalink
    def get_absolute_url(self):
        return ('unit:update', (),
                {'pk': self.pk})
