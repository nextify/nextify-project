from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    url(r'^create/$', login_required(views.UnitCreate.as_view()), name='create'),
    url(r'^(?P<pk>\d+)/update/$', login_required(views.UnitUpdate.as_view()), name='update'),
    url(r'^$', login_required(views.UnitList.as_view()), name='list'),
    url(r'^json/$', login_required(views.UnitListJson.as_view()), name='list-json'),
    url(r'^lookup/$', login_required(views.UnitLookupView.as_view()), name='lookup'),
]
