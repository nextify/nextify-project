# -*- coding: utf-8 -*-
from django.db.models import Q
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse

from nextify.apps.unit.models import Unit
from nextify.core.views.datatables import BaseDatatableView
from nextify.core.views.generic import ObjectLookupView

from .forms import UnitForm


class UnitCreate(SuccessMessageMixin, CreateView):
    model = Unit
    form_class = UnitForm
    success_message = 'Unidade de Medida cadastrada com sucesso.'

    def get_success_url(self):
        if 'save' in self.request.POST:
            return reverse('unit:update', kwargs={'pk': self.object.pk, })
        if 'save-add' in self.request.POST:
            return reverse('unit:create')


class UnitUpdate(SuccessMessageMixin, UpdateView):
    model = Unit
    form_class = UnitForm
    success_message = 'Unidade de Medida atualizada com sucesso.'

    def get_success_url(self):
        if 'save' in self.request.POST:
            return reverse('unit:update', kwargs={'pk': self.object.pk, })
        if 'save-add' in self.request.POST:
            return reverse('unit:create')


class UnitList(ListView):
    model = Unit


class UnitListJson(BaseDatatableView):
    model = Unit
    columns = ['id', 'code', 'description', 'edit']
    order_columns = ['id', 'code', 'description']

    max_display_length = 500


class UnitLookupView(ObjectLookupView):
    model = Unit

    def lookup_filter(self, qs, term):
        return qs.filter(Q(code__istartswith=term) | Q(description__icontains=term))
