# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.apps import apps
from nextify.apps.person.managers import DataCompanyManager
from nextify.core.enum.banking import Bank
from nextify.core.enum.sequence import SequenceType


class AbstractBankAccount(models.Model):
    entity = models.ForeignKey('person.Entity', null=False, default=settings.ENTITY_ID, editable=False)
    company = models.ForeignKey('person.Company', null=False, default=settings.DATA_COMPANY_ID, editable=False)

    bank = models.IntegerField(u'Banco', null=False, choices=Bank.choices())
    description = models.CharField(u'Descrição', max_length=255, blank=False)

    bank_agency = models.IntegerField(u'Agência', null=False)
    bank_account = models.IntegerField(u'Conta Corrente', null=False)
    bank_recipient_account = models.IntegerField(u'Cedente', null=False)

    cnab_transmission_code = models.CharField(u'Cód. de Transmissão', max_length=24, blank=True)

    on_company = DataCompanyManager()
    objects = models.Manager()

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(AbstractBankAccount, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                              update_fields=update_fields)
        self.create_sequences()

    def __unicode__(self):
        return '%s' % self.description

    def create_sequences(self):
        self.create_bank_file_sequence()
        self.create_bank_number_sequence()

    def create_bank_number_sequence(self):
        model = apps.get_model('sequence', 'GenericSequence')
        o, created = model.on_company.get_or_create(sequence_type=SequenceType.BANK_NUMBER, bank_account=self)

        if created:
            o.sequence_expression = '#######'
            o.layout = '#EXPRESSION#'

            o.save()

    def create_bank_file_sequence(self):
        model = apps.get_model('sequence', 'GenericSequence')
        o, created = model.on_company.get_or_create(sequence_type=SequenceType.BANK_FILE, bank_account=self)

        if created:
            o.prefix = 'REM_%s_%s' % (str(self.bank_agency), str(self.bank_account))
            o.suffix = '%s' % str(self.bank)
            o.sequence_expression = '######'
            o.layout = '#PREFIX#_#EXPRESSION#.#SUFFIX#'

            o.save()
