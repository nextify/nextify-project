from django.contrib import admin
from .models import *


class BankingAdmin(admin.ModelAdmin):
    pass

admin.site.register(BankAccount, BankingAdmin)


