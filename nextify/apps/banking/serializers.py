from rest_framework import serializers
from nextify.apps.banking.models import BankAccount
from nextify.core.enum.banking import Bank

class BankAccountSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    bank = serializers.ChoiceField(null=False, choices=Bank.choices())
    description = serializers.CharField(max_length=255, allow_blank=False)
    bank_agency = serializers.IntegerField(null=False)
    bank_account = serializers.IntegerField(null=False)
    bank_recipient_account = serializers.IntegerField(null=False)
    cnab_transmission_code = serializers.CharField(max_length=24, allow_blank=True)

    def create(self, validated_data):
        return BankAccount.on_company.create(**validated_data)

    def update(self, instance, validated_data):
        instance.bank = validated_data.get('bank', instance.bank)
        instance.description = validated_data.get('description', instance.description)
        instance.bank_agency = validated_data.get('bank_agency', instance.bank_agency)
        instance.bank_account = validated_data.get('bank_account', instance.bank_account)
        instance.bank_recipient_account = validated_data.get('bank_recipient_account', instance.bank_recipient_account)
        instance.cnab_transmission_code = validated_data.get('cnab_transmission_code', instance.cnab_transmission_code)
        instance.save()
        return instance
