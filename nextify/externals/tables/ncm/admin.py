from django.contrib import admin
from .models import *


class NCMAdmin(admin.ModelAdmin):
    pass


admin.site.register(NcmClassification, NCMAdmin)

