from django.db.models import Q

from nextify.core.views.generic import ObjectLookupView
from .models import NcmClassification

class NcmLookupView(ObjectLookupView):
    model = NcmClassification

    def lookup_filter(self, qs, term):
        return qs.filter(Q(code__istartswith=term) | Q(description__icontains=term))

