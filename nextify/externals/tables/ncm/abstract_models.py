# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class AbstractNcmClassification(models.Model):
    code = models.CharField(_("NCM Code"), max_length=8, blank=False)
    description = models.CharField(_("Description"), max_length=255, blank=False)

    def __unicode__(self):
        return '%s - %s' % (self.code, self.description)

    class Meta:
        abstract = True