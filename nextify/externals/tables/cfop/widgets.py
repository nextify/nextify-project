from django.core.urlresolvers import reverse_lazy

from nextify.core.forms.widgets import RemoteSelect


class CfopSelect(RemoteSelect):
    lookup_url = reverse_lazy('cfop:lookup')

