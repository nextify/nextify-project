# -*- coding: utf-8 -*-
from django.db import models

from nextify.core.enum.operation import OperationType, OperationDestiny
from nextify.core.enum.default import YesNo

class AbstractCfop(models.Model):
    code = models.CharField('CFOP', max_length=4, blank=False)
    description = models.CharField('CFOP Description', max_length=500, blank=False)
    ind_nfe = models.BooleanField('Permite NFe', choices=YesNo.choices(), null=False)
    ind_communication = models.BooleanField(u'NF Comunicação', choices=YesNo.choices(), null=False)
    ind_transportation = models.BooleanField(u'NF Transporte', choices=YesNo.choices(), null=False)
    ind_return = models.BooleanField(u'NFe Devolução', choices=YesNo.choices(), null=False)
    ind_operation_type = models.IntegerField(u'Tipo da Operação', choices=OperationType.choices(), null=False)
    ind_operation_destiny = models.IntegerField(u'Destino da Operação', choices=OperationDestiny.choices(), null=False)

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s - %s' % (self.code, self.description)
