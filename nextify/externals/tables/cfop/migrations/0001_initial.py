# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-27 02:13
from __future__ import unicode_literals

from django.db import migrations, models
import nextify.core.django_enumfield.enum
import nextify.core.enum.default
import nextify.core.enum.operation


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cfop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=4, verbose_name=b'CFOP')),
                ('description', models.CharField(max_length=500, verbose_name=b'CFOP Description')),
                ('ind_nfe', models.BooleanField(choices=[(False, nextify.core.django_enumfield.enum.Value(b'NO', False, 'No', nextify.core.enum.default.YesNo)), (True, nextify.core.django_enumfield.enum.Value(b'YES', True, 'Yes', nextify.core.enum.default.YesNo))], verbose_name=b'Permite NFe')),
                ('ind_communication', models.BooleanField(choices=[(False, nextify.core.django_enumfield.enum.Value(b'NO', False, 'No', nextify.core.enum.default.YesNo)), (True, nextify.core.django_enumfield.enum.Value(b'YES', True, 'Yes', nextify.core.enum.default.YesNo))], verbose_name='NF Comunica\xe7\xe3o')),
                ('ind_transportation', models.BooleanField(choices=[(False, nextify.core.django_enumfield.enum.Value(b'NO', False, 'No', nextify.core.enum.default.YesNo)), (True, nextify.core.django_enumfield.enum.Value(b'YES', True, 'Yes', nextify.core.enum.default.YesNo))], verbose_name='NF Transporte')),
                ('ind_return', models.BooleanField(choices=[(False, nextify.core.django_enumfield.enum.Value(b'NO', False, 'No', nextify.core.enum.default.YesNo)), (True, nextify.core.django_enumfield.enum.Value(b'YES', True, 'Yes', nextify.core.enum.default.YesNo))], verbose_name='NFe Devolu\xe7\xe3o')),
                ('ind_operation_type', models.IntegerField(choices=[(0, nextify.core.django_enumfield.enum.Value(b'IN', 0, 'In', nextify.core.enum.operation.OperationType)), (1, nextify.core.django_enumfield.enum.Value(b'OUT', 1, 'Out', nextify.core.enum.operation.OperationType))], verbose_name='Tipo da Opera\xe7\xe3o')),
                ('ind_operation_destiny', models.IntegerField(choices=[(1, nextify.core.django_enumfield.enum.Value(b'STATE', 1, 'State', nextify.core.enum.operation.OperationDestiny)), (2, nextify.core.django_enumfield.enum.Value(b'INTERSTATE', 2, 'Interstate', nextify.core.enum.operation.OperationDestiny)), (3, nextify.core.django_enumfield.enum.Value(b'INTERNATIONAL', 3, 'International', nextify.core.enum.operation.OperationDestiny))], verbose_name='Destino da Opera\xe7\xe3o')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
