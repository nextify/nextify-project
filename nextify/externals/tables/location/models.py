from .abstract_models import *


class Country(AbstractCountry):
    pass


class State(AbstractState):
    pass


class City(AbstractCity):
    pass