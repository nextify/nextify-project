from django.db import models


class AbstractCountry(models.Model):
    name = models.CharField(max_length=60)
    bacen_code = models.CharField(max_length=4)

    def __unicode__(self):
        return self.name

    class Meta:
        abstract = True


class AbstractState(models.Model):
    country = models.ForeignKey('location.Country', null=True)
    name = models.CharField(max_length=60)
    acronym = models.CharField(max_length=2)
    ibge_code = models.CharField(max_length=2)

    def __unicode__(self):
        return self.name

    class Meta:
        abstract = True


class AbstractCity(models.Model):
    state = models.ForeignKey('location.State', null=False)
    name = models.CharField(max_length=60)
    ibge_code = models.CharField(max_length=7)

    def __unicode__(self):
        return '%s - %s' % (self.name, self.state.acronym)

    class Meta:
        abstract = True
