from django.contrib import admin
from .models import *


class LocationAdmin(admin.ModelAdmin):
    pass

admin.site.register(Country, LocationAdmin)
admin.site.register(State, LocationAdmin)
admin.site.register(City, LocationAdmin)

