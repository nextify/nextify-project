from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^city/lookup/$', views.CityLookupView.as_view(), name='city-lookup'),
    url(r'^state/lookup/$', views.StateLookupView.as_view(), name='state-lookup'),
    url(r'^country/lookup/$', views.CountryLookupView.as_view(), name='country-lookup'),
    ]


