from django.db.models import Q

from nextify.core.views.generic import ObjectLookupView
from .models import City, State, Country

class CityLookupView(ObjectLookupView):
    model = City

    def lookup_filter(self, qs, term):
        return qs.filter(Q(name__icontains=term))


class StateLookupView(ObjectLookupView):
    model = State

    def lookup_filter(self, qs, term):
        return qs.filter(Q(name__icontains=term))

class CountryLookupView(ObjectLookupView):
    model = Country

    def lookup_filter(self, qs, term):
        return qs.filter(Q(name__icontains=term))
