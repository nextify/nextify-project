# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class AbstractServiceCodeList(models.Model):
    code = models.CharField(_("Service Code"), max_length=5, blank=False)
    description = models.CharField(_("Description"), max_length=255, blank=False)

    def __unicode__(self):
        return self.code

    class Meta:
        abstract = True