from collections import OrderedDict
from django.forms.models import BaseInlineFormSet


class NextifyBaseInlineFormSet(BaseInlineFormSet):

    def add_fields(self, form, index):
        super(NextifyBaseInlineFormSet, self).add_fields(form, index)
        # Pop the delete field out of the OrderedDict
        delete_field = form.fields.pop('DELETE')
        # Add it at the start
        form.fields = OrderedDict(
                      [('DELETE', delete_field)] + form.fields.items())
        form.fields['DELETE'].label = 'Exc.'