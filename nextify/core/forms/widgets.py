import re

from django import forms
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.forms.utils import flatatt
from django.forms.widgets import FileInput
from django.template import Context
from django.template.loader import render_to_string
from django.utils import formats, six
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.timezone import datetime
from django.utils.safestring import mark_safe
#from django.utils.six.moves import filter, map


class RemoteSelect(forms.Widget):
    """
    Somewhat reusable widget that allows AJAX lookups in combination with
    select2.
    Requires setting the URL of a lookup view either as class attribute or when
    constructing
    """
    is_multiple = False
    lookup_url = None

    def __init__(self, *args, **kwargs):
        if 'lookup_url' in kwargs:
            self.lookup_url = kwargs.pop('lookup_url')
        if self.lookup_url is None:
            raise ValueError(
                "RemoteSelect requires a lookup ULR")
        super(RemoteSelect, self).__init__(*args, **kwargs)

    def format_value(self, value):
        return six.text_type(value or '')

    def value_from_datadict(self, data, files, name):
        value = data.get(name, None)
        if value is None:
            return value
        else:
            return six.text_type(value)

    def render(self, name, value, attrs=None, choices=()):
        final_attrs = self.build_attrs(self.attrs, attrs, **{
            'type': 'hidden',
            'data-ajax-url': self.lookup_url,
            'data-multiple': 'multiple' if self.is_multiple else '',
            'value': self.format_value(value),
            'name': name,
            'data-required': 'required' if self.is_required else '',
        })
        # attrs = self.build_attrs(attrs, **{
        #     'type': 'hidden',
        #     'data-ajax-url': self.lookup_url,
        #     'data-multiple': 'multiple' if self.is_multiple else '',
        #     'value': self.format_value(value),
        #     'data-required': 'required' if self.is_required else '',
        # }, name=name)
        return mark_safe(u'<input %s>' % flatatt(final_attrs))

    def build_attrs(self, base_attrs, extra_attrs=None, **kwargs):
        """
        Helper function for building an attribute dictionary.
        This is combination of the same method from Django<=1.10 and Django1.11+
        """
        attrs = dict(base_attrs, **kwargs)
        if extra_attrs:
            attrs.update(extra_attrs)
        return attrs

class MultipleRemoteSelect(RemoteSelect):
    is_multiple = True

    def format_value(self, value):
        if value:
            return ','.join(map(six.text_type, filter(bool, value)))
        else:
            return ''

    def value_from_datadict(self, data, files, name):
        value = data.get(name, None)
        if value is None:
            return []
        else:
            return list(filter(bool, value.split(',')))


class DayLabelWidget(forms.Widget):

    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs, name=name)
        if hasattr(self, 'initial'):
            value = self.initial
        return mark_safe(
            '%s' % value
        ) + mark_safe(
            '<input type="hidden" name="%s" value="%s" />' % (name, value)
        )

    def _has_changed(self, initial, data):
        return False


class ModelLinkWidget(forms.Widget):
    def __init__(self, obj, pdf=False, label=None, attrs=None):
        self.object = obj
        self.pdf = pdf
        self.label = label
        super(ModelLinkWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if self.object.pk:
            url = ''
            label = u'Editar'
            if self.pdf:
                url = self.object.get_pdf_url()
            else:
                url = self.object.get_absolute_url()
            if self.label is not None:
                label = self.label
            return mark_safe(
                u'<a href="%s" class="btn btn-sm btn-default btn-circle btn-editable"><i class="fa fa-pencil">'
                u'</i> %s</a>' % (url, label)
            )
        else:
            return mark_safe(u'')