from .brazilian_numbers import validate_cpf, validate_cnpj
from .email import validate_email
