# -*- coding: utf-8 -*-
import libxml2  # FIXME: verificar ambiguidade de dependencias: lxml e libxml2
import xmlsec

import lxml.etree as etree
from StringIO import StringIO
from nextify.core.util.signature.certificate import *

from .flags import NAMESPACE_NFE, NAMESPACE_SIG

def extrair_tag(root):
    return root.tag.split('}')[-1]

class Assinatura(object):
    """Classe abstrata responsavel por definir os metodos e logica das classes
    de assinatura digital."""

    certificado = None
    senha = None

    def __init__(self, certificado=None, senha=None):
        if certificado is None:
            self.certificado = CertificadoA1()
        else:
            self.certificado = certificado

        if senha is None:
            self.senha = '******'
        else:
            self.senha = senha

    def assinar_arquivo(self, caminho_arquivo):
        """Efetua a assinatura dos arquivos XML informados"""
        pass

    def assinar_xml(self, xml):
        """Efetua a assinatura numa string contendo XML valido."""
        pass

    def assinar_etree(self, raiz):
        pass

    def assinar_objetos(self, objetos):
        """Efetua a assinatura em instancias do PyNFe"""
        pass

    def verificar_arquivo(self, caminho_arquivo):
        pass

    def verificar_xml(self, xml):
        pass

    def verificar_etree(self, raiz):
        pass

    def verificar_objetos(self, objetos):
        pass

class AssinaturaA1(Assinatura):
    """Classe abstrata responsavel por efetuar a assinatura do certificado
    digital no XML informado."""
    
    def assinar_arquivo(self, caminho_arquivo, salva=True):
        # Carrega o XML do arquivo
        raiz = etree.parse(caminho_arquivo)

        # Efetua a assinatura
        xml = self.assinar_etree(raiz, retorna_xml=True)

        # Grava XML assinado no arquivo
        if salva:
            fp = file(caminho_arquivo, 'w')
            fp.write(xml)
            fp.close()

        return xml

    # def assinar_xml(self, xml):
    #     raiz = etree.parse(StringIO(xml))
    #
    #     # Efetua a assinatura
    #     return self.assinar_etree(raiz, retorna_xml=True)

    def sign_file(self, xml_doc, ref_uri=None, key_file='/tmp/key.pem', cert_file='/tmp/cert.pem'):
        # assert(xml_file)
        assert(key_file)
        assert(cert_file)

        # Load template
        # if not self.check_filename(xml_file):
        #     return -1
        doc = xml_doc
        # if doc is None or doc.getRootElement() is None:
        #     print "Error: unable to parse file \"%s\"" % xml_file
        #     return self.cleanup(doc)

        # Create signature template for RSA-SHA1 enveloped signature
        signNode = xmlsec.TmplSignature(doc, xmlsec.transformInclC14NId(),
                                        xmlsec.transformRsaSha1Id(), None)
        if signNode is None:
            print "Error: failed to create signature template"
            return self.cleanup(doc)

        # Add <dsig:Signature/> node to the doc
        doc.getRootElement().addChild(signNode)

        uri = '#' + ref_uri
        #uri = '#NFe41160213444582000147550010000123451197187698'
        # Add reference
        refNode = signNode.addReference(xmlsec.transformSha1Id(), None, uri, None)

        if refNode is None:
            print "Error: failed to add reference to signature template"
            return self.cleanup(doc)

        # Add enveloped transform
        if refNode.addTransform(xmlsec.transformEnvelopedId()) is None:
            print "Error: failed to add enveloped transform to reference"
            return self.cleanup(doc)
        refNode.addTransform(xmlsec.transformInclC14NId())

        # Add <dsig:KeyInfo/> and <dsig:X509Data/>
        keyInfoNode = signNode.ensureKeyInfo(None)
        if keyInfoNode is None:
            print "Error: failed to add key info"
            return self.cleanup(doc)

        if keyInfoNode.addX509Data() is None:
            print "Error: failed to add X509Data node"
            return self.cleanup(doc)

        # Create signature context, we don't need keys manager in this example
        dsig_ctx = xmlsec.DSigCtx()
        if dsig_ctx is None:
            print "Error: failed to create signature context"
            return self.cleanup(doc)

        # Load private key, assuming that there is not password
        if not self.check_filename(key_file):
            return self.cleanup(doc, dsig_ctx)
        key = xmlsec.cryptoAppKeyLoad(key_file, xmlsec.KeyDataFormatPem,
                                      None, None, None)
        if key is None:
            print "Error: failed to load private pem key from \"%s\"" % key_file
            return self.cleanup(doc, dsig_ctx)
        dsig_ctx.signKey = key

        # Load certificate and add to the key
        if not self.check_filename(cert_file):
            return self.cleanup(doc, dsig_ctx)
        if xmlsec.cryptoAppKeyCertLoad(key, cert_file, xmlsec.KeyDataFormatPem) < 0:
            print "Error: failed to load pem certificate \"%s\"" % cert_file
            return self.cleanup(doc, dsig_ctx)

        # Set key name to the file name, this is just an example!
        if key.setName(key_file) < 0:
            print "Error: failed to set key name for key from \"%s\"" % key_file
            return self.cleanup(doc, dsig_ctx)

        # Sign the template
        if dsig_ctx.sign(signNode) < 0:
            print "Error: signature failed"
            return self.cleanup(doc, dsig_ctx)

        # Print signed document to stdout
        s = doc.serialize('utf-8')

        # Success
        return s

    def cleanup(doc=None, dsig_ctx=None, res=-1):
        if dsig_ctx is not None:
            dsig_ctx.destroy()
        if doc is not None:
            doc.freeDoc()
        return res

    def check_filename(self, filename):
        if os.access(filename, os.R_OK):
            return 1
        else:
            print "Error: XML file \"%s\" not found OR no read access" % filename
            return 0

    def sign(self, doc, ref_uri=None):
        libxml2.initParser()
        libxml2.thrDefLoadExtDtdDefaultValue(2|4)
        libxml2.substituteEntitiesDefault(1)

        xmlsec.init()
        xmlsec.cryptoAppInit(None)
        xmlsec.cryptoInit()

        self.certificado.separar_arquivo('******')



        doc = libxml2.parseMemory(doc, len(doc))

        str = self.sign_file(doc, ref_uri)

        libxml2.cleanupParser()
        xmlsec.cryptoShutdown()
        xmlsec.cryptoAppShutdown()
        xmlsec.shutdown()

        return str

    # def cleanup(self, doc=None, dsig_ctx=None, res=-1):
    #     if dsig_ctx is not None:
    #         dsig_ctx.destroy()
    #     if doc is not None:
    #         doc.freeDoc()
    #     return res

    def assinar_etree(self, raiz, retorna_xml=False):
        # Extrai a tag do elemento raiz
        tipo = extrair_tag(raiz.getroot())

        # doctype compatível com o tipo da tag raiz
        if tipo == u'NFe':
            doctype = u'<!DOCTYPE NFe [<!ATTLIST infNFe Id ID #IMPLIED>]>'
        elif tipo == u'inutNFe':
            doctype = u'<!DOCTYPE inutNFe [<!ATTLIST infInut Id ID #IMPLIED>]>'
        elif tipo == u'cancNFe':
            doctype = u'<!DOCTYPE cancNFe [<!ATTLIST infCanc Id ID #IMPLIED>]>'
        elif tipo == u'DPEC':
            doctype = u'<!DOCTYPE DPEC [<!ATTLIST infDPEC Id ID #IMPLIED>]>'

        # Tag de assinatura
        if raiz.getroot().find('Signature') is None:
            signature = etree.Element(
                    '{%s}Signature'%NAMESPACE_SIG,
                    URI=raiz.getroot().getchildren()[0].attrib['Id'],
                    nsmap={'sig': NAMESPACE_SIG},
                    )

            signed_info = etree.SubElement(signature, '{%s}SignedInfo'%NAMESPACE_SIG)
            etree.SubElement(signed_info, 'CanonicalizationMethod', Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315")
            etree.SubElement(signed_info, 'SignatureMethod', Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1")

            reference = etree.SubElement(signed_info, '{%s}Reference'%NAMESPACE_SIG, URI=raiz.getroot().getchildren()[0].attrib['Id'])
            transforms = etree.SubElement(reference, 'Transforms', URI=raiz.getroot().getchildren()[0].attrib['Id'])
            etree.SubElement(transforms, 'Transform', Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature")
            etree.SubElement(transforms, 'Transform', Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315")
            etree.SubElement(reference, '{%s}DigestMethod'%NAMESPACE_SIG, Algorithm="http://www.w3.org/2000/09/xmldsig#sha1")
            digest_value = etree.SubElement(reference, '{%s}DigestValue'%NAMESPACE_SIG)

            signature_value = etree.SubElement(signature, '{%s}SignatureValue'%NAMESPACE_SIG)

            key_info = etree.SubElement(signature, '{%s}KeyInfo'%NAMESPACE_SIG)
            x509_data = etree.SubElement(key_info, '{%s}X509Data'%NAMESPACE_SIG)
            x509_certificate = etree.SubElement(x509_data, '{%s}X509Certificate'%NAMESPACE_SIG)

            raiz.getroot().insert(0, signature)

        # Acrescenta a tag de doctype (como o lxml nao suporta alteracao do doctype,
        # converte para string para faze-lo)
        xml = etree.tostring(raiz, xml_declaration=True, encoding='utf-8')

        if xml.find('<!DOCTYPE ') == -1:
            pos = xml.find('>') + 1
            xml = xml[:pos] + doctype + xml[pos:]
            #raiz = etree.parse(StringIO(xml))

        doc_xml, ctxt, noh_assinatura, assinador = self._antes_de_assinar_ou_verificar(raiz)
        
        # Realiza a assinatura
        assinador.sign(noh_assinatura)
    
        # Coloca na instância Signature os valores calculados
        digest_value.text = ctxt.xpathEval(u'//sig:DigestValue')[0].content.replace(u'\n', u'')
        signature_value.text = ctxt.xpathEval(u'//sig:SignatureValue')[0].content.replace(u'\n', u'')
        
        # Provavelmente retornarão vários certificados, já que o xmlsec inclui a cadeia inteira
        certificados = ctxt.xpathEval(u'//sig:X509Data/sig:X509Certificate')
        x509_certificate.text = certificados[len(certificados)-1].content.replace(u'\n', u'')
    
        resultado = assinador.status == xmlsec.DSigStatusSucceeded

        # Gera o XML para retornar
        xml = doc_xml.serialize()

        # Limpa objetos da memoria e desativa funções criptográficas
        self._depois_de_assinar_ou_verificar(doc_xml, ctxt, assinador)

        if retorna_xml:
            return xml
        else:
            return etree.parse(StringIO(xml))

    def _ativar_funcoes_criptograficas(self):
        # FIXME: descobrir forma de evitar o uso do libxml2 neste processo

        # Ativa as funções de análise de arquivos XML FIXME
        libxml2.initParser()
        libxml2.substituteEntitiesDefault(1)
        
        # Ativa as funções da API de criptografia
        xmlsec.init()
        xmlsec.cryptoAppInit(None)
        xmlsec.cryptoInit()
    
    def _desativar_funcoes_criptograficas(self):
        ''' Desativa as funções criptográficas e de análise XML
        As funções devem ser chamadas aproximadamente na ordem inversa da ativação
        '''
        
        # Shutdown xmlsec-crypto library
        xmlsec.cryptoShutdown()
        
        # Shutdown crypto library
        xmlsec.cryptoAppShutdown()
        
        # Shutdown xmlsec library
        xmlsec.shutdown()
        
        # Shutdown LibXML2 FIXME: descobrir forma de evitar o uso do libxml2 neste processo
        libxml2.cleanupParser()

    def verificar_arquivo(self, caminho_arquivo):
        # Carrega o XML do arquivo
        raiz = etree.parse(caminho_arquivo)
        return self.verificar_etree(raiz)

    def verificar_xml(self, xml):
        raiz = etree.parse(StringIO(xml))
        return self.verificar_etree(raiz)

    def verificar_etree(self, raiz):
        doc_xml, ctxt, noh_assinatura, assinador = self._antes_de_assinar_ou_verificar(raiz)

        # Verifica a assinatura
        assinador.verify(noh_assinatura)
        resultado = assinador.status == xmlsec.DSigStatusSucceeded

        # Limpa objetos da memoria e desativa funções criptográficas
        self._depois_de_assinar_ou_verificar(doc_xml, ctxt, assinador)

        return resultado

    def _antes_de_assinar_ou_verificar(self, raiz):
        # Converte etree para string
        xml = etree.tostring(raiz, xml_declaration=True, encoding='utf-8')

        # Ativa funções criptográficas
        self._ativar_funcoes_criptograficas()

        # Colocamos o texto no avaliador XML FIXME: descobrir forma de evitar o uso do libxml2 neste processo
        doc_xml = libxml2.parseMemory(xml, len(xml))
    
        # Cria o contexto para manipulação do XML via sintaxe XPATH
        ctxt = doc_xml.xpathNewContext()
        ctxt.xpathRegisterNs(u'sig', NAMESPACE_SIG)
    
        # Separa o nó da assinatura
        noh_assinatura = ctxt.xpathEval(u'//*/sig:Signature')[0]
    
        # Buscamos a chave no arquivo do certificado
        chave = xmlsec.cryptoAppKeyLoad(
                filename=str(self.certificado.caminho_arquivo),
                format=xmlsec.KeyDataFormatPkcs12,
                pwd=str(self.senha),
                pwdCallback=None,
                pwdCallbackCtx=None,
                )
    
        # Cria a variável de chamada (callable) da função de assinatura
        assinador = xmlsec.DSigCtx()
    
        # Atribui a chave ao assinador
        assinador.signKey = chave

        return doc_xml, ctxt, noh_assinatura, assinador

    def _depois_de_assinar_ou_verificar(self, doc_xml, ctxt, assinador):
        # Libera a memória do assinador; isso é necessário, pois na verdade foi feita uma chamada
        # a uma função em C cujo código não é gerenciado pelo Python
        assinador.destroy()
        ctxt.xpathFreeContext()
        doc_xml.freeDoc()

        # E, por fim, desativa todas as funções ativadas anteriormente
        self._desativar_funcoes_criptograficas()