
def cnpj_cpf(number):
    if len(number) <= 11 and len(number) >=10:
        return '%s.%s.%s-%s' % (number[0:3], number[3:6], number[6:9], number[9:11])
    elif len(number) > 11:
        return '%s.%s.%s/%s-%s' % (number[0:2], number[2:5], number[5:8], number[8:12], number[12:14])
    return number



