from django.conf import settings

def settings_context_processor(request):
    my_dict = {
        'TEST_ENVIRONMENT': settings.TEST_ENVIRONMENT,
    }

    return my_dict