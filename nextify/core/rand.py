import random
import string

def random_number(size):
    return ''.join(random.choice(string.digits) for _ in range(size))

def secure_random_number(size):
    return ''.join(random.SystemRandom().choice(string.digits) for _ in range(size))

def random_string(size):
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(size))

