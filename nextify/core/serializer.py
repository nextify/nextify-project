from lxml import etree



class Serializer(object):

    def __init__(self):
        self.root_xml = None
        self._parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')

    def __unicode__(self):
        return etree.tostring(self.root_xml)


