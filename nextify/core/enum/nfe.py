# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum


class NFeEnvironment(enum.Enum):
    PRODUCTION = 1
    TEST = 2

    labels = {
        PRODUCTION: u'Produção',
        TEST: u'Homologação',
    }


class NFeIndTot(enum.Enum):
    NO = 0
    YES = 1

    labels = {
        NO: u'Valor do Item não compõe o valor total da NFe.',
        YES: u'Valor do Item compõe o valor total da NFe.',
    }


class NFeModFrete(enum.Enum):
    ISSUER = 0
    RECEIVER = 1
    THIRD = 2
    NONE = 9

    labels = {
        ISSUER: u'Por conta do emitente',
        RECEIVER: u'Por conta do destinatário/remetente',
        THIRD: u'Por conta de terceiros',
        NONE: u'Sem frete',
    }


class NFePaymentType(enum.Enum):
    IN_CASH = 0
    IN_TERM = 1
    OTHER = 2

    labels = {
        IN_CASH: u'À vista',
        IN_TERM: u'À prazo',
        OTHER: u'Outros',
    }


class NFeIndFinalConsumer(enum.Enum):
    NORMAL = 0
    FINAL_CONSUMER = 1

    labels = {
        NORMAL: u'Normal',
        FINAL_CONSUMER: u'Consumidor final',
    }


class NFeIndConsumerPresence(enum.Enum):
    NOT_APPLICABLE = 0
    FACE_OPERATION = 1
    NOT_FACE_OPERATION_INTERNET = 2
    NOT_FACE_OPERATION_TELEMARKETING = 3
    HOME_DELIVERY = 4
    NOT_FACE_OPERATION_OTHER = 9

    labels = {
        NOT_APPLICABLE: u'Não se aplica',
        FACE_OPERATION: u'Operação Presencial',
        NOT_FACE_OPERATION_INTERNET: u'Operação não presencial (Internet)',
        NOT_FACE_OPERATION_TELEMARKETING: u'Operação não presencial (Teleatendimento)',
        HOME_DELIVERY: u'NFC-e Entrega a domicílio',
        NOT_FACE_OPERATION_OTHER: u'Operação não presencial (Outros)',
    }


class NFeDANFEPrintType(enum.Enum):
    NO_DANFE = 0
    PORTRAIT = 1
    LANDSCAPE = 2
    SIMPLIFIED_DANFE = 3
    NFCE_DANFE = 4
    NFCE_DANFE_ELETRONIC = 5

    labels = {
        NO_DANFE: u'Sem geração de DANFE',
        PORTRAIT: u'DANFE Normal, Retrato',
        LANDSCAPE: u'DANFE Normal, Paisagem',
        SIMPLIFIED_DANFE: u'DANFE Simplificado',
        NFCE_DANFE: u'DANFE NFC-e',
        NFCE_DANFE_ELETRONIC: u'DANFE NFC-e em mensagem eletrônica',
    }


class NFeIssueType(enum.Enum):
    NORMAL = 1
    CONTINGENCY_SVC_AN = 6

    labels = {
        NORMAL: u'Emissão Normal',
        CONTINGENCY_SVC_AN: u'Contingência SVC-AN'
    }


class NFeFinality(enum.Enum):
    REGULAR = 1
    COMPLEMENTARY = 2
    ADJUSTMENT = 3
    RETURN = 4

    labels = {
        REGULAR: u'Emissão Normal',
        COMPLEMENTARY: u'Complementar',
        ADJUSTMENT: u'Ajuste',
        RETURN: u'Devolução'
    }


class NFeDestination(enum.Enum):
    STATE = 1
    INTERSTATE = 2
    INTERNATIONAL = 3

    labels = {
        STATE: u'Estadual',
        INTERSTATE: u'Interestadual',
        INTERNATIONAL: u'Internacional'
    }


class NFeEventType(enum.Enum):
    CANCEL_EVENT = '110111'
    CCE_EVENT = '110110'

    labels = {
        CANCEL_EVENT: u'Cancelamento',
        CCE_EVENT: u'Carta de Correção'
    }


class NFeStatusEnum(enum.Enum):
    STAT001 = 1
    STAT002 = 2
    STAT003 = 3
    STAT004 = 4
    STAT100 = 100
    STAT101 = 101
    STAT102 = 102
    STAT103 = 103
    STAT104 = 104
    STAT105 = 105
    STAT106 = 106
    STAT107 = 107
    STAT108 = 108
    STAT109 = 109
    STAT110 = 110
    STAT999 = 999

