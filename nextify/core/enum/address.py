# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum
from django.utils.translation import ugettext_lazy as _

class AddressPersonTitle(enum.Enum):
    SR, SRA, SRTA, DR, VC = (0, 1, 2, 3, 4)

    labels = {
        SR: u'Sr.',
        SRA: u'Sra.',
        SRTA: u'Srta.',
        DR: u'Dr.',
        VC: u'Você',
    }

class AddressType(enum.Enum):
    BILLING, SHIPPING = (0, 1)
    labels = {
        BILLING: _(u'Faturamento'),
        SHIPPING: _(u'Entrega'),
    }
