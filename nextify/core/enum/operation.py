# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum
from django.utils.translation import ugettext_lazy as _


class OperationType(enum.Enum):
    IN = 0
    OUT = 1

    labels = {
        IN: u'Entrada',
        OUT: u'Saída',
    }


class OperationDestiny(enum.Enum):
    STATE = 1
    INTERSTATE = 2
    INTERNATIONAL = 3

    labels = {
        STATE: u'Estadual',
        INTERSTATE: u'Interestadual',
        INTERNATIONAL: u'Internacional',
    }

class OperationIssuer(enum.Enum):
    SELF = 0
    THIRD = 1

    labels = {
        SELF: u'Emissão Própria',
        THIRD: u'Emissão de Terceiros',
    }


class NFeFinality(enum.Enum):
    REGULAR = 1
    COMPLEMENTARY = 2
    ADJUSTMENT = 3
    RETURN = 4

    labels = {
        REGULAR: _('Regular'),
        COMPLEMENTARY: _('Complementary'),
        ADJUSTMENT: _('Adjustment'),
        RETURN: _('Return')
    }

class TaxCalcMethod(enum.Enum):
    NO_CALC = 0
    FIXED = 1
    MANUAL = 2
    AUTOMATIC = 3

    labels = {
        NO_CALC: _('No calculation'),
        FIXED: _('Fixed'),
        MANUAL: _('Manual'),
        AUTOMATIC: _('Automatic'),
    }


CREDIT_NATURE_00 = '00'
CREDIT_NATURE_01 = '01'
CREDIT_NATURE_02 = '02'
CREDIT_NATURE_03 = '03'
CREDIT_NATURE_04 = '04'
CREDIT_NATURE_05 = '05'
CREDIT_NATURE_06 = '06'
CREDIT_NATURE_07 = '07'
CREDIT_NATURE_08 = '08'
CREDIT_NATURE_09 = '09'
CREDIT_NATURE_10 = '10'
CREDIT_NATURE_11 = '11'
CREDIT_NATURE_12 = '12'
CREDIT_NATURE_13 = '13'
CREDIT_NATURE_14 = '14'
CREDIT_NATURE_15 = '15'
CREDIT_NATURE_16 = '16'
CREDIT_NATURE_17 = '17'
CREDIT_NATURE_18 = '18'
CREDIT_NATURE_CHOICES = (
    (CREDIT_NATURE_00, u'Sem crédito'),
    (CREDIT_NATURE_01, u'Aquisição de bens para revenda'),
    (CREDIT_NATURE_02, u'Aquisição de bens utilizados como insumo'),
    (CREDIT_NATURE_03, u'Aquisição de serviços utilizados como insumo'),
    (CREDIT_NATURE_04, u'Energia elétrica e térmica, inclusive sob a forma de vapor'),
    (CREDIT_NATURE_05, u'Aluguéis de prédios'),
    (CREDIT_NATURE_06, u'Aluguéis de máquinas e equipamentos'),
    (CREDIT_NATURE_07, u'Armazenagem de mercadoria e frete na operação de venda'),
    (CREDIT_NATURE_08, u'Contraprestações de arrendamento mercantil'),
    (CREDIT_NATURE_09, u'Máquinas, equipamentos e outros bens incorporados ao ativo imobilizado '
                       u'(crédito sobre encargos de depreciação).'),
    (CREDIT_NATURE_10, u'Máquinas, equipamentos e outros bens incorporados ao ativo imobilizado '
                       u'(crédito com base no valor de aquisição).'),
    (CREDIT_NATURE_11, u'Amortização e Depreciação de edificações e benfeitorias em imóveis'),
    (CREDIT_NATURE_12, u'Devolução de Vendas Sujeitas à Incidência Não-Cumulativa'),
    (CREDIT_NATURE_13, u'Outras Operações com Direito a Crédito'),
    (CREDIT_NATURE_14, u'Atividade de Transporte de Cargas – Subcontratação'),
    (CREDIT_NATURE_15, u'Atividade Imobiliária – Custo Incorrido de Unidade Imobiliária'),
    (CREDIT_NATURE_16, u'Atividade Imobiliária – Custo Orçado de unidade não concluída'),
    (CREDIT_NATURE_17, u'Atividade de Prestação de Serviços de Limpeza, Conservação e Manutenção – '
                       u'vale-transporte, vale-refeição ou vale-alimentação, fardamento ou uniforme.'),
    (CREDIT_NATURE_18, u'Estoque de abertura de bens'),
)