# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum


class PaymentForm(enum.Enum):
    CASH_PAYMENT = 0
    DEFERRED_PAYMENT = 1
    OTHER = 2

    labels = {
        CASH_PAYMENT: u'Pagamento à vista',
        DEFERRED_PAYMENT: u'Pagamento à prazo',
        OTHER: u'Outros',
    }


class PaymentType(enum.Enum):
    CASH = '01'
    CHECK = '02'
    CREDIT_CARD = '03'
    DEBIT_CARD = '04'
    STORE_CREDIT = '05'
    OTHER = '99'

    labels = {
        CASH: u'Dinheiro',
        CHECK: u'Cheque',
        CREDIT_CARD: u'Cartão de Crédito',
        DEBIT_CARD: u'Cartão de Débito',
        STORE_CREDIT: u'Crédito na Loja',
        OTHER: u'Outros',
    }


class PaymentCardFlag(enum.Enum):
    VISA = '01'
    MASTERCARD = '02'
    AMEX = '03'
    SOROCRED = '04'
    OTHER = '99'

    labels = {
        VISA: u'Visa',
        MASTERCARD: u'Mastercard',
        AMEX: u'American Express',
        SOROCRED: u'Sorocred',
        OTHER: u'Outros',
    }