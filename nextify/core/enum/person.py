# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum
from django.utils.translation import ugettext_lazy as _


class PersonCategory(enum.Enum):

    INDIVIDUAL = 0
    CORPORATE = 1
    FOREIGN = 2

    labels = {
        INDIVIDUAL: u'Pessoa Física',
        CORPORATE:  u'Pessoa Jurídica',
        FOREIGN:  u'Estrangeiro',
    }


class PersonCategoryBR(enum.Enum):

    INDIVIDUAL = 0
    CORPORATE = 1

    labels = {
        INDIVIDUAL: u'Pessoa Física',
        CORPORATE:  u'Pessoa Jurídica',
    }


class PersonCRT(enum.Enum):
    CONSUMER = 0
    SIMPLE = 1
    SIMPLE_SUB = 2
    NORMAL = 3
    labels = {
        CONSUMER: u'Consumidor',
        SIMPLE: u'Simples Nacional',
        SIMPLE_SUB: u'Simples Nacional - Excesso de sublimite da receita bruta',
        NORMAL: u'Normal',
    }

class PersonIE(enum.Enum):
    TAXPAYER = 1
    EXEMPT_TAXPAYER = 2
    NO_TAXPAYER = 9

    labels = {
        TAXPAYER: u'Contribuinte',
        EXEMPT_TAXPAYER: u'Contribuinte Isento',
        NO_TAXPAYER: u'Não Contribuinte',
    }


class PersonType(enum.Enum):
    CUSTOMER = 1
    SUPPLIER = 2
    COMPANY = 3
    SHIPPING_COMPANY = 4

    labels = {
        CUSTOMER: u'Cliente',
        SUPPLIER: u'Fornecedor',
        COMPANY: u'Empresa',
        SHIPPING_COMPANY: u'Transportadora'
    }