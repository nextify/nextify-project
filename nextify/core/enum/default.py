# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum
from django.utils.translation import ugettext_lazy as _

class YesNo(enum.Enum):
    YES, NO = (True, False)

    labels = {
        YES: _('Yes'),
        NO: _('No'),
    }


class Active(enum.Enum):
    ACTIVE, INACTIVE = (True, False)
    labels = {
        ACTIVE: _('Active'),
        INACTIVE: _('Inactive'),
    }
