# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum


class Bank(enum.Enum):
    SANTANDER = 33

    labels = {
        SANTANDER: u'Santander'
    }


class InvoiceStatus(enum.Enum):
    OPEN = 0
    PARTIAL_PAY = 1
    PAYED = 2
    CANCELED = 3

    labels = {
        OPEN: u'Em Aberto',
        PARTIAL_PAY: u'Pagamento Parcial',
        PAYED: u'Pago',
        CANCELED: u'Cancelada'
    }


class BankEvents(enum.Enum):
    ENTRADA_TITULO = 1
    BAIXA_TITULO = 2
    CONCESSAO_ABATIMENTO = 4
    CANCELAMENTO_ABATIMENTO = 5
    PRORROGACAO_VENCIMENTO = 6
    ALTERACAO_CONTROLE_CEDENTE = 7
    ALTERACAO_SEU_NUMERO = 8
    PROTESTAR = 9
    SUSTAR_PROTESTO = 18
    NAO_PROTESTAR = 98


class BankInstructions(enum.Enum):
    NAO_HA_INSTRUCOES = 0
    BAIXAR_APOS_15_DIAS = 2
    BAIXAR_APOS_30_DIAS = 3
    NAO_BAIXAR = 4
    PROTESTAR = 6
    NAO_PROTESTAR = 7
    NAO_COBRAR_JUROS = 8


class BankFileStatus(enum.Enum):
    NO_FILE = 0
    FILE_CREATED = 1
    FILE_CANCELED = 2
    FILE_SENT = 3

    labels = {
        NO_FILE: u'Arquivo não gerado',
        FILE_CREATED: u'Arquivo criado',
        FILE_CANCELED: u'Arquivo cancelado',
        FILE_SENT: u'Enviado para o banco'
    }
