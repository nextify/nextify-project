# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum
from django.utils.translation import ugettext_lazy as _


class ItemType(enum.Enum):

    REVENDA = 0
    INSUMO = 1
    EMBALAGEM = 2
    PRODUTO_PROCESSO = 3
    PRODUTO_ACABADO = 4
    SUBPRODUTO = 5
    PRODUTO_INTERMEDIARIO = 6
    USO_CONSUMO = 7
    ATIVO_IMOBILIZADO = 8
    SERVICO = 9
    OUTROS_INSUMOS = 10
    OUTROS = 99

    labels = {
        REVENDA: u'Mercadoria para revenda',
        INSUMO: u'Matéria Prima',
        EMBALAGEM: u'Embalagem',
        PRODUTO_PROCESSO: u'Produto em Processo',
        PRODUTO_ACABADO: u'Produto Acabado',
        SUBPRODUTO: u'Subproduto',
        PRODUTO_INTERMEDIARIO: u'Produto Intermediário',
        USO_CONSUMO: u'Material de Uso e Consumo',
        ATIVO_IMOBILIZADO: u'Ativo Imobilizado',
        SERVICO: u'Serviços',
        OUTROS_INSUMOS: u'Outros Insumos',
        OUTROS: u'Outros',
    }


class ItemGenre(enum.Enum):
    GENRE_39 = 39
    GENRE_82 = 82
    GENRE_83 = 83

    labels = {
        GENRE_39: u'39 - Plásticos e suas obras',
        GENRE_82: u'82 - Ferramentas, artefatos de cutelaria e talheres, e suas partes, de metais comuns',
        GENRE_83: u'83 - Obras diversas de metais comuns',
    }


