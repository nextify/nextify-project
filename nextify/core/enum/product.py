# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum
from django.utils.translation import ugettext_lazy as _


class ProductOrigin(enum.Enum):
    NACIONAL = 0

    labels = {
        NACIONAL: 'Nacional'
    }
