# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum
from django.utils.translation import ugettext_lazy as _


class SequenceType(enum.Enum):

    BASE_PRODUCT = 'base_product'
    PRODUCT = 'product'
    ITEM_CATEGORY = 'item_category'
    SUPPLIER = 'supplier'
    CUSTOMER = 'customer'
    SHIPPING_COMPANY = 'shipping_company'
    BANK_NUMBER = 'bank_number'
    BANK_FILE = 'bank_file'

    labels = {
        BASE_PRODUCT: 'Produto Base',
        PRODUCT: 'Produto',
        ITEM_CATEGORY: 'Categoria de Item',
        SUPPLIER: 'Fornecedor',
        CUSTOMER: 'Cliente',
        SHIPPING_COMPANY: 'Transportadora',
        BANK_NUMBER: 'Nosso Numero',
        BANK_FILE: 'Arquivo Bancario',
    }
