# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum


class StockType(enum.Enum):
    OWN_STOCK = 0
    STOCK_HELD_BY_THIRD_PARTIES = 1
    THIRD_PARTIES_STOCK = 2


    labels = {
        OWN_STOCK: u'Estoque Próprio',
        STOCK_HELD_BY_THIRD_PARTIES: u'Estoque Próprio em Poder de Terceiros',
        THIRD_PARTIES_STOCK: u'Estoque de Terceiros',
    }


class StockLineStatus(enum.Enum):
    OPEN = 0
    NORMAL = 1
    CANCELED = 2

    labels = {
        OPEN: u'Aberta',
        NORMAL: u'Normal',
        CANCELED: u'Cancelada'
    }
