# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum
from django.utils.translation import ugettext_lazy as _

class TaxType(enum.Enum):
    ISS = 0
    PIS = 1
    COFINS = 2
    ICMS = 3
    IPI = 4
    ICMSSN = 5
    IBPT = 9

    labels = {
        ISS: 'Iss',
        PIS: 'Pis',
        COFINS: 'Cofins',
        ICMS: 'Icms',
        ICMSSN: 'Icms Simples Nacional',
        IPI: 'Ipi',
        IBPT: 'Ibpt',
    }

class TaxIcmsSNType(enum.Enum):
    CSOSN101 = 101
    CSOSN102 = 102
    CSOSN103 = 103
    CSOSN201 = 201
    CSOSN202 = 202
    CSOSN203 = 203
    CSOSN300 = 300
    CSOSN400 = 400
    CSOSN500 = 500
    CSOSN900 = 900

    labels = {
        CSOSN101: u'101 - Tributada pelo Simples Nacional com permissão de crédito',
        CSOSN102: u'102 - Tributada pelo Simples Nacional sem permissão de crédito',
        CSOSN103: u'103 - Isenção do ICMS pelo Simples Nacional para faixa de Receita Bruta',
        CSOSN201: u'201 - Tributada pelo Simples Nacional com permissão de Crédito e com cobrança do ICMS por Substituição Tributária',
        CSOSN202: u'202 - Tributada pelo Simples Nacional sem permissão de Crédito e com cobrança do ICMS por Substituição Tributária',
        CSOSN203: u'203 - Isençao do ICMS no Simples Nacional para faixa de receita bruta e com cobrança do ICMS por Substituição Tributária',
        CSOSN300: u'300 - Imune',
        CSOSN400: u'400 - Não Tributada pelo Simples Nacional',
        CSOSN500: u'500 - ICMS cobrado anteriormente por Substituição Tributária (substituido) ou por antecipação.',
        CSOSN900: u'900 - Outros',
    }

class TaxIcmsDetCalcBase(enum.Enum):
    MODBC0 = 0
    MODBC1 = 1
    MODBC2 = 2
    MODBC3 = 3

    labels = {
        MODBC0: u'0 - Margem Valor Agregado',
        MODBC1: u'1 - Pauta (Valor)',
        MODBC2: u'2 - Preço Tabelado Max. (valor)',
        MODBC3: u'3 - Valor da operação',
    }

TABLE_TYPE_GENERIC = 'GN'
TABLE_TYPE_OPERATION = 'OP'
TABLE_TYPE_NBS = 'NS'
TABLE_TYPE_SERVICE_CODE = 'SC'
TABLE_TYPE_OPERATION_NBS = 'ON'
TABLE_TYPE_OPERATION_SERVICE_CODE = 'OS'

class TaxTableType(enum.Enum):
    GENERIC = 0
    OPERATION = 1
    NBS = 2
    SERVICE_CODE = 3
    OPERATION_NBS = 4
    OPERATION_SERVICE_CODE = 5

    labels = {
        GENERIC: _("Generic"),
        OPERATION: _("Operation"),
        NBS: _("NBS"),
        SERVICE_CODE: _("Service Code"),
        OPERATION_NBS: _("Operation/NBS"),
        OPERATION_SERVICE_CODE: _("Operation/Service Code"),
    }

class IssTaxationType(enum.Enum):
    COUNTY = 1
    OUTSIDE_COUNTY = 2
    EXEMPT = 3
    IMMUNE = 4
    SUSPENDED_ADM = 5
    SUSPENDED_JUD = 6

    labels = {
        COUNTY: _(u'Tributação no município'),
        OUTSIDE_COUNTY: _(u'Tributação fora do município'),
        EXEMPT: _(u'Isenção'),
        IMMUNE: _(u'Imune'),
        SUSPENDED_ADM: _(u'Exigibilidade suspensa (Decisão Judicial)'),
        SUSPENDED_JUD: _(u'Exigibilidade suspensa (Procedimento Administrativo)'),
    }


class TaxIpiType(enum.Enum):
    IPI00 = '00'
    IPI01 = '01'
    IPI02 = '02'
    IPI03 = '03'
    IPI04 = '04'
    IPI05 = '05'
    IPI49 = '49'
    IPI50 = '50'
    IPI51 = '51'
    IPI52 = '52'
    IPI53 = '53'
    IPI54 = '54'
    IPI55 = '55'
    IPI99 = '99'

    labels = {
        IPI00: u'00 - Entrada com recuperação de crédito',
        IPI01: u'01 - Entrada tributada com alíquota zero',
        IPI02: u'02 - Entrada Isenta',
        IPI03: u'03 - Entrada não tributada',
        IPI04: u'04 - Entrada imune',
        IPI05: u'05 - Entrada com suspensão',
        IPI49: u'49 - Outras Entradas',
        IPI50: u'50 - Saída Tributada',
        IPI51: u'51 - Saída tributada com alíquota zero',
        IPI52: u'52 - Saída Isenta',
        IPI53: u'53 - Saída não tributada',
        IPI54: u'54 - Saída imune',
        IPI55: u'55 - Saída com suspensão',
        IPI99: u'99 - Outras Saídas',
    }


class TaxPisType(enum.Enum):
    PIS01 = '01'
    PIS02 = '02'
    PIS03 = '03'
    PIS04 = '04'
    PIS05 = '05'
    PIS06 = '06'
    PIS07 = '07'
    PIS08 = '08'
    PIS09 = '09'
    PIS49 = '49'
    PIS50 = '50'
    PIS51 = '51'
    PIS52 = '52'
    PIS53 = '53'
    PIS54 = '54'
    PIS55 = '55'
    PIS56 = '56'
    PIS60 = '60'
    PIS61 = '61'
    PIS62 = '62'
    PIS63 = '63'
    PIS64 = '64'
    PIS65 = '65'
    PIS66 = '66'
    PIS67 = '67'
    PIS70 = '70'
    PIS71 = '71'
    PIS72 = '72'
    PIS73 = '73'
    PIS74 = '74'
    PIS75 = '75'
    PIS98 = '98'
    PIS99 = '99'

    labels = {
        PIS01: u'01 - Operação Tributável com Alíquota Básica.',
        PIS02: u'02 - Operação Tributável com Alíquota Diferenciada.',
        PIS03: u'03 - Operação Tributável com Alíquota por Unidade de Medida de Produto.',
        PIS04: u'04 - Operação Tributável Monofásica - Revenda a Alíquota Zero.',
        PIS05: u'05 - Operação Tributável por Substituição Tributária.',
        PIS06: u'06 - Operação Tributável a Alíquota Zero.',
        PIS07: u'07 - Operação Isenta da Contribuição.',
        PIS08: u'08 - Operação sem Incidência da Contribuição.',
        PIS09: u'09 - Operação com Suspensão da Contribuição.',
        PIS49: u'49 - Outras Operações de Saída.',
        PIS50: u'50 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno.',
        PIS51: u'51 - Operação com Direito a Crédito – Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno.',
        PIS52: u'52 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação.',
        PIS53: u'53 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno.',
        PIS54: u'54 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação.',
        PIS55: u'55 - Operação com Direito a Crédito - Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação.',
        PIS56: u'56 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação.',
        PIS60: u'60 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno.',
        PIS61: u'61 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno.',
        PIS62: u'62 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação.',
        PIS63: u'63 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno.',
        PIS64: u'64 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação.',
        PIS65: u'65 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação.',
        PIS66: u'66 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação.',
        PIS67: u'67 - Crédito Presumido - Outras Operações.',
        PIS70: u'70 - Operação de Aquisição sem Direito a Crédito.',
        PIS71: u'71 - Operação de Aquisição com Isenção.',
        PIS72: u'72 - Operação de Aquisição com Suspensão.',
        PIS73: u'73 - Operação de Aquisição a Alíquota Zero.',
        PIS74: u'74 - Operação de Aquisição sem Incidência da Contribuição.',
        PIS75: u'75 - Operação de Aquisição por Substituição Tributária.',
        PIS98: u'98 - Outras Operações de Entrada.',
        PIS99: u'99 - Outras Operações.',
    }


class TaxCofinsType(enum.Enum):
    COFINS01 = '01'
    COFINS02 = '02'
    COFINS03 = '03'
    COFINS04 = '04'
    COFINS05 = '05'
    COFINS06 = '06'
    COFINS07 = '07'
    COFINS08 = '08'
    COFINS09 = '09'
    COFINS49 = '49'
    COFINS50 = '50'
    COFINS51 = '51'
    COFINS52 = '52'
    COFINS53 = '53'
    COFINS54 = '54'
    COFINS55 = '55'
    COFINS56 = '56'
    COFINS60 = '60'
    COFINS61 = '61'
    COFINS62 = '62'
    COFINS63 = '63'
    COFINS64 = '64'
    COFINS65 = '65'
    COFINS66 = '66'
    COFINS67 = '67'
    COFINS70 = '70'
    COFINS71 = '71'
    COFINS72 = '72'
    COFINS73 = '73'
    COFINS74 = '74'
    COFINS75 = '75'
    COFINS98 = '98'
    COFINS99 = '99'

    labels = {
        COFINS01: u'01 - Operação Tributável com Alíquota Básica.',
        COFINS02: u'02 - Operação Tributável com Alíquota Diferenciada.',
        COFINS03: u'03 - Operação Tributável com Alíquota por Unidade de Medida de Produto.',
        COFINS04: u'04 - Operação Tributável Monofásica - Revenda a Alíquota Zero.',
        COFINS05: u'05 - Operação Tributável por Substituição Tributária.',
        COFINS06: u'06 - Operação Tributável a Alíquota Zero.',
        COFINS07: u'07 - Operação Isenta da Contribuição.',
        COFINS08: u'08 - Operação sem Incidência da Contribuição.',
        COFINS09: u'09 - Operação com Suspensão da Contribuição.',
        COFINS49: u'49 - Outras Operações de Saída.',
        COFINS50: u'50 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno.',
        COFINS51: u'51 - Operação com Direito a Crédito – Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno.',
        COFINS52: u'52 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação.',
        COFINS53: u'53 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno.',
        COFINS54: u'54 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação.',
        COFINS55: u'55 - Operação com Direito a Crédito - Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação.',
        COFINS56: u'56 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação.',
        COFINS60: u'60 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno.',
        COFINS61: u'61 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno.',
        COFINS62: u'62 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação.',
        COFINS63: u'63 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno.',
        COFINS64: u'64 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação.',
        COFINS65: u'65 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação.',
        COFINS66: u'66 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação.',
        COFINS67: u'67 - Crédito Presumido - Outras Operações.',
        COFINS70: u'70 - Operação de Aquisição sem Direito a Crédito.',
        COFINS71: u'71 - Operação de Aquisição com Isenção.',
        COFINS72: u'72 - Operação de Aquisição com Suspensão.',
        COFINS73: u'73 - Operação de Aquisição a Alíquota Zero.',
        COFINS74: u'74 - Operação de Aquisição sem Incidência da Contribuição.',
        COFINS75: u'75 - Operação de Aquisição por Substituição Tributária.',
        COFINS98: u'98 - Outras Operações de Entrada.',
        COFINS99: u'99 - Outras Operações.',
    }