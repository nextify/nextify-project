# -*- coding: utf-8 -*-
from nextify.core.django_enumfield import enum
from django.utils.translation import ugettext_lazy as _


class DocumentModel(enum.Enum):
    NFE = 55
    CTE = 57
    NFCE = 65
    NFS = 98
    NFSE = 99

    labels = {
        NFS: u'NFS - Nota Fiscal de Serviço',
        NFSE: u'NFSE - Nota Fiscal Eletrônica de Serviço',
        NFE: u'55 - Nota Fiscal Eletrônica',
        CTE: u'57 - Conhecimento de Transporte Eletrônico',
        NFCE: u'65 - Nota Fiscal Eletrônica para Consumidor Final',
    }

class DocumentStatus(enum.Enum):
    TYPING = 0
    FINALIZED = 1
    CANCELED = 2

    labels = {
        TYPING: u'Digitação',
        FINALIZED: u'Finalizada',
        CANCELED: u'Cancelada',
    }

class DocumentType(enum.Enum):
    NFSE = 0
    NFE = 1

    labels = {
        NFSE: 'NFS-e',
        NFE: 'NF-e',
    }
