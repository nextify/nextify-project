var PriceTable = function () {
    var handlePriceTable = function () {
        var count = parseInt($('#id_pricetableline_set-TOTAL_FORMS').attr('value'), 10);

        for (var i = 0 ; i<count ; i++){
            var prod_aux = 'id_pricetableline_set-__prefix__-product';
            var unit_aux = 'id_pricetableline_set-__prefix__-unit';
            prod_aux = prod_aux.replace(/__prefix__/g, i);
            unit_aux = unit_aux.replace(/__prefix__/g, i);
            prod = $('#'+prod_aux);
            unit = $('#'+unit_aux);
            handleSelect2wParent(unit, prod)
        }

        $('.productselect').each(handleSelect2);

        $('.add-product').click(function(ev){
            ev.preventDefault();
            var count = parseInt($('#id_pricetableline_set-TOTAL_FORMS').attr('value'), 10);
            var tmplMarkup = $('#pricetableline_template').html()
            var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);

            $(compiledTmpl).prependTo('#pricetableline_table > tbody:first');
            $('#id_pricetableline_set-TOTAL_FORMS').attr('value', count + 1)

            var delete_aux = 'id_pricetableline_set-__prefix__-DELETE';
            delete_aux = delete_aux.replace(/__prefix__/g, count);
            d = $('#' + delete_aux)
            $(d).each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });

            var select_aux = 'id_pricetableline_set-__prefix__-product'
            select_aux = select_aux.replace(/__prefix__/g, count);
            prod = $('#' + select_aux)
            $(prod).each(handleSelect2);

            var select_aux = 'id_pricetableline_set-__prefix__-unit'
            select_aux = select_aux.replace(/__prefix__/g, count);
            x = $('#' + select_aux)
            $(x).each(function(i, e){
                handleSelect2wParent(e, prod);
            });
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handlePriceTable();
        }
    };

}();

jQuery(document).ready(function() {
   PriceTable.init();
});

