
var AddressList = function () {

    var handleAddresses = function() {
        var grid = new Datatable();

        grid.init({
            src: $("#datatable_products"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "ajax": {
                    //"url": "http://127.0.0.1:8000/product/json/", // ajax source
                    //"url": $("#datatable_products").attr('ajax-url'), // ajax source
                },
                "paging": false,
                "info": false,
                "ordering": false
            }
        });

         // handle group actionsubmit button click
        //grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
        //    e.preventDefault();
        //    var action = $(".table-group-action-input", grid.getTableWrapper());
        //    if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
        //        grid.setAjaxParam("customActionType", "group_action");
        //        grid.setAjaxParam("customActionName", action.val());
        //        grid.setAjaxParam("id", grid.getSelectedRows());
        //        grid.getDataTable().ajax.reload();
        //        grid.clearAjaxParams();
        //    } else if (action.val() == "") {
        //        App.alert({
        //            type: 'danger',
        //            icon: 'warning',
        //            message: 'Please select an action',
        //            container: grid.getTableWrapper(),
        //            place: 'prepend'
        //        });
        //    } else if (grid.getSelectedRowsCount() === 0) {
        //        App.alert({
        //            type: 'danger',
        //            icon: 'warning',
        //            message: 'No record selected',
        //            container: grid.getTableWrapper(),
        //            place: 'prepend'
        //        });
        //    }
        //});
    }

    return {

        //main function to initiate the module
        init: function () {

            handleAddresses();
            //initPickers();

        }

    };

}();

jQuery(document).ready(function() {
   AddressList.init();
});