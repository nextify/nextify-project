var FormInputMask = function () {
    var handleInputMasks = function () {
        $("#id_phone").inputmask({
            "mask": "9",
            "repeat": 14,
            "greedy": false
        });

        $("#id_phone2").inputmask({
            "mask": "9",
            "repeat": 14,
            "greedy": false
        });

        $("#id_zipcode").inputmask({
            "mask": "9",
            "repeat": 8,
            "greedy": false
        });
    }
    return {
        //main function to initiate the module
        init: function () {
            handleInputMasks();
        }
    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        FormInputMask.init(); // init metronic core componets
    });
}