
var Invoice = function () {
    var handleDuplicate = function () {
        $('.customerselect').each(handleSelect2);
        $('.add-duplicate').click(function(ev){
            ev.preventDefault();
            var count = parseInt($('#id_duplicates-TOTAL_FORMS').attr('value'), 10);
            var tmplMarkup = $('#invoiceduplicate_template').html()
            var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);

            $(compiledTmpl).appendTo('#invoiceduplicate_table > tbody');
            $('#id_duplicates-TOTAL_FORMS').attr('value', count + 1)

            var delete_aux = 'id_duplicates-__prefix__-DELETE';
            delete_aux = delete_aux.replace(/__prefix__/g, count);
            d = $('#' + delete_aux)
            $(d).each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });

            var invoice_due_date_aux = 'id_duplicates-__prefix__-invoice_due_date';
            invoice_due_date_aux = invoice_due_date_aux.replace(/__prefix__/g, count);
            d = $('#' + invoice_due_date_aux)

            $(d).datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true
            });
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleDuplicate();
        }
    };

}();

jQuery(document).ready(function() {
   Invoice.init();
});

