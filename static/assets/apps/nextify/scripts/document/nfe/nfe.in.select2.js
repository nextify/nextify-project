var selectAddress = function(elem, person, type) {
    var customer = '';
    var supplier = '';
    if (type == "customer") {
        customer = $(person).val();
    } else if (type == "supplier") {
        supplier = $(person).val();
    }

    var opts = {
        //placeholder: 'Selecione um Unidade ...',
        //minimumInputLength: 1,
        allowClear: true,
        ajax: {
            url: $(elem).data('ajax-url'),
            dataType: 'json',
            results: function (data, page) {
                if ((page == 1) && !($(elem).data('required') == 'required')) {
                    data.results.unshift({'id': '', 'text': '------------'});
                }
                return data;
            },
            data: function (term, page) {
                return {
                    q: term,
                    customer: customer,
                    supplier: supplier,
                    page: page
                };
            }
        },
        initSelection: function (e, callback) {
            $.ajax({
                type: 'GET',
                url: $(e).data('ajax-url'),
                data: [{'name': 'initial', 'value': $(e).val()}],
                success: function (data) {
                    if (data.results) {
                        if ($(e).data('multiple')) {
                            callback(data.results);
                        } else {
                            callback(data.results[0]);
                        }
                    }
                },
                'dataType': 'json'
            });
        }
    }
    $(elem).select2(opts);
}

var NFe = function () {
    var handleNFe = function () {
        $('.supplierselect').each(handleSelect2);
        $('.customerselect').each(handleSelect2);
        $('.shippingcompanyselect').each(handleSelect2);
        $('.paymentmethodselect').each(handleSelect2);
        $('.pricetableselect').each(handleSelect2);
        $('.productselect').each(handleSelect2);
        $('.unitselect').each(handleSelect2);
        $('.cfopselect').each(handleSelect2);
        $('.cityselect').each(handleSelect2);
        $('.stateselect').each(handleSelect2);

        var issuer = $('#id_document_issuer');
        $('.issuer_address').each(function(i,e) {
            selectAddress(e, issuer, "supplier");
        });

        var issuer = $('#id_document_receiver');
        $('.receiver_address').each(function(i,e) {
            selectAddress(e, issuer, "customer");
        });

        var count = parseInt($('#id_lines-TOTAL_FORMS').attr('value'), 10);

        for (var i = 0 ; i<count ; i++){
            var product_aux = 'id_lines-__prefix__-product';
            var unit_aux = 'id_lines-__prefix__-unit';
            product_aux = product_aux.replace(/__prefix__/g, i);
            unit_aux = unit_aux.replace(/__prefix__/g, i);
            product = $('#'+product_aux);
            unit = $('#'+unit_aux);
            handleSelect2wParent(unit, product)
        }

        $('.add-product').click(function(ev){
            ev.preventDefault();
            var count = parseInt($('#id_lines-TOTAL_FORMS').attr('value'), 10);
            var tmplMarkup = $('#nfeline_template').html()
            var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);

            $(compiledTmpl).prependTo('#nfeline_table > tbody');
            //coll$(compiledTmpl).appendTo('#nfeline_table > tbody');
            $('#id_lines-TOTAL_FORMS').attr('value', count + 1)

            var delete_aux = 'id_lines-__prefix__-DELETE';
            delete_aux = delete_aux.replace(/__prefix__/g, count);
            d = $('#' + delete_aux)
            $(d).each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });

            var product_aux = 'id_lines-__prefix__-product'
            var cfop_aux = 'id_lines-__prefix__-cfop'
            product_aux = product_aux.replace(/__prefix__/g, count);
            cfop_aux = cfop_aux.replace(/__prefix__/g, count);
            product = $('#' + product_aux)
            cfop = $('#' + cfop_aux)
            $(product).each(handleSelect2);
            $(cfop).each(handleSelect2);

            var unit_aux = 'id_lines-__prefix__-unit'
            unit_aux = unit_aux.replace(/__prefix__/g, count);
            unit = $('#' + unit_aux)
            $(unit).each(function(i, e){
                handleSelect2wParent(e, product);
            });
        });

        $('.add-invoice').click(function(ev){
            ev.preventDefault();
            var count = parseInt($('#id_invoices-TOTAL_FORMS').attr('value'), 10);
            var tmplMarkup = $('#invoice_template').html()
            var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);

            $(compiledTmpl).appendTo('#nfeinvoice_table > tbody');
            $('#id_invoices-TOTAL_FORMS').attr('value', count + 1)

            var delete_aux = 'id_invoices-__prefix__-DELETE';
            delete_aux = delete_aux.replace(/__prefix__/g, count);
            d = $('#' + delete_aux)
            $(d).each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });

            var invoice_due_date_aux = 'id_invoices-__prefix__-invoice_due_date';
            invoice_due_date_aux = invoice_due_date_aux.replace(/__prefix__/g, count);
            d = $('#' + invoice_due_date_aux)

            $(d).datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true
            });
        });

        $('.add-volume').click(function(ev){
            ev.preventDefault();
            var count = parseInt($('#id_shipping_volumes-TOTAL_FORMS').attr('value'), 10);
            var tmplMarkup = $('#volumes_template').html()
            var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);

            $(compiledTmpl).appendTo('#nfe_shipping_volumes_table > tbody');
            $('#id_shipping_volumes-TOTAL_FORMS').attr('value', count + 1)

            var delete_aux = 'id_shipping_volumes-__prefix__-DELETE';
            delete_aux = delete_aux.replace(/__prefix__/g, count);
            d = $('#' + delete_aux)
            $(d).each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        });
    }

    var handleInputMasks = function () {
        $("#id_nfe_access_key").inputmask({
            "mask": "9",
            "repeat": 44,
            "greedy": false
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleNFe();
            handleInputMasks();
        }
    };

}();

jQuery(document).ready(function() {
   NFe.init();
});

