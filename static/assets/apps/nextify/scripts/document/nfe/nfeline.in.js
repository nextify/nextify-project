var NFeLine = function () {
    var handleNFeLine = function () {
        $('.ncmselect').each(handleSelect2);
        $('.productselect').each(handleSelect2);
        $('.cfopselect').each(handleSelect2);

        var product = $('#id_product');
        var unit = $('#id_unit');
        handleSelect2wParent(unit, product);

        //var issuer = $('#id_document_issuer');
        //$('.addressselect').each(function(i,e) {
        //    selectAddress(e, issuer, "supplier");
        //});
        //
        //var count = parseInt($('#id_lines-TOTAL_FORMS').attr('value'), 10);
        //
        //for (var i = 0 ; i<count ; i++){
        //    var product_aux = 'id_lines-__prefix__-product';
        //    var unit_aux = 'id_lines-__prefix__-unit';
        //    product_aux = product_aux.replace(/__prefix__/g, i);
        //    unit_aux = unit_aux.replace(/__prefix__/g, i);
        //    product = $('#'+product_aux);
        //    unit = $('#'+unit_aux);
        //    handleSelect2wParent(unit, product)
        //}
        //
        //$('.add-product').click(function(ev){
        //    ev.preventDefault();
        //    var count = parseInt($('#id_lines-TOTAL_FORMS').attr('value'), 10);
        //    var tmplMarkup = $('#nfeline_template').html()
        //    var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);
        //
        //    $(compiledTmpl).appendTo('#nfeline_table > tbody');
        //    $('#id_lines-TOTAL_FORMS').attr('value', count + 1)
        //
        //    var delete_aux = 'id_lines-__prefix__-DELETE';
        //    delete_aux = delete_aux.replace(/__prefix__/g, count);
        //    d = $('#' + delete_aux)
        //    $(d).each(function() {
        //        if ($(this).parents(".checker").size() === 0) {
        //            $(this).show();
        //            $(this).uniform();
        //        }
        //    });
        //
        //    var product_aux = 'id_lines-__prefix__-product'
        //    var cfop_aux = 'id_lines-__prefix__-cfop'
        //    product_aux = product_aux.replace(/__prefix__/g, count);
        //    cfop_aux = cfop_aux.replace(/__prefix__/g, count);
        //    product = $('#' + product_aux)
        //    cfop = $('#' + cfop_aux)
        //    $(product).each(handleSelect2);
        //    $(cfop).each(handleSelect2);
        //
        //    var unit_aux = 'id_lines-__prefix__-unit'
        //    unit_aux = unit_aux.replace(/__prefix__/g, count);
        //    unit = $('#' + unit_aux)
        //    $(unit).each(function(i, e){
        //        handleSelect2wParent(e, product);
        //    });
        //});
        //
        //$('.add-invoice').click(function(ev){
        //    ev.preventDefault();
        //    var count = parseInt($('#id_invoices-TOTAL_FORMS').attr('value'), 10);
        //    var tmplMarkup = $('#invoice_template').html()
        //    var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);
        //
        //    $(compiledTmpl).appendTo('#nfeinvoice_table > tbody');
        //    $('#id_invoices-TOTAL_FORMS').attr('value', count + 1)
        //
        //    var delete_aux = 'id_invoices-__prefix__-DELETE';
        //    delete_aux = delete_aux.replace(/__prefix__/g, count);
        //    d = $('#' + delete_aux)
        //    $(d).each(function() {
        //        if ($(this).parents(".checker").size() === 0) {
        //            $(this).show();
        //            $(this).uniform();
        //        }
        //    });
        //
        //    var invoice_due_date_aux = 'id_invoices-__prefix__-invoice_due_date';
        //    invoice_due_date_aux = invoice_due_date_aux.replace(/__prefix__/g, count);
        //    d = $('#' + invoice_due_date_aux)
        //
        //    $(d).datepicker({
        //        rtl: App.isRTL(),
        //        orientation: "left",
        //        autoclose: true
        //    });
        //});
    }

    return {
        //main function to initiate the module
        init: function () {
            handleNFeLine();
        }
    };

}();

jQuery(document).ready(function() {
   NFeLine.init();
});

