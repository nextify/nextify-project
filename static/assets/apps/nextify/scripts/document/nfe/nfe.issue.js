var NFeIssue = function () {

    var handleOperation = function () {
        $('#id_operation').each(handleSelect2);
    }

    return {
        //main function to initiate the module
        init: function () {
            handleOperation();
        }
    };

}();

jQuery(document).ready(function() {
   NFeIssue.init();
});
