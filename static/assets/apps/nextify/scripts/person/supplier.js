var doStuff = function () {
    var opts = {
        placeholder: "Selecione um fornecedor ...",
        minimumInputLength: 1,
        ajax: {
            url: $(this).data('ajax-url'),
            dataType: 'json',
            results: function (data, page) {
                if ((page == 1) && !($(this).data('required') == 'required')) {
                    data.results.unshift({'id': '', 'text': '------------'});
                }
                return data;
            },
            data: function (term, page) {
                return {
                    q: term,
                    page: page
                };
            }
        },
        initSelection: function (e, callback) {
            $.ajax({
                type: 'GET',
                url: $(e).data('ajax-url'),
                data: [{'name': 'initial', 'value': $(e).val()}],
                success: function (data) {
                    if (data.results) {
                        if ($(e).data('multiple')) {
                            callback(data.results);
                        } else {
                            callback(data.results[0]);
                        }
                    }
                },
                'dataType': 'json'
            });
        }
    }
    $(this).select2(opts);
}

var Supplier = function () {
    var handleSupplier = function () {
        $('.supplierselect').each(doStuff);

        $('.add-supplier').click(function(ev){
            ev.preventDefault();
            var count = parseInt($('#id_productsupplier_set-TOTAL_FORMS').attr('value'), 10);
            var tmplMarkup = $('#supplier_template').html()
            var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);

            $('#productsupplier_table > tbody:last-child').append(compiledTmpl);
            $('#id_productsupplier_set-TOTAL_FORMS').attr('value', count + 1)

            var delete_aux = 'id_productsupplier_set-__prefix__-DELETE';
            delete_aux = delete_aux.replace(/__prefix__/g, count);
            d = $('#' + delete_aux)
            $(d).each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });

            var select_aux = 'id_productsupplier_set-__prefix__-supplier'
            select_aux = select_aux.replace(/__prefix__/g, count);
            x = $('#' + select_aux)
            $(x).each(doStuff);
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleSupplier();
        }
    };

}();

jQuery(document).ready(function() {
   Supplier.init();
});

