var FormInputMask = function () {
    var handleInputMasks = function () {
        $("#id_document_number").inputmask({
            "mask": "9",
            "repeat": 20,
            "greedy": false
        });

        $("#id_state_registry").inputmask({
            "mask": "9",
            "repeat": 14,
            "greedy": false
        });

        $("#id_city_registry").inputmask({
            "mask": "9",
            "repeat": 15,
            "greedy": false
        });

        $("#id_suframa_registry").inputmask({
            "mask": "9",
            "repeat": 14,
            "greedy": false
        });
    }
    return {
        //main function to initiate the module
        init: function () {
            handleInputMasks();
        }
    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        FormInputMask.init(); // init metronic core componets
    });
}