var Product = function () {
    var handleSupplier = function () {
        $('.supplierselect').each(handleSelect2);
        $('.unitselect').each(handleSelect2);

        $('.add-supplier').click(function(ev){
            ev.preventDefault();
            var count = parseInt($('#id_productsupplier_set-TOTAL_FORMS').attr('value'), 10);
            var tmplMarkup = $('#supplier_template').html()
            var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);

            $('#product_supplier_table > tbody:last-child').append(compiledTmpl);
            $('#id_productsupplier_set-TOTAL_FORMS').attr('value', count + 1)

            var delete_aux = 'id_productsupplier_set-__prefix__-DELETE';
            delete_aux = delete_aux.replace(/__prefix__/g, count);
            d = $('#' + delete_aux)
            $(d).each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });

            var select_aux = 'id_productsupplier_set-__prefix__-supplier'
            select_aux = select_aux.replace(/__prefix__/g, count);
            x = $('#' + select_aux)
            $(x).each(handleSelect2);
        });
    }

    var handlePriceTable = function () {
        $('.pricetableselect').each(handleSelect2);

        var prod_aux = 'product_id';
        prod = $('#'+prod_aux);
        unit = $('.productunitselect');

        $(unit).each(function(i, e){
            handleSelect2wParent(e, prod);
        });

        $('.add-pricetable').click(function(ev) {
            ev.preventDefault();
            var count = parseInt($('#id_pricing-TOTAL_FORMS').attr('value'), 10);
            var tmplMarkup = $('#pricing_template').html()
            var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);

            $('#pricetableline_table > tbody:last-child').append(compiledTmpl);
            $('#id_pricing-TOTAL_FORMS').attr('value', count + 1)

            var delete_aux = 'id_pricing-__prefix__-DELETE';
            delete_aux = delete_aux.replace(/__prefix__/g, count);
            d = $('#' + delete_aux)
            $(d).each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });

            var price_table_aux = 'id_pricing-__prefix__-price_table'
            price_table_aux = price_table_aux.replace(/__prefix__/g, count);
            price_table = $('#' + price_table_aux)
            $(price_table).each(handleSelect2);

            var prod_aux = 'product_id';
            var unit_aux = 'id_pricing-__prefix__-unit';
            unit_aux = unit_aux.replace(/__prefix__/g, count);
            prod = $('#'+prod_aux);
            unit = $('#'+unit_aux);

            $(unit).each(function(i, e){
                handleSelect2wParent(e, prod);
            });
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleSupplier();
            handlePriceTable();
        }
    };

}();

jQuery(document).ready(function() {
   Product.init();
});

