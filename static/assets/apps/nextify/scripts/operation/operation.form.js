var Operation = function () {
    var handleOperation = function () {
        //
        var count = parseInt($('#id_operationcfop_set-TOTAL_FORMS').attr('value'), 10);

        for (var i = 0 ; i<count ; i++){
            var cfop_aux = 'id_operationcfop_set-__prefix__-cfop';
            cfop_aux = cfop_aux.replace(/__prefix__/g, i);
            cfop = $('#'+cfop_aux);
            $(cfop).each(handleSelect2);
        }
        //
        //$('.add-product').click(function(ev){
        //    ev.preventDefault();
        //    var count = parseInt($('#id_lines-TOTAL_FORMS').attr('value'), 10);
        //    var tmplMarkup = $('#nfeline_template').html()
        //    var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);
        //
        //    $(compiledTmpl).appendTo('#nfeline_table > tbody');
        //    $('#id_lines-TOTAL_FORMS').attr('value', count + 1)
        //
        //    var delete_aux = 'id_lines-__prefix__-DELETE';
        //    delete_aux = delete_aux.replace(/__prefix__/g, count);
        //    d = $('#' + delete_aux)
        //    $(d).each(function() {
        //        if ($(this).parents(".checker").size() === 0) {
        //            $(this).show();
        //            $(this).uniform();
        //        }
        //    });
        //
        //    var product_aux = 'id_lines-__prefix__-product'
        //    var cfop_aux = 'id_lines-__prefix__-cfop'
        //    product_aux = product_aux.replace(/__prefix__/g, count);
        //    cfop_aux = cfop_aux.replace(/__prefix__/g, count);
        //    product = $('#' + product_aux)
        //    cfop = $('#' + cfop_aux)
        //    $(product).each(handleSelect2);
        //    $(cfop).each(handleSelect2);
        //
        //    var unit_aux = 'id_lines-__prefix__-unit'
        //    unit_aux = unit_aux.replace(/__prefix__/g, count);
        //    unit = $('#' + unit_aux)
        //    $(unit).each(function(i, e){
        //        handleSelect2wParent(e, product);
        //    });
        //});
        //
        $('.add-cfop').click(function(ev){
            ev.preventDefault();
            var count = parseInt($('#id_operationcfop_set-TOTAL_FORMS').attr('value'), 10);
            var tmplMarkup = $('#cfop_template').html()
            var compiledTmpl = tmplMarkup.replace(/__prefix__/g, count);

            $(compiledTmpl).appendTo('#operationcfop_table > tbody');
            $('#id_operationcfop_set-TOTAL_FORMS').attr('value', count + 1)

            var delete_aux = 'id_operationcfop_set-__prefix__-DELETE';
            delete_aux = delete_aux.replace(/__prefix__/g, count);
            d = $('#' + delete_aux)
            $(d).each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });

            var cfop_aux = 'id_operationcfop_set-__prefix__-cfop';
            cfop_aux = cfop_aux.replace(/__prefix__/g, count);
            cfop = $('#'+cfop_aux);
            $(cfop).each(handleSelect2);
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleOperation();
        }
    };

}();

jQuery(document).ready(function() {
   Operation.init();
});

