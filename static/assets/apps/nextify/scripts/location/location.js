var Location = function () {

    var handleLocation = function () {

        $('#id_city').each(function(i, e) {
            var opts = {
                placeholder: "Selecione uma cidade ...",
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: $(e).data('ajax-url'),
                    dataType: 'json',
                    results: function(data, page) {
                        if((page==1) && !($('#id_city').data('required')=='required')) {
                            data.results.unshift({'id': '', 'text': '------------'});
                        }
                        return data;
                    },
                    data: function(term, page) {
                        return {
                            q: term,
                            page: page
                        };
                    }
                },
                initSelection: function(e, callback){
                    $.ajax({
                        type: 'GET',
                        url: $(e).data('ajax-url'),
                        data: [{'name': 'initial', 'value': $(e).val()}],
                        success: function(data){
                            if(data.results) {
                                if($(e).data('multiple')){
                                    callback(data.results);
                                } else {
                                    callback(data.results[0]);
                                }
                            }
                        },
                        'dataType': 'json'
                    });
                }
            }
            $(e).select2(opts);
        });

        $('#id_country').each(function(i, e) {
            var opts = {
                placeholder: "Selecione um País ...",
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: $(e).data('ajax-url'),
                    dataType: 'json',
                    results: function(data, page) {
                        if((page==1) && !($('#id_country').data('required')=='required')) {
                            data.results.unshift({'id': '', 'text': '------------'});
                        }
                        return data;
                    },
                    data: function(term, page) {
                        return {
                            q: term,
                            page: page
                        };
                    }
                },
                initSelection: function(e, callback){
                    $.ajax({
                        type: 'GET',
                        url: $(e).data('ajax-url'),
                        data: [{'name': 'initial', 'value': $(e).val()}],
                        success: function(data){
                            if(data.results) {
                                if($(e).data('multiple')){
                                    callback(data.results);
                                } else {
                                    callback(data.results[0]);
                                }
                            }
                        },
                        'dataType': 'json'
                    });
                }
            }
            $(e).select2(opts);
        });
    }
    //

    //}

    return {
        //main function to initiate the module
        init: function () {
            handleLocation();
        }
    };

}();

jQuery(document).ready(function() {
   Location.init();
});