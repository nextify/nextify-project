var handleSelect2 = function () {
    var placeholder = (typeof $(this).data('placeholder') === 'undefined' ? 'Selecione uma opção ...' : $(this).data('placeholder'));
    var min_length = (typeof $(this).data('min-input-length') === 'undefined' ? parseInt('0') : parseInt($(this).data('min-input-length')));
    var opts = {
        placeholder: placeholder,
        minimumInputLength: min_length,
        allowClear: true,
        width: 'off',
        ajax: {
            url: $(this).data('ajax-url'),
            dataType: 'json',
            results: function (data, page) {
                if ((page == 1) && !($(this).data('required') == 'required')) {
                    data.results.unshift({'id': '', 'text': '------------'});
                }
                return data;
            },
            data: function (term, page) {
                return {
                    q: term,
                    page: page
                };
            }
        },
        initSelection: function (e, callback) {
            $.ajax({
                type: 'GET',
                url: $(e).data('ajax-url'),
                data: [{'name': 'initial', 'value': $(e).val()}],
                success: function (data) {
                    if (data.results) {
                        if ($(e).data('multiple')) {
                            callback(data.results);
                        } else {
                            callback(data.results[0]);
                        }
                    }
                },
                'dataType': 'json'
            });
        }
    }
    $(this).select2(opts);
}

var handleSelect2wParent = function(element, parentElement) {
    var placeholder = (typeof $(this).data('placeholder') === 'undefined' ? 'Selecione uma opção ...' : $(this).data('placeholder'));
    var min_length = (typeof $(this).data('min-input-length') === 'undefined' ? parseInt('0') : parseInt($(this).data('min-input-length')));
    var opts = {
        placeholder: placeholder,
        minimumInputLength: min_length,
        allowClear: true,
        ajax: {
            url: $(element).data('ajax-url'),
            dataType: 'json',
            results: function (data, page) {
                if ((page == 1) && !($(element).data('required') == 'required')) {
                    data.results.unshift({'id': '', 'text': '------------'});
                }
                return data;
            },
            data: function (term, page) {
                return {
                    q: term,
                    p: $(parentElement).val(),
                    page: page
                };
            }
        },
        initSelection: function (e, callback) {
            $.ajax({
                type: 'GET',
                url: $(e).data('ajax-url'),
                data: [{'name': 'initial', 'value': $(e).val()}],
                success: function (data) {
                    if (data.results) {
                        if ($(e).data('multiple')) {
                            callback(data.results);
                        } else {
                            callback(data.results[0]);
                        }
                    }
                },
                'dataType': 'json'
            });
        }
    }
    $(element).select2(opts);
}


var NextifyApp = function () {

    var handleDatePickers = function () {
        if (jQuery().datepicker) {
            $('.dateinput').datepicker({
                rtl: App.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
        /* Workaround to restrict daterange past date select: http://stackoverflow.com/questions/11933173/how-to-restrict-the-selectable-date-ranges-in-bootstrap-datepicker */
    }

    var handleDefaultDatatables = function() {

        var grid = new Datatable();

        grid.init({
            src: $("#default_datatable"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            loadingMessage: 'Carregando...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "lengthMenu": [
                    [10, 20, 50, 100, -1],
                    [10, 20, 50, 100, "Todos"]
                ]
            }
        });
    }



    return {

        //main function to initiate the module
        init: function () {
            try {
                handleDefaultDatatables();
            } catch(e) {}
            handleDatePickers();
        }

    };

}();

jQuery(document).ready(function() {    
   NextifyApp.init();
});