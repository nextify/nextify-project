var Ncm = function () {

    var handleNcm = function () {

        $('#id_ncm_classification').each(function(i, e) {
            var opts = {
                placeholder: "Selecione uma ncm ...",
                allowClear: true,
                minimumInputLength: 1,
                ajax: {
                    url: $(e).data('ajax-url'),
                    dataType: 'json',
                    results: function(data, page) {
                        if((page==1) && !($('#id_ncm_classification').data('required')=='required')) {
                            data.results.unshift({'id': '', 'text': '------------'});
                        }
                        return data;
                    },
                    data: function(term, page) {
                        return {
                            q: term,
                            page: page
                        };
                    }
                },
                initSelection: function(e, callback){
                    $.ajax({
                        type: 'GET',
                        url: $(e).data('ajax-url'),
                        data: [{'name': 'initial', 'value': $(e).val()}],
                        success: function(data){
                            if(data.results) {
                                if($(e).data('multiple')){
                                    callback(data.results);
                                } else {
                                    callback(data.results[0]);
                                }
                            }
                        },
                        'dataType': 'json'
                    });
                }
            }
            $(e).select2(opts);
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleNcm();
        }
    };

}();

jQuery(document).ready(function() {
   Ncm.init();
});