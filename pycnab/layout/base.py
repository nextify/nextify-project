from pycnab.base import CampoTxt, CampoFiller, CampoFixo, CampoData
from decimal import Decimal
from datetime import datetime


class Segmento(object):
    def __init__(self, *args, **kwargs):
        self._txt = None
        self.alertas = []

    def get_txt(self):
        return self._txt

    txt = property(get_txt)


class ArquivoCNAB400(Segmento):
    def __init__(self, *args, **kwargs):
        super(ArquivoCNAB400, self).__init__(*args, **kwargs)
        self.header = SegmentoHeader()
        self.movimento = []
        self.trailler = SegmentoTrailler()

    def get_txt(self):
        txt = ''
        if self.header is not None:
            txt += self.header.txt
            txt += '\n'
        else:
            return txt

        i = 2
        d = Decimal('0.00')
        for m in self.movimento:
            m.numero_sequencial.valor = i
            d += m.vlr_titulo.raw_value
            txt += m.txt
            txt += '\n'
            i += 1

        if self.trailler is not None:
            self.trailler.numero_sequencial.valor = i
            self.trailler.qtd_documentos.valor = i
            self.trailler.vlr_total_titulos.valor = d
            txt += self.trailler.txt
            txt += '\n'
            self._txt = txt
        return txt

    txt = property(get_txt)


class SegmentoHeader(Segmento):
    def __init__(self, *args, **kwargs):
        super(SegmentoHeader, self).__init__(*args, **kwargs)
        self.cod_registro = CampoFixo(nome='', pos_inicial=1, pos_final=1, formato='alfa', default='0')
        self.cod_remessa = CampoFixo(nome='', pos_inicial=2, pos_final=2, formato='alfa', default='1')
        self.literal_transmissao = CampoFixo(nome='', pos_inicial=3, pos_final=9, formato='alfa', default='REMESSA')
        self.cod_servico = CampoFixo(nome='', pos_inicial=10, pos_final=11, formato='alfa', default='01')
        self.literal_servico = CampoFixo(nome='', pos_inicial=12, pos_final=26, formato='alfa', default='COBRANCA')
        self.cod_transmissao = CampoTxt(nome='', pos_inicial=27, pos_final=46, formato='alfa')
        self.nome_beneficiario = CampoTxt(nome='', pos_inicial=47, pos_final=76, formato='alfa')
        self.cod_banco = CampoFixo(nome='', pos_inicial=77, pos_final=79, formato='alfa', default='033')
        self.nome_banco = CampoFixo(nome='', pos_inicial=80, pos_final=94, formato='alfa', default='SANTANDER')
        self.data_gravacao = CampoData(nome='', pos_inicial=94, pos_final=100, formato='ddmmaa')
        self.zeros = CampoFiller(nome='', pos_inicial=101, pos_final=116, formato='filler', default='0')
        self.mensagem1 = CampoFiller(nome='', pos_inicial=117, pos_final=163, formato='filler', default=' ')
        self.mensagem2 = CampoFiller(nome='', pos_inicial=164, pos_final=210, formato='filler', default=' ')
        self.mensagem3 = CampoFiller(nome='', pos_inicial=211, pos_final=257, formato='filler', default=' ')
        self.mensagem4 = CampoFiller(nome='', pos_inicial=258, pos_final=304, formato='filler', default=' ')
        self.mensagem5 = CampoFiller(nome='', pos_inicial=305, pos_final=351, formato='filler', default=' ')
        self.mensagem6 = CampoFiller(nome='', pos_inicial=352, pos_final=391, formato='filler', default=' ')
        self.versao_remessa = CampoFixo(nome='', pos_inicial=392, pos_final=394, formato='alfa', default='000')
        self.numero_sequencial = CampoFixo(nome='', pos_inicial=395, pos_final=400, formato='alfa', default='000001')

    def get_txt(self):
        txt = ''
        txt += self.cod_registro.valor
        txt += self.cod_remessa.valor
        txt += self.literal_transmissao.valor
        txt += self.cod_servico.valor
        txt += self.literal_servico.valor
        txt += self.cod_transmissao.valor
        txt += self.nome_beneficiario.valor
        txt += self.cod_banco.valor
        txt += self.nome_banco.valor
        txt += self.data_gravacao.valor
        txt += self.zeros.valor
        txt += self.mensagem1.valor
        txt += self.mensagem2.valor
        txt += self.mensagem3.valor
        txt += self.mensagem4.valor
        txt += self.mensagem5.valor
        txt += self.mensagem6.valor
        txt += self.versao_remessa.valor
        txt += self.numero_sequencial.valor

        return txt

    txt = property(get_txt)


class SegmentoTrailler(Segmento):
    def __init__(self, *args, **kwargs):
        super(SegmentoTrailler, self).__init__(*args, **kwargs)
        self.cod_registro = CampoFixo(nome='', pos_inicial=1, pos_final=1, formato='alfa', default='9')
        self.qtd_documentos = CampoFixo(nome='', pos_inicial=2, pos_final=7, formato='number', digitos=6)
        self.vlr_total_titulos = CampoTxt(nome='', pos_inicial=8, pos_final=20, formato='decimal', digitos=13, decimais=2)
        self.zeros = CampoFiller(nome='', pos_inicial=21, pos_final=394, formato='filler', default='0')
        self.numero_sequencial = CampoFixo(nome='', pos_inicial=395, pos_final=400, formato='number', digitos=6)

    def get_txt(self):
        txt = ''
        txt += self.cod_registro.valor
        txt += self.qtd_documentos.valor
        txt += self.vlr_total_titulos.valor
        txt += self.zeros.valor
        txt += self.numero_sequencial.valor

        return txt

    txt = property(get_txt)


class SegmentoMovimento(Segmento):
    def __init__(self, *args, **kwargs):
        super(SegmentoMovimento, self).__init__(*args, **kwargs)
        self.cod_registro = CampoFixo(nome='', pos_inicial=1, pos_final=1, formato='alfa', default='1')
        self.tipo_inscricao = CampoTxt(nome='', pos_inicial=2, pos_final=3, formato='alfa', default='02')
        self.cnpj_cpf_beneficiario = CampoTxt(nome='', pos_inicial=4, pos_final=17, formato='alfa')
        self.cod_agencia_beneficiario = CampoTxt(nome='', pos_inicial=18, pos_final=21, formato='number', digitos=4)
        self.conta_movimento_beneficiario = CampoFixo(nome='', pos_inicial=22, pos_final=29, formato='number', digitos=8)
        self.conta_cobranca_beneficiario = CampoTxt(nome='', pos_inicial=30, pos_final=37, formato='number', digitos=8)
        # num_controle_participante
        self.num_controle_participante_p1 = CampoTxt(nome='', pos_inicial=38, pos_final=40, formato='number', digitos=3)
        self.num_controle_participante_p2 = CampoTxt(nome='', pos_inicial=41, pos_final=43, formato='number', digitos=3)
        self.num_controle_participante_p3 = CampoTxt(nome='', pos_inicial=44, pos_final=62, formato='number', digitos=19)
        self.nosso_numero = CampoTxt(nome='', pos_inicial=63, pos_final=69, formato='alfa')
        self.nosso_numero_dv = CampoTxt(nome='', pos_inicial=70, pos_final=70, formato='alfa')
        self.data_segundo_desconto = CampoTxt(nome='', pos_inicial=71, pos_final=76, formato='alfa', default='000000')
        self.brancos1 = CampoFiller(nome='', pos_inicial=77, pos_final=77, formato='filler', default=' ')
        self.info_multa = CampoFixo(nome='', pos_inicial=78, pos_final=78, formato='alfa', default='0')
        self.perc_multa = CampoTxt(nome='', pos_inicial=79, pos_final=82, formato='decimal', digitos=4, decimais=2, default=Decimal('0.00'))
        self.moeda_corrente = CampoFixo(nome='', pos_inicial=83, pos_final=84, formato='alfa', default='00')
        self.vlr_titulo_outra_moeda = CampoTxt(nome='', pos_inicial=85, pos_final=97, formato='decimal', digitos=13, decimais=5, default=Decimal('0.00000'))
        self.brancos2 = CampoFiller(nome='', pos_inicial=98, pos_final=101, formato='filler', default=' ')
        self.data_cobr_multa = CampoTxt(nome='', pos_inicial=102, pos_final=107, formato='alfa', default='000000')
        self.cod_carteira = CampoFixo(nome='', pos_inicial=108, pos_final=108, formato='alfa', default='5')
        self.cod_ocorrencia = CampoFixo(nome='', pos_inicial=109, pos_final=110, formato='alfa', default='01')
        self.seu_numero = CampoTxt(nome='', pos_inicial=111, pos_final=117, formato='number', digitos=7)
        self.seu_numero_bar = CampoFixo(nome='', pos_inicial=118, pos_final=118, formato='alfa', default='/')
        self.seu_numero_seq = CampoTxt(nome='', pos_inicial=119, pos_final=120, formato='number', digitos=2)
        self.data_venc_titulo = CampoData(nome='', pos_inicial=121, pos_final=126, formato='ddmmaa')
        self.vlr_titulo = CampoTxt(nome='', pos_inicial=127, pos_final=139, formato='decimal', digitos=13, decimais=2)
        self.cod_banco_cobrador = CampoFixo(nome='', pos_inicial=140, pos_final=142, formato='alfa', default='033')
        self.cod_agencia_cobradora = CampoTxt(nome='', pos_inicial=143, pos_final=147, formato='number', digitos=5)
        self.especie_documento = CampoTxt(nome='', pos_inicial=148, pos_final=149, formato='alfa', default='01')
        self.tipo_aceite = CampoFixo(nome='', pos_inicial=150, pos_final=150, formato='alfa', default='N')
        self.data_emissao_titulo = CampoData(nome='', pos_inicial=151, pos_final=156, formato='ddmmaa')
        self.instrucao_cobranca_1 = CampoFixo(nome='', pos_inicial=157, pos_final=158, formato='alfa', default='00')
        self.instrucao_cobranca_2 = CampoFixo(nome='', pos_inicial=159, pos_final=160, formato='alfa', default='06')
        self.vlr_mora_dia_atraso = CampoTxt(nome='', pos_inicial=161, pos_final=173, formato='decimal', digitos=13, decimais=2, default=Decimal('0.00'))
        self.data_limite_desconto = CampoFiller(nome='', pos_inicial=174, pos_final=179, formato='filler', default='0')
        self.vlr_desconto_concedido = CampoTxt(nome='', pos_inicial=180, pos_final=192, formato='decimal', digitos=13, decimais=2, default=Decimal('0.00'))
        self.vlr_iof = CampoTxt(nome='', pos_inicial=193, pos_final=205, formato='decimal', digitos=13, decimais=5, default=Decimal('0.00000'))
        self.vlr_segundo_desconto = CampoTxt(nome='', pos_inicial=206, pos_final=218, formato='decimal', digitos=13, decimais=2, default=Decimal('0.00'))
        self.tipo_inscricao_pagador = CampoTxt(nome='', pos_inicial=219, pos_final=220, formato='number', digitos=2)
        self.cnpj_cpf_pagador = CampoTxt(nome='', pos_inicial=221, pos_final=234, formato='number', digitos=14)
        self.nome_pagador = CampoTxt(nome='', pos_inicial=235, pos_final=274, formato='alfa')
        self.endereco_pagador = CampoTxt(nome='', pos_inicial=275, pos_final=314, formato='alfa')
        self.bairro_pagador = CampoTxt(nome='', pos_inicial=315, pos_final=326, formato='alfa')
        self.cep_pagador = CampoTxt(nome='', pos_inicial=327, pos_final=334, formato='alfa')
        self.municipio_pagador = CampoTxt(nome='', pos_inicial=335, pos_final=349, formato='alfa')
        self.estado_pagador = CampoTxt(nome='', pos_inicial=350, pos_final=351, formato='alfa')
        self.nome_sacador_coobrigado = CampoTxt(nome='', pos_inicial=352, pos_final=381, formato='alfa', default='')
        self.brancos3 = CampoFiller(nome='', pos_inicial=382, pos_final=382, formato='filler', default=' ')
        self.identificador_complemento = CampoTxt(nome='', pos_inicial=383, pos_final=383, formato='alfa', default='I')
        self.complemento = CampoTxt(nome='', pos_inicial=384, pos_final=385, formato='alfa', default='12')
        self.brancos4 = CampoFiller(nome='', pos_inicial=386, pos_final=391, formato='filler', default=' ')
        self.num_dias_protesto = CampoFixo(nome='', pos_inicial=392, pos_final=393, formato='number', digitos=2)
        self.brancos5 = CampoFiller(nome='', pos_inicial=394, pos_final=394, formato='filler', default=' ')
        self.numero_sequencial = CampoFixo(nome='', pos_inicial=395, pos_final=400, formato='number', digitos=6)

        #
        self.fator_vencimento = CampoFixo(nome='', pos_inicial=1, pos_final=4, formato='number', digitos=4)

    def get_txt(self):
        txt = ''
        txt += self.cod_registro.valor
        txt += self.tipo_inscricao.valor
        txt += self.cnpj_cpf_beneficiario.valor
        txt += self.cod_agencia_beneficiario.valor
        txt += self.conta_movimento_beneficiario.valor
        txt += self.conta_cobranca_beneficiario.valor
        txt += self.num_controle_participante_p1.valor
        txt += self.num_controle_participante_p2.valor
        txt += self.num_controle_participante_p3.valor
        txt += self.nosso_numero.valor
        txt += self.nosso_numero_dv.valor
        txt += self.data_segundo_desconto.valor
        txt += self.brancos1.valor
        txt += self.info_multa.valor
        txt += self.perc_multa.valor
        txt += self.moeda_corrente.valor
        txt += self.vlr_titulo_outra_moeda.valor
        txt += self.brancos2.valor
        txt += self.data_cobr_multa.valor
        txt += self.cod_carteira.valor
        txt += self.cod_ocorrencia.valor
        txt += self.seu_numero.valor
        txt += self.seu_numero_bar.valor
        txt += self.seu_numero_seq.valor
        txt += self.data_venc_titulo.valor
        txt += self.vlr_titulo.valor
        txt += self.cod_banco_cobrador.valor
        txt += self.cod_agencia_cobradora.valor
        txt += self.especie_documento.valor
        txt += self.tipo_aceite.valor
        txt += self.data_emissao_titulo.valor
        txt += self.instrucao_cobranca_1.valor
        txt += self.instrucao_cobranca_2.valor
        txt += self.vlr_mora_dia_atraso.valor
        txt += self.data_limite_desconto.valor
        txt += self.vlr_desconto_concedido.valor
        txt += self.vlr_iof.valor
        txt += self.vlr_segundo_desconto.valor
        txt += self.tipo_inscricao_pagador.valor
        txt += self.cnpj_cpf_pagador.valor
        txt += self.nome_pagador.valor
        txt += self.endereco_pagador.valor
        txt += self.bairro_pagador.valor
        txt += self.cep_pagador.valor
        txt += self.municipio_pagador.valor
        txt += self.estado_pagador.valor
        txt += self.nome_sacador_coobrigado.valor
        txt += self.brancos3.valor
        txt += self.identificador_complemento.valor
        txt += self.complemento.valor
        txt += self.brancos4.valor
        txt += self.num_dias_protesto.valor
        txt += self.brancos5.valor
        txt += self.numero_sequencial.valor

        return txt

    txt = property(get_txt)
