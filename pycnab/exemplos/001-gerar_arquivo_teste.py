# -*- coding: utf-8 -*-
from pycnab.layout.base import ArquivoCNAB400, SegmentoMovimento
from pycnab.boleto import BOLETO
from pycnab.interface import CNABInterface, CNABLine
from datetime import datetime
from decimal import Decimal

if __name__ == '__main__':
    cnab = CNABInterface()
    line = CNABLine()

    cnab.nome_beneficiario = 'D.D.M. SUPRIMENTOS LTDA ME'
    cnab.cnpj_cpf_beneficiario = '13073206000193'
    cnab.cod_agencia_beneficiario = 1012
    cnab.conta_movimento_beneficiario = 7973527
    cnab.conta_cobranca_beneficiario = 1300126
    line.num_controle_participante = '0000200001000000000201635'
    line.nosso_numero = '2000026'
    line.data_segundo_desconto = '000000'
    line.seu_numero = '0001627/16'
    line.data_venc_titulo = datetime.strptime('010217', '%d%m%y')
    line.vlr_titulo = Decimal('1892.45')
    line.cod_agencia_cobradora = '01012'
    line.data_emissao_titulo = datetime.strptime('041016', '%d%m%y')
    line.tipo_inscricao_pagador = '01'
    line.cnpj_cpf_pagador = '27413829991'
    line.nome_pagador = 'CARLOS MEIRA'
    line.endereco_pagador = 'ALAMEDA CABRAL,747 - AP 404'
    line.bairro_pagador = 'SAO FRANCISCO'
    line.cep_pagador = '80410210'
    line.municipio_pagador = 'CURITIBA'
    line.estado_pagador = 'PR'
    line.num_dias_protesto = 10
    line.numero_sequencial = 1
    line.boleto_aceite = u'NÃO'
    line.boleto_especie = u'REAL'
    line.boleto_carteira = u'RCR'
    line.boleto_especie_doc = u'DM'


    cnab.add_boleto(line)
    cnab.gerar_boletos()

    # Header
    # arq.header.data_gravacao.valor = datetime.now()
    # arq.header.cod_transmissao.valor = '10120797352701300126'
    # arq.header.nome_beneficiario.valor = 'D.D.M. SUPRIMENTOS LTDA ME'

    # dados
    # mov = SegmentoMovimento()
    #
    # line.cnpj_cpf_beneficiario.valor = '13073206000193'
    # mov.cod_agencia_beneficiario.valor = '1012'
    # mov.conta_movimento_beneficiario.valor = 7973527
    # mov.conta_cobranca_beneficiario.valor = '01300126'
    # mov.num_controle_participante.valor = '0000200001000000000201635'
    # mov.nosso_numero.valor = '2000026'
    # mov.data_segundo_desconto.valor = '000000'
    # mov.seu_numero.valor = '0001627/16'
    # mov.data_venc_titulo.valor = datetime.strptime('010217', '%d%m%y')
    # mov.vlr_titulo.valor = Decimal('892.45')
    # mov.cod_agencia_cobradora.valor = '01012'
    # mov.data_emissao_titulo.valor = datetime.strptime('041016', '%d%m%y')
    #
    # mov.tipo_inscricao_pagador.valor = '01'
    # mov.cnpj_cpf_pagador.valor = '27413829991'
    # mov.nome_pagador.valor = 'CARLOS MEIRA'
    # mov.endereco_pagador.valor = 'ALAMEDA CABRAL,747 - AP 404'
    # mov.bairro_pagador.valor = 'SAO FRANCISCO'
    # mov.cep_pagador.valor = '80410210'
    # mov.municipio_pagador.valor = 'CURITIBA'
    # mov.estado_pagador.valor = 'PR'
    # mov.num_dias_protesto.valor = 10
    # mov.numero_sequencial.valor = 1
    #
    #
    # arq.movimento.append(mov)

    # Trailler
    # arq.trailler.qtd_documentos.valor = 132
    # vlr = 1554562.98
    # arq.trailler.vlr_total_titulos.valor = Decimal(str(1554562.98))
    # arq.trailler.numero_sequencial.valor = 138
    #
    # print ('CNAB400')
    # print(arq.txt)
    # arqs.append(arq)



