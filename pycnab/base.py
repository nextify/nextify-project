from decimal import Decimal, InvalidOperation
from datetime import datetime
from . import exceptions


class CampoBase(object):
    def __init__(self, *args, **kwargs):
        self.nome = ''
        self.pos_inicial = None
        self.pos_final = None
        self.formato = None
        self.default = None
        self._valor = None
        self.raw_value = None
        self.decimais = 0
        self.digitos = 0

        for k, v in kwargs.items():
            setattr(self, k, v)

        if self.default is not None:
            # print(unicode(self.default))
            if self.formato == 'decimal':
                self.set_valor(self.default)
            else:
                self.set_valor(unicode(self.default))

    @property
    def tamanho(self):
        return (self.pos_final - self.pos_inicial) + 1

    def set_valor(self, valor):
        self.raw_value = valor
        if self.formato == 'alfa':
            self._valor = ' ' * self.tamanho
            self._valor = valor + self._valor
            self._valor = self._valor[0:self.tamanho]
            return
            # if not isinstance(valor, unicode):
            #     raise exceptions.TipoError(self, valor)
            # if len(valor) > self.digitos:
            #     raise exceptions.NumDigitosExcedidoError(self, valor)
        elif self.formato == 'filler':
            self._valor = str(valor) * self.tamanho
            return

        elif self.formato == 'ddmmaa':
            self._valor = datetime.strftime(valor, '%d%m%y')
            return

        elif self.formato == 'decimal':
            if not isinstance(valor, Decimal):
                raise exceptions.TipoError(self, valor)

            num_decimais = valor.as_tuple().exponent * -1
            if num_decimais != self.decimais:
                print(num_decimais)
                raise exceptions.NumDecimaisError(self, valor)

            if len(str(valor).replace('.', '')) > self.digitos:
                raise exceptions.NumDigitosExcedidoError(self, valor)

            aux = unicode(valor).replace('.', '')
            chars_faltantes = self.digitos - len(aux)
            self._valor = (u'0' * chars_faltantes) + aux
            return

        elif self.formato == 'number':
            if not isinstance(valor, (int, long)):
                raise exceptions.TipoError(self, valor)
            if len(str(valor)) > self.digitos:
                raise exceptions.NumDigitosExcedidoError(self, valor)
            aux = '0' * self.digitos
            aux += str(valor)
            self._valor = self.right(aux, self.digitos)
            return

        self._valor = valor

    def get_valor(self):
        return str(self._valor)

    valor = property(get_valor, set_valor)

    def right(self, s, amount):
        return s[-amount:]

    def left(self, s, amount):
        return s[:amount]

    def __unicode__(self):
        if self.valor is None:
            if self.default is not None:
                if self.decimais:
                    self.valor = Decimal('{0:0.{1}f}'.format(self.default,
                                                             self.decimais))
                else:
                    self.valor = self.default
            else:
                raise exceptions.CampoObrigatorioError(self.nome)

        if self.formato == 'alfa' or self.formato == 'decimal':
            if self.formato == 'decimal':
                valor = unicode(self.valor).replace('.', '')
                chars_faltantes = self.digitos - len(valor)
                return (u'0' * chars_faltantes) + valor
            else:
                chars_faltantes = self.digitos - len(self.valor)
                return self.valor + (u' ' * chars_faltantes)

        return u'{0:0{1}d}'.format(self.valor, self.digitos)

    def __repr__(self):
        return unicode(self)

    def __set__(self, instance, value):
        self.valor = value

    def __get__(self, instance, owner):
        return self.valor


class CampoTxt(CampoBase):
    pass


class CampoNumerico(CampoBase):
    pass


class CampoDecimal(CampoBase):
    pass


class CampoData(CampoBase):
    pass

class CampoFixo(CampoBase):
    pass


class CampoFiller(CampoBase):
    pass