# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals


from reportlab.lib.units import cm
from reportlab.lib.colors import HexColor

from geraldo import Report, SubReport
from geraldo import SystemField, Line, Rect, Image
from geraldo.barcodes import BarCode

from pycnab.formatacao import (BandaDANFE,
                               Campo,
                               TEXTO_NEGRITO_TITULO,
                               TEXTO_TITULO,
                               TEXTO_LINHA_DIG,
                               DADO_CAMPO_NEGRITO,
                                     DADO_CAMPO_NUMERICO_NEGRITO,
                                     DADO_CHAVE,
                                     DADO_COMPLEMENTAR,
                                     DADO_PRODUTO,
                                     DADO_PRODUTO_CENTRALIZADO,
                                     DADO_VARIAVEL_CANCELAMENTO,
                                     DADO_VARIAVEL,
                                     DESCRITIVO_CAMPO,
                                     DESCRITIVO_CAMPO_CANCELAMENTO,
                                     DESCRITIVO_CAMPO_NEGRITO,
                                     DESCRITIVO_DANFE,
                                     DESCRITIVO_DANFE_NFCE,
                                     DESCRITIVO_DANFE_NFCE_CENTRO,
                                     DESCRITIVO_DANFE_NFCE_ESQUERDA,
                                     DESCRITIVO_DANFE_ES,
                                     DESCRITIVO_DANFE_GERAL,
                                     DESCRITIVO_NUMERO,
                                     DESCRITIVO_PRODUTO,
                                     EMIT_DADOS,
                                     EMIT_NOME,
                                     FONTES_ADICIONAIS,
                                     LabelMargemEsquerda,
                                     OBS_CANCELAMENTO,
                                     OBS_DENEGACAO,
                                     OBS_HOMOLOGACAO,
                                     OBS_CONTINGENCIA,
                                     MARGEM_DIREITA,
                                     MARGEM_ESQUERDA,
                                     MARGEM_INFERIOR,
                                     MARGEM_SUPERIOR,
                                     RETRATO,
                                     Texto)
from pysped.nfe.manual_401 import Vol_200, Pag_310


class BoletoRetrato(Report):
    def __init__(self, dict, *args, **kargs):
        super(BoletoRetrato, self).__init__(*args, **kargs)
        self.title = 'Boleto Santander'
        self.print_if_empty = True
        self.additional_fonts = FONTES_ADICIONAIS


        self.page_size = RETRATO
        self.margin_top = MARGEM_SUPERIOR
        self.margin_bottom = MARGEM_INFERIOR
        self.margin_left = MARGEM_ESQUERDA
        self.margin_right = MARGEM_DIREITA

        # Bandas e observações
        self.ficha_caixa = FichaCaixaRetrato(dict=dict)
        self.recibo_sacado = ReciboSacadoRetrato(dict=dict)
        self.ficha_compensacao = FichaCompensacaoRetrato(dict=dict)

        #
        # Guarda a definição do cabeçalho e rodapé da 1ª página
        #
        self.cabecalho_primeira_pagina = None
        self.cabecalho_primeira_pagina_filhos = None
        self.remetente_filhos = None
        self.rodape_primeira_pagina = None

    def on_new_page(self, page, page_number, generator):
        if page_number == 1:
            if self.cabecalho_primeira_pagina is None:
                self.cabecalho_primeira_pagina = self.band_page_header
                self.cabecalho_primeira_pagina_filhos = list(self.band_page_header.child_bands)
                # self.remetente_filhos = list(self.remetente.child_bands)
                self.rodape_primeira_pagina = self.band_page_footer
            #
            # else:
            #     self.band_page_header = self.cabecalho_primeira_pagina
            #     #self.band_page_header.child_bands = []
            #     self.band_page_header.child_bands = self.cabecalho_primeira_pagina_filhos
            #     #self.remetente.child_bands = []
            #     self.remetente.child_bands = self.remetente_filhos
            #     self.band_page_footer = self.rodape_primeira_pagina

        # else:
        #     self.band_page_footer = self.rodape_final
            #
        #     self.band_page_header = self.remetente

    def format_date(self, data, formato):
        return data.strftime(formato.encode('utf-8')).decode('utf-8')


class FichaCaixaRetrato(BandaDANFE):
    def __init__(self, dict):
        super(FichaCaixaRetrato, self).__init__()
        self.elements = []
        self.titulo = 'FICHA DE CAIXA'
        self.via = 0
        self.dict = dict
        top = 0
        # self.apply()

    def logo_banco(self):
        img = Image()
        img.top = 0 * cm
        img.left = 0.5 * cm
        #
        # Tamanhos equilaventes, em centímetros, a 2,5 x 3,8, em 128 dpi
        # estranhamente, colocar os tamanhos em centímetros encolhe a imagem
        #
        img.width = 156
        img.height = 50
        #img.filename = '/home/nextify/santander.png'
        img.filename = '/home/carlos/santander.png'
        self.elements.append(img)

    def apply(self):
        self.logo_banco()

        fld = self.inclui_texto_sem_borda(nome='santander_marca', texto='', top=0*cm, left=0*cm, width=4*cm, height=1*cm)
        fld.style = TEXTO_NEGRITO_TITULO

        fld = self.inclui_texto_sem_borda(nome='santander_banco', texto='033-7', top=0 * cm, left=4 * cm,
                                          width=2 * cm, height=1 * cm)
        fld.style = TEXTO_NEGRITO_TITULO
        self.elements.append(Line(top=0 * cm, bottom=1 * cm, left=4 * cm, right=4 * cm, stroke_width=0.6))
        self.elements.append(Line(top=0 * cm, bottom=1 * cm, left=6 * cm, right=6 * cm, stroke_width=0.6))

        if self.via != 3:
            fld = self.inclui_texto_sem_borda(nome='santander_banco', texto=self.titulo, top=0*cm, left=13.4*cm,
                                              width=6 * cm, height=1 * cm)
            fld.style = TEXTO_TITULO
            self.elements.append(Line(top=0 * cm, bottom=1 * cm, left=13.4 * cm, right=13.4 * cm, stroke_width=0.6))

        self.elements.append(Line(top=1.0*cm, bottom=1*cm, left=0*cm, right=19.4 * cm, stroke_width=0.6))

        # LINHA DIGITAVEL
        if self.via == 3:
            txt = self.inclui_texto_sem_borda(nome='danfe_ext', texto=self.dict['boleto_linha_digitavel'], top=0.3 * cm,
                                          left=5.4 * cm, width=14 * cm, height=0.65 * cm)
            txt.style = TEXTO_LINHA_DIG

        # CAMPOS LADO DIREITO
        tam_campo = 0.65
        linha = 0
        lbl, fld = self.inclui_texto_numerico_label(nome='ficha_vencimento', titulo='Vencimento', texto=self.dict['boleto_data_vencimento'],
                                                    top=(1 + (linha * tam_campo)) * cm, left=13.4*cm,
                                                    width=6*cm, height=tam_campo*cm, margem=False, margem_bottom=0.1, margem_left=0.6)
        linha += 1
        if self.via == 3:
            lbl, fld = self.inclui_texto_numerico_label(nome='cedente_agencia_codigo',
                                                        titulo='Agência/Código do Beneficiário', texto=self.dict['boleto_agencia_cod_cedente'],
                                                        top=(1 + (linha * tam_campo)) * cm, left=13.4 * cm,
                                                        width=6 * cm, height=tam_campo * cm, margem=False,
                                                        margem_bottom=0.1, margem_left=0.6)
            linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='ficha_nossonumero', titulo='Nosso Número', texto=self.dict['boleto_nosso_numero'],
                                                    top=(1+(linha*tam_campo))* cm, left=13.4 * cm,
                                                    width=6 * cm, height=tam_campo*cm, margem=False, margem_bottom=0.1, margem_left=0.6)
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='ficha_vlr_documento', titulo='(=) Valor do Documento', texto=self.dict['boleto_valor_titulo'],
                                                    top=(1+(linha*tam_campo))* cm, left=13.4 * cm,
                                                    width=6 * cm, height=tam_campo*cm, margem=False, margem_bottom=0.1, margem_left=0.6)
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='ficha_desconto', titulo='(-) Desconto', texto='',
                                                    top=(1+(linha*tam_campo))* cm, left=13.4 * cm,
                                                    width=6 * cm, height=tam_campo*cm, margem=False, margem_bottom=0.1, margem_left=0.6)
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='ficha_outras_ded', titulo='(-) Outras Deduçoes/Abatimentos', texto='',
                                                    top=(1+(linha*tam_campo))* cm, left=13.4 * cm,
                                                    width=6 * cm, height=tam_campo*cm, margem=False, margem_bottom=0.1, margem_left=0.6)
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='ficha_mora', titulo='(+) Mora/Multa/Juros', texto='',
                                                    top=(1+(linha*tam_campo))* cm, left=13.4 * cm,
                                                    width=6 * cm, height=tam_campo*cm, margem=False, margem_bottom=0.1, margem_left=0.6)
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='ficha_outros_acrescimos', titulo='(+) Outros Acrescimos', texto='',
                                                    top=(1+(linha*tam_campo))* cm, left=13.4 * cm,
                                                    width=6 * cm, height=tam_campo*cm, margem=False, margem_bottom=0.1, margem_left=0.6)
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='ficha_valor_cobrado', titulo='(=) Valor Cobrado', texto='',
                                                    top=(1+(linha*tam_campo))* cm, left=13.4 * cm,
                                                    width=6 * cm, height=tam_campo*cm, margem=False, margem_bottom=0.6, margem_left=0.6)

        # CAMPOS CEDENTE
        # LINHA 0
        linha = 0
        largura_cedente = 8.4
        if self.via == 3:
            lbl, fld = self.inclui_texto_numerico_label(nome='local_pagto', titulo='Local de Pagamento',
                                                        texto=self.dict['boleto_local_pagamento'],
                                                        top=(1 + (linha * tam_campo)) * cm, left=0 * cm,
                                                        width=13.4 * cm, height=tam_campo * cm, margem=False,
                                                        margem_bottom=0.1, align='center')
            linha += 1
            largura_cedente = 13.4
        # LINHA 1
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente', titulo='Beneficiário', texto=self.dict['boleto_beneficiario'],
                                                    top=(1 + (linha * tam_campo)) * cm, left=0*cm,
                                                    width=largura_cedente * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1, align='left')
        if self.via != 3:
            lbl, fld = self.inclui_texto_numerico_label(nome='cedente_agencia_codigo', titulo='Agência/Código do Beneficiário',
                                                        texto=self.dict['boleto_agencia_cod_cedente'],
                                                        top=(1 + (linha * tam_campo)) * cm, left=8.4 * cm,
                                                        width=5 * cm, height=tam_campo * cm, margem=False,
                                                        margem_bottom=0.1, margem_left=0.1)
        # LINHA 2
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_data_doc', titulo='Data do Documento', texto=self.dict['boleto_data_processamento'],
                                                    top=(1 + (linha * tam_campo)) * cm, left=0* cm,
                                                    width=3.1 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1)
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_num_documento', titulo='Numero do Documento', texto=self.dict['boleto_seu_numero'],
                                                    top=(1 + (linha * tam_campo)) * cm, left=3.1* cm,
                                                    width=3.1 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1, margem_left=0.1)
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_especie', titulo='Espécie Doc', texto=self.dict['boleto_especie_doc'],
                                                    top=(1 + (linha * tam_campo)) * cm, left=6.2 * cm,
                                                    width=2.2 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1, margem_left=0.1, align='center')

        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_aceite', titulo='Aceite', texto=self.dict['boleto_aceite'],
                                                    top=(1 + (linha * tam_campo)) * cm, left=8.4 * cm,
                                                    width=1.5 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1, margem_left=0.1, align='center')
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_data_processamento', titulo='Data do Processamento', texto=self.dict['boleto_data_processamento'],
                                                    top=(1 + (linha * tam_campo)) * cm, left=9.9 * cm,
                                                    width=3.5 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1, margem_left=0.1)
        # LINHA 3
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_uso_banco', titulo='Uso do Banco', texto='',
                                                    top=(1 + (linha * tam_campo)) * cm, left=0 * cm,
                                                    width=3.1 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1)
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_carteira', titulo='Carteira', texto=self.dict['boleto_carteira'],
                                                    top=(1 + (linha * tam_campo)) * cm, left=3.1 * cm,
                                                    width=1.55 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1, margem_left=0.1, align='center')
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_especie', titulo='Espécie', texto=self.dict['boleto_especie'],
                                                    top=(1 + (linha * tam_campo)) * cm, left=4.65 * cm,
                                                    width=1.55 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1, margem_left=0.1, align='center')
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_quantidade', titulo='Quantidade', texto='',
                                                    top=(1 + (linha * tam_campo)) * cm, left=6.2 * cm,
                                                    width=2.2 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1, margem_left=0.1)
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_valor', titulo='Valor',
                                                    texto='',
                                                    top=(1 + (linha * tam_campo)) * cm, left=8.4 * cm,
                                                    width=5 * cm, height=tam_campo * cm, margem=False,
                                                    margem_bottom=0.1, margem_left=0.1)
        # LINHA 4
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_instrucoes', titulo='Instruções (texto de exclusiva responsabilidade do beneficiário)', texto='',
                                                    top=(1 + (linha * tam_campo)) * cm, left=0 * cm,
                                                    width=13.4 * cm, height=tam_campo * 5 * cm, margem=False,
                                                    margem_bottom=0.6)

        # SACADO
        linha += 1
        lbl, fld = self.inclui_texto_numerico_label(nome='cedente_instrucoes',
                                                    titulo='Pagador:',
                                                    texto='',
                                                    top=(1 + ((linha + 4) * tam_campo)) * cm, left=0 * cm,
                                                    width=19.4 * cm, height=tam_campo * 2.5 * cm, margem=False,
                                                    margem_bottom=0.6)

        tam_txt = 0.45
        top_txt = (1.1 + ((linha + 4) * tam_campo))
        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=self.dict['boleto_pagador'], top=top_txt * cm,
                                          left=0.5 * cm, width=12 * cm, height=tam_txt * cm)
        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=self.dict['boleto_endereco_pagador'], top=(top_txt + (1 * tam_txt)) * cm,
                                          left=0.5 * cm, width=12 * cm, height=tam_txt * cm)
        txt = self.inclui_texto_sem_borda(nome='danfe_ext',
                                          texto=self.dict['boleto_cep_municipio_pagador'], top=(top_txt + (2 * tam_txt)) * cm,
                                          left=0.5 * cm, width=12 * cm, height=tam_txt * cm)

        if self.via == 3:
            self.cod_barras(top=(top_txt + (2 * tam_txt)) * cm)
            self.height = 9.3 * cm
        else:
            self.height = 8.3 * cm

    def cod_barras(self, top):
        self.elements.append(
            BarCode(type=u'I2of5', attribute_name=u'boleto_codigo_barras', top=8.5 * cm, left=0 * cm, height=2 * cm, width=0.038 * cm))
        self.elements.append(Line(top=8.65 * cm, bottom=8.65 * cm, left=0 * cm, right=15.4 * cm, stroke_width=10, stroke_color='red'))


class ReciboSacadoRetrato(FichaCaixaRetrato):
    def __init__(self, dict):
        super(ReciboSacadoRetrato, self).__init__(dict)
        self.titulo = 'RECIBO DO PAGADOR'


class FichaCompensacaoRetrato(FichaCaixaRetrato):
    def __init__(self, dict):
        super(FichaCompensacaoRetrato, self).__init__(dict)
        self.titulo = 'FICHA DE COMPENSAÇAO'
        self.via = 3
