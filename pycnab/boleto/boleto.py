# -*- coding: utf-8 -*-
from StringIO import StringIO
from geraldo.generators import PDFGenerator
from boleto_retrato import BoletoRetrato


class BOLETO(object):
    def __init__(self):
        self.caminho           = ''
        self.salvar_arquivo    = True

        self.boleto       = None
        self.conteudo_pdf = None
        self.queryset = None
        self.dict = None

    def gerar_boleto(self, canvas=None, filename=None):
        #
        # Prepara as bandas de impressão para cada formato
        #
        self.boleto = BoletoRetrato(dict=self.dict)
        self.boleto.queryset = self.queryset
        self.boleto.band_page_header = self.boleto.ficha_caixa
        self.boleto.band_page_header.child_bands = []
        self.boleto.band_page_header.child_bands.append(self.boleto.recibo_sacado)
        self.boleto.band_page_footer = self.boleto.ficha_compensacao
        self.boleto.ficha_caixa.apply()
        self.boleto.recibo_sacado.apply()
        self.boleto.ficha_compensacao.apply()


        if canvas is None:
            c = self.boleto.generate_by(PDFGenerator, filename=filename, return_canvas=True)
        else:
            c = self.boleto.generate_by(PDFGenerator, canvas=canvas, return_canvas=True)
        return c

        # if self.salvar_arquivo:
        #     nome_arq = '/home/carlos/boleto_santander.pdf'
        #     self.boleto.generate_by(PDFGenerator, filename=nome_arq)

    def pdf_generate(self):
        #
        # Prepara as bandas de impressão para cada formato
        #
        self.boleto = BoletoRetrato(dict=self.dict)
        self.boleto.queryset = self.queryset
        self.boleto.band_page_header = self.boleto.ficha_caixa
        self.boleto.band_page_header.child_bands = []
        self.boleto.band_page_header.child_bands.append(self.boleto.recibo_sacado)
        self.boleto.band_page_footer = self.boleto.ficha_compensacao
        self.boleto.ficha_caixa.apply()
        self.boleto.recibo_sacado.apply()
        self.boleto.ficha_compensacao.apply()

        return self.boleto

