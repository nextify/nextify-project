# -*- coding: utf-8 -*-
from StringIO import StringIO
from datetime import datetime
from pycnab.layout.base import ArquivoCNAB400, SegmentoMovimento
from pycnab.boleto import BOLETO
from decimal import Decimal
from nextify.utils.masks import mask_cnpj, mask_cpf


class CNABInterface(object):
    def __init__(self):
        self.one_file_print = False
        self.boleto = []

        # CNAB
        self.cnab_400 = ArquivoCNAB400()

        self.nome_beneficiario = ''
        self.cnpj_cpf_beneficiario = ''
        self.cod_agencia_beneficiario = 0
        self.conta_movimento_beneficiario = 0
        self.conta_cobranca_beneficiario = 0
        self.cod_transmissao = ''
        
    def add_boleto(self, line):
        line.nome_beneficiario = self.nome_beneficiario
        line.cnpj_cpf_beneficiario = self.cnpj_cpf_beneficiario
        line.cod_agencia_beneficiario = self.cod_agencia_beneficiario
        line.conta_movimento_beneficiario = self.conta_movimento_beneficiario
        line.conta_cobranca_beneficiario = self.conta_cobranca_beneficiario
        line.update_values()
        self.boleto.append(line)

    def process_file(self):
        self.cnab_400.header.nome_beneficiario.valor = self.nome_beneficiario
        self.cnab_400.header.data_gravacao.valor = datetime.now()
        self.cnab_400.header.cod_transmissao.valor = self.cod_transmissao

        for b in self.boleto:
            b.process_cnab()
            self.cnab_400.movimento.append(b.cnab_400_line)

    def get_txt(self):
        return self.cnab_400.txt

    txt = property(get_txt)

    # def gerar_boletos(self):
    #     for b in self.boleto:
    #         bol = BOLETO()
    #         bol.queryset = [b.dict, b.dict]
    #         bol.dict = b.dict
    #         bol.salvar_arquivo = True
    #         bol.gerar_boleto()

    def return_boletos(self):
        buff = StringIO()
        canvas = None
        for b in self.boleto:
            bol = BOLETO()
            bol.queryset = [b.dict]
            bol.dict = b.dict
            bol.salvar_arquivo = False
            canvas = bol.gerar_boleto(canvas=canvas, filename=buff)
        canvas.save()

        return buff.getvalue()

    def salvar_boletos(self, filename):
        canvas = None
        for b in self.boleto:
            bol = BOLETO()
            bol.queryset = [b.dict]
            bol.dict = b.dict
            canvas = bol.gerar_boleto(canvas=canvas, filename=filename)
        canvas.save()


    # def boleto(self):
    #     for b in self.boleto:
    #         bol = BOLETO()
    #         bol.queryset = [b.dict, b.dict]
    #         bol.dict = b.dict
    #         bol.salvar_arquivo = False
    #         bol.gerar_boleto()
    #         return bol.conteudo_pdf


class CNABLine(object):
    def __init__(self):
        self.nome_beneficiario = ''
        self.cnpj_cpf_beneficiario = ''
        self.cod_agencia_beneficiario = 0
        self.conta_movimento_beneficiario = 0
        self.conta_cobranca_beneficiario = 0
        self.num_controle_participante_p1 = 0
        self.num_controle_participante_p2 = 0
        self.num_controle_participante_p3 = 0
        self.nosso_numero = '0000000'
        self.nosso_numero_dv = '0'
        self.data_segundo_desconto = '000000'
        self.seu_numero = 0
        self.seu_numero_seq = 0
        self.data_venc_titulo = None
        self.vlr_titulo = Decimal('0.00')
        self.cod_agencia_cobradora = ''
        self.data_emissao_titulo = None
        self.tipo_inscricao_pagador = '01' # 01 - CPF / 02 - CNPJ
        self.cnpj_cpf_pagador = ''
        self.nome_pagador = ''
        self.endereco_pagador = ''
        self.bairro_pagador = ''
        self.cep_pagador = ''
        self.municipio_pagador = ''
        self.estado_pagador = ''
        self.num_dias_protesto = 0

        # Campos formatados para boleto
        self.boleto_beneficiario = ''
        self.boleto_aceite = ''
        self.boleto_carteira = ''
        self.boleto_especie = ''
        self.boleto_especie_doc = ''
        self.boleto_agencia_cod_cedente = ''
        self.boleto_data_processamento = ''
        self.boleto_data_vencimento = ''
        self.boleto_pagador = ''
        self.boleto_endereco_pagador = ''
        self.boleto_cep_municipio_pagador = ''
        self.boleto_local_pagamento = ''
        self.boleto_codigo_barras = ''
        self.boleto_linha_digitavel = ''
        self.boleto_valor_titulo = '0,00'
        self.boleto_nosso_numero = '%s%s' % (self.nosso_numero, self.nosso_numero_dv)
        self.boleto_seu_numero = '%s/%s' % (self.seu_numero, self.seu_numero_seq)

        # CNAB
        self.cnab_400_line = None

    def get_dict(self):
        return dict(
            boleto_beneficiario=self.boleto_beneficiario,
            boleto_aceite=self.boleto_aceite,
            boleto_carteira=self.boleto_carteira,
            boleto_especie=self.boleto_especie,
            boleto_agencia_cod_cedente=self.boleto_agencia_cod_cedente,
            boleto_data_processamento=self.boleto_data_processamento,
            boleto_data_vencimento=self.boleto_data_vencimento,
            boleto_especie_doc=self.boleto_especie_doc,
            boleto_pagador=self.boleto_pagador,
            boleto_endereco_pagador=self.boleto_endereco_pagador,
            boleto_cep_municipio_pagador=self.boleto_cep_municipio_pagador,
            boleto_local_pagamento=self.boleto_local_pagamento,
            boleto_codigo_barras=self.boleto_codigo_barras,
            boleto_linha_digitavel=self.boleto_linha_digitavel,
            boleto_valor_titulo=self.boleto_valor_titulo,
            boleto_nosso_numero=self.boleto_nosso_numero,
            boleto_seu_numero=self.boleto_seu_numero,
            nome_beneficiario=self.nome_beneficiario,
            cnpj_cpf_beneficiario=self.cnpj_cpf_beneficiario,
            cod_agencia_beneficiario=self.cod_agencia_beneficiario,
            conta_movimento_beneficiario=self.conta_movimento_beneficiario,
            conta_cobranca_beneficiario=self.conta_cobranca_beneficiario,
            num_controle_participante=self.num_controle_participante_p1,
            nosso_numero=self.nosso_numero,
            data_segundo_desconto=self.data_segundo_desconto,
            seu_numero=self.seu_numero,
            data_venc_titulo=self.data_venc_titulo,
            vlr_titulo=self.vlr_titulo,
            cod_agencia_cobradora=self.cod_agencia_cobradora,
            data_emissao_titulo=self.data_emissao_titulo,
            tipo_inscricao_pagador=self.tipo_inscricao_pagador,
            cnpj_cpf_pagador=self.cnpj_cpf_pagador,
            nome_pagador=self.nome_pagador,
            endereco_pagador=self.endereco_pagador,
            bairro_pagador=self.bairro_pagador,
            cep_pagador=self.cep_pagador,
            municipio_pagador=self.municipio_pagador,
            estado_pagador=self.estado_pagador,
            num_dias_protesto=self.num_dias_protesto
        )

    dict = property(get_dict)

    def update_values(self):
        self.boleto_beneficiario = '%s - %s' % (mask_cnpj(self.cnpj_cpf_beneficiario), self.nome_beneficiario)
        self.boleto_agencia_cod_cedente = '%s/%s' % (str(self.cod_agencia_beneficiario), str(self.conta_movimento_beneficiario))
        self.boleto_data_processamento = datetime.strftime(self.data_emissao_titulo, '%d/%m/%Y')
        self.boleto_data_vencimento = datetime.strftime(self.data_venc_titulo, '%d/%m/%Y')
        self.boleto_local_pagamento = u'Pagar preferencialmente nas agências do SANTANDER'
        self.boleto_valor_titulo = '{:20,.2f}'.format(self.vlr_titulo).replace('.', '#').replace(',', '.').replace('#', ',')
        self.boleto_nosso_numero = '%s-%s' % (self.nosso_numero, self.nosso_numero_dv)
        self.boleto_seu_numero = '%s/%s' % (self.seu_numero, self.seu_numero_seq)

        # Nome Pagador
        if self.tipo_inscricao_pagador == 1:
            self.boleto_pagador = u'Nome: %s CPF: %s' % (self.nome_pagador, mask_cpf(str(self.cnpj_cpf_pagador)))
        else:
            self.boleto_pagador = u'Nome: %s CNPJ: %s' % (self.nome_pagador, mask_cnpj(str(self.cnpj_cpf_pagador)))

        # Endereço Pagador
        self.boleto_endereco_pagador = u'Endereço: %s Bairro: %s' % (self.endereco_pagador, self.bairro_pagador)
        self.boleto_cep_municipio_pagador = u'CEP: %s Cidade/UF: %s/%s' % (self.cep_pagador, self.municipio_pagador, self.estado_pagador)

    def process_cnab(self):
        self.cnab_400_line = SegmentoMovimento()
        self.cnab_400_line.cnpj_cpf_beneficiario.valor = self.cnpj_cpf_beneficiario
        self.cnab_400_line.cod_agencia_beneficiario.valor = self.cod_agencia_beneficiario
        self.cnab_400_line.conta_movimento_beneficiario.valor = self.conta_movimento_beneficiario
        self.cnab_400_line.conta_cobranca_beneficiario.valor = self.conta_cobranca_beneficiario
        self.cnab_400_line.num_controle_participante_p1.valor = self.num_controle_participante_p1
        self.cnab_400_line.num_controle_participante_p2.valor = self.num_controle_participante_p2
        self.cnab_400_line.num_controle_participante_p3.valor = self.num_controle_participante_p3
        self.cnab_400_line.nosso_numero.valor = self.nosso_numero
        self.cnab_400_line.nosso_numero_dv.valor = self.nosso_numero_dv
        self.cnab_400_line.data_segundo_desconto.valor = self.data_segundo_desconto
        self.cnab_400_line.seu_numero.valor = self.seu_numero
        self.cnab_400_line.seu_numero_seq.valor = self.seu_numero_seq
        self.cnab_400_line.data_venc_titulo.valor = self.data_venc_titulo
        self.cnab_400_line.vlr_titulo.valor = self.vlr_titulo
        self.cnab_400_line.cod_agencia_cobradora.valor = self.cod_agencia_cobradora
        self.cnab_400_line.data_emissao_titulo.valor = self.data_emissao_titulo
        self.cnab_400_line.tipo_inscricao_pagador.valor = self.tipo_inscricao_pagador
        self.cnab_400_line.cnpj_cpf_pagador.valor = self.cnpj_cpf_pagador
        self.cnab_400_line.nome_pagador.valor = self.nome_pagador
        self.cnab_400_line.endereco_pagador.valor = self.endereco_pagador
        self.cnab_400_line.bairro_pagador.valor = self.bairro_pagador
        self.cnab_400_line.cep_pagador.valor = self.cep_pagador
        self.cnab_400_line.municipio_pagador.valor = self.municipio_pagador
        self.cnab_400_line.estado_pagador.valor = self.estado_pagador
        self.cnab_400_line.num_dias_protesto.valor = self.num_dias_protesto
